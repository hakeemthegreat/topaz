package org.coco.topaz.web.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class UnknownMimeTypeExceptionTest extends ExceptionTest<UnknownMimeTypeException> {

	public UnknownMimeTypeExceptionTest() {
		super(UnknownMimeTypeException.class);
	}

}
