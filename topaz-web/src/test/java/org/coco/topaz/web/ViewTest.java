package org.coco.topaz.web;

import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ViewTest extends GetterSetterTest<View> {

	public ViewTest() {
		super(new View());
	}

	@Test
	public void toString_Test() {
		instance.setName(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.toString());
	}

}
