package org.coco.topaz.web.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class UnresolvedArgumentExceptionTest extends ExceptionTest<UnresolvedArgumentException> {

	public UnresolvedArgumentExceptionTest() {
		super(UnresolvedArgumentException.class);
	}

}
