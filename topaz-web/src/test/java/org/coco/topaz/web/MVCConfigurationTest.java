package org.coco.topaz.web;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MVCConfigurationTest {

	private static final String KEY = Dummy.STRING;
	private static final String KEY2 = Dummy.STRING2;
	private static final String KEY3 = Dummy.STRING3;

	private static final String VALUE = Dummy.LONG.toString();
	private static final String VALUE2 = Dummy.LONG2.toString();
	private static final String VALUE3 = Dummy.LONG3.toString();

	@Test
	public void load_Test() {
		Map<String, String> initParameters = new HashMap<>();

		initParameters.put(KEY, VALUE);
		initParameters.put(KEY2, VALUE2);
		initParameters.put(KEY3, VALUE3);

		MVCConfiguration.load(initParameters);
		assertEquals(VALUE, MVCConfiguration.get(KEY));
		assertEquals(VALUE2, MVCConfiguration.get(KEY2));
		assertEquals(VALUE3, MVCConfiguration.get(KEY3));
	}

	@Test
	public void put_get_Test() {
		MVCConfiguration.put(KEY, VALUE);
		assertEquals(VALUE, MVCConfiguration.get(KEY));
	}

	@Test(expected = IllegalArgumentException.class)
	public void put_Empty_Key_Test() {
		MVCConfiguration.put(null, VALUE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void put_Empty_Value_Test() {
		MVCConfiguration.put(KEY, null);
	}

	@Test
	public void basePackage_Getter_Setter_Test() {
		MVCConfiguration.setBasePackage(Dummy.STRING);
		assertEquals(Dummy.STRING, MVCConfiguration.getBasePackage());
	}

	@Test
	public void controllerPackage_Getter_Setter_Test() {
		MVCConfiguration.setControllerPackage(Dummy.STRING);
		assertEquals(Dummy.STRING, MVCConfiguration.getControllerPackage());
	}

	@Test
	public void webContent_Getter_Setter_Test() {
		MVCConfiguration.setWebContent(Dummy.STRING);
		assertEquals(Dummy.STRING, MVCConfiguration.getWebContent());
	}

	@Test
	public void viewContent_Getter_Setter_Test() {
		MVCConfiguration.setViewContent(Dummy.STRING);
		assertEquals(Dummy.STRING, MVCConfiguration.getViewContent());
	}

	@Test
	public void viewExtension_Getter_Setter_Test() {
		MVCConfiguration.setViewExtension(Dummy.STRING);
		assertEquals(Dummy.STRING, MVCConfiguration.getViewExtension());
	}

}
