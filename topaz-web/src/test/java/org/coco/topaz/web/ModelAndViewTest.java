package org.coco.topaz.web;

import org.coco.topaz.util.test.Dummy;
import mockit.Tested;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ModelAndViewTest {

	@Tested
	private ModelAndView instance;

	@Test
	public void view_Getter_Setter_Test() {
		View view = new View();

		instance.setView(view);
		assertEquals(view, instance.getView());
	}

	@Test
	public void viewName_Getter_Setter_Test() {
		instance.setViewName(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getViewName());
	}

	@Test
	public void objects_Getter_Setter_Test() {
		Map<String, Object> objects = new HashMap<>();

		instance.setObjects(objects);
		assertEquals(objects, instance.getObjects());
	}

	@Test
	public void addObject_Test() {
		instance.addObject(Dummy.STRING, Dummy.OBJECT);

		Map<String, Object> results = instance.getObjects();
		assertEquals(Dummy.OBJECT, results.get(Dummy.STRING));
	}

}
