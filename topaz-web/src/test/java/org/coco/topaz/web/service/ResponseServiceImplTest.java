package org.coco.topaz.web.service;

import mockit.Injectable;
import org.coco.topaz.util.service.JSONService;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.Streams;
import org.coco.topaz.util.Strings;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.coco.topaz.web.MVCConfiguration;
import org.coco.topaz.web.ModelAndView;
import org.coco.topaz.web.View;
import org.coco.topaz.web.util.WebContentUtils;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import java.io.*;

import static org.junit.Assert.assertEquals;

public class ResponseServiceImplTest {

	@Injectable
	private JSONService jsonService;

	@Tested
	private ResponseServiceImpl instance;

	private static final ModelAndView MODEL_AND_VIEW = new ModelAndView();
	private static final String HEADER = "header.jsp";
	private static final String FOOTER = "footer.jsp";

	@Test
	public void transform_ModelAndView_Test(@Mocked Response response) {
		new Expectations(instance, Strings.class) {{
			instance.toResponse(response); result = response;
			response.getEntity(); result = MODEL_AND_VIEW;
			instance.processModelAndView(MODEL_AND_VIEW); result = "dummy";
			response.getMediaType(); result = MediaType.TEXT_HTML_TYPE;
			response.getStatus(); result = HttpServletResponse.SC_OK;
			Strings.toString(MediaType.TEXT_HTML_TYPE); result = MediaType.TEXT_HTML;
			instance.toResponse("dummy", MediaType.TEXT_HTML, HttpServletResponse.SC_OK); result = response;
		}};

		Response result = instance.transform(response);
		assertEquals(response, result);
	}

	@Test
	public void transform_ModelAndView_String_Test(@Mocked Response response) {
		new Expectations(instance, Strings.class) {{
			instance.toResponse(response); result = response;
			response.getEntity(); result = "dummy";
			instance.processString("dummy"); result = "dummy2";
			response.getMediaType(); result = MediaType.TEXT_HTML_TYPE;
			response.getStatus(); result = HttpServletResponse.SC_OK;
			Strings.toString(MediaType.TEXT_HTML_TYPE); result = "text/html";
			instance.toResponse("dummy2", "text/html", HttpServletResponse.SC_OK); result = response;
		}};

		Response result = instance.transform(response);
		assertEquals(response, result);
	}

	@Test
	public void transform_ModelAndView_Default_Test(@Mocked Response response) {
		new Expectations(instance, Strings.class) {{
			instance.toResponse(response); result = response;
			response.getEntity(); result = Dummy.OBJECT;
			instance.processDefault(Dummy.OBJECT); result = "dummy";
			response.getMediaType(); result = MediaType.TEXT_HTML_TYPE;
			response.getStatus(); result = HttpServletResponse.SC_OK;
			Strings.toString(MediaType.TEXT_HTML_TYPE); result = "text/html";
			instance.toResponse("dummy", "text/html", HttpServletResponse.SC_OK); result = response;
		}};

		Response result = instance.transform(response);
		assertEquals(response, result);
	}

	@Test
	public void processModelAndView_Test(@Mocked ByteArrayOutputStream byteArrayOutputStream) {
		ModelAndView mav = new ModelAndView();
		View view = new View();

		view.setName("dummy");
		mav.setView(view);

		new Expectations(instance, MVCConfiguration.class, Streams.class) {{
			Streams.newByteArrayOutputStream(); result = byteArrayOutputStream;
			instance.load(HEADER, byteArrayOutputStream); //do nothing
			MVCConfiguration.getViewExtension(); result = ".jsp";
			instance.load("dummy.jsp", byteArrayOutputStream); //do nothing
			instance.load(FOOTER, (ByteArrayOutputStream) any); //do nothing
			byteArrayOutputStream.toString(); result = "dummy2";
		}};

		String result = instance.processModelAndView(mav);
		assertEquals("dummy2", result);
	}

	@Test
	public void processString_Test() {
		String result = instance.processString("dummy");
		assertEquals("dummy", result);
	}

	@Test
	public void process_Default_Test() {
		new Expectations() {{
			jsonService.toJSON(Dummy.OBJECT); result = "dummy";
		}};

		String result = instance.processDefault(Dummy.OBJECT);
		assertEquals("dummy", result);
	}

	@Test
	public void load_Test(@Mocked OutputStream outputStream) throws Exception {
		FileInputStream fileInputStream = null;
		String viewContent = "viewContent/";
		String fileLocation = "fileLocation";
		String fileName = "file.txt";

		new Expectations(MVCConfiguration.class, WebContentUtils.class, instance, Streams.class) {{
			MVCConfiguration.getViewContent(); result = viewContent;
			WebContentUtils.getLocation("viewContent/file.txt"); result = fileLocation;
			Streams.newFileInputStream(fileLocation); result = fileInputStream;
			Streams.copy(fileInputStream, outputStream); //do nothing
		}};

		instance.load(fileName, outputStream);
	}

	@Test(expected = RuntimeException.class)
	public void load_IOException_Test(@Mocked OutputStream outputStream) throws Exception {
		String viewContent = "viewContent/";
		String fileLocation = "fileLocation";
		String fileName = "file.txt";

		new Expectations(MVCConfiguration.class, WebContentUtils.class, instance, Streams.class) {{
			MVCConfiguration.getViewContent(); result = viewContent;
			WebContentUtils.getLocation("viewContent/file.txt"); result = fileLocation;
			Streams.newFileInputStream(fileLocation); result = new IOException();
			Streams.copy((InputStream) any, (OutputStream) any); times = 0;
		}};

		instance.load(fileName, outputStream);
	}

	@Test
	public void toResponse_Response_Object_Test(@Mocked Response response) {
		new Expectations(Strings.class, instance) {{
			response.getEntity(); result = Dummy.OBJECT;
			response.getMediaType(); result = MediaType.APPLICATION_JSON_TYPE;
			Strings.toString(MediaType.APPLICATION_JSON_TYPE); result = MediaType.APPLICATION_JSON;
			response.getStatus(); result = HttpServletResponse.SC_OK;
			instance.toResponse(Dummy.OBJECT, MediaType.APPLICATION_JSON, HttpServletResponse.SC_OK); result = response;
		}};

		Response result = instance.toResponse(response);
		assertEquals(response, result);
	}

	@Test
	public void toResponse_Response_Test(@Mocked Response response) {
		Response result = instance.toResponse(response, "dummy", Dummy.INT);
		assertEquals(response, result);
	}

	@Test
	public void toResponse_Default_Test(@Mocked Response response, @Mocked ResponseBuilder responseBuilder) {
		new Expectations() {{
			Response.status(Dummy.INT); result = responseBuilder;
			responseBuilder.type("dummy"); result = responseBuilder;
			responseBuilder.entity(Dummy.OBJECT); result = responseBuilder;
			responseBuilder.build(); result = response;
		}};

		Response result = instance.toResponse(Dummy.OBJECT, "dummy", Dummy.INT);
		assertEquals(response, result);
	}

}
