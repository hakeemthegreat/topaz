package org.coco.topaz.web.util;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.net.URL;
import java.util.Enumeration;

import static org.junit.Assert.*;

public class WebContentUtilsTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void getLocation_Relative_Path_Test(@Mocked Enumeration<URL> resources, @Mocked URL resource) throws Exception {
		String path = "topaz/src";
		String folder = "folder";

		new Expectations(WebContentUtils.class) {{
			WebContentUtils.getResources(); result = resources;
			WebContentUtils.isAbsolutePath(anyString); result = false;
			resources.hasMoreElements(); result = true;
			resources.nextElement(); result = resource;
			resource.getFile(); result = folder;
		}};

		String result = WebContentUtils.getLocation(path);
		assertEquals("folder../..topaz/src", result);

		new Verifications() {{
			WebContentUtils.isAbsolutePath(path); times = 1;
		}};
	}

	@Test
	public void getLocation_Absolute_Path_Test(@Mocked Enumeration<URL> resources, @Mocked URL resource) throws Exception {
		String path = "C:/docs/file.txt";

		new Expectations(WebContentUtils.class) {{
			WebContentUtils.getResources(); result = resources;
			WebContentUtils.isAbsolutePath(anyString); result = true;
		}};

		String result = WebContentUtils.getLocation(path);
		assertEquals(path, result);

		new Verifications() {{
			WebContentUtils.isAbsolutePath(path); times = 1;
			resources.hasMoreElements(); times = 0;
			resources.nextElement(); times = 0;
			resource.getFile(); times = 0;
		}};
	}

	@Test
	public void getLocation_Empty_Enumeration_Test(@Mocked Enumeration<URL> resources) throws Exception {
		String path = "path";

		new Expectations(WebContentUtils.class) {{
			WebContentUtils.getResources(); result = resources;
			WebContentUtils.isAbsolutePath(anyString); result = false;
			resources.hasMoreElements(); result = false;
		}};

		String result = WebContentUtils.getLocation(path);
		assertNull(result);

		new Verifications() {{
			WebContentUtils.isAbsolutePath(path); times = 1;
		}};
	}

	@Test
	public void getResources_Test() throws Exception {
		Enumeration<URL> result = WebContentUtils.getResources();
		assertNotNull(result);
	}

	@Test
	public void isAbsolutePath_Test() throws Exception {
		File file = temporaryFolder.newFile();
		String absolutePath = file.getAbsolutePath();

		boolean result = WebContentUtils.isAbsolutePath(absolutePath);
		assertTrue(result);
	}

}
