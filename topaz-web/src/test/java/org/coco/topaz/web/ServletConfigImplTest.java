package org.coco.topaz.web;

import mockit.Mocked;
import mockit.Tested;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import javax.servlet.ServletContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import static org.junit.Assert.*;

public class ServletConfigImplTest {

	@Tested
	private ServletConfigImpl instance;

	@Test
	public void servletName_Getter_Setter_Test() {
		instance.setServletName("dummy");
		assertEquals("dummy", instance.getServletName());
	}

	@Test
	public void servletContext_Getter_Setter_Test(@Mocked ServletContext servletContext) {
		instance.setServletContext(servletContext);
		assertEquals(servletContext, instance.getServletContext());
	}

	@Test
	public void initParameters_Adder_Getter_Test() {
		instance.addInitParameter("dummy", Dummy.LONG.toString());
		instance.addInitParameter("dummy2", Dummy.LONG2.toString());
		instance.addInitParameter("dummy3", Dummy.LONG3.toString());

		assertEquals(Dummy.LONG.toString(), instance.getInitParameter("dummy"));
		assertEquals(Dummy.LONG2.toString(), instance.getInitParameter("dummy2"));
		assertEquals(Dummy.LONG3.toString(), instance.getInitParameter("dummy3"));
	}

	@Test
	public void getInitParameterNames_Test() {
		instance.addInitParameter("dummy", Dummy.LONG.toString());
		instance.addInitParameter("dummy2", Dummy.LONG2.toString());
		instance.addInitParameter("dummy3", Dummy.LONG3.toString());

		Enumeration<String> results = instance.getInitParameterNames();
		Collection<String> parameterNames = new ArrayList<>();

		while (results.hasMoreElements()) {
			parameterNames.add(results.nextElement());
		}

		assertEquals(3, parameterNames.size());
		assertTrue(parameterNames.contains("dummy"));
		assertTrue(parameterNames.contains("dummy2"));
		assertTrue(parameterNames.contains("dummy3"));
	}

}
