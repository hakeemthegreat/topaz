package org.coco.topaz.web.resolver;

import org.coco.topaz.util.test.Dummy;
import mockit.*;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.*;

@SuppressWarnings("deprecation")
public class HttpServletBeanTest {

	@Injectable
	private HttpServletRequest request;

	@Injectable
	private HttpServletResponse response;

	@Tested
	private HttpServletBean instance;

	@Test
	public void request_Getter_Setter_Test() {
		instance.setRequest(request);
		assertEquals(request, instance.getRequest());
	}

	@Test
	public void response_Getter_Setter_Test() {
		instance.setResponse(response);
		assertEquals(response, instance.getResponse());
	}
	
	@Test
	public void getAttribute_Test() {
		new Expectations() {{
			request.getAttribute(Dummy.STRING); result = Dummy.OBJECT;
		}};

		Object result = instance.getAttribute(Dummy.STRING);
		assertEquals(Dummy.OBJECT, result);
	}

	@Test
	public void getAttributeNames(@Mocked Enumeration<String> attributeNames) {
		new Expectations() {{
			request.getAttributeNames(); result = attributeNames;
		}};

		Enumeration<String> results = instance.getAttributeNames();
		assertEquals(attributeNames, results);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getCharacterEncoding_Test() {
		instance.getCharacterEncoding();
	}

	@Test
	public void getRequestCharacterEncoding_Test() {
		new Expectations() {{
			request.getCharacterEncoding(); result = Dummy.STRING;
		}};

		String result = instance.getRequestCharacterEncoding();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getResponseCharacterEncoding_Test() {
		new Expectations() {{
			response.getCharacterEncoding(); result = Dummy.STRING;
		}};

		String result = instance.getResponseCharacterEncoding();
		assertEquals(Dummy.STRING, result);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setCharacterEncoding_Test() {
		instance.setCharacterEncoding(Dummy.STRING);
	}

	@Test
	public void setRequestCharacterEncoding_Test() throws Exception {
		instance.setRequestCharacterEncoding(Dummy.STRING);

		new Verifications() {{
			request.setCharacterEncoding(Dummy.STRING); times = 1;
		}};
	}

	@Test
	public void setResponseCharacterEncoding_Test() {
		instance.setResponseCharacterEncoding(Dummy.STRING);

		new Verifications() {{
			response.setCharacterEncoding(Dummy.STRING); times = 1;
		}};
	}

	@Test
	public void getContentLength_Test() {
		new Expectations() {{
			request.getContentLength(); result = Dummy.INT;
		}};

		int result = instance.getContentLength();
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getContentType_Test() {
		instance.getContentType();
	}

	@Test
	public void getRequestContentType() {
		new Expectations() {{
			request.getContentType(); result = Dummy.STRING;
		}};

		String result = instance.getRequestContentType();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getResponseContentType() {
		new Expectations() {{
			response.getContentType(); result = Dummy.STRING;
		}};

		String result = instance.getResponseContentType();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getInputStream_Test(@Mocked ServletInputStream servletInputStream) throws IOException {
		new Expectations() {{
			request.getInputStream(); result = servletInputStream;
		}};

		ServletInputStream result = instance.getInputStream();
		assertEquals(servletInputStream, result);
	}

	@Test
	public void getParameter_Test() {
		new Expectations() {{
			request.getParameter(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.getParameter(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void getParameterNames_Test(@Mocked Enumeration<String> parameterNames) {
		new Expectations() {{
			request.getParameterNames(); result = parameterNames;
		}};

		Enumeration<String> results = instance.getParameterNames();
		assertEquals(parameterNames, results);
	}

	@Test
	public void getParameterValues_Test() {
		String[] parameterValues = Dummy.stringArray();

		new Expectations() {{
			request.getParameterValues(Dummy.STRING); result = parameterValues;
		}};

		String[] results = instance.getParameterValues(Dummy.STRING);
		assertArrayEquals(parameterValues, results);
	}

	@Test
	public void getParameterMap_Test() {
		Map<String, String[]> parameterMap = new HashMap<>();

		new Expectations() {{
			request.getParameterMap(); result = parameterMap;
		}};

		Map<String, String[]> results = instance.getParameterMap();
		assertEquals(parameterMap, results);
	}

	@Test
	public void getProtocol_Test() {
		new Expectations() {{
			request.getProtocol(); result = Dummy.STRING;
		}};

		String result = instance.getProtocol();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getScheme_Test() {
		new Expectations() {{
			request.getScheme(); result = Dummy.STRING;
		}};

		String result = instance.getScheme();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getServerName_Test() {
		new Expectations() {{
			request.getServerName(); result = Dummy.STRING;
		}};

		String result = instance.getServerName();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getServerPort_Test() {
		new Expectations() {{
			request.getServerPort(); result = Dummy.INT;
		}};

		int result = instance.getServerPort();
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test
	public void getReader_Test(@Mocked BufferedReader bufferedReader) throws IOException {
		new Expectations() {{
			request.getReader(); result = bufferedReader;
		}};

		BufferedReader result = instance.getReader();
		assertEquals(bufferedReader, result);
	}

	@Test
	public void getRemoteAddr_Test() {
		new Expectations() {{
			request.getRemoteAddr(); result = Dummy.STRING;
		}};

		String result = instance.getRemoteAddr();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getRemoteHost_Test() {
		new Expectations() {{
			request.getRemoteHost(); result = Dummy.STRING;
		}};

		String result = instance.getRemoteHost();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void setAttribute_Test() {
		instance.setAttribute(Dummy.STRING, Dummy.OBJECT);

		new Verifications() {{
			request.setAttribute(Dummy.STRING, Dummy.OBJECT); times = 1;
		}};
	}

	@Test
	public void removeAttribute_Test() {
		instance.removeAttribute(Dummy.STRING);

		new Verifications() {{
			request.removeAttribute(Dummy.STRING); times = 1;
		}};
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getLocale_Test() {
		instance.getLocale();
	}

	@Test
	public void getRequestLocale_Test() {
		new Expectations() {{
			request.getLocale(); result = Locale.ENGLISH;
		}};

		Locale result = instance.getRequestLocale();
		assertEquals(Locale.ENGLISH, result);
	}

	@Test
	public void getResponseLocale_Test() {
		new Expectations() {{
			response.getLocale(); result = Locale.ENGLISH;
		}};

		Locale result = instance.getResponseLocale();
		assertEquals(Locale.ENGLISH, result);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getLocales_Test() {
		instance.getLocales();
	}

	@Test
	public void getRequestLocales_Test(@Mocked Enumeration<Locale> locales) {
		new Expectations() {{
			request.getLocales(); result = locales;
		}};

		Enumeration<Locale> results = instance.getRequestLocales();
		assertEquals(locales, results);
	}

	@Test
	public void isSecure_Test() {
		new Expectations() {{
			request.isSecure(); result = true;
		}};

		boolean result = instance.isSecure();
		assertTrue(result);
	}

	@Test
	public void getRequestDispatcher_Test(@Mocked RequestDispatcher requestDispatcher) {
		new Expectations() {{
			request.getRequestDispatcher(Dummy.STRING); result = requestDispatcher;
		}};

		RequestDispatcher result = instance.getRequestDispatcher(Dummy.STRING);
		assertEquals(requestDispatcher, result);
	}

	@Test
	public void getRealPath_Test() {
		new Expectations() {{
			request.getRealPath(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.getRealPath(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void getRemotePort_Test() {
		new Expectations() {{
			request.getRemotePort(); result = Dummy.INT;
		}};

		int result = instance.getRemotePort();
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test
	public void getLocalName_Test() {
		new Expectations() {{
			request.getLocalName(); result = Dummy.STRING;
		}};

		String result = instance.getLocalName();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getLocalAddr_Test() {
		new Expectations() {{
			request.getLocalAddr(); result = Dummy.STRING;
		}};

		String result = instance.getLocalAddr();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getLocalPort_Test() {
		new Expectations() {{
			request.getLocalPort(); result = Dummy.INT;
		}};

		int result = instance.getLocalPort();
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test
	public void getAuthType_Test() {
		new Expectations() {{
			request.getAuthType(); result = Dummy.STRING;
		}};

		String result = instance.getAuthType();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getCookies_Test(@Mocked Cookie cookie) {
		Cookie[] cookies = {cookie};

		new Expectations() {{
			request.getCookies(); result = cookies;
		}};

		Cookie[] results = instance.getCookies();
		assertArrayEquals(cookies, results);
	}

	@Test
	public void getDateHeader_Test() {
		new Expectations() {{
			request.getDateHeader(Dummy.STRING); result = Dummy.LONG;
		}};

		long result = instance.getDateHeader(Dummy.STRING);
		assertEquals(Dummy.LONG.longValue(), result);
	}

	@Test
	public void getHeader_Test() {
		new Expectations() {{
			request.getHeader(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.getHeader(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void getHeaders(@Mocked Enumeration<String> headers) {
		new Expectations() {{
			request.getHeaders(Dummy.STRING); result = headers;
		}};

		Enumeration<String> results = instance.getHeaders(Dummy.STRING);
		assertEquals(headers, results);
	}

	@Test
	public void getHeaderNames(@Mocked Enumeration<String> headerNames) {
		new Expectations() {{
			request.getHeaderNames(); result = headerNames;
		}};

		Enumeration<String> results = instance.getHeaderNames();
		assertEquals(headerNames, results);
	}

	@Test
	public void getIntHeader_Test() {
		new Expectations() {{
			request.getIntHeader(Dummy.STRING); result = Dummy.INT;
		}};

		int result = instance.getIntHeader(Dummy.STRING);
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test
	public void getMethod_Test() {
		new Expectations() {{
			request.getMethod(); result = Dummy.STRING;
		}};

		String result = instance.getMethod();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getPathInfo_Test() {
		new Expectations() {{
			request.getPathInfo(); result = Dummy.STRING;
		}};

		String result = instance.getPathInfo();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getPathTranslated_Test() {
		new Expectations() {{
			request.getPathTranslated(); result = Dummy.STRING;
		}};

		String result = instance.getPathTranslated();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getContextPath_Test() {
		new Expectations() {{
			request.getContextPath(); result = Dummy.STRING;
		}};

		String result = instance.getContextPath();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getQueryString_Test() {
		new Expectations() {{
			request.getQueryString(); result = Dummy.STRING;
		}};

		String result = instance.getQueryString();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getRemoteUser_Test() {
		new Expectations() {{
			request.getRemoteUser(); result = Dummy.STRING;
		}};

		String result = instance.getRemoteUser();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void isUserInRole_Test() {
		new Expectations() {{
			request.isUserInRole(Dummy.STRING); result = true;
		}};

		boolean result = instance.isUserInRole(Dummy.STRING);
		assertTrue(result);
	}

	@Test
	public void getUserPrincipal_Test(@Mocked Principal principal) {
		new Expectations() {{
			request.getUserPrincipal(); result = principal;
		}};

		Principal result = instance.getUserPrincipal();
		assertEquals(principal, result);
	}

	@Test
	public void getRequestedSessionId_Test() {
		new Expectations() {{
			request.getRequestedSessionId(); result = Dummy.STRING;
		}};

		String result = instance.getRequestedSessionId();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getRequestURI_Test() {
		new Expectations() {{
			request.getRequestURI(); result = Dummy.STRING;
		}};

		String result = instance.getRequestURI();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getRequestURL_Test() {
		StringBuffer requestUrl = new StringBuffer(Dummy.STRING);

		new Expectations() {{
			request.getRequestURL(); result = requestUrl;
		}};

		StringBuffer result = instance.getRequestURL();
		assertEquals(requestUrl, result);
	}

	@Test
	public void getServletPath_Test() {
		new Expectations() {{
			request.getServletPath(); result = Dummy.STRING;
		}};

		String result = instance.getServletPath();
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getSession_Test(@Mocked HttpSession httpSession) {
		new Expectations() {{
			request.getSession(); result = httpSession;
		}};

		HttpSession result = instance.getSession();
		assertEquals(httpSession, result);
	}

	@Test
	public void getSession_Create_Session_Test(@Mocked HttpSession httpSession) {
		new Expectations() {{
			request.getSession(true); result = httpSession;
		}};

		HttpSession result = instance.getSession(true);
		assertEquals(httpSession, result);
	}

	@Test
	public void isRequestedSessionIdValid_Test() {
		new Expectations() {{
			request.isRequestedSessionIdValid(); result = true;
		}};

		boolean result = instance.isRequestedSessionIdValid();
		assertTrue(result);
	}

	@Test
	public void isRequestedSessionIdFromCookie_Test() {
		new Expectations() {{
			request.isRequestedSessionIdFromCookie(); result = true;
		}};

		boolean result = instance.isRequestedSessionIdFromCookie();
		assertTrue(result);
	}

	@Test
	public void isRequestedSessionIdFromURL_Test() {
		new Expectations() {{
			request.isRequestedSessionIdFromURL(); result = true;
		}};

		boolean result = instance.isRequestedSessionIdFromURL();
		assertTrue(result);
	}

	@Test
	public void isRequestedSessionIdFromUrl_Test() {
		new Expectations() {{
			request.isRequestedSessionIdFromUrl(); result = true;
		}};

		boolean result = instance.isRequestedSessionIdFromUrl();
		assertTrue(result);
	}

	@Test
	public void getOutputStream_Test(@Mocked ServletOutputStream servletOutputStream) throws IOException {
		new Expectations() {{
			response.getOutputStream(); result = servletOutputStream;
		}};

		ServletOutputStream result = instance.getOutputStream();
		assertEquals(servletOutputStream, result);
	}

	@Test
	public void getWriter_Test() throws IOException {
		PrintWriter printWriter = new PrintWriter(System.out);

		new Expectations() {{
			response.getWriter(); result = printWriter;
		}};

		PrintWriter result = instance.getWriter();
		assertEquals(printWriter, result);
	}

	@Test
	public void setContentLength_Test() {
		instance.setContentLength(Dummy.INT);

		new Verifications() {{
			response.setContentLength(Dummy.INT); times = 1;
		}};
	}

	@Test
	public void setContentType_Test() {
		instance.setContentType(Dummy.STRING);

		new Verifications() {{
			response.setContentType(Dummy.STRING); times = 1;
		}};
	}

	@Test
	public void setBufferSize_Test() {
		instance.setBufferSize(Dummy.INT);

		new Verifications() {{
			response.setBufferSize(Dummy.INT); times = 1;
		}};
	}

	@Test
	public void getBufferSize_Test() {
		new Expectations() {{
			response.getBufferSize(); result = Dummy.INT;
		}};

		int result = instance.getBufferSize();
		assertEquals(Dummy.INT.intValue(), result);
	}

	@Test
	public void flushBuffer_Test() throws IOException {
		instance.flushBuffer();

		new Verifications() {{
			response.flushBuffer(); times = 1;
		}};
	}

	@Test
	public void resetBuffer_Test() {
		instance.resetBuffer();

		new Verifications() {{
			response.resetBuffer(); times = 1;
		}};
	}

	@Test
	public void isCommitted_Test() {
		new Expectations() {{
			response.isCommitted(); result = true;
		}};

		boolean result = instance.isCommitted();
		assertTrue(result);
	}

	@Test
	public void reset_Test() {
		instance.reset();

		new Verifications() {{
			response.reset(); times = 1;
		}};
	}

	@Test
	public void setLocale_Test() {
		instance.setLocale(Locale.ENGLISH);

		new Verifications() {{
			response.setLocale(Locale.ENGLISH); times = 1;
		}};
	}

	@Test
	public void addCookie_Test(@Mocked Cookie cookie) {
		instance.addCookie(cookie);

		new Verifications() {{
			response.addCookie(cookie); times = 1;
		}};
	}

	@Test
	public void containsHeader_Test() {
		new Expectations() {{
			response.containsHeader(Dummy.STRING); result = true;
		}};

		boolean result = instance.containsHeader(Dummy.STRING);
		assertTrue(result);
	}

	@Test
	public void encodeURL_Test() {
		new Expectations() {{
			response.encodeURL(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.encodeURL(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void encodeRedirectURL_Test() {
		new Expectations() {{
			response.encodeRedirectURL(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.encodeRedirectURL(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void encodeUrl_Test() {
		new Expectations() {{
			response.encodeUrl(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.encodeUrl(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void encodeRedirectUrl_Test() {
		new Expectations() {{
			response.encodeRedirectUrl(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.encodeRedirectUrl(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

	@Test
	public void sendError_Test() throws IOException {
		instance.sendError(Dummy.INT, Dummy.STRING);

		new Verifications() {{
			response.sendError(Dummy.INT, Dummy.STRING); times = 1;
		}};
	}

	@Test
	public void sendError_Test2() throws IOException {
		instance.sendError(Dummy.INT);

		new Verifications() {{
			response.sendError(Dummy.INT); times = 1;
		}};
	}

	@Test
	public void sendRedirect_Test() throws IOException {
		instance.sendRedirect(Dummy.STRING);

		new Verifications() {{
			response.sendRedirect(Dummy.STRING); times = 1;
		}};
	}

	@Test
	public void setDateHeader_Test() {
		instance.setDateHeader(Dummy.STRING, Dummy.LONG);

		new Verifications() {{
			response.setDateHeader(Dummy.STRING, Dummy.LONG); times = 1;
		}};
	}

	@Test
	public void addDateHeader_Test() {
		instance.addDateHeader(Dummy.STRING, Dummy.LONG);

		new Verifications() {{
			response.addDateHeader(Dummy.STRING, Dummy.LONG); times = 1;
		}};
	}

	@Test
	public void setHeader_Test() {
		instance.setHeader(Dummy.STRING, Dummy.STRING2);

		new Verifications() {{
			response.setHeader(Dummy.STRING, Dummy.STRING2); times = 1;
		}};
	}

	@Test
	public void addHeader_Test() {
		instance.addHeader(Dummy.STRING, Dummy.STRING2);

		new Verifications() {{
			response.addHeader(Dummy.STRING, Dummy.STRING2); times = 1;
		}};
	}

	@Test
	public void setIntHeader_Test() {
		instance.setIntHeader(Dummy.STRING, Dummy.INT);

		new Verifications() {{
			response.setIntHeader(Dummy.STRING, Dummy.INT); times = 1;
		}};
	}

	@Test
	public void addIntHeader_Test() {
		instance.addIntHeader(Dummy.STRING, Dummy.INT);

		new Verifications() {{
			response.addIntHeader(Dummy.STRING, Dummy.INT); times = 1;
		}};
	}

	@Test
	public void setStatus_Test() {
		instance.setStatus(Dummy.INT);

		new Verifications() {{
			response.setStatus(Dummy.INT); times = 1;
		}};
	}

	@Test
	public void setStatus_And_Message_Test() {
		instance.setStatus(Dummy.INT, Dummy.STRING);

		new Verifications() {{
			response.setStatus(Dummy.INT, Dummy.STRING); times = 1;
		}};
	}

}
