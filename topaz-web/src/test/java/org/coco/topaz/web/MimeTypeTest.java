package org.coco.topaz.web;

import org.coco.topaz.web.exception.UnknownMimeTypeException;
import org.junit.Assert;
import org.junit.Test;

public class MimeTypeTest {

	@Test
	public void get_Test() {
		Assert.assertEquals(MimeType.CSS, MimeType.get("css"));
		Assert.assertEquals(MimeType.JAVASCRIPT, MimeType.get("js"));
		Assert.assertEquals(MimeType.HANDLEBARS, MimeType.get("handlebars"));
		Assert.assertEquals(MimeType.HTML, MimeType.get("html"));
		Assert.assertEquals(MimeType.TEXT, MimeType.get("txt"));
	}

	@Test(expected = UnknownMimeTypeException.class)
	public void get_Null_Test() {
		MimeType.get(null);
	}

}
