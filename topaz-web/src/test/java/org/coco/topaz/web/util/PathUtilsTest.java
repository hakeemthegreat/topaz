package org.coco.topaz.web.util;

import mockit.Expectations;
import mockit.Mocked;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import javax.ws.rs.Path;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PathUtilsTest {

	@Path("/controller/")
	private static class Controller {

		@Path("/method/")
		public void method() {

		}

	}

	@Test
	public void getPathValue_Test(@Mocked AnnotatedElement annotatedElement, @Mocked Path path) {
		new Expectations() {{
			annotatedElement.getAnnotation(Path.class); result = path;
			path.value(); result = Dummy.STRING;
		}};

		String result = PathUtils.getPathValue(annotatedElement);
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void getPathValue_Null_Path_Test(@Mocked AnnotatedElement annotatedElement) {
		new Expectations() {{
			annotatedElement.getAnnotation(Path.class); result = null;
		}};

		String result = PathUtils.getPathValue(annotatedElement);
		assertNull(result);
	}

	@Test
	public void getPathValue_Null_AnnotatedElement_Test() {
		String result = PathUtils.getPathValue(null);
		assertNull(result);
	}

	@Test
	public void getURL_Test() throws Exception {
		Method method = Controller.class.getMethod("method");

		String result = PathUtils.getURL(method);
		assertEquals("/controller/method/", result);
	}

}
