package org.coco.topaz.web.resolver.impl;

import mockit.Tested;
import org.coco.topaz.web.exception.UnresolvedArgumentException;
import org.coco.topaz.util.test.Dummy;
import mockit.Expectations;
import mockit.Mocked;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.util.PathUtils;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArgumentResolverTest {

	@Tested
	private ArgumentResolverImpl instance;

	private static final Long EMPLOYEE_ID = Dummy.LONG;
	private static final String USER_ID = Dummy.STRING;

	@Path("topaz")
	private static class Controller {
		@Path("employee/{employeeId}")
		public void methodA(HttpServletRequest request, HttpServletResponse response, @PathParam("employeeId") Long employeeId, @QueryParam("userId") String userId) {
			//do nothing
		}

		public void methodB(Object object) {
			//do nothing
		}

		public void methodC(String string) {
			//do nothing
		}
	}

	@Test
	public void resolve_Test(@Mocked HttpServletRequest request, @Mocked HttpServletResponse response) throws Exception {
		HttpServletBean httpServletBean = new HttpServletBean(request, response);
		Method method = Controller.class.getDeclaredMethod("methodA", HttpServletRequest.class, HttpServletResponse.class, Long.class, String.class);
		Map<String, String> pathParamMap = new HashMap<>();

		pathParamMap.put("employeeId", EMPLOYEE_ID.toString());

		new Expectations(instance) {{
			instance.buildPathParamMap(method, httpServletBean); result = pathParamMap;
			request.getParameter("userId"); result = USER_ID;
		}};

		Object[] results = instance.resolve(httpServletBean, method);
		assertNotNull(results);
		assertEquals(4, results.length);
		assertEquals(request, results[0]);
		assertEquals(response, results[1]);
		assertEquals(EMPLOYEE_ID, results[2]);
		assertEquals(USER_ID, results[3]);
	}

	@Test(expected = UnresolvedArgumentException.class)
	public void resolve_UnresolvedArgumentException_Test(@Mocked HttpServletRequest request, @Mocked HttpServletResponse response) throws Exception {
		HttpServletBean httpServletBean = new HttpServletBean(request, response);
		Method method = Controller.class.getDeclaredMethod("methodB", Object.class);

		instance.resolve(httpServletBean, method);
	}

	@Test
	public void resolve_String_Test(@Mocked HttpServletRequest request, @Mocked HttpServletResponse response) throws Exception {
		HttpServletBean httpServletBean = new HttpServletBean(request, response);
		Method method = Controller.class.getDeclaredMethod("methodC", String.class);

		new Expectations(instance) {{
			instance.resolveBasicClass(httpServletBean, method, (Parameter) any); result = Dummy.STRING;
		}};

		Object[] results = instance.resolve(httpServletBean, method);
		assertNotNull(results);
		assertEquals(1, results.length);
		assertEquals(Dummy.STRING, results[0]);
	}

	@Test
	public void resolveBasicClass_QueryParam_Test(@Mocked HttpServletBean httpServletBean, @Mocked Parameter parameter, @Mocked QueryParam queryParam) throws Exception {
		Method method = Object.class.getMethod("toString");
		Annotation[] annotations = {queryParam};
		String parameterName = Dummy.STRING;
		String parameterValue = Dummy.LONG.toString();
		Class<?> parameterClass = Long.class;

		new Expectations(instance) {{
			instance.buildPathParamMap(method, httpServletBean); result = Collections.emptyMap();
			parameter.getAnnotations(); result = annotations;
			parameter.getAnnotation(QueryParam.class); result = queryParam;
			queryParam.value(); result = parameterName;
			httpServletBean.getParameter(parameterName); result = parameterValue;
			parameter.getType(); result = parameterClass;
		}};

		Object result = instance.resolveBasicClass(httpServletBean, method, parameter);
		assertEquals(Dummy.LONG, result);
	}

	@Test
	public void resolveBasicClass_PathParam_Test(@Mocked HttpServletBean httpServletBean, @Mocked Parameter parameter, @Mocked PathParam pathParam) throws Exception {
		Method method = Object.class.getMethod("toString");
		Annotation[] annotations = {pathParam};
		Map<String, String> pathParamMap = new HashMap<>();
		String parameterName = Dummy.STRING;
		String parameterValue = Dummy.LONG.toString();
		Class<?> parameterClass = Long.class;

		pathParamMap.put(parameterName, parameterValue);

		new Expectations(instance) {{
			instance.buildPathParamMap(method, httpServletBean); result = pathParamMap;
			parameter.getAnnotations(); result = annotations;
			parameter.getAnnotation(PathParam.class); result = pathParam;
			pathParam.value(); result = parameterName;
			parameter.getType(); result = parameterClass;
		}};

		Object result = instance.resolveBasicClass(httpServletBean, method, parameter);
		assertEquals(Dummy.LONG, result);
	}

	@Test(expected = UnresolvedArgumentException.class)
	public void resolveBasicClass_UnresolvedArgumentException_Test(@Mocked HttpServletBean httpServletBean, @Mocked Parameter parameter) throws Exception {
		Method method = Object.class.getMethod("toString");
		Annotation[] annotations = {};

		new Expectations(instance) {{
			instance.buildPathParamMap(method, httpServletBean); result = Collections.emptyMap();
			parameter.getAnnotations(); result = annotations;
		}};

		Object result = instance.resolveBasicClass(httpServletBean, method, parameter);
		assertEquals(Dummy.LONG, result);
	}

	@Test
	public void buildPathParamMap_Test(@Mocked HttpServletBean httpServletBean) throws Exception {
		Method method = Controller.class.getDeclaredMethod("methodA", HttpServletRequest.class, HttpServletResponse.class, Long.class, String.class);
		Map<String, String> map = new HashMap<>();

		new Expectations(PathUtils.class, instance) {{
			PathUtils.getPathValue(Controller.class); result = "controller";
			PathUtils.getPathValue(method); result = "method";
			httpServletBean.getServletPath(); result = Dummy.STRING;
			instance.buildPathParamMap(Dummy.STRING, "controller/method"); result = map;
		}};

		Map<String, String> results = instance.buildPathParamMap(method, httpServletBean);
		assertEquals(map, results);
	}

	@Test
	public void buildPathParamMap_Null_Controller_Path_Test(@Mocked HttpServletBean httpServletBean) throws Exception {
		Method method = Controller.class.getDeclaredMethod("methodA", HttpServletRequest.class, HttpServletResponse.class, Long.class, String.class);
		Map<String, String> map = new HashMap<>();

		new Expectations(PathUtils.class, instance) {{
			PathUtils.getPathValue(Controller.class); result = null;
			PathUtils.getPathValue(method); result = "method";
			httpServletBean.getServletPath(); result = Dummy.STRING;
			instance.buildPathParamMap(Dummy.STRING, "method"); result = map;
		}};

		Map<String, String> results = instance.buildPathParamMap(method, httpServletBean);
		assertEquals(map, results);
	}

	@Test
	public void buildPathParamMap_Properly_Formed_Controller_Path_Test() {
		String servletPath = "/employee/1";
		String controllerPath = "/employee/{employeeId}";

		Map<String, String> results = instance.buildPathParamMap(servletPath, controllerPath);
		assertNotNull(results);
		assertEquals(1, results.size());
		assertEquals("1", results.get("employeeId"));
	}

	@Test
	public void buildPathParamMap_Missing_Right_Brace_Test() {
		String servletPath = "/employee/1";
		String controllerPath = "/employee/{employeeId";

		Map<String, String> results = instance.buildPathParamMap(servletPath, controllerPath);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

}
