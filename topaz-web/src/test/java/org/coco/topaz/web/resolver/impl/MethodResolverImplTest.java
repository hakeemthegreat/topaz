package org.coco.topaz.web.resolver.impl;

import org.coco.topaz.web.exception.UnsupportedRequestMethodException;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.junit.Test;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;

public class MethodResolverImplTest {

	@Path("controller")
	private static class Controller {

		@DELETE
		@Path("{id}")
		public void deleteOne() {

		}

		@DELETE
		@Path("all")
		public void deleteAll() {

		}

		@SuppressWarnings("unused") //used for code coverage
		public void anotherMethod() {

		}

	}

	@Tested
	private MethodResolverImpl instance;

	@Test
	public void resolve_DeleteOne_Test(@Mocked HttpServletBean httpServletBean) throws Exception {
		Collection<Class<?>> classes = Collections.singleton(Controller.class);
		Method deleteOne = Controller.class.getMethod("deleteOne");

		new Expectations(instance) {{
			httpServletBean.getMethod(); result = "DELETE";
			httpServletBean.getServletPath(); result = "/controller/1";
		}};

		Method result = instance.resolve(httpServletBean, classes);
		assertEquals(deleteOne, result);
	}

	@Test
	public void resolve_DeleteAll_Test(@Mocked HttpServletBean httpServletBean) throws Exception {
		Collection<Class<?>> classes = Collections.singleton(Controller.class);
		Method deleteAll = Controller.class.getMethod("deleteAll");

		new Expectations(instance) {{
			httpServletBean.getMethod(); result = "DELETE";
			httpServletBean.getServletPath(); result = "/controller/all";
		}};

		Method result = instance.resolve(httpServletBean, classes);
		assertEquals(deleteAll, result);
	}

	@Test
	public void resolve_No_Methods_Test(@Mocked HttpServletBean httpServletBean) {
		Collection<Class<?>> classes = Collections.singleton(Object.class);
		Method[] methods = {};

		new Expectations(instance) {{
			httpServletBean.getMethod(); result = "GET";
			instance.getMethods(Object.class); result = methods;
		}};

		Method result = instance.resolve(httpServletBean, classes);
		assertNull(result);
	}

	@Test
	public void getMethods_Test() throws Exception {
		Method deleteOne = Controller.class.getMethod("deleteOne");
		Method deleteAll = Controller.class.getMethod("deleteAll");

		Collection<Method> results = instance.getMethods(Controller.class);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertTrue(results.contains(deleteOne));
		assertTrue(results.contains(deleteAll));
	}

	@Test
	public void getSupportedRestMethods_Test() throws Exception {
		Method deleteOne = Controller.class.getMethod("deleteOne");

		Collection<String> results = instance.getSupportedRestMethods(deleteOne);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertTrue(results.contains("GET"));
		assertTrue(results.contains("DELETE"));
	}

	@Test
	public void validateRequestMethod_Test() throws Exception {
		Method deleteAll = Controller.class.getMethod("deleteAll");

		instance.validateRequestMethod("DELETE", deleteAll);
	}

	@Test
	public void validateRequestMethod_Null_Test() {
		new Expectations(instance) {{
			instance.getSupportedRestMethods((Method) any); times = 0;
		}};

		instance.validateRequestMethod("DELETE", null);
	}

	@Test(expected = UnsupportedRequestMethodException.class)
	public void validateRequestMethod_UnsupportedRequestMethodException_Test() throws Exception {
		Method deleteAll = Controller.class.getMethod("deleteAll");

		instance.validateRequestMethod("PATCH", deleteAll);
	}

}
