package org.coco.topaz.web.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class UnsupportedRequestMethodExceptionTest extends ExceptionTest<UnsupportedRequestMethodException> {

	public UnsupportedRequestMethodExceptionTest() {
		super(UnsupportedRequestMethodException.class);
	}

}
