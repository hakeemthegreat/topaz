package org.coco.topaz.web.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class UnresolvedMappingExceptionTest extends ExceptionTest<UnresolvedMappingException> {

	public UnresolvedMappingExceptionTest() {
		super(UnresolvedMappingException.class);
	}

}
