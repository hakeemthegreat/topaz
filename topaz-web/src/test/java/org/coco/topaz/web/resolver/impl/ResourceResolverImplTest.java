package org.coco.topaz.web.resolver.impl;

import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.Streams;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.coco.topaz.web.MimeType;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.util.WebContentUtils;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.*;

import static org.junit.Assert.assertEquals;

public class ResourceResolverImplTest {

	@Tested
	private ResourceResolverImpl instance;

	@Test
	public void resolve_Test(@Mocked HttpServletBean httpServletBean, @Mocked ByteArrayOutputStream byteArrayOutputStream, @Mocked Response response, @Mocked Response.ResponseBuilder responseBuilder) throws Exception {
		FileInputStream fileInputStream = null;

		new Expectations(WebContentUtils.class, Streams.class, instance, Response.class) {{
			httpServletBean.getServletPath(); result = Dummy.STRING;
			WebContentUtils.getLocation(Dummy.STRING); result = Dummy.STRING2;
			Streams.newFileInputStream(Dummy.STRING2); result = fileInputStream;
			Streams.newByteArrayOutputStream(); result = byteArrayOutputStream;
			Streams.copy(fileInputStream, byteArrayOutputStream); //do nothing
			instance.getMediaType(Dummy.STRING); result = Dummy.STRING3;
			byteArrayOutputStream.toString(); result = Dummy.STRING4;
			Response.ok(Dummy.STRING4, Dummy.STRING3); result = responseBuilder;
			responseBuilder.build(); result = response;
		}};

		Response result = instance.resolve(httpServletBean);
		assertEquals(response, result);
	}

	@Test(expected = RuntimeException.class)
	public void resolve_IOException_Test(@Mocked HttpServletBean httpServletBean, @Mocked ByteArrayOutputStream byteArrayOutputStream) throws Exception {
		FileInputStream fileInputStream = null;

		new Expectations(WebContentUtils.class, Streams.class, instance, Response.class) {{
			httpServletBean.getServletPath(); result = Dummy.STRING;
			WebContentUtils.getLocation(Dummy.STRING); result = Dummy.STRING2;
			Streams.newFileInputStream(Dummy.STRING2); result = fileInputStream;
			Streams.newByteArrayOutputStream(); result = byteArrayOutputStream;
			Streams.copy((InputStream) any, (OutputStream) any); result = new IOException();
		}};

		instance.resolve(httpServletBean);
	}

	@Test
	public void getMediaType_Test() {
		new Expectations(MimeType.class) {{
			MimeType.get(Dummy.STRING); result = Dummy.STRING2;
		}};

		String result = instance.getMediaType(Dummy.STRING);
		assertEquals(Dummy.STRING2, result);
	}

}
