package org.coco.topaz.web;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ServletPathTest {

	private final ServletPath instance = new ServletPath(PATH);

	private static final String PATH = "//www.site.org//employee//1";
	private static final String NORMALIZED_PATH = "www.site.org/employee/1";

	@Test
	public void parts_Getter_Setter_Test() {
		instance.setParts(Dummy.stringList());
		assertEquals(Dummy.stringList(), instance.getParts());
	}

	@Test
	public void getBase_Test() {
		String result = instance.getBase();
		assertEquals("www.site.org", result);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals(NORMALIZED_PATH, result);
	}

}
