package org.coco.topaz.web.resolver.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class URLUtilsTest {

	@Test
	public void match_Partial_Test() {
		Match result = URLUtils.match("/system/employee/all", "/system/employee/{1}");
		assertNotNull(result);
		assertEquals(0.8333333333333334, result.getAccuracy(), 0.0);
	}

	@Test
	public void match_Partial_Test2() {
		Match result = URLUtils.match("/system/employee/{1}", "/system/employee/all");
		assertNotNull(result);
		assertEquals(0.8333333333333334, result.getAccuracy(), 0.0);
	}

	@Test
	public void match_Full_Test() {
		Match result = URLUtils.match("/system/employee/all", "/system/employee/all");
		assertNotNull(result);
		assertEquals(1, result.getAccuracy(), 0.0);
	}

	@Test
	public void match_Request_Larger_Than_Resource_Test() {
		Match result = URLUtils.match("/system/employee/all", "/system/orders");
		assertNotNull(result);
		assertEquals(0.3333333333333333, result.getAccuracy(), 0.0);
	}

}
