package org.coco.topaz.web;

import org.coco.topaz.injection.DependencyInjection;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.IteratorEnumeration;
import mockit.*;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.resolver.Resolver;
import org.coco.topaz.web.resolver.ResourceResolver;
import org.coco.topaz.web.resolver.ViewResolver;
import org.coco.topaz.web.service.ResponseService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import static org.junit.Assert.*;

public class MasterServletTest {

	@Mocked
	private HttpServletRequest httpServletRequest;

	@Mocked
	private HttpServletResponse httpServletResponse;

	@Mocked
	private ServletConfig servletConfig;

	@Mocked
	private ResourceResolver resourceResolver;

	@Mocked
	private ViewResolver viewResolver;

	@Mocked
	private ResponseService responseService;

	private final MasterServlet instance = new MasterServlet();

	static {
		WebBeans.init();
	}

	@Before
	public void before() throws Exception {
		instance.init(servletConfig);
		Deencapsulation.setField(instance, "responseService", responseService);
		Deencapsulation.setField(instance, "resourceResolver", resourceResolver);
		Deencapsulation.setField(instance, "viewResolver", viewResolver);
	}

	@Test
	public void initializeMvcConfiguration_Test() {
		Collection<String> parameterNames = Arrays.asList(Dummy.STRING, Dummy.STRING2);
		Enumeration<String> enumeration = new IteratorEnumeration<>(parameterNames.iterator());

		new Expectations(instance, MVCConfiguration.class) {{
			instance.getInitParameterNames(); result = enumeration;
			instance.getInitParameter(Dummy.STRING); result = Dummy.STRING;
			instance.getInitParameter(Dummy.STRING2); result = Dummy.STRING2;
			MVCConfiguration.load((Map<String, String>) any); //do nothing
		}};

		instance.initializeMvcConfiguration();

		new Verifications() {{
			Map<String, String> initParameters;
			MVCConfiguration.load(initParameters = withCapture()); times = 1;
			assertEquals(Dummy.STRING, initParameters.get(Dummy.STRING));
			assertEquals(Dummy.STRING2, initParameters.get(Dummy.STRING2));
		}};
	}

	@Test
	public void injectDependencies_Test() {
		new Expectations(DependencyInjection.class) {{
			DependencyInjection.injectDependencies(instance); //do nothing
		}};

		instance.injectDependencies();
	}

	@Test
	public void doGet_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doGet(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doPost_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doPost(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doDelete_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doDelete(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doHead_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doHead(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doOptions_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doOptions(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doPut_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit(httpServletRequest, httpServletResponse); //do nothing
		}};

		instance.doPut(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doSubmit_Test() throws Exception {
		new Expectations(instance) {{
			instance.doSubmit((HttpServletBean) any); //do nothing
		}};

		instance.doSubmit(httpServletRequest, httpServletResponse);
	}

	@Test
	public void doSubmit_Test2(@Mocked Resolver resolver, @Mocked Response response) throws Exception {
		HttpServletBean httpServletBean = new HttpServletBean(httpServletRequest, httpServletResponse);

		new Expectations(instance) {{
			httpServletRequest.getServletPath(); result = Dummy.STRING;
			instance.getResolver(Dummy.STRING); result = resolver;
			resolver.resolve(httpServletBean); result = response;
			responseService.transform(response); result = response;
			instance.sendOutput(httpServletBean, response); //do nothing
		}};

		instance.doSubmit(httpServletBean);
	}

	@Test
	public void getResolver_ViewResolver_Test() {
		Resolver result = instance.getResolver("employee");
		assertTrue(result instanceof ViewResolver);
	}

	@Test
	public void getResolver_ResourceResolver_Test() {
		Resolver result = instance.getResolver("image.jpg");
		assertTrue(result instanceof ResourceResolver);
	}

	@Test
	public void sendOutput_Test(@Mocked HttpServletBean httpServletBean, @Mocked Response response, @Mocked ServletOutputStream servletOutputStream, @Mocked MediaType mediaType) throws Exception {
		new Expectations() {{
			httpServletBean.getOutputStream(); result = servletOutputStream;
			response.getStatus(); result = HttpServletResponse.SC_OK;
			response.getEntity(); result = Dummy.LONG;
			response.getMediaType(); result = mediaType;
			mediaType.toString(); result = MediaType.APPLICATION_JSON;
		}};

		instance.sendOutput(httpServletBean, response);

		new Verifications() {{
			httpServletBean.setStatus(HttpServletResponse.SC_OK); times = 1;
			httpServletBean.setContentType(MediaType.APPLICATION_JSON); times = 1;
			servletOutputStream.print(Dummy.LONG.toString()); times = 1;
		}};
	}

	@Test
	public void sendOutput_Internal_Server_Error_Test(@Mocked HttpServletBean httpServletBean, @Mocked Response response, @Mocked ServletOutputStream servletOutputStream) throws Exception {
		new Expectations() {{
			httpServletBean.getOutputStream(); result = servletOutputStream;
			response.getStatus(); result = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			response.getEntity(); result = Dummy.OBJECT;
		}};

		instance.sendOutput(httpServletBean, response);

		new Verifications() {{
			httpServletBean.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); times = 1;
			httpServletBean.setContentType(anyString); times = 0;
			servletOutputStream.print(anyString); times = 0;
		}};
	}

	@Test
	public void sendOutput_StatusCode_Zero_Test(@Mocked HttpServletBean httpServletBean, @Mocked Response response, @Mocked ServletOutputStream servletOutputStream, @Mocked MediaType mediaType) throws Exception {
		new Expectations() {{
			httpServletBean.getOutputStream(); result = servletOutputStream;
			response.getStatus(); result = 0;
			response.getEntity(); result = Dummy.STRING;
			response.getMediaType(); result = mediaType;
			mediaType.toString(); result = MediaType.APPLICATION_JSON;
		}};

		instance.sendOutput(httpServletBean, response);

		new Verifications() {{
			httpServletBean.setStatus(HttpServletResponse.SC_OK); times = 1;
			httpServletBean.setContentType(MediaType.APPLICATION_JSON); times = 1;
			servletOutputStream.print(Dummy.STRING); times = 1;
		}};
	}

}
