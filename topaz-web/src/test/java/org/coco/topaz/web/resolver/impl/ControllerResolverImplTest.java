package org.coco.topaz.web.resolver.impl;

import org.coco.topaz.web.exception.UnresolvedMappingException;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.Packages;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.coco.topaz.web.MVCConfiguration;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.junit.Test;

import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import static org.junit.Assert.*;

public class ControllerResolverImplTest {

	@Path("controller")
	private static class Controller {

		@Path("method")
		public void method() {
			//do work
		}

	}

	@Tested
	private ControllerResolverImpl instance;

	private static final String SERVLET_PATH = "/controller";
	private static final String CONTROLLER_PACKAGE = "org.example.something.mvc";

	@Test
	public void resolve_Test(@Mocked HttpServletBean httpServletBean) {
		List<Class<?>> all = Collections.singletonList(Controller.class);

		new Expectations(MVCConfiguration.class, instance) {{
			httpServletBean.getServletPath(); result = SERVLET_PATH;
			MVCConfiguration.getControllerPackage(); result = CONTROLLER_PACKAGE;
			instance.getControllerClasses(CONTROLLER_PACKAGE); result = all;
		}};

		List<Class<?>> results = instance.resolve(httpServletBean);
		assertNotNull(results);
		assertTrue(results.contains(Controller.class));
	}

	@Test(expected = UnresolvedMappingException.class)
	public void resolve_UnresolvedMappingException_Test(@Mocked HttpServletBean httpServletBean) {
		List<Class<?>> all = Collections.singletonList(Controller.class);

		new Expectations(MVCConfiguration.class, instance) {{
			httpServletBean.getServletPath(); result = "/bad/request";
			MVCConfiguration.getControllerPackage(); result = CONTROLLER_PACKAGE;
			instance.getControllerClasses(CONTROLLER_PACKAGE); result = all;
		}};

		instance.resolve(httpServletBean);
	}

	@Test
	public void getControllerClasses_Test() {
		List<Class<?>> controllerClasses = new ArrayList<>();

		controllerClasses.add(Controller.class);
		controllerClasses.add(Object.class);

		new Expectations(Packages.class) {{
			Packages.getClasses(Dummy.STRING); result = controllerClasses;
		}};

		List<Class<?>> results = instance.getControllerClasses(Dummy.STRING);
		assertNotNull(results);
		assertEquals(1, results.size());
		assertTrue(results.contains(Controller.class));
	}

}
