package org.coco.topaz.web.resolver.impl;

import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MatchTest extends GetterSetterTest<Match> {

	public MatchTest() {
		super(new Match());
	}

	@Test
	public void toString_Test() {
		instance.setAccuracy(Dummy.DOUBLE);

		String result = instance.toString();
		assertEquals(Dummy.DOUBLE.toString(), result);
	}

}
