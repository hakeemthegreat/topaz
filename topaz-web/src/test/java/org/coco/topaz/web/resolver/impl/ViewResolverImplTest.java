package org.coco.topaz.web.resolver.impl;

import mockit.*;
import org.coco.topaz.web.exception.UnresolvedMappingException;
import org.coco.topaz.util.reflection.Methods;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.web.MimeType;
import org.coco.topaz.web.resolver.ArgumentResolver;
import org.coco.topaz.web.resolver.ControllerResolver;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.resolver.MethodResolver;
import org.coco.topaz.web.service.ResponseService;
import org.junit.Test;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class ViewResolverImplTest {

	@Injectable
	private ControllerResolver controllerResolver;

	@Injectable
	private MethodResolver methodResolver;

	@Injectable
	private ArgumentResolver argumentResolver;

	@Injectable
	private ResponseService responseService;

	@Tested
	private ViewResolverImpl instance;

	@Test
	public void resolve_Test(@Mocked HttpServletBean httpServletBean, @Mocked Response response, @Mocked Response transformedResponse, @Mocked ResponseBuilder responseBuilder) throws Exception {
		Collection<Class<?>> controllerClasses = Arrays.asList(Object.class, String.class);
		Method method = Object.class.getMethod("toString");
		Object controller = Dummy.OBJECT;
		Object entity = new Object();
		Object[] arguments = {new Object(), new Object(), new Object()};

		new Expectations(instance, Methods.class, Response.class) {{
			controllerResolver.resolve((HttpServletBean) any); result = controllerClasses;
			methodResolver.resolve((HttpServletBean) any, (Collection<Class<?>>) any); result = method;
			instance.getInstance((Class<?>) any); result = controller;
			argumentResolver.resolve((HttpServletBean) any, (Method) any); result = arguments;
			Methods.invoke(any, (Method) any, (Object[]) any); result = entity;
			Response.ok(any, anyString); result = responseBuilder;
			responseBuilder.build(); result = response;
			responseService.transform((Response) any); result = transformedResponse;
		}};

		Response result = instance.resolve(httpServletBean);
		assertEquals(transformedResponse, result);

		new Verifications() {{
			controllerResolver.resolve(httpServletBean); times = 1;
			methodResolver.resolve(httpServletBean, controllerClasses); times = 1;
			instance.getInstance(Object.class); times = 1;
			argumentResolver.resolve(httpServletBean, method); times = 1;
			Methods.invoke(controller, method, arguments); times = 1;
			Response.ok(entity, MediaType.TEXT_HTML); times = 1;
			responseService.transform(response); times = 1;
		}};
	}

	@Test(expected = UnresolvedMappingException.class)
	public void resolve_UnresolvedMappingException_Test(@Mocked HttpServletBean httpServletBean) {
		Collection<Class<?>> controllerClasses = Arrays.asList(Object.class, String.class);
		String servletPath = "servletPath";

		new Expectations() {{
			controllerResolver.resolve((HttpServletBean) any); result = controllerClasses;
			methodResolver.resolve((HttpServletBean) any, (Collection<Class<?>>) any); result = null;
			httpServletBean.getServletPath(); result = servletPath;
		}};

		instance.resolve(httpServletBean);

		new Verifications() {{
			controllerResolver.resolve(httpServletBean); times = 1;
			methodResolver.resolve(httpServletBean, controllerClasses); times = 1;
		}};
	}

	@Test
	public void getMediaType_Test(@Mocked Produces produces) throws Exception {
		Method method = Object.class.getMethod("toString");
		String[] value = {MimeType.CSS, MimeType.JAVASCRIPT};

		new Expectations(method) {{
			method.getAnnotation(Produces.class); result = produces;
			produces.value(); result = value;
		}};

		String result = ViewResolverImpl.getMediaType(method);
		assertEquals("text/css", result);
	}

	@Test
	public void getMediaType_Null_Annotation_Test() throws Exception {
		Method method = Object.class.getMethod("toString");

		new Expectations(method) {{
			method.getAnnotation(Produces.class); result = null;
		}};

		String result = ViewResolverImpl.getMediaType(method);
		assertEquals("text/html", result);
	}

	@Test
	public void getInstance_Test() {
		Class<?> clazz = Object.class;

		Object result = instance.getInstance(clazz);
		assertNotNull(result);

		Object result2 = instance.getInstance(clazz);
		assertSame(result, result2);
	}

}
