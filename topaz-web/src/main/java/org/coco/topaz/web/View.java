package org.coco.topaz.web;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class View {

	private String name;

	@Override
	public String toString() {
		return name;
	}

}
