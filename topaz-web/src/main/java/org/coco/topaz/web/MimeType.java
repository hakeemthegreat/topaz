package org.coco.topaz.web;

import org.coco.topaz.web.exception.UnknownMimeTypeException;
import org.coco.topaz.util.Constants;

import java.util.HashMap;
import java.util.Map;

public class MimeType {
	
	private static final Map<String, String> MAP = new HashMap<>();
	
	public static final String CSS = "text/css";
	public static final String JAVASCRIPT = "text/javascript";
	public static final String HANDLEBARS = "text/x-handlebars";
	public static final String HTML = "text/html";
	public static final String TEXT = "text/plain";
	
	static {
		MAP.put("css", CSS);
		MAP.put("js", JAVASCRIPT);
		MAP.put("handlebars", HANDLEBARS);
		MAP.put("html", HTML);
		MAP.put("ico", "image/x-icon");
		MAP.put("m4a", "audio/mp4a-latm");
		MAP.put("mp3", "audio/mpeg");
		MAP.put("txt", TEXT);
	}

	private MimeType() {

	}
	
	public static String get(String extension) {
		String value = MAP.get(extension);
				
		if (value == null) {
			throw new UnknownMimeTypeException(Constants.Strings.PERIOD + extension);
		}
				
		return value;
	}

}
