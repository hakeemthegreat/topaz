package org.coco.topaz.web.resolver.impl;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Match {

	private double accuracy;
	private Object target;

	@Override
	public String toString() {
		return Double.toString(accuracy);
	}
}
