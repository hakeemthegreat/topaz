package org.coco.topaz.web.util;

import org.coco.topaz.util.Constants;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

public class WebContentUtils {

	private WebContentUtils() {

	}
	
	public static String getLocation(String path) throws IOException {
		Enumeration<URL> resources = getResources();
		String location = null;

		if (isAbsolutePath(path)) {
			location = path;
		} else if (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			location = resource.getFile() + "../.." + path;
		}
		
		return location;
	}

	protected static Enumeration<URL> getResources() throws IOException {
		ClassLoader classLoader = WebContentUtils.class.getClassLoader();
		return classLoader.getResources(Constants.Strings.EMPTY);
	}

	protected static boolean isAbsolutePath(String path) {
		File file = new File(path);
		return file.isAbsolute();
	}

}
