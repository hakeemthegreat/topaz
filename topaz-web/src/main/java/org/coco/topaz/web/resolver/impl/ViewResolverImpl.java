package org.coco.topaz.web.resolver.impl;

import java.lang.reflect.Method;
import java.util.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.coco.topaz.web.exception.UnresolvedMappingException;
import org.coco.topaz.injection.Dependency;
import org.coco.topaz.injection.DependencyInjection;
import org.coco.topaz.util.reflection.Methods;
import org.coco.topaz.web.resolver.*;
import org.coco.topaz.web.service.ResponseService;

public class ViewResolverImpl implements ViewResolver {
	
	private final ControllerResolver controllerResolver;
	private final MethodResolver methodResolver;
	private final ArgumentResolver argumentResolver;
	private final ResponseService responseService;

	private static final Map<Class<?>, Object> CONTROLLERS_INSTANCES = new HashMap<>();
	
	@Dependency
	public ViewResolverImpl(ControllerResolver controllerResolver, MethodResolver methodResolver, ArgumentResolver argumentResolver, ResponseService responseService) {
		this.controllerResolver = Objects.requireNonNull(controllerResolver);
		this.methodResolver = Objects.requireNonNull(methodResolver);
		this.argumentResolver = Objects.requireNonNull(argumentResolver);
		this.responseService = Objects.requireNonNull(responseService);
	}
	
	@Override
	public Response resolve(HttpServletBean httpServletBean) {
		List<Class<?>> controllerClasses = controllerResolver.resolve(httpServletBean);
		Method method = methodResolver.resolve(httpServletBean, controllerClasses);
		Object controller;
		Object[] arguments;
		Object entity;

		if (method == null) {
			throw new UnresolvedMappingException(httpServletBean.getServletPath());
		}

		controller = getInstance(method.getDeclaringClass());
		arguments = argumentResolver.resolve(httpServletBean, method);
		entity = Methods.invoke(controller, method, arguments);
		return responseService.transform(Response.ok(entity, getMediaType(method)).build());
	}
	
	protected static String getMediaType(Method method) {
		Produces produces = method.getAnnotation(Produces.class);
		RuntimeException e = new RuntimeException("No mimeType specified for " + method);
		String mediaType = MediaType.TEXT_HTML;
		
		if (produces != null) {
			mediaType = Arrays.stream(produces.value()).findFirst().orElseThrow(() -> e);
		}
		
		return mediaType;
	}

	protected Object getInstance(Class<?> clazz) {
		Object instance = CONTROLLERS_INSTANCES.get(clazz);

		if (instance == null) {
			instance = DependencyInjection.newInstance(clazz);
			CONTROLLERS_INSTANCES.put(clazz, instance);
		}

		return instance;
	}

}
