package org.coco.topaz.web.exception;

import org.coco.topaz.exception.TopazException;

public class UnresolvedArgumentException extends TopazException {

	public UnresolvedArgumentException(String message) {
		super(message);
	}

}
