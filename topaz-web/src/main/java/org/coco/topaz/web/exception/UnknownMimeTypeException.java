package org.coco.topaz.web.exception;

import org.coco.topaz.exception.TopazException;

public class UnknownMimeTypeException extends TopazException {

	public UnknownMimeTypeException(String message) {
		super(message);
	}

}
