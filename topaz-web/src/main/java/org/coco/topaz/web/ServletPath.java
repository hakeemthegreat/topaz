package org.coco.topaz.web;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.Strings;

import java.util.ArrayList;
import java.util.List;

import static org.coco.topaz.util.Constants.Strings.FORWARD_SLASH;

@Getter
@Setter
public class ServletPath {

	private List<String> parts = new ArrayList<>();

	public ServletPath(String servletPath) {
		String[] split = servletPath.split("/");

		for (String part : split) {
			if (Strings.isNotBlank(part)) {
				parts.add(part);
			}
		}
	}

	public String getBase() {
		return parts.stream().findFirst().orElse(null);
	}


	@Override
	public String toString() {
		List<String> strings = new ArrayList<>(parts);
		return String.join(FORWARD_SLASH, strings);
	}

}
