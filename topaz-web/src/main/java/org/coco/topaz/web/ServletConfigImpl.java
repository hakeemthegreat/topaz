package org.coco.topaz.web;

import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.IteratorEnumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ServletConfigImpl implements ServletConfig {

	private String servletName;
	private ServletContext servletContext;

	private final Map<String, String> initParameters = new HashMap<>();

	public void setServletName(String servletName) {
		this.servletName = Objects.requireNonNull(servletName);
	}

	@Override
	public String getServletName() {
		return servletName;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = Objects.requireNonNull(servletContext);
	}

	@Override
	public ServletContext getServletContext() {
		return servletContext;
	}

	public void addInitParameter(String name, String value) {
		Ensure.notNull(name);
		initParameters.put(name, value);
	}

	@Override
	public String getInitParameter(String name) {
		return initParameters.get(name);
	}

	@Override
	public Enumeration<String> getInitParameterNames() {
		return new IteratorEnumeration<>(initParameters.keySet().iterator());
	}

}
