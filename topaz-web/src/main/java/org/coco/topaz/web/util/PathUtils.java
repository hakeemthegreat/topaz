package org.coco.topaz.web.util;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

import javax.ws.rs.Path;

import static org.coco.topaz.util.Constants.Strings.FORWARD_SLASH;

public class PathUtils {
	
	private PathUtils() {
		
	}
	
	public static String getPathValue(AnnotatedElement annotatedElement) {
		String value = null;

		if (annotatedElement != null) {
			Path path = annotatedElement.getAnnotation(Path.class);

			if (path != null) {
				value = path.value();
			}
		}
		
		return value;
	}

	public static String getURL(Method method) {
		Class<?> declaringClass = method.getDeclaringClass();
		String declaringClassPathValue = getPathValue(declaringClass);
		String pathValue = getPathValue(method);
		String result = FORWARD_SLASH + declaringClassPathValue + FORWARD_SLASH + pathValue;

		result = result.replaceAll("/+", "/");
		return result;
	}

}
