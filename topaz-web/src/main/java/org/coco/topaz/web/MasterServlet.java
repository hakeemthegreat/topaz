package org.coco.topaz.web;

import org.coco.topaz.injection.Dependency;
import org.coco.topaz.injection.DependencyInjection;
import org.coco.topaz.util.Constants;
import org.coco.topaz.util.Replace;
import org.coco.topaz.util.Strings;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.resolver.Resolver;
import org.coco.topaz.web.resolver.ResourceResolver;
import org.coco.topaz.web.resolver.ViewResolver;
import org.coco.topaz.web.service.ResponseService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MasterServlet extends HttpServlet {
	
	@Dependency
	private static ResponseService responseService;
	
	@Dependency
	private static ResourceResolver resourceResolver;
	
	@Dependency
	private static ViewResolver viewResolver;

	private static final Pattern PATTERN = Pattern.compile("\\.[a-z0-9]+$");

	@Override
	public void init() {
		initializeMvcConfiguration();
		injectDependencies();
	}
	
	protected void initializeMvcConfiguration() {
		Map<String, String> initParameters = new HashMap<>();
		Enumeration<String> enumeration = this.getInitParameterNames();
		
		while (enumeration.hasMoreElements()) {
			String name = enumeration.nextElement();
			String value = this.getInitParameter(name);
			
			initParameters.put(name, value);
		}
		
		MVCConfiguration.load(initParameters);
	}
	
	protected void injectDependencies() {
		DependencyInjection.injectDependencies(this);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	@Override
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	@Override
	protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doSubmit(request, response);
	}

	protected void doSubmit(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpServletBean httpServletBean = new HttpServletBean(request, response);

		doSubmit(httpServletBean);
	}

	protected void doSubmit(HttpServletBean httpServletBean) throws IOException {
		String servletPath = httpServletBean.getServletPath();
		Resolver resolver = getResolver(servletPath);
		Response response = resolver.resolve(httpServletBean);

		response = responseService.transform(response);
		sendOutput(httpServletBean, response);
	}
	
	protected Resolver getResolver(String path) {
		Resolver resolver;
		Matcher matcher = PATTERN.matcher(path);
		
		if (matcher.find()) {
			resolver = resourceResolver;
		} else {
			resolver = viewResolver;
		}
		
		return resolver;
	}

	protected void sendOutput(HttpServletBean httpServletBean, Response response) throws IOException {
		try (ServletOutputStream servletOutputStream = httpServletBean.getOutputStream()) {
			int statusCode = response.getStatus();
			Object entity = response.getEntity();
			String entityString;

			if (statusCode == 0) {
				statusCode = HttpServletResponse.SC_OK;
			}

			httpServletBean.setStatus(statusCode);
			entity = Replace.with(Constants.Strings.EMPTY).should(entity).beNull();
			entityString = entity.toString();

			if (statusCode == HttpServletResponse.SC_OK) {
				httpServletBean.setContentType(Strings.toString(response.getMediaType()));
				servletOutputStream.print(entityString);
			}
		}
	}

}
