package org.coco.topaz.web.resolver.impl;

import org.coco.topaz.web.ServletPath;

import java.util.List;

import static org.coco.topaz.util.Constants.Strings.LEFT_BRACE;

public class URLUtils {

	private URLUtils() {

	}

	public static Match match(String url1, String url2) {
		Match match = new Match();
		ServletPath servletPath1 = new ServletPath(url1);
		ServletPath servletPath2 = new ServletPath(url2);
		List<String> parts1 = servletPath1.getParts();
		List<String> parts2 = servletPath2.getParts();
		int passes = Math.min(parts1.size(), parts2.size());
		double total = parts1.size();
		double count = 0;

		for (int i = 0; i < passes; i++) {
			String thisPart = parts1.get(i);
			String thatPart = parts2.get(i);

			if (thisPart.equals(thatPart)) {
				count++;
			}

			if (thisPart.startsWith(LEFT_BRACE) || thatPart.startsWith(LEFT_BRACE)) {
				count += 0.5;
			}
		}

		match.setAccuracy(count / total);
		return match;
	}

}
