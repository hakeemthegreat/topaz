package org.coco.topaz.web;

import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.Strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MVCConfiguration {

	private static final String BASE_PACKAGE = "org.coco.topaz.mvc.config.property.packages";
	private static final String CONTROLLER_PACKAGE = "org.coco.topaz.mvc.config.property.controllers";
	private static final String WEB_CONTENT = "org.coco.topaz.mvc.config.property.web.content";
	private static final String VIEW_CONTENT = "org.coco.topaz.mvc.config.property.view.content";
	private static final String VIEW_EXTENSION = "org.coco.topaz.mvc.config.property.view.extension";
	private static final Map<String, String> MAP = new HashMap<>();

	private MVCConfiguration() {

	}
	
	public static void load(Map<String, String> initParameters) {
		Ensure.notNull(initParameters);
		
		for (Entry<String, String> entry : initParameters.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}
	
	protected static void put(String key, String value) {
		if (Strings.isEmpty(key)) {
			throw new IllegalArgumentException();
		}
		
		if (Strings.isEmpty(value)) {
			throw new IllegalArgumentException();
		}
		
		MAP.put(key, value);
	}

	protected static String get(String key) {
		return MAP.get(key);
	}

	public static void setBasePackage(String value) {
		put(BASE_PACKAGE, value);
	}

	public static String getBasePackage() {
		return get(BASE_PACKAGE);
	}

	public static void setControllerPackage(String value) {
		put(CONTROLLER_PACKAGE, value);
	}

	public static String getControllerPackage() {
		return get(CONTROLLER_PACKAGE);
	}

	public static void setWebContent(String value) {
		put(WEB_CONTENT, value);
	}

	public static String getWebContent() {
		return get(WEB_CONTENT);
	}

	public static void setViewContent(String value) {
		put(VIEW_CONTENT, value);
	}

	public static String getViewContent() {
		return get(VIEW_CONTENT);
	}

	public static void setViewExtension(String value) {
		put(VIEW_EXTENSION, value);
	}

	public static String getViewExtension() {
		return get(VIEW_EXTENSION);
	}

}
