package org.coco.topaz.web;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;

public class ModelAndView {

	@Getter
	private View view = new View();

	@Getter
	private Map<String, Object> objects = new HashMap<>();

	public void setView(View view) {
		this.view = Objects.requireNonNull(view);
	}

	public void setObjects(Map<String, Object> objects) {
		this.objects = Objects.requireNonNull(objects);
	}

	public void addObject(String name, Object value) {
		objects.put(name, value);
	}

	public void setViewName(String viewName) {
		view.setName(viewName);
	}

	public String getViewName() {
		return view.getName();
	}

}
