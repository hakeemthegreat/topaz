package org.coco.topaz.web.resolver.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.core.Response;

import org.coco.topaz.web.MimeType;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.resolver.ResourceResolver;
import org.coco.topaz.util.Resources;
import org.coco.topaz.util.Streams;
import org.coco.topaz.web.util.WebContentUtils;
import org.coco.topaz.util.Constants;

public class ResourceResolverImpl implements ResourceResolver {
	
	@Override
	public Response resolve(HttpServletBean httpServletBean) {
		String servletPath = httpServletBean.getServletPath();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String fileLocation;
		Response response;
		Object entity;
		String mediaType;
		
		try {
			fileLocation = WebContentUtils.getLocation(servletPath);
			inputStream = Streams.newFileInputStream(fileLocation);
			outputStream = Streams.newByteArrayOutputStream();
			Streams.copy(inputStream, outputStream);
			mediaType = getMediaType(servletPath);
			entity = outputStream.toString();
			response = Response.ok(entity, mediaType).build();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			Resources.close(inputStream, outputStream);
		}
		
		return response;
	}
	
	protected String getMediaType(String path) {
		int beginIndex = path.lastIndexOf(Constants.Strings.PERIOD) + 1;
		String extension = path.substring(beginIndex);
		return MimeType.get(extension);
	}

}
