package org.coco.topaz.web.resolver;

import java.util.List;

public interface ControllerResolver {
	
	List<Class<?>> resolve(HttpServletBean httpServletBean);
	
}
