package org.coco.topaz.web.resolver;

import javax.ws.rs.core.Response;

public interface Resolver {

	Response resolve(HttpServletBean httpServletBean);

}
