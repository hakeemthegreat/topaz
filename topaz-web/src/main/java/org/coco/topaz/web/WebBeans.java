package org.coco.topaz.web;

import org.coco.topaz.injection.Beans;
import org.coco.topaz.injection.TopazBeans;
import org.coco.topaz.util.service.*;
import org.coco.topaz.web.resolver.*;
import org.coco.topaz.web.resolver.impl.*;
import org.coco.topaz.web.service.ResponseService;
import org.coco.topaz.web.service.ResponseServiceImpl;

public class WebBeans {

	private static final Beans BEANS = Beans.getInstance();

	private WebBeans() {

	}

	public static void init() {
		TopazBeans.init();
		BEANS.newBean(ArgumentResolver.class, ArgumentResolverImpl.class);
		BEANS.newBean(CacheService.class, SimpleCacheService.class);
		BEANS.newBean(CloneService.class, SerializingCloneService.class);
		BEANS.newBean(ControllerResolver.class, ControllerResolverImpl.class);
		BEANS.newBean(HashService.class, SimpleHashService.class);
		BEANS.newBean(JSONService.class, JSONServiceImpl.class);
		BEANS.newBean(MethodResolver.class, MethodResolverImpl.class);
		BEANS.newBean(ResourceResolver.class, ResourceResolverImpl.class);
		BEANS.newBean(ResponseService.class, ResponseServiceImpl.class);
		BEANS.newBean(ViewResolver.class, ViewResolverImpl.class);
	}

}
