package org.coco.topaz.web.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import javax.ws.rs.core.Response;

import org.coco.topaz.injection.Dependency;

import org.coco.topaz.util.*;
import org.coco.topaz.util.service.JSONService;
import org.coco.topaz.web.MVCConfiguration;
import org.coco.topaz.web.ModelAndView;
import org.coco.topaz.web.View;
import org.coco.topaz.web.util.WebContentUtils;

public class ResponseServiceImpl implements ResponseService {
	
	private static final String HEADER = "header.jsp";
	private static final String FOOTER = "footer.jsp";

	private final JSONService jsonService;

	@Dependency
	public ResponseServiceImpl(JSONService jsonService) {
		this.jsonService = Objects.requireNonNull(jsonService);
	}
	
	public Response transform(Response response) {
		Object entity;

		response = toResponse(response);
		entity = response.getEntity();

		if (entity instanceof ModelAndView) {
			entity = processModelAndView((ModelAndView) entity);
		} else if (entity instanceof String) {
			entity = processString((String) entity);
		} else {
			entity = processDefault(entity);
		}

		return toResponse(entity, Strings.toString(response.getMediaType()), response.getStatus());
	}

	protected String processModelAndView(ModelAndView modelAndView) {
		View view = modelAndView.getView();
		OutputStream outputStream = Streams.newByteArrayOutputStream();
		String entity;
		/*Map<String, Object> objects = modelAndView.getObjects();

		for (Entry<String, Object> entry : objects.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
		}*/

		try {
			load(HEADER, outputStream);
			load(view.getName() + MVCConfiguration.getViewExtension(), outputStream);
			load(FOOTER, outputStream);
			entity = outputStream.toString();
		} finally {
			Resources.close(outputStream);
		}

		return entity;
	}

	protected String processString(String string) {
		return string;
	}

	protected String processDefault(Object object) {
		return jsonService.toJSON(object);
	}

	protected void load(String fileName, OutputStream outputStream) {
		InputStream inputStream = null;

		try {
			String fileLocation = WebContentUtils.getLocation(MVCConfiguration.getViewContent() + fileName);

			inputStream = Streams.newFileInputStream(fileLocation);
			Streams.copy(inputStream, outputStream);
		} catch(IOException e) {
			throw new RuntimeException("Could not load " + fileName, e);
		} finally {
			Resources.close(inputStream);
		}
	}

	protected Response toResponse(Response response) {
		Object entity = response.getEntity();
		String mediaType = Strings.toString(response.getMediaType());
		int status = response.getStatus();
		return toResponse(entity, mediaType, status);
	}

	protected Response toResponse(Object entity, String mediaType, int status) {
		Response response;
		
		if (entity instanceof Response) {
			response = (Response) entity;
		} else {
			response = Response.status(status).type(mediaType).entity(entity).build();
		}
		
		return response;
	}

}
