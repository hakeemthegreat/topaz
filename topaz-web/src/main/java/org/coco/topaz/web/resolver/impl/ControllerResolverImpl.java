package org.coco.topaz.web.resolver.impl;

import java.util.ArrayList;
import java.util.List;

import org.coco.topaz.web.exception.UnresolvedMappingException;
import org.coco.topaz.web.MVCConfiguration;
import org.coco.topaz.web.resolver.ControllerResolver;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.Packages;

import javax.ws.rs.Path;

import static org.coco.topaz.util.Constants.Strings.FORWARD_SLASH;

public class ControllerResolverImpl implements ControllerResolver {
	
	@Override
	public List<Class<?>> resolve(HttpServletBean httpServletBean) {
		String servletPath = httpServletBean.getServletPath();
		List<Class<?>> matches = new ArrayList<>();
		List<Class<?>> all = getControllerClasses(MVCConfiguration.getControllerPackage());
		
		for (Class<?> clazz : all) {
			Path path = clazz.getAnnotation(Path.class);
			String pathValue;

			Ensure.notNull(path);
			pathValue = path.value();

			if (servletPath.startsWith(FORWARD_SLASH + pathValue)) {
				matches.add(clazz);
			}
		}

		if (matches.isEmpty()) {
			throw new UnresolvedMappingException(servletPath);
		}

		return matches;
	}
	
	protected List<Class<?>> getControllerClasses(String controllerPackage) {
		List<Class<?>> classes = Packages.getClasses(controllerPackage);

		classes.removeIf(clazz -> clazz.getAnnotation(Path.class) == null);
		return classes;
	}

}
