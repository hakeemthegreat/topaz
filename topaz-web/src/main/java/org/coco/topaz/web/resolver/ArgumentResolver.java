package org.coco.topaz.web.resolver;

import java.lang.reflect.Method;

public interface ArgumentResolver {
	
	Object[] resolve(HttpServletBean httpServletBean, Method method);
	
}
