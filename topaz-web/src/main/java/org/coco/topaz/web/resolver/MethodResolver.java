package org.coco.topaz.web.resolver;

import java.lang.reflect.Method;
import java.util.Collection;

public interface MethodResolver {
	
	Method resolve(HttpServletBean httpServletBean, Collection<Class<?>> classes);
	
}
