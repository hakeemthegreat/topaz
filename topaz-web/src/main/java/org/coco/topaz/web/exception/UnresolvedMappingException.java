package org.coco.topaz.web.exception;

import org.coco.topaz.exception.TopazException;

public class UnresolvedMappingException extends TopazException {

	public UnresolvedMappingException(String message) {
		super(message);
	}

}
