package org.coco.topaz.web.resolver.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.coco.topaz.web.exception.UnresolvedArgumentException;
import org.coco.topaz.web.ServletPath;
import org.coco.topaz.web.resolver.ArgumentResolver;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.Ensure;
import org.coco.topaz.web.util.PathUtils;

import static org.coco.topaz.util.Constants.Strings.*;

public class ArgumentResolverImpl implements ArgumentResolver {

	private final ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public Object[] resolve(HttpServletBean httpServletBean, Method method) {
		Parameter[] parameters = method.getParameters();
		Object[] arguments = new Object[parameters.length];

		for (int i = 0; i < parameters.length ; i++) {
			Parameter parameter = parameters[i];
			Class<?> parameterType = parameter.getType();

			if (parameterType == HttpServletRequest.class) {
				arguments[i] = httpServletBean.getRequest();
			} else if (parameterType == HttpServletResponse.class) {
				arguments[i] = httpServletBean.getResponse();
			} else if (ClassUtils.isNativeJavaClass(parameterType)) {
				arguments[i] = resolveBasicClass(httpServletBean, method, parameter);
			} else {
				throw newUnresolvedArgumentException(parameter, method);
			}
		}

		return arguments;
	}

	protected Object resolveBasicClass(HttpServletBean httpServletBean, Method method, Parameter parameter) {
		Map<String, String> map = buildPathParamMap(method, httpServletBean);
		List<Annotation> annotations = Arrays.asList(parameter.getAnnotations());
		String value;

		if (annotations.stream().anyMatch(a -> a instanceof QueryParam)) {
			QueryParam requestParam = parameter.getAnnotation(QueryParam.class);
			String name = requestParam.value();

			value = httpServletBean.getParameter(name);
		} else if (annotations.stream().anyMatch(a -> a instanceof PathParam)) {
			PathParam pathVariable = parameter.getAnnotation(PathParam.class);
			String key = pathVariable.value();

			value = map.get(key);
		} else {
			throw newUnresolvedArgumentException(parameter, method);
		}

		return objectMapper.convertValue(value, parameter.getType());
	}
	
	protected UnresolvedArgumentException newUnresolvedArgumentException(Parameter parameter, Method method) {
		List<String> parameterTypes = Arrays.stream(method.getParameterTypes()).map(Class::getName).collect(Collectors.toList());
		return new UnresolvedArgumentException(String.format("%s in method %s.%s(%s)", parameter.getName(), method.getDeclaringClass().getName(), method.getName(), String.join(", ", parameterTypes)));
	}
	
	
	protected Map<String, String> buildPathParamMap(Method method, HttpServletBean httpServletBean) {
		String controllerPath = PathUtils.getPathValue(method.getDeclaringClass());
		String fullPath = PathUtils.getPathValue(method);
		
		if (controllerPath != null) {
			fullPath = controllerPath + FORWARD_SLASH + fullPath;
		}

		return buildPathParamMap(httpServletBean.getServletPath(), fullPath);
	}

	protected Map<String, String> buildPathParamMap(String servletPath, String controllerPath) {
		Map<String, String> map = new HashMap<>();
		List<String> requestParts = new ServletPath(servletPath).getParts();
		List<String> controllerParts = new ServletPath(controllerPath).getParts();
		Ensure.areEqual(requestParts.size(), controllerParts.size());

		for (int i = 0; i < controllerParts.size(); i++) {
			String controllerPart = controllerParts.get(i);

			if (controllerPart.startsWith(LEFT_BRACE) && controllerPart.endsWith(RIGHT_BRACE)) {
				int length = controllerPart.length();
				String key = controllerPart.substring(1, length - 1);
				String value = requestParts.get(i);

				map.put(key, value);
			}
		}


		return map;
	}


}
