package org.coco.topaz.web.resolver.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

import javax.ws.rs.*;

import org.coco.topaz.web.exception.UnsupportedRequestMethodException;
import org.coco.topaz.util.reflection.Methods;
import org.coco.topaz.web.resolver.HttpServletBean;
import org.coco.topaz.web.resolver.MethodResolver;
import org.coco.topaz.web.util.PathUtils;

public class MethodResolverImpl implements MethodResolver {

	private static final List<Class<? extends Annotation>> ALL_REST_METHODS = Arrays.asList(GET.class, POST.class, DELETE.class, HEAD.class, OPTIONS.class, PUT.class, PATCH.class);
	private static final Set<String> DEFAULT_SUPPORTED_REST_METHODS = Collections.singleton(GET.class.getSimpleName());

	@Override
	public Method resolve(HttpServletBean httpServletBean, Collection<Class<?>> classes) {
		Method result;
		String requestMethod = httpServletBean.getMethod();
		Match bestMatch;
		List<Match> matches = new ArrayList<>();

		for (Class<?> clazz : classes) {
			List<Method> methods = getMethods(clazz);

			for (Method method : methods) {
				String requestURL = httpServletBean.getServletPath();
				String resourceURL = PathUtils.getURL(method);
				Match match = URLUtils.match(requestURL, resourceURL);

				match.setTarget(method);
				matches.add(match);
			}
		}

		matches.sort(Comparator.comparing(Match::getAccuracy).reversed());
		bestMatch = matches.stream().findFirst().orElse(new Match());
		result = (Method) bestMatch.getTarget();
		validateRequestMethod(requestMethod, result);

		return result;
	}

	protected void validateRequestMethod(String requestMethod, Method method) {
		if (method != null) {
			List<String> resourceMethods = getSupportedRestMethods(method);

			if (!resourceMethods.contains(requestMethod)) {
				throw new UnsupportedRequestMethodException(requestMethod + " not supported");
			}
		}
	}

	protected List<Method> getMethods(Class<?> clazz) {
		List<Method> allMethods = Methods.getMethods(clazz);
		List<Method> annotatedMethods = new ArrayList<>();

		for (Method method : allMethods) {
			if (method.getAnnotation(Path.class) != null) {
				annotatedMethods.add(method);
			}
		}

		return annotatedMethods;
	}

	protected List<String> getSupportedRestMethods(Method method) {
		List<String> resourceMethods = new ArrayList<>(DEFAULT_SUPPORTED_REST_METHODS);

		for (Class<? extends Annotation> clazz : ALL_REST_METHODS) {
			Annotation annotation = method.getAnnotation(clazz);

			if (annotation != null) {
				resourceMethods.add(clazz.getSimpleName());
			}
		}

		return resourceMethods;
	}

}
