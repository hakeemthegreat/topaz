package org.coco.topaz.web.exception;

import org.coco.topaz.exception.TopazException;

public class UnsupportedRequestMethodException extends TopazException {

	public UnsupportedRequestMethodException(String message) {
		super(message);
	}

}
