package org.coco.topaz.web.service;

import javax.ws.rs.core.Response;

public interface ResponseService {
	
	Response transform(Response response);

}
