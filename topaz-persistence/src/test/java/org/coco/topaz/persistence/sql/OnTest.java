package org.coco.topaz.persistence.sql;

import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OnTest {

	@Tested
	private String leftColumn = "LEFT_COLUMN";

	@Tested
	private String rightColumn = "RIGHT_COLUMN";

	@Tested
	private On instance;

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("ON LEFT_COLUMN = RIGHT_COLUMN", result);
	}

}
