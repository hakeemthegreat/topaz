package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplexConditionTest {

	@Injectable
	private Condition left;

	@Tested
	private Operation operation = Operation.OR;

	@Injectable
	private Condition right;

	@Tested
	private ComplexCondition instance;

	@Test
	public void toString_Test() {
		new Expectations() {{
			left.toString(); result = "dummy = 1";
			right.toString(); result = "dummy2 = 2";
		}};

		String result = instance.toString();
		assertEquals("(dummy = 1 OR dummy2 = 2)", result);
	}

}
