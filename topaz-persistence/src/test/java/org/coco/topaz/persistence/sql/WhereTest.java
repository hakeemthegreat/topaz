package org.coco.topaz.persistence.sql;

import mockit.Mocked;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WhereTest {

	@Injectable
	private Condition condition;

	@Tested
	private Where instance;

	@Test
	public void simpleCondition_Constructor_Test() {
		Long id = 1L;

		instance = new Where("dummy", Operation.GREATER_THAN, id);

		Condition result = instance.getCondition();
		assertTrue(result instanceof SimpleCondition);
		assertEquals("dummy", result.getLeft());
		assertEquals(Operation.GREATER_THAN, result.getOperation());
		assertEquals(id.toString(), result.getRight());
	}

	@Test
	public void condition_Getter_Setter_Test(@Mocked Condition condition) {
		instance.setCondition(condition);
		assertEquals(condition, instance.getCondition());
	}

	@Test
	public void toString_Test() {
		new Expectations() {{
			condition.toString(); result = "dummy";
		}};

		String result = instance.toString();
		assertEquals("WHERE dummy", result);
	}

}
