package org.coco.topaz.persistence.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class SQLUtilsTest {

	private enum State {
		READY
	}

	private static final String UUID_SOURCE = "ef5c9ae1-a003-4227-bab1-9cd6a8f716e5";
	private static final UUID DUMMY_UUID = UUID.fromString(UUID_SOURCE);
	private static final List<String> COLLECTION = Arrays.asList("dummy", "dummy2", "dummy3");

	@Test
	public void toLiteral_Null_Test() {
		assertEquals("NULL", SQLUtils.toLiteral(null));
	}

	@Test
	public void toLiteral_byte_Test() {
		assertEquals("x'7f'", SQLUtils.toLiteral((byte) 127));
	}

	@Test(expected = IllegalArgumentException.class)
	public void toLiteral_Unsupported_Array_Type_Test() {
		int[] array = {-128, -1, 0, 1, 127};
		SQLUtils.toLiteral(array);
	}

	@Test
	public void toLiteral_byte_Array_Test() {
		byte[] array = {-128, -1, 0, 1, 127};
		assertEquals("x'80ff00017f'", SQLUtils.toLiteral(array));
	}

	@Test
	public void toLiteral_Byte_Array_Test() {
		Byte[] array = {-128, -1, 0, 1, 127};
		assertEquals("x'80ff00017f'", SQLUtils.toLiteral(array));
	}

	@Test
	public void toLiteral_String_Test() {
		assertEquals("'dummy'", SQLUtils.toLiteral("dummy"));
	}

	@Test
	public void toLiteral_String_With_Single_Quote_Test() {
		String expected = "'John''s code has it''s issues'";
		String input = "John's code has it's issues";

		String result = SQLUtils.toLiteral(input);
		assertEquals(expected, result);
	}

	@Test
	public void toLiteral_Char_Test() {
		assertEquals("'a'", SQLUtils.toLiteral('a'));
	}

	@Test
	public void toLiteral_UUID_Test() {
		assertEquals("'" + UUID_SOURCE + "'", SQLUtils.toLiteral(DUMMY_UUID));
	}

	@Test
	public void toLiteral_Enum_Test() {
		assertEquals("'READY'", SQLUtils.toLiteral(State.READY));
	}

	@Test
	public void toLiteral_Long_Test() {
		assertEquals("1", SQLUtils.toLiteral(1L));
	}

	@Test
	public void toLiteral_Boolean_Test() {
		assertEquals("true", SQLUtils.toLiteral(true));
	}

	@Test
	public void toLiteral_Date_Test() {
		assertEquals("1234567890", SQLUtils.toLiteral(new Date(1234567890)));
	}

	@Test
	public void toLiteral_Iterable_Test() {
		assertEquals("('dummy', 'dummy2', 'dummy3')", SQLUtils.toLiteral(COLLECTION));
	}

	@Test
	public void toLiteral_char_Array_Test() {
		char[] array = {'a', 'b', 'c', 'd', 'e'};
		assertEquals("'abcde'", SQLUtils.toLiteral(array));
	}

	@Test
	public void toLiteral_Char_Array_Test() {
		Character[] array = {'a', 'b', 'c', 'd', 'e'};
		assertEquals("'abcde'", SQLUtils.toLiteral(array));
	}

	@Test(expected = IllegalArgumentException.class)
	public void toLiteral_IllegalArgumentException_Test() {
		SQLUtils.toLiteral(Dummy.OBJECT);
	}

}
