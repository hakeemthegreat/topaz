package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Mocked;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeleteTest {

	private final Delete instance = new Delete(SCHEMA + "." + TABLE);

	private static final String SCHEMA = "schema";
	private static final String TABLE = "table";
	private static final String WHERE = "WHERE status = 1";

	@Test
	public void where_Getter_Setter_Test(@Mocked Where where) {
		instance.setWhere(where);
		assertEquals(where, instance.getWhere());
	}

	@Test
	public void toString_Test(@Mocked Where where) {
		instance.setWhere(where);

		new Expectations() {{
			where.toString(); result = WHERE;
		}};

		String result = instance.toString();
		assertEquals("DELETE FROM \"schema\".\"table\" WHERE status = 1", result);
	}

	@Test
	public void toString_Test() {
		instance.setWhere(null);

		String result = instance.toString();
		assertEquals("DELETE FROM \"schema\".\"table\"", result);
	}

}
