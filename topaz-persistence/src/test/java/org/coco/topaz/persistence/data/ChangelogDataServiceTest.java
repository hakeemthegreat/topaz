package org.coco.topaz.persistence.data;

import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.persistence.dao.ChangelogDAO;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.util.test.Dummy;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class ChangelogDataServiceTest {

	@Injectable
	private ChangelogDAO dataAccessObject;

	@Tested
	private ChangelogDataService instance;

	private static final ChangelogEntry CHANGELOG_ENTRY = new ChangelogEntry();

	@Test
	public void execute_Test() throws Exception {
		instance.execute(Dummy.STRING, Dummy.STRING2);

		new Verifications() {{
			dataAccessObject.execute(Dummy.STRING); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void execute_SQLException_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.execute(Dummy.STRING); result = new SQLException();
		}};

		instance.execute(Dummy.STRING, Dummy.STRING2);
	}

	@Test
	public void findByName_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findByName(Dummy.STRING); result = CHANGELOG_ENTRY;
		}};

		ChangelogEntry result = instance.findByName(Dummy.STRING);
		assertEquals(CHANGELOG_ENTRY, result);
	}

	@Test(expected = ServiceException.class)
	public void findByName_SQLException_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findByName(Dummy.STRING); result = new SQLException();
		}};

		instance.findByName(Dummy.STRING);
	}

}
