package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InnerJoinTest {

	@Tested
	private String schema = "schema";

	@Tested
	private String table = "table";

	@Injectable
	private On on;

	@Tested
	private InnerJoin instance;

	@Test
	public void toString_Test() {
		new Expectations() {{
			on.toString(); result = "ON column1 = column2";
		}};

		String result = instance.toString();
		assertEquals("INNER JOIN \"schema\".\"table\" ON column1 = column2", result);
	}

}
