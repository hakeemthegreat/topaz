package org.coco.topaz.persistence.service;

import lombok.Getter;
import lombok.Setter;
import mockit.Tested;
import org.coco.topaz.util.test.Dummy;
import mockit.Expectations;
import mockit.Mocked;
import org.junit.Test;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.*;
import java.util.*;

import static org.coco.topaz.util.Constants.Strings.ID;
import static org.coco.topaz.util.Constants.Strings.NAME;
import static org.junit.Assert.*;

public class ResultSetServiceImplTest {

	@Getter
	@Setter
	@Table(name = "EMPLOYEE")
	private static class Employee {

		@Id
		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;

	}

	private static final Integer COLUMN_INDEX = -1;
	private static final String COLUMN_NAME = "column_name";

	@Tested
	private ResultSetServiceImpl instance;

	@Test
	public void getObject_With_Column_Index_Test(@Mocked ResultSet resultSet) throws Exception {
		new Expectations(ResultSetServiceImpl.class, instance) {{
			instance.getObject(resultSet, COLUMN_INDEX, Object.class); result = Dummy.OBJECT;
		}};

		Object result = instance.getObject(resultSet, COLUMN_INDEX, Object.class);
		assertEquals(Dummy.OBJECT, result);
	}

	@Test
	public void getObject_With_Column_Name_Test(@Mocked ResultSet resultSet) throws Exception {
		new Expectations(ResultSetServiceImpl.class, instance) {{
			instance.getObject(resultSet, COLUMN_NAME, Object.class); result = Dummy.OBJECT;
		}};

		Object result = instance.getObject(resultSet, COLUMN_NAME, Object.class);
		assertEquals(Dummy.OBJECT, result);
	}

	@Test
	public void populate_Test(@Mocked ResultSet resultSet) throws Exception {
		Employee employee = new Employee();
		Collection<String> columns = new ArrayList<>();

		columns.add(ID);
		columns.add(NAME);

		new Expectations(ResultSetServiceImpl.class, instance) {{
			ResultSetServiceImpl.getColumnNames(resultSet); result = columns;
			instance.getObject(resultSet, ID, Long.class); result = Dummy.LONG;
			instance.getObject(resultSet, NAME, String.class); result = Dummy.STRING;
		}};

		instance.populate(resultSet, employee);
		assertEquals(Dummy.LONG, employee.getId());
		assertEquals(Dummy.STRING, employee.getName());
	}


	@Test
	public void populate_Fields_Not_Returned_In_Query_Test(@Mocked ResultSet resultSet) throws Exception {
		Employee employee = new Employee();
		Collection<String> columns = Collections.emptyList();

		new Expectations(ResultSetServiceImpl.class, instance) {{
			ResultSetServiceImpl.getColumnNames(resultSet); result = columns;
			instance.getObject((ResultSet) any, anyString, (Class<?>) any); times = 0;
		}};

		instance.populate(resultSet, employee);
		assertNull(employee.getId());
		assertNull(employee.getName());
	}

	@Test
	public void getColumnNames_Test(@Mocked ResultSet resultSet, @Mocked ResultSetMetaData resultSetMetaData) throws Exception {
		new Expectations() {{
			resultSet.getMetaData(); result = resultSetMetaData;
			resultSetMetaData.getColumnCount(); result = 3;
			resultSetMetaData.getColumnName(1); result = Dummy.STRING;
			resultSetMetaData.getColumnName(2); result = Dummy.STRING2;
			resultSetMetaData.getColumnName(3); result = Dummy.STRING3;
		}};

		Collection<String> results = ResultSetServiceImpl.getColumnNames(resultSet);
		assertEquals(3, results.size());
		assertTrue(results.contains(Dummy.STRING.toUpperCase()));
		assertTrue(results.contains(Dummy.STRING2.toUpperCase()));
		assertTrue(results.contains(Dummy.STRING3.toUpperCase()));
	}

	@Test
	public void getColumnNames_Null_Test() throws Exception {
		Collection<String> results = ResultSetServiceImpl.getColumnNames(null);
		assertEquals(0, results.size());
	}

	@Test
	public void convertValue_Test() {
		Long expected = 0L;
		String value = expected.toString();

		Long result = instance.convertValue(value, Long.class);
		assertEquals(expected, result);
	}

	@Test
	public void convertValue_IllegalArgumentException_Test() {
		String value = "0";

		UUID result = instance.convertValue(value, UUID.class);
		assertNull(result);
	}

}
