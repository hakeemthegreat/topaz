package org.coco.topaz.persistence.service;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.persistence.sql.*;
import org.coco.topaz.persistence.util.Persistence;
import mockit.Tested;
import org.junit.Test;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SQLServiceImplTest {

	@Getter
	@Setter
	@Table(schema = "TOPAZ", name = "EMPLOYEE")
	private static class Employee {

		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;

		@Column(name = "ROWID", insertable = false, updatable = false)
		private Long rowId;

	}

	@Tested
	private SQLServiceImpl instance;

	private static final Employee EMPLOYEE = new Employee();
	private static final Class<?> CLASS = EMPLOYEE.getClass();
	private static final String SCHEMA = Persistence.getSchema(CLASS);
	private static final String TABLE = Persistence.getTable(CLASS);
	private static final String JOIN_TABLE = "TRANSACTION";
	private static final Operation OPERATION = Operation.EQUALS;
	private static final String INSERT_STATEMENT = String.format("INSERT INTO \"%s\".\"%s\"(\"ID\", \"NAME\") VALUES(1, 'dummy')", SCHEMA, TABLE);
	private static final String SELECT_STATEMENT = String.format("SELECT \"%s\".\"%s\".\"ID\", \"%s\".\"%s\".\"NAME\", \"%s\".\"%s\".\"ROWID\" FROM \"%s\".\"%s\"", SCHEMA, TABLE, SCHEMA, TABLE, SCHEMA, TABLE, SCHEMA, TABLE);
	private static final String UPDATE_STATEMENT = String.format("UPDATE \"%s\".\"%s\" SET \"ID\" = 1, \"NAME\" = 'dummy'", SCHEMA, TABLE);
	private static final String DELETE_STATEMENT = String.format("DELETE FROM \"%s\".\"%s\"", SCHEMA, TABLE);
	private static final String EXTRA_CLAUSE = "LIMIT 1";
	private static final On ON = new On("ID", "EMPLOYEE_ID");
	private static final Join JOIN = new Join(SCHEMA, JOIN_TABLE, ON);
	private static final Where WHERE = new Where("NAME", OPERATION, "Joe");
	private static final OrderBy ORDER_BY = new OrderBy("TYPE", false);
	private static final List<Join> JOINS = Collections.singletonList(JOIN);

	static {
		EMPLOYEE.setId(1L);
		EMPLOYEE.setName("dummy");
	}

	@Test
	public void insert_Test() {
		String result = instance.insert(EMPLOYEE);
		assertEquals(INSERT_STATEMENT, result);
	}

	@Test
	public void selectAll_Test() {
		String result = instance.select(CLASS);
		assertEquals(SELECT_STATEMENT, result);
	}

	@Test
	public void selectJoin_Test() {
		String result = instance.select(CLASS, JOINS);
		assertEquals(SELECT_STATEMENT + " " + JOIN, result);
	}

	@Test
	public void selectWhere_Test() {
		String result = instance.select(CLASS, WHERE);
		assertEquals(SELECT_STATEMENT + " " + WHERE, result);
	}

	@Test
	public void selectOrderBy_Test() {
		String result = instance.select(CLASS, ORDER_BY);
		assertEquals(SELECT_STATEMENT + " " + ORDER_BY, result);
	}

	@Test
	public void selectExtra_Test() {
		String result = instance.select(CLASS, EXTRA_CLAUSE);
		assertEquals(SELECT_STATEMENT + " " + EXTRA_CLAUSE, result);
	}

	@Test
	public void selectJoinWhere_Test() {
		String result = instance.select(CLASS, JOINS, WHERE);
		assertEquals(SELECT_STATEMENT + String.format(" %s %s", JOIN, WHERE), result);
	}

	@Test
	public void selectJoinWhereOrderBy_Test() {
		List<Join> JOINS = Collections.singletonList(JOIN);

		String result = instance.select(CLASS, JOINS, WHERE, ORDER_BY);
		assertEquals(SELECT_STATEMENT + String.format(" %s %s %s", JOIN, WHERE, ORDER_BY), result);
	}

	@Test
	public void selectJoinWhereOrderByExtra_Test() {
		List<Join> JOINS = Collections.singletonList(JOIN);

		String result = instance.select(CLASS, JOINS, WHERE, ORDER_BY, EXTRA_CLAUSE);
		assertEquals(SELECT_STATEMENT + String.format(" %s %s %s %s", JOIN, WHERE, ORDER_BY, EXTRA_CLAUSE), result);
	}

	@Test
	public void updateAll_Test() {
		String result = instance.update(EMPLOYEE);
		assertEquals(UPDATE_STATEMENT, result);
	}

	@Test
	public void updateWhere_Test() {
		String result = instance.update(EMPLOYEE, WHERE);
		assertEquals(UPDATE_STATEMENT + " " + WHERE, result);
	}

	@Test
	public void deleteAll_Test() {
		String result = instance.delete(CLASS);
		assertEquals(DELETE_STATEMENT, result);
	}

	@Test
	public void deleteWhere_Test() {
		String result = instance.delete(CLASS, WHERE);
		assertEquals(DELETE_STATEMENT + " " + WHERE, result);
	}

}
