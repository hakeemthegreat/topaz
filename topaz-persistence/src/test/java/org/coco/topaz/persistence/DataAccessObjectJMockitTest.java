package org.coco.topaz.persistence;

import static org.coco.topaz.persistence.sql.Operation.EQUALS;
import static org.coco.topaz.persistence.sql.Operation.IN;
import static org.coco.topaz.util.Constants.Strings.ID;
import static org.coco.topaz.util.Constants.Strings.QUERY;
import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

import mockit.*;
import org.coco.topaz.persistence.service.ResultSetService;
import org.coco.topaz.persistence.service.ResultSetServiceImpl;
import org.coco.topaz.persistence.service.SQLService;
import org.coco.topaz.persistence.sql.Condition;
import org.coco.topaz.persistence.sql.Join;
import org.coco.topaz.persistence.sql.Where;
import org.coco.topaz.persistence.sql.OrderBy;
import org.coco.topaz.persistence.util.Persistence;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.*;

public class DataAccessObjectJMockitTest {

	@Getter
	@Setter
	@Table(name = "EMPLOYEE")
	private static class Employee {

		@Id
		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;
	}

	@Mocked
	private SQLService sqlService;

	@Mocked
	private ResultSetService resultSetService;

	private static final Class<Employee> CLASS = Employee.class;

	private final DataAccessObject<Employee, Long> instance = new DataAccessObject<>(CLASS);

	private static final Employee EMPLOYEE = new Employee();
	private static final Employee EMPLOYEE2 = new Employee();
	private static final Employee EMPLOYEE3 = new Employee();
	private static final List<Employee> EMPLOYEE_LIST = Arrays.asList(EMPLOYEE, EMPLOYEE2, EMPLOYEE3);

	@Before
	public void before() {
		EMPLOYEE.setId(1L);
		EMPLOYEE.setName("dummy");

		instance.sqlService = sqlService;
		instance.resultSetService = resultSetService;
	}

	@Test
	public void execute_Test(@Mocked Connection connection, @Mocked PreparedStatement preparedStatement, @Mocked ResultSet resultSet, @Mocked ResultSet generatedKeys) throws Exception {
		new Expectations(Driver.class, instance) {{
			Driver.getConnection(); result = connection;
			connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS); result = preparedStatement;
			preparedStatement.execute(); result = true;
			preparedStatement.getResultSet(); result = resultSet;
			preparedStatement.getGeneratedKeys(); result = generatedKeys;
			instance.populateList((List<Employee>) any, (ResultSet) any, (ResultSet) any); times = 1;
		}};

		List<Employee> results = instance.execute(QUERY);
		assertNotNull(results);
		assertEquals(0, results.size());

		new Verifications() {{
			List<Employee> list;
			instance.populateList(list = withCapture(), resultSet, generatedKeys); times = 1;
			assertNotNull(list);
			assertEquals(0, list.size());
		}};
	}
	
	@Test
	public void populateList_ResultSet_Test(@Mocked ResultSet resultSet) throws Exception {
		List<Employee> employees = new ArrayList<>();
		Employee employee = EMPLOYEE;

		new Expectations(Reflection.class, ResultSetServiceImpl.class) {{
			resultSet.next(); returns(true, false);
			Reflection.newInstance((Class<?>) any); result = employee;
		}};
		
		instance.populateList(employees, resultSet, null);
		assertEquals(1, employees.size());
		assertEquals(employee, employees.get(0));

		new Verifications() {{
			Reflection.newInstance(Employee.class); times = 1;
			resultSetService.populate(resultSet, employee); times = 1;
		}};
	}

	@Test
	public void populateList_GeneratedKeys_Test(@Mocked ResultSet generatedKeys) throws Exception {
		List<Employee> employees = new ArrayList<>();
		Employee employee = EMPLOYEE;
		Object generatedKey = new Object();
		Field idField = Employee.class.getDeclaredField("id");

		new Expectations(Reflection.class, ResultSetServiceImpl.class, Fields.class) {{
			generatedKeys.next(); returns(true, false);
			Reflection.newInstance((Class<?>) any); result = employee;
			resultSetService.getObject(generatedKeys, anyInt, (Class<?>) any); result = generatedKey;
			Fields.set(any, (Field) any, any); times = 1;
		}};

		instance.populateList(employees, null, generatedKeys);
		assertEquals(1, employees.size());
		assertEquals(employee, employees.get(0));

		new Verifications() {{
			Reflection.newInstance(Employee.class); times = 1;
			resultSetService.getObject(generatedKeys, 1, Long.class); times = 1;
			Fields.set(employee, idField, generatedKey); times = 1;
		}};
	}

	@Test
	public void populateList_Generated_Key_Is_Null_Test(@Mocked ResultSet generatedKeys) throws Exception {
		List<Employee> employees = new ArrayList<>();
		Object generatedKey = null;

		new Expectations(Reflection.class, ResultSetServiceImpl.class, Fields.class) {{
			generatedKeys.next(); returns(true, false);
			resultSetService.getObject(generatedKeys, anyInt, (Class<?>) any); result = generatedKey;
		}};

		instance.populateList(employees, null, generatedKeys);
		assertEquals(0, employees.size());

		new Verifications() {{
			resultSetService.getObject(generatedKeys, 1, Long.class); times = 1;
		}};
	}

	@Test
	public void populateList_Null_ResultSet_And_Null_GeneratedKeys_Test() throws Exception {
		List<Employee> employees = new ArrayList<>();

		instance.populateList(employees, null, null);
		assertEquals(0, employees.size());
	}

	@Test
	public void findOne_Test() throws SQLException {
		List<Employee> list = Collections.singletonList(EMPLOYEE);
		Long id = 1L;
		
		new Expectations(instance) {{
			instance.find((Where) any); result = list;
		}};

		Employee result = instance.findOne(id);
		assertTrue(Objects.deepEquals(EMPLOYEE, result));

		new Verifications() {{
			Where where;
			instance.find(where = withCapture()); times = 1;
			assertNotNull(where);

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals(id.toString(), condition.getRight());
		}};
	}

	@Test
	public void findById_Test() throws SQLException {
		Collection<Long> ids = Arrays.asList(1L, 2L, 3L);

		new Expectations(instance) {{
			instance.find((Where) any); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.findById(ids);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			Where where;
			instance.find(where = withCapture()); times = 1;
			assertNotNull(where);

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(IN, condition.getOperation());
			assertEquals("(1, 2, 3)", condition.getRight());
		}};
	}

	@Test
	public void findJoin_Test(@Mocked Join join) throws SQLException {
		Collection<Join> joins = Collections.singleton(join);

		new Expectations(instance) {{
			sqlService.select(CLASS, joins); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(joins);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findWhere_Test(@Mocked Where where) throws SQLException {
		new Expectations(instance) {{
			sqlService.select(CLASS, where); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(where);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findOrderBy_Test(@Mocked OrderBy orderBy) throws SQLException {
		new Expectations(instance) {{
			sqlService.select(CLASS, orderBy); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(orderBy);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findExtra_Test() throws SQLException {
		new Expectations(instance) {{
			sqlService.select(CLASS, "dummy"); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find("dummy");
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findJoinWhere_Test(@Mocked Join join, @Mocked Where where) throws SQLException {
		Collection<Join> joins = Collections.singleton(join);

		new Expectations(instance) {{
			sqlService.select(CLASS, joins, where); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(joins, where);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findJoinWhereOrderBy_Test(@Mocked Join join, @Mocked Where where, @Mocked OrderBy orderBy) throws SQLException {
		Collection<Join> joins = Collections.singleton(join);

		new Expectations(instance) {{
			sqlService.select(CLASS, joins, where, orderBy); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(joins, where, orderBy);
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findJoinWhereOrderByExtra_Test(@Mocked Join join, @Mocked Where where, @Mocked OrderBy orderBy) throws SQLException {
		Collection<Join> joins = Collections.singleton(join);

		new Expectations(instance) {{
			sqlService.select(CLASS, joins, where, orderBy, "dummy"); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.find(joins, where, orderBy, "dummy");
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void findAll_Test() throws SQLException {
		new Expectations(instance) {{
			sqlService.select(CLASS); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		List<Employee> results = instance.findAll();
		assertEquals(EMPLOYEE_LIST, results);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void insert_Test() throws SQLException {
		new Expectations(instance) {{
			sqlService.insert(EMPLOYEE); result = QUERY;
			instance.execute(anyString); result = Collections.emptyList();
		}};

		instance.insert(EMPLOYEE);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void insert_Collection_Test() throws SQLException {
		new Expectations(instance) {{
			instance.insert(EMPLOYEE); //do nothing
			instance.insert(EMPLOYEE2); //do nothing
			instance.insert(EMPLOYEE3); //do nothing
		}};

		instance.insert(EMPLOYEE_LIST);
	}

	@Test
	public void insert_Generated_Key_Test() throws SQLException {
		Employee in = new Employee();
		Employee out = new Employee();

		out.setId(1L);

		new Expectations(instance) {{
			sqlService.insert(in); result = QUERY;
			instance.execute(anyString); result = Collections.singletonList(out);
		}};

		instance.insert(in);
		assertEquals(out.getId(), in.getId());

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test(expected = IllegalStateException.class)
	public void insert_IllegalStateException_Test() throws SQLException {
		new Expectations(instance) {{
			sqlService.insert(EMPLOYEE); result = QUERY;
			instance.execute(anyString); result = EMPLOYEE_LIST;
		}};

		instance.insert(EMPLOYEE);
	}

	@Test
	public void delete_Test() throws SQLException {
		new Expectations(instance) {{
			sqlService.delete(CLASS, (Where) any); result = QUERY;
			instance.execute(anyString); //do nothing
		}};

		instance.delete(EMPLOYEE);

		new Verifications() {{
			Where where;
			sqlService.delete(CLASS, where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals("1", condition.getRight());

			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void delete_Collection_Test() throws SQLException {
		new Expectations(instance) {{
			instance.delete(EMPLOYEE); //do nothing
			instance.delete(EMPLOYEE2); //do nothing
			instance.delete(EMPLOYEE3); //do nothing
		}};

		instance.delete(EMPLOYEE_LIST);
	}

	@Test
	public void deleteOne_Test() throws SQLException {
		Long id = 1L;

		new Expectations(instance) {{
			sqlService.delete(CLASS, (Where) any); result = QUERY;
			instance.execute(anyString); //do nothing
		}};

		instance.deleteOne(id);

		new Verifications() {{
			Where where;
			sqlService.delete(CLASS, where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals(id.toString(), condition.getRight());

			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void deleteById_Test() throws SQLException {
		Collection<Long> ids = Arrays.asList(1L, 2L, 3L);

		new Expectations(instance) {{
			instance.deleteOne(anyLong); times = 3; //do nothing
		}};

		instance.deleteById(ids);

		new Verifications() {{
			instance.deleteOne(1L); times = 1;
			instance.deleteOne(2L); times = 1;
			instance.deleteOne(3L); times = 1;
		}};
	}

	@Test
	public void deleteAll_Test() throws SQLException {
		String sql = "dummy";

		new Expectations(instance) {{
			sqlService.delete((Class<?>) any); result = sql;
			instance.execute(anyString); times = 1;
		}};

		instance.deleteAll();

		new Verifications() {{
			sqlService.delete(CLASS); times = 1;
			instance.execute(sql); times = 1;
		}};
	}

	@Test
	public void ping_True_Test() throws Exception {
		new Expectations(instance) {{
			instance.execute(anyString); //do nothing
		}};

		assertTrue(instance.ping());

		new Verifications() {{
			instance.execute(instance.pingQuery); times = 1;
		}};
	}

	@Test
	public void ping_False_Test() throws Exception {
		new Expectations(instance) {{
			instance.execute(anyString); result = new SQLException();
		}};

		assertFalse(instance.ping());

		new Verifications() {{
			instance.execute(instance.pingQuery); times = 1;
		}};
	}

	@Test
	public void update_Test() throws Exception {
		new Expectations(instance) {{
			sqlService.update(EMPLOYEE, (Where) any); result = QUERY;
			instance.execute(anyString); //do nothing
		}};

		instance.update(EMPLOYEE);

		new Verifications() {{
			Where where;
			sqlService.update(EMPLOYEE, where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals("1", condition.getRight());

			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void update_Collection_Test() throws SQLException {
		new Expectations(instance) {{
			instance.update(EMPLOYEE); //do nothing
			instance.update(EMPLOYEE2); //do nothing
			instance.update(EMPLOYEE3); //do nothing
		}};

		instance.update(EMPLOYEE_LIST);
	}

	@Test
	public void executeUnique_Test() throws Exception {
		List<Employee> list = Collections.singletonList(EMPLOYEE);

		new Expectations(instance) {{
			instance.execute(anyString); result = list;
		}};

		Employee result = instance.executeUnique(QUERY);
		assertEquals(EMPLOYEE, result);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void executeUnique_Empty_List_Test() throws Exception {
		List<Employee> list = Collections.emptyList();

		new Expectations(instance) {{
			instance.execute(anyString); result = list;
		}};

		Employee result = instance.executeUnique(QUERY);
		assertNull(result);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test(expected = NonUniqueResultException.class)
	public void executeUnique_NonUniqueResultException_Test() throws Exception {
		List<Employee> list = EMPLOYEE_LIST;

		new Expectations(instance) {{
			instance.execute(anyString); result = list;
		}};

		instance.executeUnique(QUERY);

		new Verifications() {{
			instance.execute(QUERY); times = 1;
		}};
	}

	@Test
	public void exists_True_Test() throws Exception {
		Long id = 1L;

		new Expectations(instance) {{
			instance.find((Where) any); result = EMPLOYEE_LIST;
		}};

		boolean result = instance.exists(id);
		assertTrue(result);

		new Verifications() {{
			Where where;
			instance.find(where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals(id.toString(), condition.getRight());
		}};
	}

	@Test
	public void exists_False_Test() throws Exception {
		Long id = 1L;

		new Expectations(instance) {{
			instance.find((Where) any); result = Collections.emptyList();
		}};

		boolean result = instance.exists(id);
		assertFalse(result);

		new Verifications() {{
			Where where;
			instance.find(where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(ID, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals(id.toString(), condition.getRight());
		}};
	}

	@Test
	public void save_Null_Id_Test() throws SQLException {
		Employee entity = new Employee();

		entity.setId(null);

		new Expectations(instance) {{
			instance.exists(anyLong); times = 0;
			instance.insert(entity); //do nothing
		}};

		instance.save(entity);
	}

	@Test
	public void save_Id_Already_Exists_Test() throws SQLException {
		Employee entity = new Employee();
		Long id = 1L;

		entity.setId(id);

		new Expectations(instance) {{
			instance.exists(anyLong); result = true;
			instance.update((Employee) any); //do nothing
		}};

		instance.save(entity);

		new Verifications() {{
			instance.exists(id); times = 1;
			instance.update(entity); times = 1;
		}};
	}

	@Test
	public void save_Id_Already_Does_Not_Exist_Test() throws SQLException {
		Employee entity = new Employee();
		Long id = 1L;

		entity.setId(id);

		new Expectations(instance) {{
			instance.exists(anyLong); result = false;
			instance.insert((Employee) any); //do nothing
		}};

		instance.save(entity);

		new Verifications() {{
			instance.exists(id); times = 1;
			instance.insert(entity); times = 1;
		}};
	}

	@Test
	public void save_Collection_Test() throws SQLException {
		new Expectations(instance) {{
			instance.save(EMPLOYEE); //do nothing
			instance.save(EMPLOYEE2); //do nothing
			instance.save(EMPLOYEE3); //do nothing
		}};

		instance.save(EMPLOYEE_LIST);
	}

	@Test
	public void buildPingQuery_Test() {
		Class<?> clazz = Object.class;

		new Expectations(Persistence.class) {{
			Persistence.getQualifiedTableName(clazz); result = "SCHEMA.TABLE";
		}};

		String result = instance.buildPingQuery(clazz);
		assertEquals("SELECT 1 FROM \"SCHEMA\".\"TABLE\" WHERE 1 = 0", result);
	}

	@Test
	public void buildPingQuery_No_Schema_Test() {
		Class<?> clazz = Object.class;

		new Expectations(Persistence.class) {{
			Persistence.getQualifiedTableName(clazz); result = "TABLE";
		}};

		String result = instance.buildPingQuery(clazz);
		assertEquals("SELECT 1 FROM \"TABLE\" WHERE 1 = 0", result);
	}

	@Test
	public void ensureUniqueResult_Non_Empty_Collection_Test() {
		Collection<Object> collection = Collections.singleton(1);
		Long id = 1L;
		
		instance.ensureNonEmptyResult(collection, id);
	}

	@Test(expected = EntityNotFoundException.class)
	public void ensureUniqueResult_Empty_Collection_Test() {
		Collection<Object> collection = Collections.emptyList();
		Long id = 1L;
		
		instance.ensureNonEmptyResult(collection, id);
	}

	@Test
	public void ensureUniqueResult_One_Result_Test() {
		Collection<Object> collection = Collections.singleton(1);
		Long id = 1L;
		
		instance.ensureUniqueResult(collection, id);
	}

	@Test(expected = NonUniqueResultException.class)
	public void ensureUniqueResult_More_Than_One_Result_Test() {
		Collection<Object> collection = Arrays.asList(1, 2, 3);
		Long id = 1L;
		
		instance.ensureUniqueResult(collection, id);
	}
	
}
