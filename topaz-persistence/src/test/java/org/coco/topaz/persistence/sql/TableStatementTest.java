package org.coco.topaz.persistence.sql;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TableStatementTest {

	private final TableStatement instance = new Select(Dummy.STRING, Dummy.STRING2);

	private static final String SCHEMA = "schema";
	private static final String TABLE = "table";
	private static final String BLANK = "\t \r\n";

	@Test
	public void schema_Getter_Setter_Test() {
		instance.setSchema(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getSchema());
	}

	@Test
	public void table_Getter_Setter_Test() {
		instance.setTable(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getTable());
	}

	@Test
	public void getQualifiedTableName_Test() {
		instance.setSchema("schema");
		instance.setTable("table");

		String result = instance.getQualifiedTableName();
		assertEquals("\"schema\".\"table\"", result);
	}

	@Test
	public void getQualifiedTableName_Null_Schema_Test() {
		instance.setSchema(null);
		instance.setTable("table");

		String result = instance.getQualifiedTableName();
		assertEquals("\"table\"", result);
	}

	@Test
	public void getQualifiedTableName_Schema_Already_Prepended_To_Table_Test() {
		TableStatement instance = new Select(SCHEMA + "." + TABLE);

		String result = instance.getQualifiedTableName();
		assertEquals("\"schema\".\"table\"", result);
	}

	@Test
	public void setTable_Test() {
		instance.setTable(TABLE);
		assertEquals(TABLE, instance.getTable());
	}

	@Test(expected = IllegalArgumentException.class)
	public void setTable_Blank_Test() {
		instance.setTable(BLANK);
	}

	@Test
	public void setSchema_Test() {
		instance.setSchema(SCHEMA);
		assertEquals(SCHEMA, instance.getSchema());
	}

	@Test
	public void setSchema_Null_Test() {
		instance.setSchema(null);
		assertNull(instance.getSchema());
	}

	@Test
	public void setSchema_Blank_Test() {
		instance.setSchema(BLANK);
		assertEquals(BLANK, instance.getSchema());
	}

}
