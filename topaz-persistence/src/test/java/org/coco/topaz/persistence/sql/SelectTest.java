package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Mocked;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class SelectTest {

	private final Select instance = new Select(SCHEMA + "." + TABLE);

	private static final String SCHEMA = "schema";
	private static final String TABLE = "table";
	private static final String SELECT = "SELECT \"schema\".\"table\".\"dummy\", \"schema\".\"table\".\"dummy2\", \"schema\".\"table\".\"dummy3\" FROM \"schema\".\"table\"";
	private static final String JOIN = "JOIN other_table ON column_a = column_b";
	private static final String WHERE = "WHERE status = 1";
	private static final String ORDER_BY = "ORDER BY id DESC";
	private static final String EXTRA = "LIMIT 1";

	@Test
	public void getJoins_Test(@Mocked Join join) {
		instance.addJoin(join);

		Collection<Join> result = instance.getJoins();
		assertNotNull(result);
		assertEquals(1, result.size());
		assertTrue(result.contains(join));
	}

	@Test
	public void where_Getter_Setter_Test(@Mocked Where where) {
		instance.setWhere(where);
		assertEquals(where, instance.getWhere());
	}

	@Test
	public void orderBy_Getter_Setter_Test(@Mocked OrderBy orderBy) {
		instance.setOrderBy(orderBy);
		assertEquals(orderBy, instance.getOrderBy());
	}

	@Test
	public void extra_Getter_Setter_Test() {
		instance.setExtra(EXTRA);
		assertEquals(EXTRA, instance.getExtra());
	}

	@Test
	public void toString_Test(@Mocked Join join, @Mocked Where where, @Mocked OrderBy orderBy) {
		instance.getColumns().addAll(Arrays.asList("dummy", "dummy2", "dummy3"));
		instance.addJoin(join);
		instance.setWhere(where);
		instance.setOrderBy(orderBy);
		instance.setExtra(EXTRA);

		new Expectations() {{
			join.toString(); result = JOIN;
			where.toString(); result = WHERE;
			orderBy.toString(); result = ORDER_BY;
		}};

		String result = instance.toString();
		assertEquals(SELECT + " " + JOIN + " " + WHERE + " " + ORDER_BY + " " + EXTRA, result);
	}

	@Test
	public void toString_No_Join_Where_OrderBy_Or_Extra_Clause_Test() {
		instance.getColumns().addAll(Arrays.asList("dummy", "dummy2", "dummy3"));
		instance.getJoins().clear();
		instance.setWhere(null);
		instance.setOrderBy(null);
		instance.setExtra(null);

		String result = instance.toString();
		assertEquals(SELECT, result);
	}

}
