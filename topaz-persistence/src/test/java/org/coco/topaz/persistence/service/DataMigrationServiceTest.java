package org.coco.topaz.persistence.service;

import org.coco.topaz.environment.Environment;
import org.coco.topaz.environment.TopazEnvironment;
import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.persistence.PersistenceBeans;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.util.BooleanWrapper;
import org.coco.topaz.util.FileUtils;
import org.coco.topaz.util.FileWrapper;
import org.coco.topaz.util.Streams;
import org.coco.topaz.util.service.HashService;
import org.coco.topaz.util.test.JavaTest;
import org.coco.topaz.util.test.MockInputStream;
import mockit.*;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;

import static org.coco.topaz.persistence.service.DataMigrationService.CREATE_CHANGELOG_FILE;
import static org.coco.topaz.util.Constants.Strings.*;
import static org.junit.Assert.*;

public class DataMigrationServiceTest extends JavaTest {

	@Injectable
	private ChangelogService changelogService;

	@Injectable
	private HashService hashService;

	@Tested
	private DataMigrationService instance;

	private static final String SQL = "bad sql statement";

	static {
		PersistenceBeans.init();
	}

	@Test
	public void start_Test() throws Exception {
		new Expectations(instance) {{
			instance.createChangelog(); //do nothing
			instance.executeSQLFiles(); //do nothing
		}};

		instance.start();
	}

	@Test
	public void start_IOException_Test() throws Exception {
		IOException ioException = new IOException();

		new Expectations(instance) {{
			instance.createChangelog(); result = ioException;
		}};

		expect(ServiceException.class, "Could not start DataMigrationService", ioException);

		instance.start();
	}

	@Test
	public void execute_Test() throws Exception {
		File root = new File("dummy");

		new Expectations(instance) {{
			instance.getRootDirectory(); result = "dummy";
			instance.executeDirectory(root); //do nothing
		}};

		instance.executeSQLFiles();
	}

	@Test
	public void getRootDirectory_Test() {
		new Expectations(TopazEnvironment.class) {{
			TopazEnvironment.getDatabaseSQL(); result = "dummy";
		}};

		String result = instance.getRootDirectory();
		assertEquals("dummy", result);
	}

	@Test
	public void getRootDirectory_RuntimeException_Test() {
		new Expectations(TopazEnvironment.class) {{
			TopazEnvironment.getDatabaseSQL(); result = null;
		}};

		expect(RuntimeException.class, "Make sure you set database.sql in your properties file!");

		instance.getRootDirectory();
	}

	@Test
	public void executeSQL_One_Statement_Test() throws ServiceException {
		String statement = "dummy";
		String fileName = "dummy4";

		new Expectations(TopazEnvironment.class, instance) {{
			TopazEnvironment.getDatabaseDelimiter(); result = EMPTY;
			instance.print(anyString); //do nothing
		}};

		instance.executeSQL(statement, fileName);

		new Verifications() {{
			instance.print("Executing... "); times = 1;
			instance.print(fileName); times = 1;
			instance.print(NEWLINE); times = 1;
			changelogService.execute(statement, fileName); times = 1;
		}};
	}

	@Test
	@SuppressWarnings("StringOperationCanBeSimplified")
	public void executeSQL_Multiple_Statements_Test() throws ServiceException {
		String statement1 = "dummy";
		String statement2 = "dummy2";
		String statement3 = "dummy3";
		String statement4 = "\r\n";
		String statement5 = new String(statement4);
		Collection<String> statements = Arrays.asList(statement1, statement2, statement3, statement4, statement5);
		String allStatements = String.join(SEMICOLON, statements);
		String fileName = "dummy4";

		new Expectations(TopazEnvironment.class, instance) {{
			TopazEnvironment.getDatabaseDelimiter(); result = SEMICOLON;
			instance.print(anyString); //do nothing
		}};

		instance.executeSQL(allStatements, fileName);

		new Verifications() {{
			instance.print("Executing... "); times = 1;
			instance.print(fileName); times = 1;
			instance.print(NEWLINE); times = 1;
			changelogService.execute(statement1, fileName); times = 1;
			changelogService.execute(statement2, fileName); times = 1;
			changelogService.execute(statement3, fileName); times = 1;
			changelogService.execute(statement4, fileName); times = 0;
			changelogService.execute(statement5, fileName); times = 0;
		}};
	}

	@Test
	public void executeDirectory_Test() throws Exception {
		String rootName = "rootName";
		String fileName = "fileName";
		String directoryName = "directoryName";
		File root = temporaryFolder.newFolder(rootName);
		File file = temporaryFolder.newFile(fileName);
		File directory = temporaryFolder.newFolder(directoryName);
		FileWrapper fileWrapper = new FileWrapper();
		String hashCode = "hashcode";

		fileWrapper.addFile(file);
		fileWrapper.addDirectory(directory);

		new Expectations(FileUtils.class, instance) {{
			FileUtils.buildFileWrapper(root, (FilenameFilter) any); result = fileWrapper;
			changelogService.findByName(fileName); result = null;
			FileUtils.readAll(file); result = SQL;
			hashService.hashToHex(anyString); result = hashCode;
			instance.executeSQL(SQL, fileName); //do nothing
			changelogService.saveChangelogEntry(fileName, hashCode); //do nothing
			instance.executeDirectory(directory); //do nothing
		}};

		instance.executeDirectory(root);
	}

	@Test
	public void executeDirectory_Matching_Hash_Codes_Test() throws Exception {
		File root = temporaryFolder.newFolder("root");
		File file = temporaryFolder.newFile("file");
		File directory = temporaryFolder.newFolder("directory");
		FileWrapper fileWrapper = new FileWrapper();
		ChangelogEntry changelogEntry = new ChangelogEntry();

		fileWrapper.addFile(file);
		fileWrapper.addDirectory(directory);
		changelogEntry.setHashCode("sameHashCode");

		new Expectations(FileUtils.class) {{
			FileUtils.buildFileWrapper(root, (FilenameFilter) any); result = fileWrapper;
			changelogService.findByName("file"); result = changelogEntry;
			FileUtils.readAll(file); result = "dummy";
			hashService.hashToHex("dummy"); result = "sameHashCode";
		}};

		instance.executeDirectory(root);
	}

	@Test
	public void executeDirectory_Mismatching_Hash_Codes_Test() throws Exception {
		File root = temporaryFolder.newFolder("root");
		File file = temporaryFolder.newFile("file");
		File directory = temporaryFolder.newFolder("directory");
		FileWrapper fileWrapper = new FileWrapper();
		ChangelogEntry changelogEntry = new ChangelogEntry();

		fileWrapper.addFile(file);
		fileWrapper.addDirectory(directory);
		changelogEntry.setHashCode("aHashCode");

		new Expectations(FileUtils.class) {{
			FileUtils.buildFileWrapper(root, (FilenameFilter) any); result = fileWrapper;
			changelogService.findByName("file"); result = changelogEntry;
			FileUtils.readAll(file); result = "dummy";
			hashService.hashToHex("dummy"); result = "anotherHashCode";
		}};

		expect(ServiceException.class, "Hash code does not match what is in database for 'file'");

		instance.executeDirectory(root);
	}

	@Test
	public void createChangelog_Ping_Unsuccessful_Test() throws Exception {
		new Expectations(instance, FileUtils.class) {{
			changelogService.ping(); result = false;
			instance.executeResource(anyString); //do nothing
		}};

		instance.createChangelog();

		new Verifications() {{
			instance.executeResource(CREATE_CHANGELOG_FILE); times = 1;
		}};
	}

	@Test
	public void createChangelog_Ping_Successful_Test() throws Exception {
		new Expectations(instance) {{
			changelogService.ping(); result = true;
			instance.executeResource(anyString); times = 0;
		}};

		instance.createChangelog();
	}

	@Test
	public void executeResource_Test() throws Exception {
		InputStream inputStream = new MockInputStream();
		ClassLoader classLoader = Environment.class.getClassLoader();
		String resource = "resource";
		String sql = "sql";

		new Expectations(Environment.class, instance, Streams.class) {{
			Environment.getResourceAsStream(anyString, (ClassLoader) any); result = inputStream;
			Streams.readAll((Reader) any); result = sql;
			instance.executeSQL(anyString, anyString); //do nothing
		}};

		instance.executeResource(resource);

		new Verifications() {{
			Environment.getResourceAsStream(resource, classLoader); times = 1;

			Reader reader;
			Streams.readAll(reader = withCapture()); times = 1;
			assertTrue(reader instanceof InputStreamReader);

			instance.executeSQL(sql, resource); times = 1;
		}};
	}

	@Test
	public void printStream_Getter_Setter_Test() {
		instance.setPrintStream(System.out);
		assertEquals(System.out, instance.getPrintStream());
	}

	@Test
	public void print_Test(@Mocked PrintStream printStream) {
		instance.setPrintStream(printStream);

		instance.print("dummy");

		new Verifications() {{
			printStream.print("dummy"); times = 1;
		}};
	}

	@Test
	public void print_Null_PrintStream_Test(@Mocked PrintStream printStream) {
		instance.setPrintStream(null);

		instance.print("dummy");

		new Verifications() {{
			printStream.print(anyString); times = 0;
		}};
	}

	@Test
	public void main_Test() {
		BooleanWrapper booleanWrapper = new BooleanWrapper(false);

		new MockUp<DataMigrationService>() {
			@Mock
			public void start() {
				booleanWrapper.setValue(true);
			}
		};

		DataMigrationService.main(null);
		assertEquals(Boolean.TRUE, booleanWrapper.getValue());
	}

}
