package org.coco.topaz.persistence;

import lombok.Getter;
import lombok.Setter;
import mockit.Deencapsulation;
import org.coco.topaz.environment.Environment;
import org.coco.topaz.injection.DependencyInjection;
import org.coco.topaz.persistence.service.ResultSetService;
import org.coco.topaz.persistence.service.SQLService;
import org.coco.topaz.persistence.util.Persistence;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.*;
import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

public class DataAccessObjectIntegrationTest {

	private enum EmployeeType {
		FULL_TIME,
		TEMPORARY
	}

	@Getter
	@Setter
	@Table(name = "TEMP_TABLE_69827954759435872")
	public static class Employee {

		public Employee() {

		}

		@Id
		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;

		@Column(name = "TYPE")
		private EmployeeType type;

		@Column(name = "CODE")
		private UUID code;

		@Column(name = "DATA")
		private byte[] data;

		@Override
		public boolean equals(Object object) {
			boolean result = false;

			if (object instanceof Employee) {
				result = Objects.equals(id, ((Employee) object).id);
			}

			return result;
		}

	}

	private final SQLService sqlService = DependencyInjection.newInstance(SQLService.class);
	private final ResultSetService resultSetService = DependencyInjection.newInstance(ResultSetService.class);

	private final DataAccessObject<Employee, Long> instance = new DataAccessObject<>(Employee.class);

	private static final String TABLE_NAME = Persistence.getTable(Employee.class);
	private static final String CREATE_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME VARCHAR, TYPE VARCHAR, CODE VARCHAR, DATA BLOB)", TABLE_NAME);
	private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

	private static final Employee EMPLOYEE = new Employee();
	private static final Employee EMPLOYEE2 = new Employee();
	private static final Employee EMPLOYEE3 = new Employee();
	private static final List<Employee> EMPLOYEE_LIST = Arrays.asList(EMPLOYEE, EMPLOYEE2, EMPLOYEE3);

	private static final Long ID = 1L;
	private static final Long ID2 = 2L;
	private static final Long ID3 = 3L;

	private static final String NAME = "dummy";
	private static final String NAME2 = "dummy2's";
	private static final String NAME3 = null;

	private static final EmployeeType TYPE = EmployeeType.FULL_TIME;
	private static final EmployeeType TYPE2 = EmployeeType.TEMPORARY;
	private static final EmployeeType TYPE3 = null;

	private static final UUID CODE = UUID.randomUUID();
	private static final UUID CODE2 = UUID.randomUUID();
	private static final UUID CODE3 = null;

	private static final byte[] DATA = {-128, -127, -126, -125, -124};
	private static final byte[] DATA2 = {0, 1, 2, 3, 4};
	private static final byte[] DATA3 = null;

	static {
		Properties properties = Deencapsulation.getField(Environment.class, "PROPERTIES");
		File databaseLocation = new File(System.getProperty("java.io.tmpdir"), "topaz.db");

		properties.put("topaz.database.driver", "org.sqlite.JDBC");
		properties.put("topaz.database.url", "jdbc:sqlite:" + databaseLocation.getAbsolutePath());
		PersistenceBeans.init();
	}

	static {
		EMPLOYEE.setId(ID);
		EMPLOYEE.setName(NAME);
		EMPLOYEE.setType(TYPE);
		EMPLOYEE.setCode(CODE);
		EMPLOYEE.setData(DATA);

		EMPLOYEE2.setId(ID2);
		EMPLOYEE2.setName(NAME2);
		EMPLOYEE2.setType(TYPE2);
		EMPLOYEE2.setCode(CODE2);
		EMPLOYEE2.setData(DATA2);

		EMPLOYEE3.setId(ID3);
		EMPLOYEE3.setName(NAME3);
		EMPLOYEE3.setType(TYPE3);
		EMPLOYEE3.setCode(CODE3);
		EMPLOYEE3.setData(DATA3);
	}

	@Before
	public void before() throws Exception {
		instance.sqlService = sqlService;
		instance.resultSetService = resultSetService;
		createAndPopulateTable();
	}

	@After
	public void after() throws Exception {
		dropTable();
	}

	public void createAndPopulateTable() throws Exception {
		instance.execute(CREATE_TABLE);
		instance.insert(EMPLOYEE_LIST);
	}

	public void dropTable() throws Exception {
		instance.execute(DROP_TABLE);
	}

	@Test
	public void ping_True_Test() {
		assertTrue(instance.ping());
	}

	@Test
	public void ping_False_Test() {
		instance.pingQuery = "BAD QUERY";
		assertFalse(instance.ping());
	}

	@Test
	public void insert_Auto_Generated_Key_Test() throws Exception {
		Employee entity = new Employee();
		Employee entity2 = new Employee();
		Employee entity3 = new Employee();
		Collection<Employee> collection = Arrays.asList(entity, entity2, entity3);
		Long id2 = 999L;

		entity.setId(null);
		entity2.setId(id2);
		entity3.setId(null);

		instance.insert(collection);
		assertNotNull(entity.getId());
		assertEquals(id2, entity2.getId());
		assertNotNull(entity3.getId());
	}

	@Test
	public void findAll_Test() throws Exception {
		List<Employee> results = instance.findAll();
		assertEquals(EMPLOYEE_LIST, results);
	}

	@Test
	public void findOne_Test() throws Exception {
		Employee result1 = instance.findOne(ID);
		assertNotNull(result1);
		assertEquals(ID, result1.getId());
		assertEquals(NAME, result1.getName());
		assertEquals(CODE, result1.getCode());
		assertEquals(TYPE, result1.getType());
		assertArrayEquals(DATA, result1.getData());

		Employee result2 = instance.findOne(ID2);
		assertNotNull(result2);
		assertEquals(ID2, result2.getId());
		assertEquals(NAME2, result2.getName());
		assertEquals(CODE2, result2.getCode());
		assertEquals(TYPE2, result2.getType());
		assertArrayEquals(DATA2, result2.getData());

		Employee result3 = instance.findOne(ID3);
		assertNotNull(result3);
		assertEquals(ID3, result3.getId());
		assertEquals(NAME3, result3.getName());
		assertEquals(CODE3, result3.getCode());
		assertEquals(TYPE3, result3.getType());
		assertArrayEquals(DATA3, result3.getData());
	}

	@Test(expected = EntityNotFoundException.class)
	public void findOne_EntityNotFoundException_Test() throws Exception {
		instance.findOne(-1L);
	}

	@Test
	public void delete_Test() throws Exception {
		instance.delete(EMPLOYEE);
		instance.delete(EMPLOYEE2);
		instance.delete(EMPLOYEE3);

		List<Employee> results = instance.findAll();
		assertEquals(0, results.size());
	}

	@Test
	public void delete_Collection_Test() throws Exception {
		instance.delete(EMPLOYEE_LIST);

		List<Employee> results = instance.findAll();
		assertEquals(0, results.size());
	}

	@Test
	public void deleteOne_Test() throws Exception {
		instance.deleteOne(EMPLOYEE.getId());
		instance.deleteOne(EMPLOYEE2.getId());
		instance.deleteOne(EMPLOYEE3.getId());

		List<Employee> results = instance.findAll();
		assertEquals(0, results.size());
	}

	@Test
	public void deleteById_Test() throws Exception {
		Collection<Long> ids = Arrays.asList(EMPLOYEE.getId(), EMPLOYEE2.getId(), EMPLOYEE3.getId());

		instance.deleteById(ids);

		List<Employee> results = instance.findAll();
		assertEquals(0, results.size());
	}

	@Test
	public void deleteAll_Test() throws Exception {
		instance.deleteAll();

		List<Employee> results = instance.findAll();
		assertEquals(0, results.size());
	}

	@Test
	public void update_Test() throws Exception {
		Employee entity = new Employee();

		entity.setId(ID);
		entity.setName(NAME2);

		instance.update(entity);

		Employee result = instance.findOne(ID);
		assertEquals(ID, result.getId());
		assertEquals(NAME2, result.getName());
	}

	@Test
	public void update_Collection_Test() throws Exception {
		Employee entity = new Employee();
		Collection<Employee> collection = Collections.singleton(entity);

		entity.setId(ID);
		entity.setName(NAME);

		instance.update(collection);

		Employee result = instance.findOne(ID);
		assertEquals(ID, result.getId());
		assertEquals(NAME, result.getName());
	}

}
