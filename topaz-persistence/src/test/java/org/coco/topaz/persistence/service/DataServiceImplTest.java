package org.coco.topaz.persistence.service;

import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.persistence.DataAccessObject;
import mockit.*;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DataServiceImplTest {

	@Injectable
	private DataAccessObject<Object, Long> dataAccessObject;

	@Tested
	private DataServiceImpl<Object, Long> instance;

	@Test
	public void findOne_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findOne(Dummy.LONG); result = Dummy.OBJECT;
		}};

		Object result = instance.findOne(Dummy.LONG);
		assertEquals(Dummy.OBJECT, result);
	}

	@Test
	public void findAll_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findAll(); result = Dummy.objectList();
		}};

		List<Object> results = instance.findAll();
		assertEquals(Dummy.objectList(), results);
	}

	@Test(expected = ServiceException.class)
	public void findOne_SQLException_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findOne(Dummy.LONG); result = new SQLException();
		}};

		instance.findOne(Dummy.LONG);
	}

	@Test(expected = ServiceException.class)
	public void findAll_SQLException_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.findAll(); result = new SQLException();
		}};

		instance.findAll();
	}

	@Test
	public void insert_Test() throws Exception {
		instance.insert(Dummy.OBJECT);

		new Verifications() {{
			dataAccessObject.insert(Dummy.OBJECT); times = 1;
		}};
	}

	@Test
	public void insert_Collection_Test() throws Exception {
		instance.insert(Dummy.objectList());

		new Verifications() {{
			dataAccessObject.insert(Dummy.objectList()); times = 1;
		}};
	}

	@Test
	public void delete_Test() throws Exception {
		instance.delete(Dummy.OBJECT);

		new Verifications() {{
			dataAccessObject.delete(Dummy.OBJECT); times = 1;
		}};
	}

	@Test
	public void delete_Collection_Test() throws Exception {
		instance.delete(Dummy.objectList());

		new Verifications() {{
			dataAccessObject.delete(Dummy.objectList()); times = 1;
		}};
	}

	@Test
	public void deleteOne_Test() throws Exception {
		instance.deleteOne(Dummy.LONG);

		new Verifications() {{
			dataAccessObject.deleteOne(Dummy.LONG); times = 1;
		}};
	}

	@Test
	public void deleteById_Test() throws Exception {
		Collection<Long> ids = Arrays.asList(1L, 2L, 3L);

		instance.deleteById(ids);

		new Verifications() {{
			dataAccessObject.deleteById(ids); times = 1;
		}};
	}

	@Test
	public void deleteAll_Test() throws Exception {
		instance.deleteAll();

		new Verifications() {{
			dataAccessObject.deleteAll(); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void insert_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.insert(Dummy.OBJECT); result = new SQLException();
		}};

		instance.insert(Dummy.OBJECT);
	}

	@Test(expected = ServiceException.class)
	public void insert_Collection_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.insert(Dummy.objectList()); result = new SQLException();
		}};

		instance.insert(Dummy.objectList());
	}

	@Test(expected = ServiceException.class)
	public void delete_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.delete(Dummy.LONG); result = new SQLException();
		}};

		instance.delete(Dummy.LONG);
	}

	@Test(expected = ServiceException.class)
	public void delete_Collection_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.delete(Dummy.objectList()); result = new SQLException();
		}};

		instance.delete(Dummy.objectList());
	}

	@Test(expected = ServiceException.class)
	public void deleteOne_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.deleteOne(Dummy.LONG); result = new SQLException();
		}};

		instance.deleteOne(Dummy.LONG);
	}

	@Test(expected = ServiceException.class)
	public void deleteById_Exception_Thrown_Test() throws Exception {
		Collection<Long> ids = Arrays.asList(1L, 2L, 3L);

		new Expectations() {{
			dataAccessObject.deleteById((Collection<Long>) any); result = new SQLException();
		}};

		instance.deleteById(ids);
	}

	@Test(expected = ServiceException.class)
	public void deleteAll_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.deleteAll(); result = new SQLException();
		}};

		instance.deleteAll();
	}

	@Test
	public void ping_Test() {
		new Expectations() {{
			dataAccessObject.ping(); result = true;
		}};

		assertTrue(instance.ping());
	}

	@Test
	public void update_Test() throws Exception {
		instance.update(Dummy.OBJECT);

		new Verifications() {{
			dataAccessObject.update(Dummy.OBJECT); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void update_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.update(Dummy.OBJECT); result = new SQLException();
		}};

		instance.update(Dummy.OBJECT);
	}

	@Test
	public void exists_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.exists(Dummy.LONG); result = true;
		}};

		boolean result = instance.exists(Dummy.LONG);
		assertTrue(result);
	}

	@Test(expected = ServiceException.class)
	public void exists_SQLException_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.exists(Dummy.LONG); result = new SQLException();
		}};

		instance.exists(Dummy.LONG);
	}

	@Test
	public void save_Test() throws Exception {
		instance.save(Dummy.OBJECT);

		new Verifications() {{
			dataAccessObject.save(Dummy.OBJECT); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void save_SQLException_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.save(Dummy.OBJECT); result = new SQLException();
		}};

		instance.save(Dummy.OBJECT);
	}

	@Test
	public void update_Collection_Test() throws Exception {
		instance.update(Dummy.objectList());

		new Verifications() {{
			dataAccessObject.update(Dummy.objectList()); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void update_Collection_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.update(Dummy.objectList()); result = new SQLException();
		}};

		instance.update(Dummy.objectList());
	}

	@Test
	public void save_Collection_Test() throws Exception {
		instance.save(Dummy.objectList());

		new Verifications() {{
			dataAccessObject.save(Dummy.objectList()); times = 1;
		}};
	}

	@Test(expected = ServiceException.class)
	public void save_Collection_Exception_Thrown_Test() throws Exception {
		new Expectations() {{
			dataAccessObject.save(Dummy.objectList()); result = new SQLException();
		}};

		instance.save(Dummy.objectList());
	}

	@Test
	public void findById_Test() throws Exception {
		Collection<Long> ids = Arrays.asList(Dummy.LONG, Dummy.LONG2, Dummy.LONG3);

		new Expectations() {{
			dataAccessObject.findById(ids); result = Dummy.objectList();
		}};

		List<Object> results = instance.findById(ids);
		assertEquals(Dummy.objectList(), results);
	}

	@Test(expected = ServiceException.class)
	public void findById_SQLException_Thrown_Test() throws Exception {
		Collection<Long> ids = Arrays.asList(Dummy.LONG, Dummy.LONG2, Dummy.LONG3);

		new Expectations() {{
			dataAccessObject.findById(ids); result = new SQLException();
		}};

		instance.findById(ids);
	}

}
