package org.coco.topaz.persistence.sql;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleConditionTest {

	private static final String COLUMN = "COLUMN";
	private static final String VALUE = "VALUE";
	
	private SimpleCondition instance = new SimpleCondition(COLUMN, Operation.EQUALS, VALUE);

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("\"COLUMN\" = 'VALUE'", result);
	}

	@Test
	public void toString_Equals_Null_Test() {
		instance = new SimpleCondition(COLUMN, Operation.EQUALS, null);

		String result = instance.toString();
		assertEquals("\"COLUMN\" IS NULL", result);
	}

	@Test
	public void toString_Not_Equals_Null_Test() {
		instance = new SimpleCondition(COLUMN, Operation.NOT_EQUALS, null);

		String result = instance.toString();
		assertEquals("\"COLUMN\" IS NOT NULL", result);
	}

	@Test
	public void toString_is_Null_Test() {
		instance = new SimpleCondition(COLUMN, Operation.IS, null);

		String result = instance.toString();
		assertEquals("\"COLUMN\" IS NULL", result);
	}

	@Test
	public void toString_LessThanNull_Test() {
		instance = new SimpleCondition(COLUMN, Operation.LESS_THAN, VALUE);

		String result = instance.toString();
		assertEquals("\"COLUMN\" < 'VALUE'", result);
	}

}
