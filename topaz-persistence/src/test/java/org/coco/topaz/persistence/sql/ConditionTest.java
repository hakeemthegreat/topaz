package org.coco.topaz.persistence.sql;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConditionTest {

	private final Condition instance = new Condition("dummy", Operation.GREATER_THAN, 1L) {};

	@Test
	public void left_Getter_Setter_Test() {
		assertEquals("dummy", instance.getLeft());
	}

	@Test
	public void operation_Getter_Setter_Test() {
		assertEquals(Operation.GREATER_THAN, instance.getOperation());
	}

	@Test
	public void right_Getter_Setter_Test() {
		assertEquals(1L, instance.getRight());
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("dummy > 1", result);
	}

}
