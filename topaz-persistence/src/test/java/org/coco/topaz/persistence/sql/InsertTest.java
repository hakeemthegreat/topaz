package org.coco.topaz.persistence.sql;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InsertTest {

	private final Insert instance = new Insert(SCHEMA + "." + TABLE);

	private static final String SCHEMA = "schema";
	private static final String TABLE = "table";

	@Test
	public void columns_Getter_Setter_Test() {
		Column dummyColumn = new Column(Dummy.STRING, Dummy.INT);
		Column dummyColumn2 = new Column(Dummy.STRING2, Dummy.INT2);
		Column dummyColumn3 = new Column(Dummy.STRING3, Dummy.INT3);
		List<Column> columns = Arrays.asList(dummyColumn, dummyColumn2, dummyColumn3);

		instance.setColumns(columns);
		assertEquals(columns, instance.getColumns());
	}

	@Test
	public void toString_Test() {
		Column name = new Column("NAME", "John");
		Column status = new Column("STATUS", 1);

		instance.addColumn(name);
		instance.addColumn(status);

		String result = instance.toString();
		assertEquals("INSERT INTO \"schema\".\"table\"(\"NAME\", \"STATUS\") VALUES('John', 1)", result);
	}

}
