package org.coco.topaz.persistence;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.coco.topaz.environment.TopazEnvironment;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class DriverTest extends JavaTest {

	@Test
	public void getConnection_Test(@Mocked Connection connection) throws Exception {
		String databaseUrl = "databaseUrl";

		new Expectations(Driver.class, TopazEnvironment.class, DriverManager.class) {{
			Driver.registerDriver(); //do nothing
			TopazEnvironment.getDatabaseUrl(); result = databaseUrl;
			DriverManager.getConnection(anyString); result = connection;
		}};

		Connection result = Driver.getConnection();
		assertSame(connection, result);

		new Verifications() {{
			DriverManager.getConnection(databaseUrl); times = 1;
		}};
	}

	@Test
	public void getConnection_ClassNotFoundException_Test() throws Exception {
		ClassNotFoundException classNotFoundException = new ClassNotFoundException();

		new Expectations(Driver.class) {{
			Driver.registerDriver(); result = classNotFoundException;
		}};

		expect(SQLException.class, classNotFoundException);

		Driver.getConnection();
	}

	@Test
	public void registerDriver_Test() throws Exception {
		String databaseDriver = Object.class.getName();

		new Expectations(TopazEnvironment.class) {{
			TopazEnvironment.getDatabaseDriver(); result = databaseDriver;
		}};

		Driver.registerDriver();
	}

}
