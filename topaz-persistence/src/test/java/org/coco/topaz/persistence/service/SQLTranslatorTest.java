package org.coco.topaz.persistence.service;

import org.junit.Test;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Arrays;

import static org.junit.Assert.*;

public class SQLTranslatorTest {

	@SuppressWarnings("unused")
	@Table(name = "EMPLOYEE")
	private static class Employee {

		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;

		@Column(name = "TYPE_ID")
		private Long typeId;

		private Employee parent;

	}

	private final SQLTranslator instance = new SQLTranslator(Employee.class);

	private static final String ALL_COLUMNS = "ID, NAME, TYPE_ID";
	private static final String SELECT = String.format("SELECT %s FROM EMPLOYEE", ALL_COLUMNS);
	private static final String SELECT_WHERE = SELECT + " WHERE ";

	@Test
	public void translate_Test() {
		String result = instance.translate("findWhereNameEqualsAndIdGreaterThan", "dummy", 1L);
		assertEquals(SELECT_WHERE + "NAME = 'dummy' AND ID > 1", result);
	}

	@Test
	public void translate_Test2() {
		String result = instance.translate("findWhereNameNot", "dummy");
		assertEquals(SELECT_WHERE + "NAME <> 'dummy'", result);
	}

	@Test
	public void translate_Test3() {
		String result = instance.translate("findAllWhereNameNotNull");
		assertEquals(SELECT_WHERE + "NAME IS NOT NULL", result);
	}

	@Test
	public void translate_Test4() {
		String result = instance.translate("findAllWhereIdIn", Arrays.asList(1, 2, 3));
		assertEquals(SELECT_WHERE + "ID IN (1, 2, 3)", result);
	}

	@Test
	public void translate_Test5() {
		String result = instance.translate("findAll");
		assertEquals(SELECT, result);
	}

	@Test
	public void translate_Test6() {
		try {
			instance.translate("findByBadColumn");
			fail("An RuntimeException with a cause of IllegalArgumentException was expected");
		} catch (RuntimeException e) {
			Throwable cause = e.getCause();
			assertTrue(cause instanceof IllegalArgumentException);
			assertEquals("BADCOLUMN", cause.getMessage());
		}
	}

	@Test
	public void translate_Test7() {
		String result = instance.translate("findWhereIdEqualsOne");
		assertEquals(SELECT_WHERE + "ID = 1", result);
	}

	@Test
	public void translate_Test8() {
		String result = instance.translate("findByIdEqualsAndNameIsNullAndIdEquals", 1, 2);
		assertEquals(SELECT_WHERE + "ID = 1 AND NAME IS NULL AND ID = 2", result);
	}

	@Test
	public void translate_Test9() {
		String result = instance.translate("findByTypeIdEquals", 1);
		assertEquals(SELECT_WHERE + "TYPE_ID = 1", result);
	}

}
