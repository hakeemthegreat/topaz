package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Mocked;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JoinTest {

	@Mocked
	private On on;

	private Join instance;

	private static final String SCHEMA = "schema";

	private static final String TABLE = "table";

	@Before
	public void before() {
		instance = new Join(SCHEMA + "." + TABLE, on);
	}

	@Test
	public void toString_Test() {
		new Expectations() {{
			on.toString(); result = "ON column1 = column2";
		}};

		String result = instance.toString();
		assertEquals("JOIN \"schema\".\"table\" ON column1 = column2", result);
	}

}
