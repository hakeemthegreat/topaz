package org.coco.topaz.persistence.util;

import java.lang.reflect.Field;
import java.util.*;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import mockit.Mocked;
import org.coco.topaz.util.Constants;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.test.Dummy;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.coco.topaz.util.Constants.Strings.Messages.ERROR_WHILE_EXECUTING_STATIC_BLOCK;
import static org.junit.Assert.*;

public class PersistenceTest {

	@Getter
	@Setter
	@Table(schema = "TOPAZ", name = "EMPLOYEE")
	private static class Employee {

		@Id
		@Column(name = "ID")
		private Long id;

		@Column(name = "NAME")
		private String name;
	}

	private static class SubEmployee extends Employee {

		@Column(name = "SPONSOR_ID")
		@SuppressWarnings("unused")
		private Long sponsorId;

	}
	
	private static final Employee EMPLOYEE = new Employee();
	private static final Class<?> CLASS = EMPLOYEE.getClass();
	private static final Field ID;
	private static final Field NAME;
	private static final List<Field> FIELDS = new ArrayList<>();
	
	static {
		try {
			EMPLOYEE.setId(Dummy.LONG);
			EMPLOYEE.setName(Dummy.STRING);
			ID = CLASS.getDeclaredField("id");
			NAME = CLASS.getDeclaredField("name");
			FIELDS.add(ID);
			FIELDS.add(NAME);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(ERROR_WHILE_EXECUTING_STATIC_BLOCK, e);
		}
	}

	@Test
	public void getSchema_Test() {
		String result = Persistence.getSchema(CLASS);
		assertEquals("TOPAZ", result);
	}

	@Test
	public void getSchema_Sub_Class_Test() {
		String result = Persistence.getSchema(SubEmployee.class);
		assertEquals("TOPAZ", result);
	}

	@Test
	public void getSchema_No_Table_Annotation_Test() {
		String result = Persistence.getSchema(Object.class);
		assertNull(result);
	}
	
	@Test
	public void getTable_Test() {
		String result = Persistence.getTable(CLASS);
		assertEquals("EMPLOYEE", result);
	}

	@Test
	public void getTable_Sub_Class_Test() {
		String result = Persistence.getTable(SubEmployee.class);
		assertEquals("EMPLOYEE", result);
	}

	@Test
	public void getQualifiedTableName_Test() {
		String result = Persistence.getQualifiedTableName(CLASS);
		assertEquals("TOPAZ.EMPLOYEE", result);
	}

	@Test
	public void getQualifiedTableName_Null_Table_Test() {
		String result = Persistence.getQualifiedTableName(null);
		assertNull(result);
	}

	@Test
	public void getQualifiedTableName_No_Schema_Test() {
		new Expectations(Persistence.class) {{
			Persistence.getSchema(CLASS); result = "";
		}};

		String result = Persistence.getQualifiedTableName(CLASS);
		assertEquals("EMPLOYEE", result);
	}

	@Test
	public void getTable_Null_Class_Test() {
		String result = Persistence.getTable(null);
		assertNull(result);
	}

	@Test
	public void getTable_Object_Class_Test() {
		String result = Persistence.getTable(Object.class);
		assertNull(result);
	}
	
	@Test
	public void getColumnName_Test() throws Exception {
		Field field = CLASS.getDeclaredField("name");
		String result = Persistence.getColumnName(field);
		Assert.assertEquals(Constants.Strings.NAME, result);
	}

	@Test
	public void getColumnName_Null_Test() {
		String result = Persistence.getColumnName(null);
		assertNull(result);
	}

	@Test
	public void getColumn_No_Column_Annotation_Test() throws Exception {
		Field field = Long.class.getDeclaredField("MAX_VALUE");
		String result = Persistence.getColumnName(field);
		assertNull(result);
	}
	
	@Test
	public void getIdField_Test() throws Exception {
		Field id = CLASS.getDeclaredField("id");
		Field result = Persistence.getIdField(CLASS);
		assertEquals(id, result);
	}

	@Test
	public void getIdField_No_Id_Test() {
		Field result = Persistence.getIdField(Date.class);
		assertNull(result);
	}
	
	@Test
	public void getIdValue_Test() {
		Object result = Persistence.getIdValue(EMPLOYEE);
		assertEquals(Dummy.LONG, result);
	}

	@Test
	public void getIdValue_No_Id_Test() {
		Object result = Persistence.getIdValue(Dummy.OBJECT);
		assertNull(result);
	}
	
	@Test
	public void getColumnValues_Test() {
		Map<String, Object> results = Persistence.getColumnValues(EMPLOYEE, FIELDS);
		assertEquals(2, results.size());
		assertEquals(Dummy.LONG, results.get(Constants.Strings.ID));
		assertEquals(Dummy.STRING, results.get(Constants.Strings.NAME));
	}

	@Test
	public void getColumnValues_No_Mapped_Fields_Test() {
		List<Field> fields = Fields.getFields(Date.class);

		Map<String, Object> results = Persistence.getColumnValues(Dummy.DATE, fields);
		assertEquals(0, results.size());
	}

	@Test
	public void getColumns_Test() throws Exception {
		Column id = CLASS.getDeclaredField("id").getAnnotation(Column.class);
		Column name = CLASS.getDeclaredField("name").getAnnotation(Column.class);

		List<Column> results = Persistence.getColumns(CLASS);
		assertNotNull(results);
		assertTrue(results.contains(id));
		assertTrue(results.contains(name));
	}

	@Test
	public void getColumns_No_Results_Test() {
		List<Column> results = Persistence.getColumns(Date.class);
		assertEquals(0, results.size());
	}

	@Test
	public void getInsertableFields_Test() {
		List<Field> results = Persistence.getInsertableFields(CLASS);
		assertEquals(2, results.size());
		assertTrue(results.contains(ID));
		assertTrue(results.contains(NAME));

		List<Field> results2 = Persistence.getInsertableFields(CLASS);
		assertSame(results, results2);
	}

	@Test
	public void getUpdatableFields_Test() {
		List<Field> results = Persistence.getUpdatableFields(CLASS);
		assertEquals(2, results.size());
		assertTrue(results.contains(ID));
		assertTrue(results.contains(NAME));

		List<Field> results2 = Persistence.getUpdatableFields(CLASS);
		assertSame(results, results2);
	}

	@Test
	public void getColumnNames_Test() {
		List<Field> fields = Arrays.asList(ID, NAME);

		List<String> results = Persistence.getColumnNames(fields);
		assertNotNull(results);
		assertTrue(results.contains(Constants.Strings.ID));
		assertTrue(results.contains(Constants.Strings.NAME));
	}

	@Test
	public void insertable_Test(@Mocked Column column) {
		new Expectations() {{
			column.insertable(); result = false;
		}};

		assertFalse(Persistence.insertable(null));
		assertFalse(Persistence.insertable(column));
	}

	@Test
	public void updatable_Test(@Mocked Column column) {
		new Expectations() {{
			column.updatable(); result = false;
		}};

		assertFalse(Persistence.updatable(null));
		assertFalse(Persistence.updatable(column));
	}

	@Test
	public void getSelectableFields_Test() {
		List<Field> results = Persistence.getSelectableFields(CLASS);
		assertNotNull(results);
		assertTrue(results.contains(ID));
		assertTrue(results.contains(NAME));

		List<Field> results2 = Persistence.getSelectableFields(CLASS);
		assertSame(results, results2);
	}

	@Test
	public void getSelectableFields_Fields_With_No_Column_Annotation_Test() {
		List<Field> results = Persistence.getSelectableFields(Date.class);
		assertEquals(0, results.size());
	}

}
