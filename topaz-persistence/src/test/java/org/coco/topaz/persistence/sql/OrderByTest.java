package org.coco.topaz.persistence.sql;

import org.coco.topaz.util.test.Dummy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class OrderByTest {

	private OrderBy instance;

	private static final String COLUMN = "column";

	@Before
	public void before() {
		instance = new OrderBy(COLUMN, true);
	}

	@Test
	public void column_Getter_Setter_Test() {
		instance.setColumn(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getColumn());
	}

	@Test
	public void descending_Getter_Setter_Test() {
		instance.setDescending(false);
		assertFalse(instance.isDescending());
	}

	@Test
	public void toString_Descending_Test() {
		instance.setDescending(true);

		String result = instance.toString();
		assertEquals("ORDER BY column DESC", result);
	}

	@Test
	public void toString_Ascending_Test() {
		instance.setDescending(false);

		String result = instance.toString();
		assertEquals("ORDER BY column ASC", result);
	}

}
