package org.coco.topaz.persistence.sql;

import org.junit.Test;

import static org.junit.Assert.*;

public class OperationTest {

	private static final String SYMBOL = "=";
	private final Operation instance = new Operation(SYMBOL);

	@Test
	public void getToken_Test() {
		assertEquals(SYMBOL, instance.getToken());
	}

	@Test
	public void equals_Test() {
		boolean result = instance.equals(Operation.EQUALS);
		assertTrue(result);
	}

	@Test
	public void equals_Null_Test() {
		boolean result = instance.equals(null);
		assertFalse(result);
	}

	@Test
	public void hashCode_Test() {
		int result = instance.hashCode();
		assertEquals(SYMBOL.hashCode(), result);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals(SYMBOL, result);
	}

}
