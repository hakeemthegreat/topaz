package org.coco.topaz.persistence.service;

import org.coco.topaz.persistence.data.ChangelogDataService;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.util.test.Dummy;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ChangelogServiceTest {

	@Injectable
	private ChangelogDataService changelogDataService;

	@Tested
	private ChangelogService instance;

	private static final ChangelogEntry CHANGELOG_ENTRY = new ChangelogEntry();
	private static final ChangelogEntry CHANGELOG_ENTRY2 = new ChangelogEntry();
	private static final ChangelogEntry CHANGELOG_ENTRY3 = new ChangelogEntry();
	private static final List<ChangelogEntry> CHANGELOG_ENTRIES = Arrays.asList(CHANGELOG_ENTRY, CHANGELOG_ENTRY2, CHANGELOG_ENTRY3);

	@Test
	public void ping_Test() {
		new Expectations() {{
			changelogDataService.ping(); result = false;
		}};

		boolean result = instance.ping();
		assertFalse(result);
	}

	@Test
	public void findByName_Test() {
		new Expectations() {{
			changelogDataService.findByName(Dummy.STRING); result = CHANGELOG_ENTRY;
		}};

		ChangelogEntry result = instance.findByName(Dummy.STRING);
		assertEquals(CHANGELOG_ENTRY, result);
	}

	@Test
	public void save_Test() {
		ChangelogEntry changelogEntry = new ChangelogEntry();
		String id = "id";

		changelogEntry.setId(id);

		new Expectations(UUID.class) {{
			UUID.randomUUID(); times = 0;
		}};

		instance.save(changelogEntry);
		assertEquals(id, changelogEntry.getId());

		new Verifications() {{
			changelogDataService.insert(changelogEntry); times = 1;
		}};
	}

	@Test
	public void save_Null_Id_Test() {
		ChangelogEntry changelogEntry = new ChangelogEntry();
		String id = "81193b1a-6ef3-42d2-875a-4eb3e6ebe779";
		UUID uuid = UUID.fromString(id);

		changelogEntry.setId(null);

		new Expectations(UUID.class) {{
			UUID.randomUUID(); result = uuid;
		}};

		instance.save(changelogEntry);
		assertEquals(id, changelogEntry.getId());

		new Verifications() {{
			changelogDataService.insert(changelogEntry); times = 1;
		}};
	}

	@Test
	public void execute_Test() {
		instance.execute(Dummy.STRING, Dummy.STRING2);

		new Verifications() {{
			changelogDataService.execute(Dummy.STRING, Dummy.STRING2); times = 1;
		}};
	}

	@Test
	public void findAll_Test() {
		new Expectations() {{
			changelogDataService.findAll(); result = CHANGELOG_ENTRIES;
		}};

		List<ChangelogEntry> results = instance.findAll();
		assertEquals(CHANGELOG_ENTRIES, results);
	}

	@Test
	public void saveChangelogEntry_Test() {
		new Expectations(instance) {{
			instance.save((ChangelogEntry) any); //do nothing
		}};

		instance.saveChangelogEntry(Dummy.STRING, Dummy.STRING2);

		new Verifications() {{
			ChangelogEntry changelogEntry;
			instance.save(changelogEntry = withCapture()); times = 1;
			assertEquals(Dummy.STRING, changelogEntry.getName());
			assertEquals(Dummy.STRING2, changelogEntry.getHashCode());
			assertNotNull(changelogEntry.getLastModified());
		}};
	}

}
