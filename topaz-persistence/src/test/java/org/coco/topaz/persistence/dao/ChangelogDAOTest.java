package org.coco.topaz.persistence.dao;

import mockit.Verifications;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.persistence.service.ResultSetService;
import org.coco.topaz.persistence.service.SQLService;
import org.coco.topaz.persistence.sql.Condition;
import org.coco.topaz.persistence.sql.Where;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.coco.topaz.persistence.sql.Operation.EQUALS;
import static org.coco.topaz.util.Constants.Strings.NAME;
import static org.junit.Assert.assertEquals;

public class ChangelogDAOTest {

	@Injectable
	private SQLService sqlService;

	@Injectable
	private ResultSetService resultSetService;

	@Tested
	private ChangelogDAO instance;

	private static final String SQL = "a bad sql statement";
	private static final ChangelogEntry CHANGELOG_ENTRY = new ChangelogEntry();

	@Test
	public void findByName_Test() throws Exception {
		new Expectations(instance) {{
			sqlService.select(ChangelogEntry.class, (Where) any); result = SQL;
			instance.executeUnique(anyString); result = CHANGELOG_ENTRY;
		}};

		ChangelogEntry result = instance.findByName("dummy");
		assertEquals(CHANGELOG_ENTRY, result);

		new Verifications() {{
			Where where;
			sqlService.select(ChangelogEntry.class, where = withCapture()); times = 1;

			Condition condition = where.getCondition();
			assertEquals(NAME, condition.getLeft());
			assertEquals(EQUALS, condition.getOperation());
			assertEquals("'dummy'", condition.getRight());
		}};
	}

}
