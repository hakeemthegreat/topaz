package org.coco.topaz.persistence.sql;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ColumnTest {

	private final Column instance = new Column(Dummy.STRING, Dummy.OBJECT);

	@Test
	public void name_Getter_Setter_Test() {
		instance.setName(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getName());
	}

	@Test
	public void value_Getter_Setter_Test() {
		instance.setValue(Dummy.STRING);
		assertEquals(Dummy.STRING, instance.getValue());
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertNotNull(result);
	}

}
