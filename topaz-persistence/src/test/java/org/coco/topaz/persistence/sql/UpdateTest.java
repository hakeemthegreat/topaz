package org.coco.topaz.persistence.sql;

import mockit.Expectations;
import mockit.Mocked;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UpdateTest {

	private final Update instance = new Update(SCHEMA + "." + TABLE);

	private static final String SCHEMA = "schema";
	private static final String TABLE = "table";
	private static final String UPDATE = "UPDATE \"schema\".\"table\" SET \"NAME\" = 'John', \"STATUS\" = 1";
	private static final String WHERE = "WHERE id = 123";

	@Test
	public void where_Getter_Setter_Test(@Mocked Where where) {
		instance.setWhere(where);
		assertEquals(where, instance.getWhere());
	}

	@Test
	public void toString_Test(@Mocked Where where) {
		Column name = new Column("NAME", "John");
		Column status = new Column("STATUS", 1);

		instance.addColumn(name);
		instance.addColumn(status);
		instance.setWhere(where);

		new Expectations() {{
			where.toString(); result = WHERE;
		}};

		String result = instance.toString();
		assertEquals(UPDATE + " " + WHERE, result);
	}

	@Test
	public void toString_No_Where_Clause_Test() {
		Column name = new Column("NAME", "John");
		Column status = new Column("STATUS", 1);

		instance.addColumn(name);
		instance.addColumn(status);
		instance.setWhere(null);

		String result = instance.toString();
		assertEquals(UPDATE, result);
	}

}
