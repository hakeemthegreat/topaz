package org.coco.topaz.persistence.util;

import mockit.Tested;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class SQLFilterTest extends JavaTest {

	@Tested
	private SQLFilter instance;

	@Test
	public void accept_Directory_Test() throws Exception {
		File directory = temporaryFolder.newFolder();
		File file = new File(directory, Dummy.STRING);
		boolean directorySuccessfullyCreated = file.mkdir();

		assertTrue(directorySuccessfullyCreated);

		boolean result = instance.accept(directory, file.getName());
		assertTrue(result);
	}

	@Test
	public void accept_SQL_File_Test() throws Exception {
		File directory = temporaryFolder.newFolder();
		File file = new File(directory, "file.sql");
		boolean fileSuccessfullyCreated = file.createNewFile();

		assertTrue(fileSuccessfullyCreated);

		boolean result = instance.accept(directory, file.getName());
		assertTrue(result);
	}

	@Test
	public void accept_Text_File_Test() throws Exception {
		File directory = temporaryFolder.newFolder();
		File file = new File(directory, "file.txt");
		boolean fileSuccessfullyCreated = file.createNewFile();

		assertTrue(fileSuccessfullyCreated);

		boolean result = instance.accept(directory, file.getName());
		assertFalse(result);
	}

	@Test
	public void accept_Nonexistent_File_Test() throws Exception {
		File directory = temporaryFolder.newFolder();
		File file = new File(directory, "nonexistent.txt");

		boolean result = instance.accept(directory, file.getName());
		assertFalse(result);
	}

}
