package org.coco.topaz.persistence.service;

import org.coco.topaz.persistence.util.Persistence;
import org.coco.topaz.util.Ensure;
import org.coco.topaz.persistence.util.SQLUtils;
import org.coco.topaz.util.Strings;
import org.coco.topaz.util.reflection.Fields;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static org.coco.topaz.util.Constants.Strings.SPACE;

public class SQLTranslator {

	private static final Map<String, String> RESERVED_WORDS = new HashMap<>();
	private static final Map<String, String> LITERALS = new HashMap<>();
	private final Map<String, String> fieldNameToColumnName = new HashMap<>();
	private final String allColumns;
	private final String tableName;

	static {
		RESERVED_WORDS.put("ALL", "");
		RESERVED_WORDS.put("AND", "AND");
		RESERVED_WORDS.put("BY", "WHERE");
		RESERVED_WORDS.put("DELETE", "DELETE FROM {tableName}");
		RESERVED_WORDS.put("EQUALS", "= %s");
		RESERVED_WORDS.put("FIND", "SELECT {allColumns} FROM {tableName}");
		RESERVED_WORDS.put("GREATER", "> %s");
		RESERVED_WORDS.put("IN", "IN %s");
		RESERVED_WORDS.put("IS", "IS %s");
		RESERVED_WORDS.put("LESS", "< %s");
		RESERVED_WORDS.put("LIKE", "LIKE %s");
		RESERVED_WORDS.put("NOT", "<> %s");
		RESERVED_WORDS.put("OR", "OR");
		RESERVED_WORDS.put("THAN", "");
		RESERVED_WORDS.put("WHERE", "WHERE");

		LITERALS.put("NULL", "NULL");
		LITERALS.put("ONE", "1");
		LITERALS.put("ZERO", "0");
	}

	public SQLTranslator(Class<?> clazz) {
		Ensure.notNull(clazz);
		List<Field> fields = Fields.getFields(clazz);
		List<String> columnNames;

		for (Field field : fields) {
			String fieldName = field.getName().toUpperCase();
			String columnName = Persistence.getColumnName(field);

			if (columnName != null) {
				fieldNameToColumnName.put(fieldName, columnName);
			}
		}

		columnNames = new ArrayList<>(fieldNameToColumnName.values());
		Collections.sort(columnNames);
		allColumns = String.join(", ", columnNames);
		tableName = Persistence.getTable(clazz);
	}

	public String translate(String phrase, Object... argumentArray) {
		try {
			return translate0(phrase, argumentArray);
		} catch (Exception e) {
			String message = String.format("Could not translate '%s'", phrase);
			throw new RuntimeException(message, e);
		}
	}

	protected String translate0(String phrase, Object... argumentArray) {
		List<String> argumentCollection = Arrays.stream(argumentArray).map(SQLUtils::toLiteral).collect(Collectors.toList());
		List<String> words = Strings.getWordsFromCamelCase(phrase).stream().map(String::toUpperCase).collect(Collectors.toList());
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder tokenBuilder = new StringBuilder();
		String sql;

		for (String word : words) {
			String token;

			tokenBuilder.append(word);
			token = tokenBuilder.toString();

			if (RESERVED_WORDS.containsKey(token)) {
				append(stringBuilder, RESERVED_WORDS.get(token));
				tokenBuilder.setLength(0);
			} else if (fieldNameToColumnName.containsKey(token)) {
				String columnName = fieldNameToColumnName.get(token);

				append(stringBuilder, columnName);
				tokenBuilder.setLength(0);
			} else if (LITERALS.containsKey(token)) {
				int count = Strings.count(stringBuilder.toString(), "%s");

				argumentCollection.add(count - 1, LITERALS.get(token));
				tokenBuilder.setLength(0);
			}
		}

		if (tokenBuilder.length() > 0) {
			throw new IllegalArgumentException(tokenBuilder.toString());
		}

		sql = stringBuilder.toString().trim();
		sql = sql.replace("{allColumns}", allColumns);
		sql = sql.replace("{tableName}", tableName);
		sql = String.format(sql, argumentCollection.toArray());
		sql = sql.replace("= NULL", "IS NULL");
		sql = sql.replace("<> NULL", "IS NOT NULL");
		return sql;
	}

	protected void append(StringBuilder stringBuilder, String toAppend) {
		if (Strings.isNotEmpty(toAppend)) {
			stringBuilder.append(SPACE);
			stringBuilder.append(toAppend);
		}
	}

}
