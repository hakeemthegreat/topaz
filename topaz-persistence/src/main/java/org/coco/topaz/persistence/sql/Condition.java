package org.coco.topaz.persistence.sql;

import lombok.Getter;

@Getter
public abstract class Condition {

	private final Object left;
	private final Operation operation;
	private final Object right;

	public Condition(Object left, Operation operation, Object right) {
		this.left = left;
		this.operation = operation;
		this.right = right;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s", left, operation, right);
	}

}
