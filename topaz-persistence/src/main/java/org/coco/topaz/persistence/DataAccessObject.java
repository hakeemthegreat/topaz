package org.coco.topaz.persistence;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

import org.coco.topaz.injection.Dependency;

import org.coco.topaz.persistence.service.ResultSetService;
import org.coco.topaz.persistence.service.SQLService;
import org.coco.topaz.persistence.sql.*;
import org.coco.topaz.persistence.util.Persistence;
import org.coco.topaz.util.*;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;

import static org.coco.topaz.persistence.sql.Operation.EQUALS;
import static org.coco.topaz.persistence.sql.Operation.IN;
import static org.coco.topaz.util.Constants.Numbers.Integers.ZERO;
import static org.coco.topaz.util.Constants.Strings.PERIOD;

public class DataAccessObject<T, ID> {

	protected final Class<T> clazz;
	protected final Field idField;

	@Dependency
	protected SQLService sqlService;

	@Dependency
	protected ResultSetService resultSetService;

	protected String pingQuery;

	protected DataAccessObject(Class<T> clazz) {
		Ensure.notNull(clazz);
		Ensure.notNull(this.idField = Persistence.getIdField(clazz), clazz.getName() + " must have an id!");
		Ensure.notNull(Persistence.getTable(clazz), clazz.getName() + " must be annotated with @Table");

		this.clazz = clazz;
		this.pingQuery = buildPingQuery(clazz);
	}

	protected String buildPingQuery(Class<?> clazz) {
		String qualifiedTableName = Persistence.getQualifiedTableName(clazz).replace(PERIOD, "\".\"");
		return String.format("SELECT 1 FROM \"%s\" WHERE 1 = 0", qualifiedTableName);
	}
	
	public List<T> execute(String sql) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSet generatedKeys = null;
		List<T> list;

		try {
			connection = Driver.getConnection();
			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			generatedKeys = preparedStatement.getGeneratedKeys();
			list = new ArrayList<>();
			populateList(list, resultSet, generatedKeys);
		} finally {
			Resources.close(generatedKeys, resultSet, preparedStatement, connection);
		}

		return list;
	}

	protected void populateList(List<T> list, ResultSet resultSet, ResultSet generatedKeys) throws SQLException {
		if (resultSet != null) {
			while (resultSet.next()) {
				T object = Reflection.newInstance(clazz);

				resultSetService.populate(resultSet, object);
				list.add(object);
			}
		} else if (generatedKeys != null) {
			while (generatedKeys.next()) {
				Object generatedKey = resultSetService.getObject(generatedKeys, 1, idField.getType());

				if (generatedKey != null) {
					T object = Reflection.newInstance(clazz);

					Fields.set(object, idField, generatedKey);
					list.add(object);
				}
			}
		}
	}

	public T executeUnique(String sql) throws SQLException {
		List<T> list = execute(sql);
		T result = null;

		if (list.size() > 1) {
			throw new NonUniqueResultException();
		} else if (list.size() == 1) {
			result = list.get(0);
		}

		return result;
	}

	public boolean ping() {
		boolean ping = true;

		try {
			execute(pingQuery);
		} catch (Exception e) {
			ping = false;
		}

		return ping;
	}
	
	public T findOne(ID id) throws SQLException {
		Where where = getWhereIdEquals(id);
		List<T> list = find(where);

		ensureNonEmptyResult(list, id);
		ensureUniqueResult(list, id);
		return list.get(ZERO);
	}

	public List<T> findById(Collection<ID> ids) throws SQLException {
		Ensure.notNull(ids);
		String columnName = Persistence.getColumnName(this.idField);
		Condition condition = new SimpleCondition(columnName, IN, ids);
		Where where = new Where(condition);

		return find(where);
	}

	public List<T> find(Collection<Join> joins) throws SQLException {
		String sql = sqlService.select(clazz, joins);
		return execute(sql);
	}

	public List<T> find(Where where) throws SQLException {
		String sql = sqlService.select(clazz, where);
		return execute(sql);
	}

	public List<T> find(OrderBy orderBy) throws SQLException {
		String sql = sqlService.select(clazz, orderBy);
		return execute(sql);
	}

	public List<T> find(String extra) throws SQLException {
		String sql = sqlService.select(clazz, extra);
		return execute(sql);
	}

	public List<T> find(Collection<Join> joins, Where where) throws SQLException {
		String sql = sqlService.select(clazz, joins, where);
		return execute(sql);
	}

	public List<T> find(Collection<Join> joins, Where where, OrderBy orderBy) throws SQLException {
		String sql = sqlService.select(clazz, joins, where, orderBy);
		return execute(sql);
	}

	public List<T> find(Collection<Join> joins, Where where, OrderBy orderBy, String extra) throws SQLException {
		String sql = sqlService.select(clazz, joins, where, orderBy, extra);
		return execute(sql);
	}
	
	public List<T> findAll() throws SQLException {
		String sql = sqlService.select(clazz);
		return execute(sql);
	}

	public void update(T object) throws SQLException {
		Ensure.notNull(object);
		String column = Persistence.getColumnName(idField);
		Object value = Fields.getValue(idField, object);
		Condition condition = new SimpleCondition(column, EQUALS, value);
		Where where = new Where(condition);
		String sql = sqlService.update(object, where);

		execute(sql);
	}

	public void update(Collection<T> collection) throws SQLException {
		Ensure.notNull(collection);

		for (T object : collection) {
			update(object);
		}
	}

	public void insert(T object) throws SQLException {
		Ensure.notNull(object);
		String sql = sqlService.insert(object);
		List<T> list = execute(sql);
		int size = list.size();

		if (size == 1) {
			Object generatedKey = Fields.getValue(idField, list.get(0));
			Fields.set(object, idField, generatedKey);
		} else if (size > 1) {
			throw new IllegalStateException("Expected only 1 generated key to be returned");
		}
	}

	public void insert(Collection<T> collection) throws SQLException {
		Ensure.notNull(collection);

		for (T object : collection) {
			insert(object);
		}
	}

	public void delete(T object) throws SQLException {
		Ensure.notNull(object);
		String column = Persistence.getColumnName(idField);
		Object value = Fields.getValue(idField, object);
		Condition condition = new SimpleCondition(column, EQUALS, value);
		Where where = new Where(condition);
		String sql = sqlService.delete(clazz, where);

		execute(sql);
	}

	public void delete(Collection<T> collection) throws SQLException {
		Ensure.notNull(collection);

		for (T object : collection) {
			delete(object);
		}
	}

	public void deleteOne(ID id) throws SQLException {
		Where where = getWhereIdEquals(id);
		String sql = sqlService.delete(clazz, where);

		execute(sql);
	}

	public void deleteById(Collection<ID> ids) throws SQLException {
		Ensure.notNull(ids);

		for (ID id : ids) {
			deleteOne(id);
		}
	}

	public void deleteAll() throws SQLException {
		String sql = sqlService.delete(clazz);
		execute(sql);
	}

	public boolean exists(ID id) throws SQLException {
		Where where = getWhereIdEquals(id);
		List<T> list = find(where);

		return !list.isEmpty();
	}

	public void save(T object) throws SQLException {
		ID id = (ID) Fields.getValue(this.idField, object);

		if (id == null) {
			insert(object);
		} else if (exists(id)) {
			update(object);
		} else {
			insert(object);
		}
	}

	public void save(Collection<T> collection) throws SQLException {
		Ensure.notNull(collection);

		for (T object : collection) {
			save(object);
		}
	}

	protected Where getWhereIdEquals(ID id) {
		Ensure.notNull(id);
		String columnName = Persistence.getColumnName(this.idField);
		Condition condition = new SimpleCondition(columnName, EQUALS, id);

		return new Where(condition);
	}

	protected void ensureUniqueResult(Collection<?> collection, ID id) {
		Ensure.notNull(collection);
		int size = collection.size();

		if (size > 1) {
			String message = String.valueOf(id);
			throw new NonUniqueResultException(message);
		}
	}

	protected void ensureNonEmptyResult(Collection<?> collection, ID id) {
		Ensure.notNull(collection);
		int size = collection.size();

		if (size == 0) {
			String message = String.valueOf(id);
			throw new EntityNotFoundException(message);
		}
	}

}
