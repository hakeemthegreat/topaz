package org.coco.topaz.persistence.util;

import java.lang.reflect.Array;
import java.util.*;

import static org.coco.topaz.util.Constants.Strings.*;

public class SQLUtils {

	private static final String NULL = "NULL";
	private static final String DELIMITER = ", ";

	private static final List<Class<?>> BYTE_ARRAY_CLASSES = Arrays.asList(byte[].class, Byte[].class);
	private static final List<Class<?>> CHARACTER_ARRAY_CLASSES = Arrays.asList(char[].class, Character[].class);

	private SQLUtils() {
		
	}

	public static String toLiteral(Object object) {
		String literal;

		if (object == null) {
			literal = NULL;
		} else if (object instanceof CharSequence || object instanceof Character || object instanceof UUID || object instanceof Enum) {
			literal = processString(object);
		} else if (object instanceof Byte) {
			literal = processByte((Byte) object);
		} else if (object instanceof Boolean || object instanceof Number) {
			literal = object.toString();
		} else if (object instanceof Date) {
			literal = processDate((Date) object);
		} else if (object instanceof Iterable) {
			literal = processIterable((Iterable<?>) object);
		} else if (object.getClass().isArray()) {
			literal = processArray(object);
		} else {
			throw new IllegalArgumentException(String.format("{%s} of %s", object, object.getClass())) ;
		}

		return literal;
	}

	protected static String processString(Object object) {
		String result;

		result = object.toString().replace("'", "''");
		result = SINGLE_QUOTE + result + SINGLE_QUOTE;
		return result;
	}

	protected static String processByte(Byte b) {
		return toLiteral(new byte[] {b});
	}

	protected static String processDate(Date date) {
		return Long.toString(date.getTime());
	}

	protected static String processIterable(Iterable<?> iterable) {
		List<String> strings = new ArrayList<>();

		iterable.forEach(object -> strings.add(toLiteral(object)));
		return LEFT_PARENTHESIS + String.join(DELIMITER, strings) + RIGHT_PARENTHESIS;
	}

	protected static String processArray(Object array) {
		String result;
		Class<?> clazz = array.getClass();

		if (BYTE_ARRAY_CLASSES.contains(clazz)) {
			result = processArray(array, "%02x", "x'", SINGLE_QUOTE);
		} else if (CHARACTER_ARRAY_CLASSES.contains(clazz)) {
			result = processArray(array, "%s", SINGLE_QUOTE, SINGLE_QUOTE);
		} else {
			throw new IllegalArgumentException(String.valueOf(array));
		}

		return result;
	}

	protected static String processArray(Object array, String format, String startDelimiter, String endDelimiter) {
		StringBuilder sb = new StringBuilder(startDelimiter);
		int length = Array.getLength(array);

		for (int i = 0; i < length; i++) {
			Object object = Array.get(array, i);
			String string = String.format(format, object);

			sb.append(string);
		}

		sb.append(endDelimiter);
		return sb.toString();
	}

}
