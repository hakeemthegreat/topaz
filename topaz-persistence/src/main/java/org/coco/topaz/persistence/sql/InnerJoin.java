package org.coco.topaz.persistence.sql;

public class InnerJoin extends Join {

	public InnerJoin(String schema, String table, On on) {
		super(schema, table, on);
	}

	@Override
	public String toString() {
		return String.format("INNER JOIN %s %s", getQualifiedTableName(), getOn());
	}
	
}
