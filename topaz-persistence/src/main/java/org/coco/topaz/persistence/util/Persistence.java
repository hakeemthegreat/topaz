package org.coco.topaz.persistence.util;

import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.Replace;
import org.coco.topaz.util.Strings;
import org.coco.topaz.util.reflection.Fields;

import java.lang.reflect.Field;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.coco.topaz.util.Constants.Characters.PERIOD;
import static org.coco.topaz.util.Constants.Strings.EMPTY;

public class Persistence {

	private static final Map<Class<?>, Field> IDS = new HashMap<>();
	private static final Map<Class<?>, List<Field>> SELECTABLE_FIELDS = new HashMap<>();
	private static final Map<Class<?>, List<Field>> INSERTABLE_FIELDS = new HashMap<>();
	private static final Map<Class<?>, List<Field>> UPDATABLE_FIELDS = new HashMap<>();
	
	private Persistence() {
		
	}

	public static String getSchema(Class<?> clazz) {
		String name = null;

		while (clazz != null) {
			Table table = clazz.getAnnotation(Table.class);

			if (table != null) {
				name = table.schema();
				break;
			}

			clazz = clazz.getSuperclass();
		}

		return name;
	}
	
	public static String getTable(Class<?> clazz) {
		String name = null;

		while (clazz != null) {
			Table table = clazz.getAnnotation(Table.class);

			if (table != null) {
				name = table.name();
				break;
			}

			clazz = clazz.getSuperclass();
		}

		return name;
	}

	public static String getQualifiedTableName(Class<?> clazz) {
		String schema = getSchema(clazz);
		String table = getTable(clazz);
		String name = null;

		if (Strings.isNotBlank(schema)) {
			name = schema;
			name += PERIOD;
		}

		if (Strings.isNotBlank(table)) {
			name = Replace.with(EMPTY).should(name).beNull();
			name += table;
		}

		return name;
	}
	
	public static String getColumnName(Field field) {
		String name = null;
		
		if (field != null) {
			Column column = field.getAnnotation(Column.class);
			
			if (column != null) {
				name = column.name();
			}	
		}
		
		return name;
	}
	
	public static Field getIdField(Class<?> clazz) {
		Field result = IDS.get(clazz);

		if (result == null) {
			result = getIdFromFields(clazz);
			IDS.put(clazz, result);
		}

		return result;
	}

	protected static Field getIdFromFields(Class<?> clazz) {
		Field result = null;
		List<Field> fields = Fields.getFields(clazz);

		for (Field field : fields) {
			Id id = field.getAnnotation(Id.class);

			if (id != null) {
				result = field;
				break;
			}
		}

		return result;
	}
	
	public static Object getIdValue(Object object) {
		Field id = getIdField(ClassUtils.getClass(object));
		Object value = null;
		
		if (id != null) {
			value = Fields.getValue(id, object);
		}

		return value;
	}

	public static List<Field> getSelectableFields(Class<?> clazz) {
		List<Field> selectableFields = SELECTABLE_FIELDS.get(clazz);

		if (selectableFields == null) {
			selectableFields = createSelectableFields(clazz);
			SELECTABLE_FIELDS.put(clazz, selectableFields);
		}

		return selectableFields;
	}

	public static List<Field> createSelectableFields(Class<?> clazz) {
		List<Field> selectableFields = new ArrayList<>();
		List<Field> fields = Fields.getFields(clazz);

		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);

			if (column != null) {
				selectableFields.add(field);
			}
		}

		return Collections.unmodifiableList(selectableFields);
	}

	public static List<Field> getInsertableFields(Class<?> clazz) {
		List<Field> insertableFields = INSERTABLE_FIELDS.get(clazz);

		if (insertableFields == null) {
			insertableFields = createInsertableFields(clazz);
			INSERTABLE_FIELDS.put(clazz, insertableFields);
		}

		return insertableFields;
	}

	public static List<Field> createInsertableFields(Class<?> clazz) {
		List<Field> insertableFields = new ArrayList<>();
		List<Field> fields = Fields.getFields(clazz);

		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);

			if (insertable(column)) {
				insertableFields.add(field);
			}
		}

		return Collections.unmodifiableList(insertableFields);
	}

	public static List<Field> getUpdatableFields(Class<?> clazz) {
		List<Field> updatableFields = UPDATABLE_FIELDS.get(clazz);

		if (updatableFields == null) {
			updatableFields = createUpdatableFields(clazz);
			UPDATABLE_FIELDS.put(clazz, updatableFields);
		}

		return updatableFields;
	}

	public static List<Field> createUpdatableFields(Class<?> clazz) {
		List<Field> updatableFields = new ArrayList<>();
		List<Field> fields = Fields.getFields(clazz);

		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);

			if (updatable(column)) {
				updatableFields.add(field);
			}
		}

		return Collections.unmodifiableList(updatableFields);
	}

	public static List<Column> getColumns(Class<?> clazz) {
		return getColumns(Fields.getFields(clazz));
	}

	public static List<Column> getColumns(List<Field> fields) {
		List<Column> columns = new ArrayList<>();

		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);

			if (column != null) {
				columns.add(column);
			}
		}

		return Collections.unmodifiableList(columns);
	}

	public static List<String> getColumnNames(List<Field> fields) {
		List<Column> columns = getColumns(fields);
		List<String> columnNames = new ArrayList<>();

		for (Column column : columns) {
			columnNames.add(column.name());
		}

		return columnNames;
	}
	
	public static Map<String, Object> getColumnValues(Object object, List<Field> fields) {
		Map<String, Object> map = new HashMap<>();
		
		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);

			if (column != null) {
				Object value = Fields.getValue(field, object);
				map.put(column.name(), value);
			}
		}
		
		return map;
	}

	public static boolean insertable(Column column) {
		return column != null && column.insertable();
	}

	public static boolean updatable(Column column) {
		return column != null && column.updatable();
	}

}
