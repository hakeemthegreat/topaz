package org.coco.topaz.persistence.service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.coco.topaz.persistence.DataAccessObject;
import org.coco.topaz.exception.ServiceException;

import static org.coco.topaz.util.Constants.Strings.Messages.ERROR_OCCURRED_ACCESSING_DATABASE;

public class DataServiceImpl<T, ID> implements DataService<T, ID> {
	
	private final DataAccessObject<T, ID> dataAccessObject;
	
	public DataServiceImpl(DataAccessObject<T, ID> dataAccessObject) {
		this.dataAccessObject = Objects.requireNonNull(dataAccessObject);
	}

	@Override
	public boolean ping() {
		return dataAccessObject.ping();
	}

	@Override
	public T findOne(ID id) throws ServiceException {
		try {
			return dataAccessObject.findOne(id);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public List<T> findAll() throws ServiceException {
		try {
			return dataAccessObject.findAll();
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void delete(T object) throws ServiceException {
		try {
			dataAccessObject.delete(object);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void delete(Collection<T> collection) throws ServiceException {
		try {
			dataAccessObject.delete(collection);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void deleteOne(ID id) throws ServiceException {
		try {
			dataAccessObject.deleteOne(id);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void deleteById(Collection<ID> ids) throws ServiceException {
		try {
			dataAccessObject.deleteById(ids);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void deleteAll() throws ServiceException {
		try {
			dataAccessObject.deleteAll();
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void insert(T object) throws ServiceException {
		try {
			dataAccessObject.insert(object);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void insert(Collection<T> collection) throws ServiceException {
		try {
			dataAccessObject.insert(collection);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void update(T object) throws ServiceException {
		try {
			dataAccessObject.update(object);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void update(Collection<T> collection) throws ServiceException {
		try {
			dataAccessObject.update(collection);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public boolean exists(ID id) throws ServiceException {
		try {
			return dataAccessObject.exists(id);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void save(T object) throws ServiceException {
		try {
			dataAccessObject.save(object);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public void save(Collection<T> collection) throws ServiceException {
		try {
			dataAccessObject.save(collection);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

	@Override
	public List<T> findById(Collection<ID> ids) throws ServiceException {
		try {
			return dataAccessObject.findById(ids);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

}
