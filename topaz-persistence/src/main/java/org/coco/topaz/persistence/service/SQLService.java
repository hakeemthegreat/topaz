package org.coco.topaz.persistence.service;

import org.coco.topaz.persistence.sql.Join;
import org.coco.topaz.persistence.sql.OrderBy;
import org.coco.topaz.persistence.sql.Where;

import java.util.Collection;

public interface SQLService {

	String insert(Object object);

	String select(Class<?> clazz);

	String select(Class<?> clazz, Collection<Join> joins);

	String select(Class<?> clazz, Where where);

	String select(Class<?> clazz, OrderBy orderBy);

	String select(Class<?> clazz, String extra);

	String select(Class<?> clazz, Collection<Join> joins, Where where);

	String select(Class<?> clazz, Collection<Join> joins, Where where, OrderBy orderBy);

	String select(Class<?> clazz, Collection<Join> joins, Where where, OrderBy orderBy, String extra);

	String update(Object object);

	String update(Object object, Where where);

	String delete(Class<?> clazz);

	String delete(Class<?> clazz, Where where);

}
