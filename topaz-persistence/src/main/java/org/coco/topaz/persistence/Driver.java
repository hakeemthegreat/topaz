package org.coco.topaz.persistence;

import org.coco.topaz.environment.TopazEnvironment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Driver {
	
	private static final String UNABLE_TO_CREATE_CONNECTION = "Unable to create database connection";

	private Driver() {

	}

	public static Connection getConnection() throws SQLException {
		try {
			registerDriver();
			return DriverManager.getConnection(TopazEnvironment.getDatabaseUrl());
		} catch (ClassNotFoundException e) {
			throw new SQLException(UNABLE_TO_CREATE_CONNECTION, e);
		}
	}

	protected static void registerDriver() throws ClassNotFoundException {
		String databaseDriver = TopazEnvironment.getDatabaseDriver();
		Class.forName(databaseDriver);
	}

}
