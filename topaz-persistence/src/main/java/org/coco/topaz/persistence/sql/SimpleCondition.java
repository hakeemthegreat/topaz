package org.coco.topaz.persistence.sql;

import java.util.HashSet;
import java.util.Set;

import org.coco.topaz.util.Ensure;
import org.coco.topaz.persistence.util.SQLUtils;

public class SimpleCondition extends Condition {
	
	private static final Set<Operation> ALLOWED_OPERATIONS = new HashSet<>();

	static {
		ALLOWED_OPERATIONS.add(Operation.EQUALS);
		ALLOWED_OPERATIONS.add(Operation.GREATER_THAN);
		ALLOWED_OPERATIONS.add(Operation.IN);
		ALLOWED_OPERATIONS.add(Operation.IS);
		ALLOWED_OPERATIONS.add(Operation.IS_NOT);
		ALLOWED_OPERATIONS.add(Operation.LESS_THAN);
		ALLOWED_OPERATIONS.add(Operation.LIKE);
		ALLOWED_OPERATIONS.add(Operation.NOT_EQUALS);
	}

	public SimpleCondition(String column, Operation operation, Object value) {
		super(column, getProperOperation(operation, value), SQLUtils.toLiteral(value));
		Ensure.isTrue(ALLOWED_OPERATIONS.contains(operation));
		Ensure.notNull(column);
	}

	protected static Operation getProperOperation(Operation operation, Object value) {
		Operation result = operation;

		if (value == null) {
			if (operation.equals(Operation.EQUALS)) {
				result = Operation.IS;
			} else if (operation.equals(Operation.NOT_EQUALS)) {
				result = Operation.IS_NOT;
			}
		}

		return result;
	}

	@Override
	public String toString() {
		return String.format("\"%s\" %s %s", getLeft(), getOperation(), getRight());
	}

}
