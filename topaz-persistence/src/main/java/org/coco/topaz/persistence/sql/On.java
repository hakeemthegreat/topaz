package org.coco.topaz.persistence.sql;

import java.util.Objects;

public class On {

	private final String leftColumn;
	private final String rightColumn;

	public On(String leftColumn, String rightColumn) {
		this.leftColumn = Objects.requireNonNull(leftColumn);
		this.rightColumn = Objects.requireNonNull(rightColumn);
	}

	@Override
	public String toString() {
		return String.format("ON %s = %s", leftColumn, rightColumn);
	}

}
