package org.coco.topaz.persistence.service;

import java.sql.*;

public interface ResultSetService {

	void populate(ResultSet resultSet, Object object) throws SQLException;

	Object getObject(ResultSet resultSet, int columnIndex, Class<?> clazz) throws SQLException;

	Object getObject(ResultSet resultSet, String columnName, Class<?> clazz) throws SQLException;

}
