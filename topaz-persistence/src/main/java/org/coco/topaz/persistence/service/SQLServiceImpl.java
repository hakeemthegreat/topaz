package org.coco.topaz.persistence.service;

import org.coco.topaz.persistence.sql.*;
import org.coco.topaz.persistence.util.Persistence;
import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.CollectionUtils;
import org.coco.topaz.util.Replace;

import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;

public class SQLServiceImpl implements SQLService {

	@Override
	public String insert(Object object) {
		Class<?> clazz = ClassUtils.getClass(object);
		String schema = Persistence.getSchema(clazz);
		String table = Persistence.getTable(clazz);
		List<Field> insertableFields = Persistence.getInsertableFields(clazz);
		Map<String, Object> columnValues = Persistence.getColumnValues(object, insertableFields);
		List<Column> columns = toColumns(columnValues);
		Insert insert = new Insert(schema, table);

		insert.setColumns(columns);
		return insert.toString();
	}

	@Override
	public String select(Class<?> clazz) {
		return select(clazz, null, null);
	}

	@Override
	public String select(Class<?> clazz, Collection<Join> joins) {
		return select(clazz, joins, null, null, null);
	}

	@Override
	public String select(Class<?> clazz, Where where) {
		return select(clazz, Collections.emptyList(), where, null, null);
	}

	@Override
	public String select(Class<?> clazz, OrderBy orderBy) {
		return select(clazz, Collections.emptyList(), null, orderBy, null);
	}

	@Override
	public String select(Class<?> clazz, String extra) {
		return select(clazz, Collections.emptyList(), null, null, extra);
	}

	@Override
	public String select(Class<?> clazz, Collection<Join> joins, Where where) {
		return select(clazz, joins, where, null, null);
	}

	@Override
	public String select(Class<?> clazz, Collection<Join> joins, Where where, OrderBy orderBy) {
		return select(clazz, joins, where, orderBy, null);
	}

	@Override
	public String select(Class<?> clazz, Collection<Join> joins, Where where, OrderBy orderBy, String extra) {
		String schema = Persistence.getSchema(clazz);
		String table = Persistence.getTable(clazz);
		List<Field> selectableFields = Persistence.getSelectableFields(clazz);
		List<String> selectableColumns = CollectionUtils.toList(Persistence.getColumnNames(selectableFields));
		Select select = new Select(schema, table);

		joins = Replace.with(Collections.emptyList()).should(joins).beNull();
		select.getColumns().addAll(selectableColumns);
		select.getJoins().addAll(joins);
		select.setWhere(where);
		select.setOrderBy(orderBy);
		select.setExtra(extra);
		return select.toString();
	}

	@Override
	public String update(Object object) {
		return update(object, null);
	}

	@Override
	public String update(Object object, Where where) {
		Class<?> clazz = ClassUtils.getClass(object);
		String schema = Persistence.getSchema(clazz);
		String table = Persistence.getTable(clazz);
		List<Field> updatableFields = Persistence.getUpdatableFields(clazz);
		Map<String, Object> columnValues = Persistence.getColumnValues(object, updatableFields);
		List<Column> columns = toColumns(columnValues);
		Update update = new Update(schema, table);

		update.setColumns(columns);
		update.setWhere(where);
		return update.toString();
	}

	@Override
	public String delete(Class<?> clazz) {
		return delete(clazz, null);
	}

	@Override
	public String delete(Class<?> clazz, Where where) {
		String schema = Persistence.getSchema(clazz);
		String table = Persistence.getTable(clazz);
		Delete delete = new Delete(schema, table);

		delete.setWhere(where);
		return delete.toString();
	}

	protected List<Column> toColumns(Map<String, Object> map) {
		List<Column> columns = new ArrayList<>();

		for (Entry<String, Object> entry : map.entrySet()) {
			String name = entry.getKey();
			Object value = entry.getValue();
			Column column = new Column(name, value);

			columns.add(column);
		}

		return columns;
	}

}
