package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Delete extends TableStatement {

	private Where where;

	public Delete(String table) {
		this(null, table);
	}

	public Delete(String schema, String table) {
		super(schema, table);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("DELETE FROM ");
		sb.append(getQualifiedTableName());

		if (where != null) {
			sb.append(" ");
			sb.append(where);
		}

		return sb.toString();
	}
}
