package org.coco.topaz.persistence.sql;

public class LeftJoin extends Join {

	public LeftJoin(String schema, String table, On on) {
		super(schema, table, on);
	}

	@Override
	public String toString() {
		return String.format("LEFT JOIN %s %s", getQualifiedTableName(), getOn());
	}
	
}
