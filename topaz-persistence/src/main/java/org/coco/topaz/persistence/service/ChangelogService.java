package org.coco.topaz.persistence.service;

import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.persistence.data.ChangelogDataService;
import org.coco.topaz.persistence.model.ChangelogEntry;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class ChangelogService {

	private final ChangelogDataService changelogDataService;

	public ChangelogService(ChangelogDataService changelogDataService) {
		this.changelogDataService = Objects.requireNonNull(changelogDataService);
	}

	public boolean ping() {
		return changelogDataService.ping();
	}

	public List<ChangelogEntry> findAll() throws ServiceException {
		return changelogDataService.findAll();
	}

	public ChangelogEntry findByName(String name) throws ServiceException {
		return changelogDataService.findByName(name);
	}

	public void save(ChangelogEntry changelogEntry) throws ServiceException {
		if (changelogEntry.getId() == null) {
			changelogEntry.setId(UUID.randomUUID().toString());
		}

		changelogDataService.insert(changelogEntry);
	}

	public void execute(String sql, String resource) throws ServiceException {
		changelogDataService.execute(sql, resource);
	}

	public void saveChangelogEntry(String name, String hashCode) throws ServiceException {
		ChangelogEntry changelogEntry = new ChangelogEntry();

		changelogEntry.setName(name);
		changelogEntry.setHashCode(hashCode);
		changelogEntry.setLastModified(new Date());
		save(changelogEntry);
	}

}
