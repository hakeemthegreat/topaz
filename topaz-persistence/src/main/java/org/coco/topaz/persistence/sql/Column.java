package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Column {

	private String name;
	private Object value;

	public Column(String name, Object value) {
		setName(name);
		setValue(value);
	}

	@Override
	public String toString() {
		return name + " -> " + value;
	}

}
