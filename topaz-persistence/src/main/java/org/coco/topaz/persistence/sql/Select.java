package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Select extends TableStatement {

	@Getter
	private final List<String> columns = new ArrayList<>();

	@Getter
	private final List<Join> joins = new ArrayList<>();

	@Getter
	@Setter
	private Where where;

	@Getter
	@Setter
	private OrderBy orderBy;

	@Getter
	@Setter
	private String extra;

	public Select(String table) {
		this(null, table);
	}

	public Select(String schema, String table) {
		super(schema, table);
	}

	public void addJoin(Join join) {
		Ensure.notNull(join);
		this.joins.add(join);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String qualifiedTableName = getQualifiedTableName();
		List<String> quotedColumns = columns.stream().map(column -> String.format("%s.\"%s\"", qualifiedTableName, column)).collect(Collectors.toList());

		sb.append("SELECT ");
		sb.append(Strings.toCommaDelimited(quotedColumns));
		sb.append(" FROM ");
		sb.append(getQualifiedTableName());

		for (Join join : joins) {
			sb.append(" ");
			sb.append(join);
		}

		if (where != null) {
			sb.append(" ");
			sb.append(where);
		}

		if (orderBy != null) {
			sb.append(" ");
			sb.append(orderBy);
		}

		if (Strings.isNotBlank(extra)) {
			sb.append(" ");
			sb.append(extra);
		}

		return sb.toString();
	}
}
