package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.Strings;

import static org.coco.topaz.util.Constants.Characters.DOUBLE_QUOTE;
import static org.coco.topaz.util.Constants.Characters.PERIOD;

@Getter
@Setter
public abstract class TableStatement {

	private String schema;
	private String table;

	public TableStatement(String schema, String table) {
		setSchema(schema);
		setTable(table);
	}

	public void setTable(String table) {
		Ensure.notNull(table);

		if (Strings.isBlank(table)) {
			throw new IllegalArgumentException(table);
		} else {
			this.table = table;
		}
	}

	public String getQualifiedTableName() {
		String qualifiedTableName = table;

		if (Strings.isNotBlank(schema)) {
			qualifiedTableName = schema + PERIOD + qualifiedTableName;
		}

		qualifiedTableName = DOUBLE_QUOTE + qualifiedTableName.replace(".", "\".\"") + DOUBLE_QUOTE;
		return qualifiedTableName;
	}

}

