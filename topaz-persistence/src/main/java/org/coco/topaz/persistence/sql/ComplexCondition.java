package org.coco.topaz.persistence.sql;

import java.util.HashSet;
import java.util.Set;

import org.coco.topaz.util.Ensure;

import static org.coco.topaz.persistence.sql.Operation.*;

public class ComplexCondition extends Condition {

	private static final Set<Operation> ALLOWED_OPERATIONS = new HashSet<>();

	static {
		ALLOWED_OPERATIONS.add(AND);
		ALLOWED_OPERATIONS.add(OR);
		ALLOWED_OPERATIONS.add(XOR);
	}

	public ComplexCondition(Condition left, Operation operation, Condition right) {
		super(left, operation, right);
		Ensure.isTrue(ALLOWED_OPERATIONS.contains(operation));
		Ensure.notNull(left);
		Ensure.notNull(right);
	}

	@Override
	public String toString() {
		return "(" + super.toString() + ")";
	}

}
