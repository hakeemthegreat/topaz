package org.coco.topaz.persistence;

import org.coco.topaz.injection.Beans;
import org.coco.topaz.injection.TopazBeans;
import org.coco.topaz.persistence.service.ResultSetService;
import org.coco.topaz.persistence.service.ResultSetServiceImpl;
import org.coco.topaz.persistence.service.SQLService;
import org.coco.topaz.persistence.service.SQLServiceImpl;

public class PersistenceBeans {

	private static final Beans BEANS = Beans.getInstance();

	private PersistenceBeans() {

	}

	public static void init() {
		TopazBeans.init();
		BEANS.newBean(ResultSetService.class, ResultSetServiceImpl.class);
		BEANS.newBean(SQLService.class, SQLServiceImpl.class);
	}

}
