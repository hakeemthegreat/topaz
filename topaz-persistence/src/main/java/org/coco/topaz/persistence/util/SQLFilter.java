package org.coco.topaz.persistence.util;

import java.io.File;
import java.io.FilenameFilter;

public class SQLFilter implements FilenameFilter {

	@Override
	public boolean accept(File dir, String name) {
		File file = new File(dir, name);
		boolean accept = false;

		if (file.isDirectory()) {
			accept = true;
		} else if (file.isFile() && name.endsWith(".sql")) {
			accept = true;
		}

		return accept;
	}

}
