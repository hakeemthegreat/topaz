package org.coco.topaz.persistence.sql;

import org.coco.topaz.util.Ensure;

public class Where {

	private Condition condition;

	public Where(String column, Operation operation, Object value) {
		this(new SimpleCondition(column, operation, value));
	}

	public Where(Condition condition) {
		setCondition(condition);
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		Ensure.notNull(condition);
		this.condition = condition;
	}

	@Override
	public String toString() {
		return "WHERE " + condition;
	}

}
