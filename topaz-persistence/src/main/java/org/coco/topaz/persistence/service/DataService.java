package org.coco.topaz.persistence.service;

import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.util.service.Service;

import java.util.Collection;
import java.util.List;

public interface DataService<T, ID> extends Service {

	boolean ping();

	boolean exists(ID id) throws ServiceException;
	
	T findOne(ID id) throws ServiceException;

	List<T> findById(Collection<ID> ids) throws ServiceException;
	
	List<T> findAll() throws ServiceException;

	void delete(T object) throws ServiceException;

	void delete(Collection<T> collection) throws ServiceException;

	void deleteOne(ID id) throws ServiceException;

	void deleteById(Collection<ID> id) throws ServiceException;

	void deleteAll() throws ServiceException;

	void insert(T object) throws ServiceException;

	void insert(Collection<T> collection) throws ServiceException;

	void update(T object) throws ServiceException;

	void update(Collection<T> collection) throws ServiceException;

	void save(T object) throws ServiceException;

	void save(Collection<T> collection) throws ServiceException;

}
