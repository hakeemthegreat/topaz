package org.coco.topaz.persistence.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Table(name = "CHANGELOG")
public class ChangelogEntry {

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "HASH_CODE")
	private String hashCode;

	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

}
