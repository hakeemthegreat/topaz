package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.persistence.util.SQLUtils;
import org.coco.topaz.util.Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.coco.topaz.util.Constants.Strings.DOUBLE_QUOTE;

@Getter
@Setter
public class Insert extends TableStatement {

	private List<Column> columns = new ArrayList<>();

	public Insert(String table) {
		this(null, table);
	}

	public Insert(String schema, String table) {
		super(schema, table);
	}

	public void addColumn(Column column) {
		columns.add(column);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		List<String> columnNames = columns.stream().map(column -> DOUBLE_QUOTE + column.getName() + DOUBLE_QUOTE).collect(Collectors.toList());
		List<String> columnValues = columns.stream().map(column -> SQLUtils.toLiteral(column.getValue())).collect(Collectors.toList());

		sb.append("INSERT INTO ");
		sb.append(getQualifiedTableName());
		sb.append("(");
		sb.append(Strings.toCommaDelimited(columnNames));
		sb.append(") VALUES(");
		sb.append(Strings.toCommaDelimited(columnValues));
		sb.append(")");

		return sb.toString();
	}
}
