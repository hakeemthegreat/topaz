package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.persistence.util.SQLUtils;
import org.coco.topaz.util.Strings;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Update extends TableStatement {

	private List<Column> columns = new ArrayList<>();
	private Where where;

	public Update(String table) {
		this(null, table);
	}

	public Update(String schema, String table) {
		super(schema, table);
	}

	public void addColumn(Column column) {
		columns.add(column);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		List<String> strings = new ArrayList<>();

		sb.append("UPDATE ");
		sb.append(getQualifiedTableName());
		sb.append(" SET ");

		for (Column column : columns) {
			String name = column.getName();
			Object value = column.getValue();
			String string = String.format("\"%s\" = %s", name, SQLUtils.toLiteral(value));

			strings.add(string);
		}

		sb.append(Strings.toCommaDelimited(strings));

		if (where != null) {
			sb.append(" ");
			sb.append(where);
		}

		return sb.toString();
	}
}
