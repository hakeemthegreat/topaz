package org.coco.topaz.persistence.sql;

import lombok.Getter;
import org.coco.topaz.util.Ensure;

@Getter
public class Join extends TableStatement {

	private On on;

	public Join(String table, On on) {
		this(null, table, on);
	}

	public Join(String schema, String table, On on) {
		super(schema, table);
		setOn(on);
	}

	public void setOn(On on) {
		Ensure.notNull(on);
		this.on = on;
	}

	@Override
	public String toString() {
		return String.format("JOIN %s %s", getQualifiedTableName(), on);
	}

}
