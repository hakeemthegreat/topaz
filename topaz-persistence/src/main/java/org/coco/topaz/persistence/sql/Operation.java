package org.coco.topaz.persistence.sql;

import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

@Getter
public class Operation implements Serializable {

	private final String token;

	public static final Operation AND = new Operation("AND");
	public static final Operation EQUALS = new Operation("=");
	public static final Operation GREATER_THAN = new Operation(">");
	public static final Operation IN = new Operation("IN");
	public static final Operation IS = new Operation("IS");
	public static final Operation IS_NOT = new Operation("IS NOT");
	public static final Operation LESS_THAN = new Operation("<");
	public static final Operation LIKE = new Operation("LIKE");
	public static final Operation NOT_EQUALS = new Operation("<>");
	public static final Operation OR = new Operation("OR");
	public static final Operation XOR = new Operation("XOR");

	private static final long serialVersionUID = 1L;

	public Operation(String token) {
		this.token = Objects.requireNonNull(token);
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;

		if (object instanceof Operation) {
			Operation that = (Operation) object;
			result = this.token.equals(that.token);
		}

		return result;
	}

	@Override
	public int hashCode() {
		return token.hashCode();
	}

	@Override
	public String toString() {
		return token;
	}

}
