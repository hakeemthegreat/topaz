package org.coco.topaz.persistence.service;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.environment.Environment;
import org.coco.topaz.environment.TopazEnvironment;
import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.injection.Dependency;
import org.coco.topaz.injection.DependencyInjection;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.persistence.util.SQLFilter;
import org.coco.topaz.util.*;
import org.coco.topaz.util.service.HashService;
import org.coco.topaz.util.service.Service;

import java.io.*;
import java.util.*;

import static org.coco.topaz.util.Constants.Strings.NEWLINE;

public class DataMigrationService implements Service {

	private final ChangelogService changelogService;
	private final HashService hashService;

	@Getter
	@Setter
	private PrintStream printStream;

	//points to the directory containing the files to create the changelog and whatnot
	//should NOT be put in a properties file
	protected static final String CREATE_CHANGELOG_FILE = "sql/create_changelog.sql";
	protected static final SQLFilter filter = new SQLFilter();

	@Dependency
	public DataMigrationService(ChangelogService changelogService, HashService hashService) {
		this.changelogService = Objects.requireNonNull(changelogService);
		this.hashService = Objects.requireNonNull(hashService);
	}

	public void start() throws ServiceException {
		try {
			createChangelog();
			executeSQLFiles();
		} catch (IOException e) {
			String message = "Could not start " + getClass().getSimpleName();
			throw new ServiceException(message, e);
		}
	}

	protected void createChangelog() throws IOException, ServiceException {
		if (!changelogService.ping()) {
			executeResource(CREATE_CHANGELOG_FILE);
		}
	}

	protected void executeResource(String resource) throws IOException, ServiceException {
		InputStream inputStream = Environment.getResourceAsStream(resource, Environment.class.getClassLoader());
		Reader reader = new InputStreamReader(inputStream);
		String sql = Streams.readAll(reader);

		executeSQL(sql, resource);
	}

	protected void executeSQLFiles() throws IOException, ServiceException {
		File rootDirectory = new File(getRootDirectory());
		executeDirectory(rootDirectory);
	}

	protected String getRootDirectory() {
		String rootDirectory = TopazEnvironment.getDatabaseSQL();

		if (Strings.isEmpty(rootDirectory)) {
			throw new RuntimeException("Make sure you set database.sql in your properties file!");
		}

		return rootDirectory;
	}

	protected void executeDirectory(File root) throws IOException, ServiceException {
		FileWrapper fileWrapper = FileUtils.buildFileWrapper(root, filter);
		List<File> files = CollectionUtils.toList(fileWrapper.getFiles());
		List<File> directories = CollectionUtils.toList(fileWrapper.getDirectories());

		files.sort(Comparator.comparing(File::getName));
		directories.sort(Comparator.comparing(File::getName));

		for (File file : files) {
			String fileName = file.getName();
			ChangelogEntry changelogEntry = changelogService.findByName(fileName);
			String sql = FileUtils.readAll(file);
			String currentHashCode = hashService.hashToHex(sql);

			if (changelogEntry == null) {
				executeSQL(sql, fileName);
				changelogService.saveChangelogEntry(fileName, currentHashCode);
			} else {
				String previousHashCode = changelogEntry.getHashCode();

				if (!currentHashCode.equalsIgnoreCase(previousHashCode)) {
					String message = String.format("Hash code does not match what is in database for '%s'", fileName);
					throw new ServiceException(message);
				}
			}
		}

		for (File directory : directories) {
			executeDirectory(directory);
		}
	}

	protected void executeSQL(String sql, String resource) throws ServiceException {
		String databaseDelimiter = TopazEnvironment.getDatabaseDelimiter();
		List<String> statements = Collections.singletonList(sql);

		print("Executing... ");
		print(resource);
		print(NEWLINE);

		if (Strings.isNotBlank(databaseDelimiter)) {
			statements = new ArrayList<>();
			Collections.addAll(statements, sql.split(databaseDelimiter));
			statements.removeIf(Strings::isBlank);
		}

		for (String statement : statements) {
			changelogService.execute(statement, resource);
		}
	}

	protected void print(String string) {
		if (printStream != null) {
			printStream.print(string);
		}
	}

	public static void main(String[] args) throws ServiceException {
		DataMigrationService dataMigrationService = DependencyInjection.newInstance(DataMigrationService.class);
		dataMigrationService.start();
	}

}
