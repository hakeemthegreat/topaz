package org.coco.topaz.persistence.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.coco.topaz.persistence.util.Persistence;
import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.Strings;
import org.coco.topaz.util.reflection.Fields;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

public class ResultSetServiceImpl implements ResultSetService {

	private final ObjectMapper objectMapper = new ObjectMapper();

	protected static List<String> getColumnNames(ResultSet resultSet) throws SQLException {
		List<String> columnNames = new ArrayList<>();

		if (resultSet != null) {
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
				columnNames.add(resultSetMetaData.getColumnName(i).toUpperCase());
			}
		}

		return columnNames;
	}

	@Override
	public void populate(ResultSet resultSet, Object object) throws SQLException {
		List<Field> fields = Fields.getFields(ClassUtils.getClass(object));
		List<String> columnNames = getColumnNames(resultSet);

		for (Field field : fields) {
			String columnName = Strings.toUpperCase(Persistence.getColumnName(field));

			if (columnNames.contains(columnName)) {
				Class<?> clazz = field.getType();
				Object value = getObject(resultSet, columnName, clazz);

				Fields.set(object, field, value);
			}
		}
	}

	@Override
	public Object getObject(ResultSet resultSet, int columnIndex, Class<?> clazz) throws SQLException {
		Object value = resultSet.getObject(columnIndex);
		return convertValue(value, clazz);
	}

	@Override
	public Object getObject(ResultSet resultSet, String columnName, Class<?> clazz) throws SQLException {
		Object value = resultSet.getObject(columnName);
		return convertValue(value, clazz);
	}

	protected <T> T convertValue(Object object, Class<T> clazz) {
		T result;

		try {
			result = objectMapper.convertValue(object, clazz);
		} catch (IllegalArgumentException e) {
			result = null;
		}

		return result;
	}

}
