package org.coco.topaz.persistence.data;

import java.sql.SQLException;

import org.coco.topaz.exception.ServiceException;
import org.coco.topaz.persistence.dao.ChangelogDAO;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.persistence.service.DataServiceImpl;

import static org.coco.topaz.util.Constants.Strings.Messages.ERROR_OCCURRED_ACCESSING_DATABASE;

public class ChangelogDataService extends DataServiceImpl<ChangelogEntry, Long> {

	private final ChangelogDAO dataAccessObject;

	public ChangelogDataService(ChangelogDAO dataAccessObject) {
		super(dataAccessObject);
		this.dataAccessObject = dataAccessObject;
	}

	public void execute(String sql, String resource) throws ServiceException {
		try {
			dataAccessObject.execute(sql);
		} catch (SQLException e) {
			throw new ServiceException(String.format("Could not execute '%s'", resource), e);
		}
	}

	public ChangelogEntry findByName(String name) throws ServiceException {
		try {
			return dataAccessObject.findByName(name);
		} catch (SQLException e) {
			throw new ServiceException(ERROR_OCCURRED_ACCESSING_DATABASE, e);
		}
	}

}
