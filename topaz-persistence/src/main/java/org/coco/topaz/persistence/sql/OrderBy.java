package org.coco.topaz.persistence.sql;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.Ensure;

@Getter
@Setter
public class OrderBy {

	private String column;
	private boolean descending;

	private static final String ASC = "ASC";
	private static final String DESC = "DESC";

	public OrderBy(String column, boolean descending) {
		setColumn(column);
		setDescending(descending);
	}

	public void setColumn(String column) {
		Ensure.notNull(column);
		this.column = column;
	}

	@Override
	public String toString() {
		return String.format("ORDER BY %s %s", column, descending ? DESC : ASC);
	}

}
