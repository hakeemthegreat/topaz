package org.coco.topaz.persistence.dao;

import org.coco.topaz.persistence.DataAccessObject;
import org.coco.topaz.persistence.model.ChangelogEntry;
import org.coco.topaz.persistence.sql.Condition;
import org.coco.topaz.persistence.sql.SimpleCondition;
import org.coco.topaz.persistence.sql.Where;

import java.sql.SQLException;

import static org.coco.topaz.persistence.sql.Operation.EQUALS;
import static org.coco.topaz.util.Constants.Strings.NAME;

public class ChangelogDAO extends DataAccessObject<ChangelogEntry, Long> {

	public ChangelogDAO() {
		super(ChangelogEntry.class);
	}

	public ChangelogEntry findByName(String name) throws SQLException {
		Condition condition = new SimpleCondition(NAME, EQUALS, name);
		Where where = new Where(condition);
		String sql = sqlService.select(clazz, where);
		ChangelogEntry changelogEntry = executeUnique(sql);

		return changelogEntry;
	}

}
