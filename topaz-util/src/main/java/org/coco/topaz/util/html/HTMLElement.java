package org.coco.topaz.util.html;

import lombok.Getter;
import org.coco.topaz.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HTMLElement {

	@Getter
	private final String name;

	@Getter
	private final List<HTMLElement> children = new ArrayList<>();

	private final Attributes attributes = new Attributes();
	
	private static final String STYLE = "style";
	
	public HTMLElement(String name) {
		this.name = Objects.requireNonNull(name);
	}
	
	public void addChild(HTMLElement child) {
		children.add(child);
	}

	public <T extends HTMLElement> List<T> getChildren(Class<T> clazz) {
		return CollectionUtils.filter(getChildren(), Objects.requireNonNull(clazz));
	}

	public HTMLElement getChild(int index) {
		return CollectionUtils.get(children, index);
	}
	
	public HTMLElement getFirstChild() {
		return getChild(0);
	}
	
	public void addAttribute(String name, String value) {
		attributes.add(name, value);
	}

	public <T> T getAttribute(String name) {
		return attributes.get(name);
	}
	
	public void addStyle(String name, String value) {
		Style style = attributes.get(STYLE);

		if (style == null) {
			style = new Style();
			attributes.add(STYLE, style);
		}

		style.add(name, value);
	}

	public void addText(String text) {
		TextElement textElement = new TextElement(text);
		this.children.add(textElement);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<");
		sb.append(name);
		sb.append(attributes);
		sb.append(">");
		children.forEach(sb::append);
		sb.append("</");
		sb.append(name);
		sb.append(">");
		
		return sb.toString();
	}
	
}
