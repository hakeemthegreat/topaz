package org.coco.topaz.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

	private static final File[] EMPTY_FILE_ARRAY = {};

	private FileUtils() {

	}

	public static String readAll(File file) throws IOException {
		return Streams.readAll(new BufferedReader(new FileReader(file)));
	}

	public static FileWrapper buildFileWrapper(File file, FilenameFilter filter) {
		File[] children = Replace.with(EMPTY_FILE_ARRAY).should(file.listFiles(filter)).beNull();
		FileWrapper fileWrapper = new FileWrapper();

		for (File child : children) {
			if (child.isFile()) {
				fileWrapper.addFile(child);
			}

			if (child.isDirectory()) {
				fileWrapper.addDirectory(child);
			}
		}

		return fileWrapper;
	}

	protected static List<File> getFiles(File directory) {
		List<File> files = new ArrayList<>();
		File[] children = Replace.with(EMPTY_FILE_ARRAY).should(directory.listFiles()).beNull();

		for (File child : children) {
			if (child.isFile()) {
				files.add(child);
			} else {
				files.addAll(getFiles(child));
			}
		}

		return files;
	}

	public static File newFile(String pathname) {
		return new File(pathname);
	}

}
