package org.coco.topaz.util;

import org.coco.topaz.util.exception.DuplicateKeyException;
import org.coco.topaz.util.exception.DuplicateValueException;

import java.util.HashMap;
import java.util.Map;

public class OneToOneMap<K, V> {

	private final Map<K, V> keyValueMap = new HashMap<>();
	private final Map<V, K> valueKeyMap = new HashMap<>();

	public boolean containsKey(K key) {
		return keyValueMap.containsKey(key);
	}


	public boolean containsValue(V value) {
		return keyValueMap.containsValue(value);
	}

	public void put(K key, V value) {
		ensureUniqueKey(key);
		ensureUniqueValue(value);

		keyValueMap.put(key, value);
		valueKeyMap.put(value, key);
	}

	public V getValue(K key) {
		return keyValueMap.get(key);
	}

	public K getKey(V value) {
		return valueKeyMap.get(value);
	}

	protected void ensureUniqueKey(K key) {
		if (keyValueMap.containsKey(key)) {
			String message = String.format("Key '%s' is already present in map.", key);
			throw new DuplicateKeyException(message);
		}
	}

	protected void ensureUniqueValue(V value) {
		if (valueKeyMap.containsKey(value)) {
			String message = String.format("Value '%s' is already present in map.", value);
			throw new DuplicateValueException(message);
		}
	}

}
