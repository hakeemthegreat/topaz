package org.coco.topaz.util;

import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Maps {

	private Maps() {

	}

	public static Map<String, Object> toMap(Object object) {
		return toMap(object, true);
	}

	public static Map<String, Object> toMap(Object object, boolean includeStaticFields) {
		Map<String, Object> map = new HashMap<>();
		List<Field> fields = Fields.getFields(ClassUtils.getClass(object));

		for (Field field : fields) {
			if (Reflection.isNotStatic(field) || includeStaticFields) {
				String key = field.getName();
				Object value = Fields.getValue(field, object);

				map.put(key, value);
			}
		}

		return map;
	}

}
