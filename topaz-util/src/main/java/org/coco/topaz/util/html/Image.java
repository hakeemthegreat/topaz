package org.coco.topaz.util.html;

public class Image extends HTMLElement {

	private static final String SOURCE = "src";
	
	public Image() {
		super("img");
	}

	public void setSource(String source) {
		addAttribute(SOURCE, source);
	}

	public String getSource() {
		return getAttribute(SOURCE);
	}

}
