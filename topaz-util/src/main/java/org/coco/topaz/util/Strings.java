package org.coco.topaz.util;

import java.util.*;

public class Strings {

	private static final String DELIMITER = ", ";
	
	private Strings() {
		
	}
	
	public static boolean isEmpty(String string) {
		return string == null || string.isEmpty();
	}
	
	public static boolean isNotEmpty(String string) {
		return !isEmpty(string);
	}

	public static boolean isBlank(String string) {
		boolean result = true;

		if (string != null) {
			for (int i = 0; i < string.length(); i++) {
				if (!Character.isWhitespace(string.charAt(i))) {
					result = false;
					break;
				}
			}
		}

		return result;
	}

	public static boolean isNotBlank(String string) {
		return !isBlank(string);
	}

	public static String escape(String string) {
		String escaped = null;

		if (string != null) {
			escaped = string.replace("\n", "\\n");
		}

		return escaped;
	}

	public static String toUpperCase(String string) {
		String upperCase = null;

		if (string != null) {
			upperCase = string.toUpperCase();
		}

		return upperCase;
	}

	public static List<String> toStringList(Iterable<?> objects) {
		List<String> strings = new ArrayList<>();

		if (objects != null) {
			for (Object object : objects) {
				String string = toString(object);
				strings.add(string);
			}
		}

		return strings;
	}

	public static String toCommaDelimited(Collection<?> objects) {
		return String.join(DELIMITER, toStringList(objects));
	}

	public static String toString(Object object) {
		String string = null;

		if (object != null) {
			string = object.toString();
		}

		return string;
	}

	public static String trim(String string) {
		String result = null;

		if (string != null) {
			result = string.trim();
		}

		return result;
	}

	public static String standardizeSlashes(String string) {
		String result = null;

		if (string != null) {
			result = string.replace("\\", "/");
		}

		return result;
	}

	public static String toHexString(char c) {
		return Integer.toHexString(c | 0x10000).substring(1).toUpperCase();
	}

	public static Set<Character> toCharacterSet(String string) {
		Set<Character> set = new HashSet<>();

		if (string != null) {
			for (int i = 0; i < string.length(); i++) {
				set.add(string.charAt(i));
			}
		}

		return set;
	}

	public static List<String> getWordsFromCamelCase(String string) {
		List<String> list = new ArrayList<>();

		if (string != null) {
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);

				if (Character.isUpperCase(c)) {
					list.add(sb.toString());
					sb.setLength(0);
				}

				sb.append(c);
			}

			list.add(sb.toString());
		}

		return list;
	}

	public static int count(String string, String searchFor) {
		int count = 0;

		if (string != null && searchFor != null) {
			int fromIndex = 0;
			int index = string.indexOf(searchFor, fromIndex);

			while (index != -1) {
				fromIndex = index + searchFor.length();
				index = string.indexOf(searchFor, fromIndex);
				count++;
			}
		}

		return count;
	}

	public static String replace(String string, CharSequence target, CharSequence replacement) {
		String result = null;

		if (string != null) {
			result = string.replace(target, replacement);
		}

		return result;
	}

}
