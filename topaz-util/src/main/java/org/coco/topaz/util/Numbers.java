package org.coco.topaz.util;

public class Numbers {

	private Numbers() {

	}

	public static boolean isPrime(Number number) {
		long value = number.longValue();
		boolean result = false;

		if (isNatural(number) && value > 1) {
			result = true;

			for (long i = 2 + value % 2; i <= value / 2; i += 2) {
				if (value % i == 0) {
					result = false;
					break;
				}
			}
		}

		return result;
	}

	public static boolean isEven(Number number) {
		return isWhole(number) && number.longValue() % 2 == 0;
	}

	public static boolean isOdd(Number number) {
		return isWhole(number) && number.longValue() % 2 == 1;
	}

	public static boolean isWhole(Number number) {
		return number.doubleValue() == number.longValue();
	}

	public static boolean isNatural(Number number) {
		return isWhole(number) && number.longValue() >= 1;
	}

	public static double round(double value, int decimalPlaces) {
		double modifier = Math.pow(10, decimalPlaces);
		double result;

		result = Math.round(value * modifier);
		result /= modifier;

		return result;
	}

	public static double truncate(double value, int decimalPlaces) {
		double result = value;

		result = result * Math.pow(10, decimalPlaces);
		result = Math.floor(result);

		return result / Math.pow(10, decimalPlaces);
	}

}
