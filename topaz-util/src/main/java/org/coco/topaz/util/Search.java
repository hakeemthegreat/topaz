package org.coco.topaz.util;

import java.util.List;

public class Search {

	private Search() {

	}

	public static <C extends Comparable<C>> int nearestIndexOf(List<C> list, C object) {
		int left = 0;
		int right = list.size() - 1;
		int middle = (left + right) / 2;

		while (left <= right) {
			C current = list.get(middle);
			int result = object.compareTo(current);

			if (result == 0) {
				break;
			} else if (result < 0) {
				right = middle - 1;
			} else {
				left = middle + 1;
			}

			middle = (left + right) / 2;
		}

		return middle;
	}

}
