package org.coco.topaz.util;

import java.util.Objects;
import java.util.function.Predicate;

public class NegativePredicate<T> implements Predicate<T> {

	private final Predicate<T> predicate;

	public NegativePredicate(Predicate<T> predicate) {
		this.predicate = Objects.requireNonNull(predicate);
	}

	@Override
	public boolean test(T object) {
		return !predicate.test(object);
	}

}
