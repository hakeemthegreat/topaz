package org.coco.topaz.util;

public class BooleanWrapper extends Wrapper<Boolean> {

	public BooleanWrapper(Boolean value) {
		super(value);
	}

}
