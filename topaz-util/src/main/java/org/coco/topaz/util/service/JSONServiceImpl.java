package org.coco.topaz.util.service;

import org.coco.topaz.util.Maps;
import org.coco.topaz.util.ObjectComparator;
import org.coco.topaz.util.Strings;

import java.lang.reflect.Array;
import java.util.*;

import static org.coco.topaz.util.Constants.Strings.*;

public class JSONServiceImpl implements JSONService {

	private final Map<Integer, String> cache = new HashMap<>();

	private static final String DELIMITER = ", ";
	private static final String ENTRY_FORMAT = "%s: %s";
	private static final Comparator<Object> COMPARATOR = new ObjectComparator();

	public static final String EMPTY_OBJECT = "{}";

	public String toJSON(Object object) {
		int key = System.identityHashCode(object);
		String value;

		if (cache.containsKey(key)) {
			value = cache.get(key);
		} else {
			cache.put(key, EMPTY_OBJECT);
			value = processObject(object);
			cache.replace(key, value);
		}

		return value;
	}

	protected String processObject(Object object) {
		String result;

		if (object == null) {
			result = "null";
		} else if (object.getClass().isArray()) {
			result = processArray(object);
		} else if (object instanceof Boolean) {
			result = processBoolean((Boolean) object);
		} else if (object instanceof Character) {
			result = processCharacter((Character) object);
		} else if (object instanceof CharSequence) {
			result = processCharSequence((CharSequence) object);
		} else if (object instanceof Iterable) {
			result = processIterable((Iterable<?>) object);
		} else if (object instanceof Date) {
			result = processDate((Date) object);
		} else if (object instanceof Enum) {
			result = processEnum((Enum<?>) object);
		} else if (object instanceof Number) {
			result = processNumber((Number) object);
		} else if (object instanceof Map) {
			result = processMap((Map<?, ?>) object);
		} else {
			result = processDefault(object);
		}

		return result;
	}

	protected String processCharacter(Character character) {
		return processCharSequence(character.toString());
	}

	protected String processCharSequence(CharSequence charSequence) {
		String string = charSequence.toString();

		string = string.replace("\\", "\\\\");
		string = string.replace("\"", "\\\"");

		return DOUBLE_QUOTE + string + DOUBLE_QUOTE;
	}

	protected String processNumber(Number number) {
		return number.toString();
	}

	protected String processBoolean(Boolean bool) {
		return bool.toString();
	}

	protected String processDate(Date date) {
		return String.valueOf(date.getTime());
	}

	protected String processIterable(Iterable<?> iterable) {
		List<String> values = new ArrayList<>();

		for (Object object : iterable) {
			values.add(toJSON(object));
		}

		return LEFT_BRACKET + String.join(DELIMITER, values) + RIGHT_BRACKET;
	}

	protected String processArray(Object array) {
		List<Object> list = new ArrayList<>();
		int length = Array.getLength(array);

		for (int i = 0; i < length; i++) {
			list.add(Array.get(array, i));
		}

		return processIterable(list);
	}

	protected String processDefault(Object object) {
		Map<String, Object> map = Maps.toMap(object);
		return processMap(map);
	}

	protected String processMap(Map<?, ?> map) {
		List<String> strings = new ArrayList<>();
		List<?> keys = new ArrayList<>(map.keySet());

		keys.sort(COMPARATOR);

		for (Object key : keys) {
			Object value = map.get(key);

			key = toJSON(Strings.toString(key));
			value = toJSON(value);
			strings.add(String.format(ENTRY_FORMAT, key, value));
		}

		return LEFT_BRACE + String.join(DELIMITER, strings) + RIGHT_BRACE;
	}

	protected String processEnum(Enum<?> e) {
		return DOUBLE_QUOTE + e + DOUBLE_QUOTE;
	}

}
