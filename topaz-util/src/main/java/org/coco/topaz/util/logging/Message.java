package org.coco.topaz.util.logging;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Message {

	private String id;
	private String content;
	private Logger.Classifier classifier;
	private int lineNumber;
	private Class<?> clazz;
	private Date date;
	private Throwable throwable;

}
