package org.coco.topaz.util;

public class Constants {

	private Constants() {

	}

	public static class Characters {

		private Characters() {

		}

		public static final Character ASTERISK = '*';
		public static final Character BACK_SLASH = '\\';
		public static final Character COLON = ':';
		public static final Character COMMA = ',';
		public static final Character DOUBLE_QUOTE = '"';
		public static final Character EQUALS = '=';
		public static final Character FORWARD_SLASH = '/';
		public static final Character GREATER_THAN = '>';
		public static final Character LEFT_BRACE = '{';
		public static final Character LEFT_BRACKET = '[';
		public static final Character LEFT_PARENTHESIS = '(';
		public static final Character LESS_THAN = '<';
		public static final Character NEWLINE = '\n';
		public static final Character PERIOD = '.';
		public static final Character RIGHT_BRACE = '}';
		public static final Character RIGHT_BRACKET = ']';
		public static final Character RIGHT_PARENTHESIS = ')';
		public static final Character SEMICOLON = ';';
		public static final Character SINGLE_QUOTE = '\'';
		public static final Character SPACE = ' ';
		public static final Character TAB = '\t';

	}

	public static class Numbers {

		private Numbers() {

		}

		public static class Integers {

			private Integers() {

			}

			public static final Integer NEGATIVE_ONE = -1;
			public static final Integer ZERO = 0;
			public static final Integer ONE = 1;

		}

	}

	public static class Strings {

		private Strings() {

		}

		public static final String ASTERISK = Characters.ASTERISK.toString();
		public static final String BACK_SLASH = Characters.BACK_SLASH.toString();
		public static final String CODE = "CODE";
		public static final String COLON = Characters.COLON.toString();
		public static final String COMMA = Characters.COMMA.toString();
		public static final String DOUBLE_QUOTE = Characters.DOUBLE_QUOTE.toString();
		public static final String EMPTY = "";
		public static final String EQUALS = Characters.EQUALS.toString();
		public static final String FORWARD_SLASH = Characters.FORWARD_SLASH.toString();
		public static final String GREATER_THAN = Characters.GREATER_THAN.toString();
		public static final String ID = "ID";
		public static final String LEFT_BRACE = Characters.LEFT_BRACE.toString();
		public static final String LEFT_BRACKET = Characters.LEFT_BRACKET.toString();
		public static final String LEFT_PARENTHESIS = Characters.LEFT_PARENTHESIS.toString();
		public static final String LESS_THAN = Characters.LESS_THAN.toString();
		public static final String NAME = "NAME";
		public static final String NEWLINE = Characters.NEWLINE.toString();
		public static final String NULL = null;
		public static final String PERIOD = Characters.PERIOD.toString();
		public static final String QUERY = "query";
		public static final String RIGHT_BRACE = Characters.RIGHT_BRACE.toString();
		public static final String RIGHT_BRACKET = Characters.RIGHT_BRACKET.toString();
		public static final String RIGHT_PARENTHESIS = Characters.RIGHT_PARENTHESIS.toString();
		public static final String SEMICOLON = Characters.SEMICOLON.toString();
		public static final String SINGLE_QUOTE = Characters.SINGLE_QUOTE.toString();
		public static final String SPACE = Characters.SPACE.toString();
		public static final String TAB = Characters.TAB.toString();

		public static class Messages {

			private Messages() {

			}

			public static final String ALREADY_LOGGED_IN = "You are already logged in!";
			public static final String ARGUMENT_NULL = "Argument cannot be null";
			public static final String COULD_NOT_INSTANTIATE = "Could not instantiate %s";
			public static final String COULD_NOT_CLOSE_RESOURCE = "Could not close resource";
			public static final String OBJECT_MUST_BE_EMPTY = "Object must be empty";
			public static final String OBJECT_MUST_NOT_BE_EMPTY = "Object must not be empty";
			public static final String ERROR_OCCURRED_ACCESSING_DATABASE = "An error occurred while accessing the database";
			public static final String ERROR_OCCURRED_DURING_SERVICE = "An error occurred while accessing the service";
			public static final String ERROR_WHILE_EXECUTING_STATIC_BLOCK = "Error while executing static block";
			public static final String EXPRESSION_MUST_BE_TRUE = "Expression must be true";
			public static final String STREAM_ALREADY_CLOSED = "Stream is already closed.";
			public static final String TOO_MANY_ELEMENTS = "Collection contains more than the allowed number elements";
			public static final String UNABLE_TO_PERFORM_OPERATION = "Unable to perform operation";
			public static final String UNSUPPORTED_OPERATION = "Unsupported operation";

		}

	}

}
