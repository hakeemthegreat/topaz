package org.coco.topaz.util;

import org.coco.topaz.util.reflection.Constructors;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Supplier;

public class Genesis {

	private static final Map<Class<?>, Supplier<?>> SUPPLIERS = new HashMap<>();

	static {
		putSupplier(boolean.class, Genesis::createBoolean);
		putSupplier(byte.class, Genesis::createByte);
		putSupplier(char.class, Genesis::createChar);
		putSupplier(double.class, Genesis::createDouble);
		putSupplier(float.class, Genesis::createFloat);
		putSupplier(int.class, Genesis::createInt);
		putSupplier(long.class, Genesis::createLong);
		putSupplier(Number.class, Genesis::createNumber);
		putSupplier(short.class, Genesis::createShort);

		putSupplier(Blob.class, Genesis::createBlob);
		putSupplier(Class.class, Genesis::createClass);
		putSupplier(Clob.class, Genesis::createClob);
		putSupplier(Collection.class, Genesis::createCollection);
		putSupplier(InputStream.class, Genesis::createInputStream);
		putSupplier(List.class, Genesis::createList);
		putSupplier(Map.class, Genesis::createMap);
		putSupplier(OutputStream.class, Genesis::createOutputStream);
		putSupplier(Reader.class, Genesis::createReader);
		putSupplier(Set.class, Genesis::createSet);
		putSupplier(UUID.class, Genesis::createUUID);
		putSupplier(Writer.class, Genesis::createWriter);
	}

	private Genesis() {

	}

	protected static <T> void putSupplier(Class<T> clazz, Supplier<T> supplier) {
		SUPPLIERS.put(clazz, supplier);
	}

	protected static<T> Supplier<T> getSupplier(Class<T> clazz) {
		return (Supplier<T>) SUPPLIERS.get(clazz);
	}

	public static <T> T create(Class<T> clazz) {
		T newInstance;
		Supplier<T> supplier = getSupplier(clazz);

		try {
			if (supplier != null) {
				newInstance = supplier.get();
			} else if (clazz == null) {
				newInstance = null;
			} else if (Enum.class.isAssignableFrom(clazz)) {
				newInstance = createEnum(clazz);
			} else if (clazz.isArray()) {
				newInstance = createArray(clazz.getComponentType());
			} else {
				newInstance = createViaConstructor(clazz);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to create new instance of " + clazz.getName(), e);
		}

		return newInstance;
	}

	protected static <T> T createArray(Class<?> clazz) {
		return (T) Array.newInstance(clazz, 0);
	}

	protected static <T> T createViaConstructor(Class<T> clazz) {
		List<Constructor<T>> constructors = Arrays.asList((Constructor<T>[]) clazz.getDeclaredConstructors());
		Constructor<T> constructor;
		Class<?>[] parameterTypes;
		Object[] arguments;

		constructors.sort(Comparator.comparingInt(Constructor::getParameterCount));
		constructor = constructors.get(0);
		parameterTypes = constructor.getParameterTypes();
		arguments = new Object[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			Class<?> parameterType = parameterTypes[i];
			Object argument = null;

			if (!parameterType.equals(clazz)) {
				argument = create(parameterType); //this is to prevent a StackOverflowError when a constructor takes an argument of the same class
			}

			arguments[i] = argument;
		}

		return Constructors.newInstance(constructor, arguments);
	}

	protected static Collection<?> createCollection() {
		return new LinkedList<>();
	}

	protected static List<?> createList() {
		return new ArrayList<>();
	}

	protected static Set<?> createSet() {
		return new HashSet<>();
	}

	protected static Map<?, ?> createMap() {
		return new HashMap<>();
	}

	protected static Blob createBlob() {
		try {
			return new SerialBlob(create(byte[].class));
		} catch (SQLException e) {
			throw new RuntimeException("Unable to create Blob", e);
		}
	}

	protected static Clob createClob() {
		try {
			return new SerialClob(create(char[].class));
		} catch (SQLException e) {
			throw new RuntimeException("Unable to create Clob", e);
		}
	}

	protected static Class<?> createClass() {
		return Object.class;
	}

	protected static <T> T createEnum(Class<T> clazz) {
		T[] constants = clazz.getEnumConstants();
		T result = null;

		if (constants.length > 0) {
			result = constants[0];
		}

		return result;
	}

	protected static InputStream createInputStream() {
		return create(ByteArrayInputStream.class);
	}

	protected static OutputStream createOutputStream() {
		return create(ByteArrayOutputStream.class);
	}

	protected static UUID createUUID() {
		return UUID.randomUUID();
	}

	protected static Reader createReader() {
		return create(CharArrayReader.class);
	}

	protected static Writer createWriter() {
		return create(CharArrayWriter.class);
	}

	protected static boolean createBoolean() {
		return Math.random() > 0.5;
	}

	protected static byte createByte() {
		return createNumber().byteValue();
	}

	protected static char createChar() {
		return (char) (createNumber().intValue() % 26 + 'a');
	}

	protected static double createDouble() {
		return createNumber().doubleValue();
	}

	protected static float createFloat() {
		return createNumber().floatValue();
	}

	protected static int createInt() {
		return createNumber().intValue();
	}

	protected static long createLong() {
		return createNumber().longValue();
	}

	protected static short createShort() {
		return createNumber().shortValue();
	}

	protected static Number createNumber() {
		return Math.random() * Integer.MAX_VALUE;
	}

}
