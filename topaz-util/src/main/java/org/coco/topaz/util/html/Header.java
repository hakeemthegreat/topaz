package org.coco.topaz.util.html;

public class Header extends HTMLElement {
	
	public Header() {
		super("th");
	}
	
	public Header(String text) {
		this();
		addText(text);
	}

}
