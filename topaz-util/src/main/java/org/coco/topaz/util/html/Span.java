package org.coco.topaz.util.html;

public class Span extends HTMLElement {

	public Span() {
		this("");
	}

	public Span(String text) {
		super("span");
		addText(text);
	}

}
