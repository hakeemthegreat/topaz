package org.coco.topaz.util.service;

public interface JSONService {

	String toJSON(Object object);

}
