package org.coco.topaz.util.service;

public interface CacheService {

	void put(Object key, Object value);

	<T> T get(Object key);

}
