package org.coco.topaz.util;

import org.coco.topaz.util.node.TrieNode;

import java.util.regex.Pattern;

public class Trie {

	private final TrieNode root = new TrieNode('*');

	private static final Pattern PATTERN = Pattern.compile("[a-zA-Z]+");

	public boolean add(String string) {
		boolean added = false;

		if (string != null && PATTERN.matcher(string).matches()) {
			added = proceedWithAdd(string);
		}

		return added;
	}

	protected boolean proceedWithAdd(String string) {
		TrieNode node = root;

		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			int childIndex = getIndex(c);
			TrieNode child = node.getChild(childIndex);

			if (child == null) {
				child = new TrieNode(c);
				node.setChild(childIndex, child);
			}

			node = child;
		}

		return true;
	}

	public boolean contains(Object object) {
		if (object instanceof String) {
			return contains((String) object);
		} else {
			return false;
		}
	}

	public boolean contains(String string) {
		StringBuilder stringBuilder = new StringBuilder();
		TrieNode node = root;

		if (string != null) {
			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);
				int childIndex = getIndex(c);
				TrieNode child = node.getChild(childIndex);

				if (child == null) {
					break;
				} else {
					char value = child.getValue();
					stringBuilder.append(value);
				}

				node = child;
			}
		}

		return stringBuilder.toString().equalsIgnoreCase(string);
	}

	public int size() {
		boolean hasChildren = false;

		for (int i = 0; i < TrieNode.CAPACITY; i++) {
			if (root.getChild(i) != null) {
				hasChildren = true;
				break;
			}
		}

		return hasChildren ? root.size() : 0;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public boolean isNotEmpty() {
		return !isEmpty();
	}

	public void clear() {
		for (int i = 0; i < TrieNode.CAPACITY; i++) {
			root.setChild(i, null);
		}
	}

	@Override
	public String toString() {
		return root.toString();
	}

	protected int getIndex(char c) {
		return Character.toLowerCase(c) - 'a';
	}

}
