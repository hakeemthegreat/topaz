package org.coco.topaz.util.service;

import org.coco.topaz.util.exception.CloneException;

public interface CloneService {

	String UNABLE_TO_CLONE_MESSAGE = "Unable to clone object";

	<T> T clone(T object) throws CloneException;

}
