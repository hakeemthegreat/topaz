package org.coco.topaz.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileWrapper {

	private final List<File> directories = new ArrayList<>();
	private final List<File> files = new ArrayList<>();

	public List<File> getDirectories() {
		return directories;
	}

	public void addDirectory(File directory) {
		Ensure.notNull(directory);
		Ensure.isTrue(directory.isDirectory());
		getDirectories().add(directory);
	}

	public List<File> getFiles() {
		return files;
	}

	public void addFile(File file) {
		Ensure.notNull(file);
		Ensure.isTrue(file.isFile());
		getFiles().add(file);
	}

}
