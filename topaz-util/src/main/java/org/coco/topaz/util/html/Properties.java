package org.coco.topaz.util.html;

import org.coco.topaz.util.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class Properties {
	
	private final Map<String, Object> map = new HashMap<>();
	
	public void add(String name, Object value) {
		map.put(name, value);
	}
	
	public <T> T get(String name) {
		return (T) map.get(name);
	}
	
	abstract String getFormat();

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String entryFormat = getFormat();
		
		for (Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = String.valueOf(entry.getValue()).replace(Constants.Characters.DOUBLE_QUOTE, Constants.Characters.SINGLE_QUOTE);
			String property = String.format(entryFormat, key, value);
			
			sb.append(property);
		}
		
		return sb.toString();
	}

}
