package org.coco.topaz.util.node;

public class TrieNode extends Node {

	private final TrieNode[] children = new TrieNode[CAPACITY];

	public static final int CAPACITY = 26;

	public TrieNode(char value) {
		super(value);
	}

	public void setChild(int index, TrieNode node) {
		children[index] = node;
	}

	public TrieNode getChild(int index) {
		return children[index];
	}


	public int size() {
		return sizeRecursive(this);
	}

	protected int sizeRecursive(TrieNode node) {
		int result = 0;

		for (TrieNode child : node.children) {
			if (child != null) {
				result += sizeRecursive(child);
			}
		}

		if (result == 0) { //node has no children so return 1 as the size
			result = 1;
		}

		return result;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for (TrieNode child : children) {
			if (child != null) {
				char value = child.getValue();
				stringBuilder.append(value);
			}
		}

		return getValue() + " -> " + stringBuilder;
	}

}
