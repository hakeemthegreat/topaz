package org.coco.topaz.util.service;

public interface HashService extends Service {

	int hash(Object object);

	String hashToHex(Object object);

}
