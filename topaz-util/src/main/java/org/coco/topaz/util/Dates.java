package org.coco.topaz.util;

import org.coco.topaz.util.logging.Logger;
import org.coco.topaz.util.logging.Loggers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Date;
import java.util.TimeZone;

public class Dates {

	public static final String DATE_FORMAT1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String DATE_FORMAT2 = "yyyy-MM-dd HH:mm";
	public static final String DATE_FORMAT3 = "yyyy-MM-dd";
	public static final String DATE_FORMAT4 = "yyyyMMdd";
	private static final List<String> DATE_FORMATS = Arrays.asList(DATE_FORMAT1 ,DATE_FORMAT2, DATE_FORMAT3, DATE_FORMAT4);
	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");
	private static final String PARSE_ERROR_TEMPLATE = "Could not parse '%s' with %s format";

	protected static Logger logger = Loggers.getLogger(Dates.class);

	private Dates() {

	}

	public static Date parseDate(String source) throws ParseException {
		Date result = null;

		if (source != null) {
			for (String dateFormat : DATE_FORMATS) {
				try {
					result = parseDate(source, dateFormat);
					break;
				} catch (Exception e) {
					String message = String.format(PARSE_ERROR_TEMPLATE, source, dateFormat);
					logger.debug(message, e);
				}
			}

			if (result == null) {
				String message = String.format(PARSE_ERROR_TEMPLATE, source, "any");
				throw new ParseException(message, -1);
			}
		}

		return result;
	}

	public static Date parseDate(String source, String format) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

		simpleDateFormat.setTimeZone(UTC);
		return simpleDateFormat.parse(source);
	}

}
