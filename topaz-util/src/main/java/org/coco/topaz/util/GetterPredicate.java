package org.coco.topaz.util;

import org.coco.topaz.util.Ensure;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class GetterPredicate implements Predicate<Method> {

	private static final List<Class<?>> RETURN_TYPES = Arrays.asList(void.class, Void.class);

	@Override
	public boolean test(Method method) {
		Ensure.notNull(method);
		boolean result = true;

		if (RETURN_TYPES.contains(method.getReturnType())) {
			result = false;
		} else if (method.getParameterCount() != 0) {
			result = false;
		}

		return result;
	}

}
