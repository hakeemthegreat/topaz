package org.coco.topaz.util;

import org.coco.topaz.util.Constants.Strings.Messages;

import java.util.Collection;
import java.util.Objects;

import static java.lang.Boolean.TRUE;

public class Ensure {

	private Ensure() {

	}

	public static void notNull(Object object) {
		Ensure.notNull(object, Messages.ARGUMENT_NULL);
	}

	public static void notNull(Object object, String message) {
		if (object == null) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isTrue(Boolean b) {
		if (!TRUE.equals(b)) {
			throw new IllegalArgumentException(Messages.EXPRESSION_MUST_BE_TRUE);
		}
	}

	public static void empty(Object object) {
		if (ObjectUtils.isNotEmpty(object)) {
			throw new IllegalArgumentException(Messages.OBJECT_MUST_BE_EMPTY);
		}
	}

	public static void notEmpty(Object object) {
		if (ObjectUtils.isEmpty(object)) {
			throw new IllegalArgumentException(Messages.OBJECT_MUST_NOT_BE_EMPTY);
		}
	}

	public static void hasAtMost(Collection<?> collection, int limit) {
		Ensure.notNull(collection);

		if (collection.size() > limit) {
			throw new IllegalArgumentException(Messages.TOO_MANY_ELEMENTS);
		}
	}

	public static void areEqual(Object object1, Object object2) {
		if (!Objects.equals(object1, object2)) {
			throw new IllegalArgumentException(object1 + ", " + object2);
		}
	}

}
