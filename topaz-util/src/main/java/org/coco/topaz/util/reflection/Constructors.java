package org.coco.topaz.util.reflection;

import java.lang.reflect.Constructor;
import java.util.*;

public class Constructors {

	private static final Map<Class<?>, List<Constructor<?>>> CONSTRUCTORS = new HashMap<>();

	private Constructors() {

	}

	public static <T> T newInstance(Constructor<T> constructor, Object... arguments) {
		T instance = null;

		if (constructor != null) {
			try {
				constructor.setAccessible(true);
				instance = constructor.newInstance(arguments);
			} catch (ReflectiveOperationException e) {
				throw new RuntimeException("Error while invoking " + constructor, e);
			}
		}

		return instance;
	}

	public static <T> Constructor<T> getDefaultConstructor(Class<T> clazz) {
		Constructor<T> result = null;

		if (clazz != null) {
			Constructor<T>[] constructors = (Constructor<T>[]) clazz.getDeclaredConstructors();

			for (Constructor<T> constructor : constructors) {
				if (constructor.getParameterCount() == 0) {
					result = constructor;
					break;
				}
			}
		}

		return result;
	}

	public static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameterTypes) {
		Constructor<T> constructor = null;

		try {
			constructor = clazz.getConstructor(parameterTypes);
		} catch (NoSuchMethodException e) {
			//continue and return null
		}

		return constructor;
	}

	public static List<Constructor<?>> getConstructors(Class<?> clazz) {
		List<Constructor<?>> constructors = CONSTRUCTORS.get(clazz);

		if (constructors == null) {
			constructors = createConstructorList(clazz);
			CONSTRUCTORS.put(clazz, constructors);
		}

		return constructors;
	}

	protected static List<Constructor<?>> createConstructorList(Class<?> clazz) {
		List<Constructor<?>> constructors = new ArrayList<>();

		Collections.addAll(constructors, clazz.getDeclaredConstructors());
		return Collections.unmodifiableList(constructors);
	}

}
