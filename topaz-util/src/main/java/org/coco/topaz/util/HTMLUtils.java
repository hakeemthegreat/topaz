package org.coco.topaz.util;

import org.coco.topaz.util.html.Cell;
import org.coco.topaz.util.html.Header;
import org.coco.topaz.util.html.Row;
import org.coco.topaz.util.html.Table;
import org.coco.topaz.util.reflection.Fields;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

public class HTMLUtils {
	
	private HTMLUtils() {
		
	}
	
	public static Table toTable(Collection<?> collection) {
		Table table = new Table();
		Object first = collection.stream().findFirst().orElse(null);
		List<Field> fields = Fields.getFields(ClassUtils.getClass(first));
		Row headers = new Row();

		fields.forEach(field -> headers.addChild(new Header(field.getName().toUpperCase())));
		table.addChild(headers);

		for (Object object : collection) {
			Row row = toRow(object, fields);
			table.addChild(row);
		}
		
		return table;
	}
	
	protected static Row toRow(Object object, Collection<Field> fields) {
		Row row = null;
		
		if (object != null) {
			row = new Row();
			
			for (Field field : fields) {
				Object value = Fields.getValue(field, object);
				String text = Strings.toString(value);
				Cell cell = toCell(text);

				row.addChild(cell);
			}
		}
		
		return row;
	}

	protected static Cell toCell(String text) {
		return new Cell(text);
	}
	
}
