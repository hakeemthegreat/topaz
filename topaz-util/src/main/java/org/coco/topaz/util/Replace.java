package org.coco.topaz.util;

import java.util.*;

public class Replace {

	private final Object replacement;
	private Object originalValue;

	private Replace(Object replacement) {
		this.replacement = replacement;
	}

	public static Replace with(Object replacement) {
		return new Replace(replacement);
	}

	public Replace should(Object originalValue) {
		this.originalValue = originalValue;
		return this;
	}

	public <T> T beEqualTo(Object value) {
		Object result = originalValue;

		if (Objects.equals(this.originalValue, value)) {
			result = replacement;
		}

		return (T) result;
	}

	public <T> T be(Object value) {
		Object result = originalValue;

		if (this.originalValue == value) {
			result = replacement;
		}

		return (T) result;
	}

	public <T> T beNull() {
		return be(null);
	}

	public <T> T beLessThan(Number number) {
		Object result = originalValue;

		if (compare(originalValue, number) < 0) {
			result = replacement;
		}

		return (T) result;
	}

	public <T> T beGreaterThan(Number number) {
		Object result = originalValue;

		if (compare(originalValue, number) > 0) {
			result = replacement;
		}

		return (T) result;
	}

	protected static int compare(Object value, Number number) {
		int result;

		if (value instanceof Number) {
			Comparable<Number> valueAsNumber = (Comparable<Number>) value;
			result = valueAsNumber.compareTo(number);
		} else {
			String message = String.valueOf(value);
			throw new IllegalArgumentException(message);
		}

		return result;
	}

}
