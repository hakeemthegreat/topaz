package org.coco.topaz.util.service;

import org.coco.topaz.util.Resources;

import java.io.*;

public class SerializingCloneService implements CloneService {

	@Override
	public <T> T clone(T object) {
		T clone = null;

		if (object != null) {
			try {
				byte[] bytes = serialize(object);
				clone = (T) deserialize(bytes);
			} catch (IOException | ClassNotFoundException e) {
				throw new RuntimeException(UNABLE_TO_CLONE_MESSAGE, e);
			}
		}

		return clone;
	}

	protected byte[] serialize(Object object) throws IOException {
		byte[] bytes;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = null;

		try {
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			bytes = byteArrayOutputStream.toByteArray();
		} finally {
			Resources.close(objectOutputStream);
		}

		return bytes;
	}

	protected Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		Object object;
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		ObjectInputStream objectInputStream = null;

		try {
			objectInputStream = new ObjectInputStream(byteArrayInputStream);
			object = objectInputStream.readObject();
		} finally {
			Resources.close(objectInputStream);
		}

		return object;
	}

}
