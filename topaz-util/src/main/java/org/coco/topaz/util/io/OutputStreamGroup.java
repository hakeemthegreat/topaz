package org.coco.topaz.util.io;

import org.coco.topaz.util.Exceptions;
import org.coco.topaz.util.Resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class OutputStreamGroup extends OutputStream {

	private final List<OutputStream> outputStreams = new ArrayList<>();

	public OutputStreamGroup(OutputStream... outputStreams) {
		for (OutputStream outputStream : outputStreams) {
			if (outputStream != null) {
				this.outputStreams.add(outputStream);
			}
		}
	}

	@Override
	public void write(int b) throws IOException {
		for (OutputStream outputStream : outputStreams) {
			outputStream.write(b);
		}
	}

	@Override
	public void write(byte[] b) throws IOException {
		for (OutputStream outputStream : outputStreams) {
			outputStream.write(b);
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		for (OutputStream outputStream : outputStreams) {
			outputStream.write(b, off, len);
		}
	}

	@Override
	public void flush() throws IOException {
		for (OutputStream outputStream : outputStreams) {
			outputStream.flush();
		}
	}

	@Override
	public void close() throws IOException {
		List<Exception> exceptions = Resources.close(outputStreams);

		if (!exceptions.isEmpty()) {
			Exception exception = exceptions.get(0);
			throw Exceptions.toIOException(exception);
		}
	}

}
