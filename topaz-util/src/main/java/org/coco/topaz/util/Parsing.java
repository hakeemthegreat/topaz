package org.coco.topaz.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Parsing {

	private String source;
	private Class<?> target;

	private static final Parsing INSTANCE = new Parsing();
	private static final Map<Class<?>, Function<String, ?>> FUNCTIONS = new HashMap<>();

	static {
		FUNCTIONS.put(Boolean.class, Boolean::valueOf);
		FUNCTIONS.put(Byte.class, Byte::valueOf);
		FUNCTIONS.put(Double.class, Double::valueOf);
		FUNCTIONS.put(Float.class, Float::valueOf);
		FUNCTIONS.put(Integer.class, Integer::valueOf);
		FUNCTIONS.put(Long.class, Long::valueOf);
		FUNCTIONS.put(Short.class, Short::valueOf);
	}

	private Parsing() {

	}

	public static Parsing getInstance() {
		return INSTANCE;
	}

	public Parsing from(String source) {
		this.source = source;
		return this;
	}

	public Parsing to(Class<?> target) {
		if (!FUNCTIONS.containsKey(target)) {
			throw new IllegalArgumentException(String.valueOf(target));
		}

		this.target = target;
		return this;
	}

	public <T> T now() {
		Function<String, T> function = (Function<String, T>) FUNCTIONS.get(target);
		T result = null;

		if (source != null) {
			result = function.apply(source);
		}

		return result;
	}

}
