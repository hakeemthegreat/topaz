package org.coco.topaz.util.test;

import org.coco.topaz.util.Genesis;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Methods;
import org.coco.topaz.util.reflection.Signature;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

public abstract class GetterSetterTest<T> {

	protected final T instance;
	private final Class<?> clazz;
	private final List<Field> fields;
	private final List<Method> methods;

	private static final String IS = "is";
	private static final String GET = "get";
	private static final String SET = "set";

	public GetterSetterTest(T instance) {
		this.instance = Objects.requireNonNull(instance);
		this.clazz = instance.getClass();
		this.fields = Fields.getFields(clazz);
		this.methods = Methods.getMethods(clazz);
	}

	@Test
	public void run() {
		int testsRun = 0;

		for (Field field : fields) {
			Method getter = findGetter(field);
			Method setter = findSetter(field);

			if (getter != null && setter != null) {
				Object input = Genesis.create(field.getType());
				Object output;

				Methods.invoke(instance, setter, input);
				output = Methods.invoke(instance, getter);
				assertEquals(input, output);
				testsRun++;
			}
		}

		if (testsRun == 0) {
			throw new IllegalArgumentException(clazz + " has no getters or setters to test!");
		}
	}

	protected Method findGetter(Field field) {
		Signature signature = newSignature(field.getType(), GET + field.getName());
		Method getter = findMatchingSignature(signature);

		if (getter == null) {
			signature = newSignature(field.getType(), IS + field.getName());
			getter = findMatchingSignature(signature);
		}

		return getter;
	}

	protected Method findSetter(Field field) {
		Signature signature = newSignature(void.class, SET + field.getName(), field.getType());
		return findMatchingSignature(signature);
	}

	protected Method findMatchingSignature(Signature toFind) {
		Method result = null;

		for (Method method : methods) {
			Signature signature = newSignature(method);

			if (toFind.equals(signature)) {
				result = method;
				break;
			}
		}

		return result;
	}

	protected Signature newSignature(Method method) {
		return newSignature(method.getReturnType(), method.getName(), method.getParameterTypes());
	}

	protected Signature newSignature(Class<?> returnType, String name, Class<?>... parameterTypes) {
		return new Signature(returnType, name.toUpperCase(), parameterTypes);
	}

}
