package org.coco.topaz.util.reflection;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Signature {

	private Class<?> returnType;
	private String name;
	private Class<?>[] parameterTypes;

	public Signature(Class<?> returnType, String name, Class<?>... parameterTypes) {
		this.returnType = Objects.requireNonNull(returnType);
		this.name = Objects.requireNonNull(name);
		this.parameterTypes = Objects.requireNonNull(parameterTypes);
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;

		if (object instanceof Signature) {
			Signature that = (Signature) object;
			List<Object> myFields = getFieldsAsList(this);
			List<Object> theirFields = getFieldsAsList(that);

			result = myFields.equals(theirFields);
		}

		return result;
	}

	protected List<Object> getFieldsAsList(Signature signature) {
		List<Object> myObjects = new ArrayList<>();

		myObjects.add(signature.returnType);
		myObjects.add(signature.name);
		Collections.addAll(myObjects, signature.parameterTypes);

		return myObjects;
	}

	@Override
	public int hashCode() {
		return returnType.hashCode() * name.hashCode() * Arrays.hashCode(parameterTypes);
	}

	@Override
	public String toString() {
		List<String> strings = new ArrayList<>();

		for (Class<?> parameterType : parameterTypes) {
			strings.add(parameterType.getSimpleName());
		}

		return String.format("%s %s(%s)", returnType.getSimpleName(), name, String.join(", ", strings));
	}
}
