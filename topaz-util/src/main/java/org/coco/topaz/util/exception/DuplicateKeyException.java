package org.coco.topaz.util.exception;

import java.util.Objects;

public class DuplicateKeyException extends IllegalArgumentException {

	public DuplicateKeyException(String message) {
		super(Objects.requireNonNull(message));
	}

}
