package org.coco.topaz.util.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class FileLogger extends PrintStreamLogger {

	public FileLogger(Class<?> clazz, File file) throws FileNotFoundException {
		super(clazz, new PrintStream(new FileOutputStream(file)));
	}

}
