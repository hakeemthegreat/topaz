package org.coco.topaz.util.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.Genesis;
import org.coco.topaz.util.exception.CloneException;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.List;

public class R2CloneService implements CloneService {

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public <T> T clone(T object) throws CloneException {
		Object clone = null;

		if (object != null) {
			if (object.getClass() == String.class) {
				clone = cloneString((String) object);
			} else if (ClassUtils.isNativeJavaClass(object.getClass())) {
				clone = cloneSimpleObject(object);
			} else if (object.getClass().isArray()) {
				clone = cloneArray(object);
			} else {
				clone = cloneComplexObject(object);
			}
		}

		return (T) clone;
	}

	@SuppressWarnings("StringOperationCanBeSimplified")
	protected String cloneString(String string) {
		return new String(string);
	}

	protected <T> T cloneSimpleObject(T object) {
		Class<T> clazz = (Class<T>) object.getClass();
		return objectMapper.convertValue(object, clazz);
	}

	protected Object cloneArray(Object array) throws CloneException {
		int length = Array.getLength(array);
		Object clone = Array.newInstance(array.getClass().getComponentType(), length);

		for (int i = 0; i < length; i++) {
			Object value = Array.get(array, i);
			Array.set(clone, i, this.clone(value));
		}

		return clone;
	}

	protected <T> T cloneComplexObject(T object) throws CloneException {
		T clone = (T) Genesis.create(object.getClass());

		cloneAndSetFields(object, clone);
		return clone;
	}
	
	protected <T> void cloneAndSetFields(T source, T target) throws CloneException {
		List<Field> fields = Fields.getFields(source.getClass());

		for (Field field : fields) {
			if (Reflection.isNotStatic(field)) {
				Object value = Fields.getValue(field, source);
				Object clonedValue = clone(value);

				Fields.set(target, field, clonedValue);
			}
		}
	}

}
