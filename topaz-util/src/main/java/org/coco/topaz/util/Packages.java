package org.coco.topaz.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;

import static org.coco.topaz.util.Constants.Strings.*;

public class Packages {

	private static final String CLASS_EXTENSION = ".class";

	private Packages() {

	}

	public static List<Class<?>> getClasses(Package p) {
		List<Class<?>> classes = new ArrayList<>();

		if (p != null) {
			classes = getClasses(p.getName());
		}

		return classes;
	}

	public static List<Class<?>> getClasses(String packageName) {
		try {
			return getClasses0(packageName);
		} catch (IOException | ClassNotFoundException e) {
			throw new RuntimeException("Unable to get classes for " + packageName, e);
		}
	}

	protected static List<Class<?>> getClasses0(String packageName) throws IOException, ClassNotFoundException {
		ClassLoader classLoader = Packages.class.getClassLoader();
		String packageNameWithSlashes = packageName.replace(PERIOD, FORWARD_SLASH);
		Enumeration<URL> resources = classLoader.getResources(packageNameWithSlashes);
		List<Class<?>> classes = new ArrayList<>();

		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			File directory = new File(resource.getFile());
			List<File> files = FileUtils.getFiles(directory);

			for (File file : files) {
				String className = file.getAbsolutePath().replace(BACK_SLASH, FORWARD_SLASH);
				int startIndex = className.indexOf(packageNameWithSlashes);
				int endIndex = className.lastIndexOf(CLASS_EXTENSION);
				Class<?> clazz;

				className = className.substring(startIndex, endIndex);
				className = className.replace(FORWARD_SLASH, PERIOD);
				clazz = Class.forName(className);
				classes.add(clazz);
			}
		}

		return classes;
	}

	public static Package getParentPackage(Package p) {
		String parentPackageName = getParentPackageName(p);
		Package parent = Package.getPackage(parentPackageName);

		return parent;
	}

	public static String getParentPackageName(Package p) {
		String parentPackageName = null;

		if (p != null) {
			parentPackageName = getParentPackageName(p.getName());
		}

		return parentPackageName;
	}

	public static String getParentPackageName(String packageName) {
		int endIndex = packageName.lastIndexOf(".");
		String parentPackageName = null;

		if (endIndex > -1) {
			parentPackageName = packageName.substring(0, endIndex);
		}

		return parentPackageName;
	}

}
