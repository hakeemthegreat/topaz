package org.coco.topaz.util.test;

import org.coco.topaz.util.*;
import org.coco.topaz.util.reflection.Methods;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public abstract class AbstractFactoryTest {

	private final Object instance;

	private final List<Method> methods;

	public AbstractFactoryTest(Object instance) {
		Ensure.notNull(instance);
		Class<?> clazz = instance.getClass();
		NegativePredicate<Method> predicate = new NegativePredicate<>(new GetterPredicate());

		this.instance = instance;
		this.methods = CollectionUtils.toList(clazz.getMethods());
		this.methods.removeIf(predicate);
	}

	@Test
	public void test_Methods_That_Return_Something_And_Have_Zero_Parameters() {
		boolean factoryMethodsFound = false;

		methods.removeIf(method -> method.getDeclaringClass() == Object.class);

		for (Method method : methods) {
			Object result = Methods.invoke(instance, method);

			assertNotNull(String.format("%s returned null", method), result);
			factoryMethodsFound = true;
		}

		if (!factoryMethodsFound) {
			fail("This factory has no methods.");
		}
	}

}
