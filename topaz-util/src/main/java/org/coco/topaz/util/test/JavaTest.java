package org.coco.topaz.util.test;

import org.hamcrest.core.IsEqual;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.util.Objects;

public abstract class JavaTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	public void expect(Class<? extends Throwable> type) {
		expect(type, null, null);
	}


	public void expect(Class<? extends Throwable> type, String message) {
		expect(type, message, null);
	}


	public void expect(Class<? extends Throwable> type, Throwable cause) {
		expect(type, null, cause);
	}

	public void expect(Class<? extends Throwable> type, String message, Throwable cause) {
		expectedException.expect(Objects.requireNonNull(type));

		if (message != null) {
			expectedException.expectMessage(message);
		}

		expectedException.expectCause(IsEqual.equalTo(cause));
	}

}
