package org.coco.topaz.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;

public class IteratorEnumeration<E> implements Enumeration<E> {

	private final Iterator<E> iterator;

	public IteratorEnumeration(Iterator<E> iterator) {
		this.iterator = Objects.requireNonNull(iterator);
	}

	@Override
	public boolean hasMoreElements() {
		return iterator.hasNext();
	}

	@Override
	public E nextElement() {
		return iterator.next();
	}

}
