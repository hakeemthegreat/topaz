package org.coco.topaz.util.logging;

public class ConsoleLogger implements Logger {

	private final SystemOutLogger systemOutLogger;
	private final SystemErrLogger systemErrLogger;

	public ConsoleLogger(Class<?> clazz) {
		this.systemOutLogger = new SystemOutLogger(clazz);
		this.systemErrLogger = new SystemErrLogger(clazz);
	}

	@Override
	public void trace(String message) {
		systemOutLogger.trace(message);
	}

	@Override
	public void trace(String message, Throwable throwable) {
		systemOutLogger.trace(message, throwable);
	}

	@Override
	public void debug(String message) {
		systemOutLogger.debug(message);
	}

	@Override
	public void debug(String message, Throwable throwable) {
		systemOutLogger.debug(message, throwable);
	}

	@Override
	public void info(String message) {
		systemOutLogger.info(message);
	}

	@Override
	public void info(String message, Throwable throwable) {
		systemOutLogger.info(message, throwable);
	}

	@Override
	public void warn(String message) {
		systemErrLogger.warn(message);
	}

	@Override
	public void warn(String message, Throwable throwable) {
		systemErrLogger.warn(message, throwable);
	}

	@Override
	public void error(String message) {
		systemErrLogger.error(message);
	}

	@Override
	public void error(String message, Throwable throwable) {
		systemErrLogger.error(message, throwable);
	}

	@Override
	public void fatal(String message) {
		systemErrLogger.fatal(message);
	}

	@Override
	public void fatal(String message, Throwable throwable) {
		systemErrLogger.fatal(message, throwable);
	}

}
