package org.coco.topaz.util.test;

import org.coco.topaz.util.ImmutableDate;

import java.util.*;

public class Dummy {
	
	private Dummy() {
		
	}
	
	public static final String STRING = stringValue();
	public static final String STRING2 = stringValue();
	public static final String STRING3 = stringValue();
	public static final String STRING4 = stringValue();
	public static final String STRING5 = stringValue();
	public static final String STRING6 = stringValue();
	public static final String STRING7 = stringValue();

	public static String stringValue() {
		StringBuilder stringBuilder = new StringBuilder();

		for (int i = 0; i < 16; i++) {
			stringBuilder.append(charValue());
		}

		return stringBuilder.toString();
	}

	public static String[] stringArray() {
		return new String[] {STRING, STRING2, STRING3, STRING4, STRING5, STRING6, STRING7};
	}

	public static List<String> stringList() {
		List<String> list = new ArrayList<>();
		Collections.addAll(list, stringArray());

		return list;
	}

	public static Set<String> stringSet() {
		return new HashSet<>(stringList());
	}

	public static final Character CHAR = charValue();
	public static final Character CHAR2 = charValue();
	public static final Character CHAR3 = charValue();
	public static final Character CHAR4 = charValue();
	public static final Character CHAR5 = charValue();
	public static final Character CHAR6 = charValue();
	public static final Character CHAR7 = charValue();

	public static Character charValue() {
		return (char) (intValue() % 26 + 'a');
	}

	public static Character[] characterArray() {
		return new Character[] {CHAR, CHAR2, CHAR3, CHAR4, CHAR5, CHAR6, CHAR7};
	}

	public static List<Character> characterList() {
		List<Character> list = new ArrayList<>();
		Collections.addAll(list, characterArray());

		return list;
	}

	public static Set<Character> characterSet() {
		return new HashSet<>(characterList());
	}
	
	public static final Object OBJECT = objectValue();
	public static final Object OBJECT2 = objectValue();
	public static final Object OBJECT3 = objectValue();
	public static final Object OBJECT4 = objectValue();
	public static final Object OBJECT5 = objectValue();
	public static final Object OBJECT6 = objectValue();
	public static final Object OBJECT7 = objectValue();

	public static Object objectValue() {
		return new Object();
	}

	public static Object[] objectArray() {
		return new Object[] {OBJECT, OBJECT2, OBJECT3, OBJECT4, OBJECT5, OBJECT6, OBJECT7};
	}

	public static List<Object> objectList() {
		List<Object> list = new ArrayList<>();
		Collections.addAll(list, objectArray());
		return list;
	}

	public static Set<Object> objectSet() {
		return new HashSet<>(objectList());
	}
	
	public static final Long LONG = longValue();
	public static final Long LONG2 = longValue();
	public static final Long LONG3 = longValue();
	public static final Long LONG4 = longValue();
	public static final Long LONG5 = longValue();
	public static final Long LONG6 = longValue();
	public static final Long LONG7 = longValue();

	public static Long longValue() {
		return (long) (Math.random() * Long.MAX_VALUE);
	}

	public static Long[] longArray() {
		return new Long[] {LONG, LONG2, LONG3, LONG4, LONG5, LONG6, LONG7};
	}

	public static List<Long> longList() {
		List<Long> list = new ArrayList<>();
		Collections.addAll(list, longArray());

		return list;
	}

	public static Set<Long> longSet() {
		return new HashSet<>(longList());
	}

	public static final Integer INT = intValue();
	public static final Integer INT2 = intValue();
	public static final Integer INT3 = intValue();
	public static final Integer INT4 = intValue();
	public static final Integer INT5 = intValue();
	public static final Integer INT6 = intValue();
	public static final Integer INT7 = intValue();

	public static Integer intValue() {
		return (int) (Math.random() * Integer.MAX_VALUE);
	}

	public static Integer[] integerArray() {
		return new Integer[] {INT, INT2, INT3, INT4, INT5, INT6, INT7};
	}

	public static List<Integer> integerList() {
		List<Integer> list = new ArrayList<>();
		Collections.addAll(list, integerArray());

		return list;
	}

	public static Set<Integer> integerSet() {
		return new HashSet<>(integerList());
	}

	public static final Double DOUBLE = doubleValue();
	public static final Double DOUBLE2 = doubleValue();
	public static final Double DOUBLE3 = doubleValue();
	public static final Double DOUBLE4 = doubleValue();
	public static final Double DOUBLE5 = doubleValue();
	public static final Double DOUBLE6 = doubleValue();
	public static final Double DOUBLE7 = doubleValue();

	public static Double doubleValue() {
		return Math.random() * Double.MAX_VALUE;
	}

	public static Double[] doubleArray() {
		return new Double[] {DOUBLE, DOUBLE2, DOUBLE3, DOUBLE4, DOUBLE5, DOUBLE6, DOUBLE7};
	}

	public static List<Double> doubleList() {
		List<Double> list = new ArrayList<>();
		Collections.addAll(list, doubleArray());

		return list;
	}

	public static Set<Double> doubleSet() {
		return new HashSet<>(doubleList());
	}

	public static final Date DATE = dateValue();
	public static final Date DATE2 = dateValue();
	public static final Date DATE3 = dateValue();
	public static final Date DATE4 = dateValue();
	public static final Date DATE5 = dateValue();
	public static final Date DATE6 = dateValue();
	public static final Date DATE7 = dateValue();

	public static Date dateValue() {
		return new ImmutableDate(longValue());
	}

	public static Date[] dateArray() {
		return new Date[] {DATE, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7};
	}

	public static List<Date> dateList() {
		List<Date> list = new ArrayList<>();
		Collections.addAll(list, dateArray());

		return list;
	}

	public static Set<Date> dateSet() {
		return new HashSet<>(dateList());
	}

	public static final Byte BYTE = byteValue();
	public static final Byte BYTE2 = byteValue();
	public static final Byte BYTE3 = byteValue();
	public static final Byte BYTE4 = byteValue();
	public static final Byte BYTE5 = byteValue();
	public static final Byte BYTE6 = byteValue();
	public static final Byte BYTE7 = byteValue();

	public static Byte byteValue() {
		return (byte) (Math.random() * Byte.MAX_VALUE);
	}

	public static Byte[] byteArray() {
		return new Byte[] {BYTE, BYTE2, BYTE3, BYTE4, BYTE5, BYTE6, BYTE7};
	}

	public static List<Byte> byteList() {
		List<Byte> list = new ArrayList<>();
		Collections.addAll(list, byteArray());

		return list;
	}

	public static Set<Byte> byteSet() {
		return new HashSet<>(byteList());
	}

}
