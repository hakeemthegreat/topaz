package org.coco.topaz.util;

import org.coco.topaz.util.Constants.Strings.Messages;
import org.coco.topaz.util.reflection.Constructors;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;

public class Exceptions {

	private Exceptions() {

	}

	public static IOException toIOException(Exception cause) {
		return toException(cause, IOException.class);
	}

	public static <T extends Exception> T toException(Exception exception, Class<T> desiredClass) {
		T result;

		if (desiredClass.isInstance(exception)) {
			result = desiredClass.cast(exception);
		} else {
			Constructor<T> constructor = Constructors.getConstructor(desiredClass, String.class, Throwable.class);
			result =  Constructors.newInstance(constructor, Messages.UNABLE_TO_PERFORM_OPERATION, exception);
		}

		return result;
	}

	public static String getStackTrace(Exception exception) {
		CharArrayWriter charArrayWriter = new CharArrayWriter();
		PrintWriter printWriter = new PrintWriter(charArrayWriter);
		String stackTrace;

		try {
			exception.printStackTrace(printWriter);
			stackTrace = charArrayWriter.toString();
		} finally {
			printWriter.close();
		}

		return stackTrace;
	}

}
