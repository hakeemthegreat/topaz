package org.coco.topaz.util.exception;

import java.util.Objects;

public class DuplicateValueException extends IllegalArgumentException {

	public DuplicateValueException(String message) {
		super(Objects.requireNonNull(message));
	}

}
