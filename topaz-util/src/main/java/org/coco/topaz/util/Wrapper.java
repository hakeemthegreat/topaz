package org.coco.topaz.util;

import lombok.Getter;
import lombok.Setter;

public abstract class Wrapper<T> {

	@Getter
	@Setter
	private T value;

	public Wrapper(T value) {
		setValue(value);
	}

}
