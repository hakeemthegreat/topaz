package org.coco.topaz.util.logging;

import java.lang.reflect.Method;
import java.util.*;

import static org.coco.topaz.util.logging.Logger.Classifier.*;

public abstract class AbstractLogger implements Logger {

	protected final Class<?> clazz;

	private static final String PIVOT = Method.class.getName();

	public AbstractLogger(Class<?> clazz) {
		this.clazz = Objects.requireNonNull(clazz);
	}

	@Override
	public void trace(String message) {
		trace(message, null);
	}

	@Override
	public void trace(String message, Throwable throwable) {
		log(TRACE, message, throwable);
	}

	@Override
	public void debug(String message) {
		debug(message, null);
	}

	@Override
	public void debug(String message, Throwable throwable) {
		log(DEBUG, message, throwable);
	}

	@Override
	public void info(String message) {
		info(message, null);
	}

	@Override
	public void info(String message, Throwable throwable) {
		log(INFO, message, throwable);
	}

	@Override
	public void warn(String message) {
		warn(message, null);
	}

	@Override
	public void warn(String message, Throwable throwable) {
		log(WARN, message, throwable);
	}

	@Override
	public void error(String message) {
		error(message, null);
	}

	@Override
	public void error(String message, Throwable throwable) {
		log(ERROR, message, throwable);
	}

	@Override
	public void fatal(String message) {
		fatal(message, null);
	}

	@Override
	public void fatal(String message, Throwable throwable) {
		log(FATAL, message, throwable);
	}

	abstract protected void log(Message message);

	protected void log(Classifier classifier, String content, Throwable throwable) {
		Message message = buildMessage(classifier, content, throwable);
		log(message);
	}

	protected Message buildMessage(Classifier classifier, String content, Throwable throwable) {
		String id = UUID.randomUUID().toString();
		Message messageObject = new Message();
		Date now = new Date();

		messageObject.setId(id);
		messageObject.setContent(content);
		messageObject.setClassifier(classifier);
		messageObject.setClazz(clazz);
		messageObject.setLineNumber(getLineNumber());
		messageObject.setDate(now);
		messageObject.setThrowable(throwable);
		return messageObject;
	}

	protected int getLineNumber() {
		Thread currentThread = Thread.currentThread();
		List<StackTraceElement> stackTrace = Arrays.asList(currentThread.getStackTrace());
		StackTraceElement pivot = stackTrace.stream().filter(item -> item.getClassName().equals(PIVOT)).findFirst().orElse(null);
		int index = stackTrace.indexOf(pivot);
		StackTraceElement stackTraceElement = stackTrace.get(index - 4);

		return stackTraceElement.getLineNumber();
	}

}
