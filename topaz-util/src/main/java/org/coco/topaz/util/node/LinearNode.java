package org.coco.topaz.util.node;

import lombok.Setter;

@Setter
public class LinearNode extends Node {

	private Node next;

	public LinearNode() {
		this(null);
	}

	public LinearNode(Object value) {
		setValue(value);
	}

	public <N extends Node> N getNext() {
		return (N) next;
	}
}
