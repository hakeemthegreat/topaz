package org.coco.topaz.util.test;

import java.io.OutputStream;

import static org.coco.topaz.util.Constants.Strings.Messages.STREAM_ALREADY_CLOSED;

public class MockOutputStream extends OutputStream {

	private boolean closed;

	@Override
	public void write(int b) {
		if (closed) {
			throw new UnsupportedOperationException(STREAM_ALREADY_CLOSED);
		}
	}

	@Override
	public void close() {
		this.closed = true;
	}

}
