package org.coco.topaz.util.logging;

public class Loggers {

	private Loggers() {

	}

	public static Logger getLogger(Class<?> clazz) {
		ConsoleLogger consoleLogger = new ConsoleLogger(clazz);
		MasterLogger masterLogger = new MasterLogger(consoleLogger);

		return masterLogger;
	}

}
