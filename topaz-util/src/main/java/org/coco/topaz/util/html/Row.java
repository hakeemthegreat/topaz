package org.coco.topaz.util.html;

import java.util.List;

public class Row extends HTMLElement {
	
	public Row() {
		super("tr");
	}
	
	public List<Cell> getCells() {
		return getChildren(Cell.class);
	}
	
	public Cell getCell(int index) {
		return getCells().get(index);
	}
	
	public Cell getFirstCell() {
		return getCell(0);
	}

}
