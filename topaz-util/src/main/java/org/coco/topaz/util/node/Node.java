package org.coco.topaz.util.node;

import lombok.Setter;

import java.io.Serializable;

@Setter
public class Node implements Serializable {

	private Object value;

	private static final long serialVersionUID = 1L;

	public Node() {
		this(null);
	}

	public Node(Object value) {
		setValue(value);
	}

	public <T> T getValue() {
		return (T) value;
	}

}
