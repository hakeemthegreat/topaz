package org.coco.topaz.util.logging;

public class SystemErrLogger extends PrintStreamLogger {

	public SystemErrLogger(Class<?> clazz) {
		super(clazz, System.err);
	}

}
