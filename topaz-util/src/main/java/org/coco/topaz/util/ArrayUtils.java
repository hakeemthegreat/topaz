package org.coco.topaz.util;

import java.lang.reflect.Array;
import java.util.*;

public class ArrayUtils {

	private ArrayUtils() {

	}

	public static boolean isArray(Object object) {
		return object != null && object.getClass().isArray();
	}

	public static byte[] toPrimitive(Byte[] array) {
		byte[] primitive = {};

		if (array != null) {
			primitive = new byte[array.length];

			for (int i = 0; i < array.length; i++) {
				primitive[i] = array[i];
			}
		}

		return primitive;
	}

	public static char[] toPrimitive(Character[] array) {
		char[] primitive = {};

		if (array != null) {
			primitive = new char[array.length];

			for (int i = 0; i < array.length; i++) {
				primitive[i] = array[i];
			}
		}

		return primitive;
	}

	public static <T> T[] toArray(Collection<T> collection) {
		T[] array = null;
		Class<T> clazz = ClassUtils.getDataType(collection);

		if (clazz != null) {
			array = toArray(collection, clazz);
		}

		return array;
	}

	public static <T> T[] toArray(Collection<T> collection, Class<T> clazz) {
		T[] array = null;
		Iterator<T> iterator;

		if (collection != null) {
			array = (T[]) Array.newInstance(clazz, collection.size());
			iterator = collection.iterator();

			for (int i = 0; i < array.length; i++) {
				array[i] = iterator.next();
			}

		}

		return array;
	}

	@SafeVarargs
	public static <T> Set<T> asSet(T... a) {
		Set<T> set = new HashSet<>();

		if (a != null) {
			Collections.addAll(set, a);
		}

		return set;
	}

	public static String get(String[] array, int index) {
		String result = null;

		if (array != null && index < array.length) {
			result = array[index];
		}

		return result;
	}

}
