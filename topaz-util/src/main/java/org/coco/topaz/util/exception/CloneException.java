package org.coco.topaz.util.exception;

public class CloneException extends Exception {

	public CloneException(String message, Exception cause) {
		super(message, cause);
	}

}
