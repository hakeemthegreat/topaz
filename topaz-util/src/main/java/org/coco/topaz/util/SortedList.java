package org.coco.topaz.util;

import java.util.*;

public class SortedList<C extends Comparable<C>> extends AbstractList<C> {

	private final List<C> list = new ArrayList<>();

	public SortedList() {

	}

	@SafeVarargs
	public SortedList(C... objects) {
		Collections.addAll(this, objects);
	}

	public SortedList(Collection<C> collection) {
		this.addAll(collection);
	}

	@Override
	public boolean add(C object) {
		int nearestIndex = Search.nearestIndexOf(list, object);
		int insertHere = 0;

		if (nearestIndex < list.size()) {
			C nearestMatch = list.get(nearestIndex);

			if (object.compareTo(nearestMatch) > 0) {
				insertHere = nearestIndex + 1;
			}
		}

		list.add(insertHere, object);
		return true;
	}

	@Override
	public C set(int index, C element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, C element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public C remove(int index) {
		return list.remove(index);
	}

	@Override
	public boolean remove(Object object) {
		boolean result = false;
		int index = indexOf(object);

		if (index != -1) {
			remove(index);
			result = true;
		}

		return result;
	}

	@Override
	public boolean contains(Object object) {
		return indexOf(object) >= 0;
	}

	@Override
	public int indexOf(Object object) {
		try {
			return indexOf((C) object);
		} catch (ClassCastException e) {
			return -1;
		}
	}

	public int indexOf(C object) {
		int nearestIndex = Search.nearestIndexOf(list, object);
		int result = -1;

		if (nearestIndex < list.size()) {
			C nearestMatch = list.get(nearestIndex);

			if (object.equals(nearestMatch)) {
				result = nearestIndex;
			}
		}

		return result;
	}

	@Override
	public int lastIndexOf(Object object) {
		return list.lastIndexOf(object);
	}

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public C get(int index) {
		return list.get(index);
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}
}
