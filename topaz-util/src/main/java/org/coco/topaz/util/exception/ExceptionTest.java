package org.coco.topaz.util.exception;

import org.coco.topaz.util.Genesis;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.util.Objects;

import static org.junit.Assert.assertNotNull;

public abstract class ExceptionTest<E extends Exception> {

	private final Class<E> clazz;

	public ExceptionTest(Class<E> clazz) {
		this.clazz = Objects.requireNonNull(clazz);
	}

	@Test
	public void instantiate_Using_Every_Constructor_Test() throws Exception {
		Constructor<E>[] constructors = (Constructor<E>[]) clazz.getDeclaredConstructors();

		for (Constructor<E> constructor : constructors) {
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			Object[] arguments = new Object[parameterTypes.length];

			for (int i = 0; i < parameterTypes.length; i++) {
				Class<?> parameterType = parameterTypes[i];
				arguments[i] = Genesis.create(parameterType);
			}

			Exception result = constructor.newInstance(arguments);
			assertNotNull(result);
		}
	}

}
