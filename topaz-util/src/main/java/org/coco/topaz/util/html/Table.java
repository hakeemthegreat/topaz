package org.coco.topaz.util.html;

import java.util.List;

public class Table extends HTMLElement {
	
	public Table() {
		super("table");
	}
	
	public List<Row> getRows() {
		return getChildren(Row.class);
	}

	public Row getRow(int index) {
		return getRows().get(index);
	}

	public Row getFirstRow() {
		return getRow(0);
	}

}
