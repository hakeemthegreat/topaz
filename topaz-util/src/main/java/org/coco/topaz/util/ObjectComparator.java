package org.coco.topaz.util;

import java.util.Comparator;

public class ObjectComparator implements Comparator<Object> {

	@Override
	public int compare(Object left, Object right) {
		String leftString = Strings.toString(left);
		String rightString = Strings.toString(right);

		return ObjectUtils.compare(leftString, rightString);
	}

}
