package org.coco.topaz.util.reflection;

import java.lang.reflect.*;

import static org.coco.topaz.util.Constants.Strings.Messages.COULD_NOT_INSTANTIATE;

public class Reflection {

	private Reflection() {

	}

	public static void makeAccessible(AccessibleObject accessibleObject) {
		if (accessibleObject != null) {
			accessibleObject.setAccessible(true);
		}
	}

	public static <T> T newInstance(Class<T> clazz, Object... arguments) {
		Constructor<T> constructor = Constructors.getDefaultConstructor(clazz);
		return newInstance(constructor, arguments);
	}

	public static <T> T newInstance(Constructor<T> constructor, Object... arguments) {
		T instance = null;

		if (constructor != null) {
			try {
				constructor.setAccessible(true);
				instance = constructor.newInstance(arguments);
			} catch (ReflectiveOperationException e) {
				String message = String.format(COULD_NOT_INSTANTIATE, constructor.getDeclaringClass());
				throw new RuntimeException(message, e);
			}
		}

		return instance;
	}

	public static void removeFinalModifier(Member member) {
		removeModifier(member, Modifier.FINAL);
	}

	public static void removeModifier(Member member, int modifier) {
		if (member != null) {
			Field modifiers = Fields.getField("modifiers", member);

			try {
				Reflection.makeAccessible(modifiers);
				modifiers.set(member, member.getModifiers() & ~modifier);
			} catch (IllegalAccessException e) {
				throw new RuntimeException("Unable to remove final modifier from " + member, e);
			}
		}
	}

	public static boolean isStatic(Member member) {
		return Modifier.isStatic(member.getModifiers());
	}

	public static boolean isNotStatic(Member member) {
		return !isStatic(member);
	}

	public static boolean isFinal(Member member) {
		return Modifier.isFinal(member.getModifiers());
	}

	public static boolean isNotFinal(Member member) {
		return !isFinal(member);
	}

}
