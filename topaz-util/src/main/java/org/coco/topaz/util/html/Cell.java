package org.coco.topaz.util.html;

public class Cell extends HTMLElement {
	
	public Cell() {
		super("td");
	}
	
	public Cell(String text) {
		this();
		addText(text);
	}

}
