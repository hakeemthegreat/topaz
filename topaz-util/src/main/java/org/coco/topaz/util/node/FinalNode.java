package org.coco.topaz.util.node;

import lombok.Getter;

@Getter
public class FinalNode extends Node {

	private final Object value;

	public FinalNode() {
		this(null);
	}

	public FinalNode(Object value) {
		this.value = value;
	}

}
