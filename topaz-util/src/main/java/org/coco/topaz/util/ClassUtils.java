package org.coco.topaz.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ClassUtils {
	
	private static final Set<Class<?>> NATIVE_JAVA_CLASSES = new HashSet<>();

	static {
		NATIVE_JAVA_CLASSES.add(Boolean.class);
		NATIVE_JAVA_CLASSES.add(Byte.class);
		NATIVE_JAVA_CLASSES.add(Character.class);
		NATIVE_JAVA_CLASSES.add(java.sql.Date.class);
		NATIVE_JAVA_CLASSES.add(java.util.Date.class);
		NATIVE_JAVA_CLASSES.add(Double.class);
		NATIVE_JAVA_CLASSES.add(Float.class);
		NATIVE_JAVA_CLASSES.add(Integer.class);
		NATIVE_JAVA_CLASSES.add(Long.class);
		NATIVE_JAVA_CLASSES.add(Short.class);
		NATIVE_JAVA_CLASSES.add(String.class);

		NATIVE_JAVA_CLASSES.add(boolean.class);
		NATIVE_JAVA_CLASSES.add(byte.class);
		NATIVE_JAVA_CLASSES.add(char.class);
		NATIVE_JAVA_CLASSES.add(double.class);
		NATIVE_JAVA_CLASSES.add(float.class);
		NATIVE_JAVA_CLASSES.add(int.class);
		NATIVE_JAVA_CLASSES.add(long.class);
		NATIVE_JAVA_CLASSES.add(short.class);
	}

	private ClassUtils() {

	}

	public static <T> Class<T> getDataType(Collection<T> collection) {
		Class<T> clazz = null;

		if (collection != null) {
			Iterator<T> iterator = collection.iterator();

			if (iterator.hasNext()) {
				Object object = iterator.next();
				clazz = (Class<T>) object.getClass();
			}
		}

		return clazz;
	}

	public static Class<?> getDataType(Object object) {
		Class<?> type = null;

		if (object != null) {
			type = object.getClass().getComponentType();
		}

		return type;
	}

	public static boolean areAssignableFrom(Object[] arguments, Class<?>[] classes) {
		Class<?>[] argumentTypes = new Class<?>[arguments.length];

		for (int i = 0; i < arguments.length; i++) {
			argumentTypes[i] = arguments[i].getClass();
		}

		return areAssignableFrom(argumentTypes, classes);
	}

	public static boolean areAssignableFrom(Class<?>[] subClasses, Class<?>[] classes) {
		boolean result = subClasses.length == classes.length;

		if (result) {
			for (int i = 0; i < subClasses.length; i++) {
				if (!classes[i].isAssignableFrom(subClasses[i])) {
					result = false;
					break;
				}
			}
		}

		return result;
	}

	public static boolean instanceOf(Class<?> subClass, Class<?> superClass) {
		boolean result = false;

		if (subClass != null && superClass != null) {
			result = superClass.isAssignableFrom(subClass);
		}

		return result;
	}
	
	public static boolean isNativeJavaClass(Class<?> clazz) {
		return NATIVE_JAVA_CLASSES.contains(clazz);
	}

	public static <T> Class<T> getClass(T object) {
		Class<T> clazz = null;

		if (object != null) {
			clazz = (Class<T>) object.getClass();
		}

		return clazz;
	}

	public static String getClassName(Object object) {
		String className = null;
		Class<?> clazz = getClass(object);

		if (clazz != null) {
			className = clazz.getName();
		}

		return className;
	}

	public static <T> Class<T> getClass(String className) {
		try {
			return (Class<T>) Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Unable to get class " + className, e);
		}
	}

}
