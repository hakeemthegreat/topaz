package org.coco.topaz.util.logging;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class PrintStreamLogger extends AbstractLogger {

	private final PrintStream printStream;
	private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");

	public PrintStreamLogger(Class<?> clazz, PrintStream printStream) {
		super(clazz);
		this.printStream = Objects.requireNonNull(printStream);
	}

	@Override
	protected void log(Message message) {
		String date = simpleDateFormat.format(message.getDate());
		Classifier classifier = message.getClassifier();
		Throwable throwable = message.getThrowable();
		int lineNumber = message.getLineNumber();
		String content = message.getContent();
		String className = clazz.getName();
		String output;

		output = String.format("[%s] %s %s:%s - %s", classifier, date, className, lineNumber, content);
		printStream.println(output);

		if (throwable != null) {
			throwable.printStackTrace(printStream);
		}
	}

}
