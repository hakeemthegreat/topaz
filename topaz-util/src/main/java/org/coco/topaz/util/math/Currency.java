package org.coco.topaz.util.math;

import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;

@Getter
@Setter
public class Currency extends Number implements Comparable<Currency> {

	private Double value;

	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("$#0.00");

	public Currency(double value) {
		setValue(value);
	}

	public Currency add(double value) {
		return new Currency(this.value + value);
	}

	public Currency subtract(double value) {
		return new Currency(this.value - value);
	}
	
	public Currency multiply(double value) {
		return new Currency(this.value * value);
	}

	public Currency divide(double value) {
		return new Currency(this.value / value);
	}

	@Override
	public byte byteValue() {
		return value.byteValue();
	}

	@Override
	public short shortValue() {
		return value.shortValue();
	}

	@Override
	public int intValue() {
		return value.intValue();
	}

	@Override
	public long longValue() {
		return value.longValue();
	}

	@Override
	public float floatValue() {
		return value.floatValue();
	}

	@Override
	public double doubleValue() {
		return value;
	}

	@Override
	public int compareTo(Currency number) {
		int result = 1;

		if (number != null) {
			result = value.compareTo(number.doubleValue());
		}

		return result;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;

		if (object instanceof Currency) {
			result = compareTo((Currency) object) == 0;
		}

		return result;
	}

	@Override
	public int hashCode() {
		return DECIMAL_FORMAT.hashCode() * value.hashCode();
	}

	@Override
	public String toString() {
		return DECIMAL_FORMAT.format(value);
	}

}
