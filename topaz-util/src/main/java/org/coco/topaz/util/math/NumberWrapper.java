package org.coco.topaz.util.math;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.Ensure;

@Getter
@Setter
public class NumberWrapper {

	private Number value;

	public NumberWrapper(Number value) {
		this.value = value;
	}

	public boolean isGreaterThan(Number number) {
		Ensure.notNull(number);
		boolean result = false;

		if (this.value != null) {
			result = this.value.doubleValue() > number.doubleValue();
		}

		return result;
	}

	public boolean isLessThan(Number number) {
		Ensure.notNull(number);
		boolean result = false;

		if (this.value != null) {
			result = this.value.doubleValue() < number.doubleValue();
		}

		return result;
	}

	public boolean isEqualTo(Number number) {
		Ensure.notNull(number);
		boolean result = false;

		if (this.value != null) {
			result = this.value.doubleValue() == number.doubleValue();
		}

		return result;
	}

}
