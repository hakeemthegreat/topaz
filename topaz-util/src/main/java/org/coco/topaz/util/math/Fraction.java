package org.coco.topaz.util.math;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fraction extends Number implements Comparable<Fraction> {

	private long numerator;
	private long denominator;

	public Fraction(long numerator, long denominator) {
		setNumerator(numerator);
		setDenominator(denominator);
	}

	public Fraction add(Fraction fraction) {
		return new Fraction(numerator * fraction.denominator + fraction.numerator * denominator, denominator * fraction.denominator);
	}

	public Fraction subtract(Fraction fraction) {
		return new Fraction(numerator * fraction.denominator - fraction.numerator * denominator, denominator * fraction.denominator);
	}

	public Fraction multiply(Fraction fraction) {
		return new Fraction(numerator * fraction.numerator, denominator * fraction.denominator);
	}

	public Fraction divide(Fraction fraction) {
		return new Fraction(numerator * fraction.denominator, denominator * fraction.numerator);
	}

	public Fraction invert() {
		return new Fraction(denominator, numerator);
	}

	@Override
	public byte byteValue() {
		return (byte) (numerator / denominator);
	}

	@Override
	public short shortValue() {
		return (short) (numerator / denominator);
	}

	@Override
	public int intValue() {
		return (int) (numerator / denominator);
	}

	@Override
	public long longValue() {
		return numerator / denominator;
	}

	@Override
	public float floatValue() {
		return (float) numerator / denominator;
	}

	@Override
	public double doubleValue() {
		return (double) numerator / denominator;
	}

	@Override
	public int hashCode() {
		return intValue();
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;

		if (object instanceof Fraction) {
			Fraction that = (Fraction) object;
			result = compareTo(that) == 0;
		}

		return result;
	}

	@Override
	public int compareTo(Fraction fraction) {
		Double x = this.doubleValue();
		Double y = fraction.doubleValue();

		return x.compareTo(y);
	}

	@Override
	public String toString() {
		return numerator + "/" + denominator;
	}

}
