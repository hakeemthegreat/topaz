package org.coco.topaz.util.node;

import lombok.Setter;

@Setter
public class BinaryNode extends Node {

	private Node left;
	private Node right;

	public BinaryNode() {
		this(null);
	}

	public BinaryNode(Object value) {
		setValue(value);
	}

	public <N extends Node> N getLeft() {
		return (N) left;
	}

	public <N extends Node> N getRight() {
		return (N) right;
	}

}
