package org.coco.topaz.util.reflection;

import org.coco.topaz.util.ClassUtils;

import java.lang.reflect.Method;
import java.util.*;

public class Methods {

	private static final Map<Class<?>, List<Method>> METHODS = new HashMap<>();
	private static final Class<?> DEFAULT_RETURN_TYPE = void.class;

	private Methods() {

	}

	public static List<Method> getMethods(Class<?> clazz) {
		List<Method> methods = METHODS.get(clazz);

		if (methods == null) {
			methods = createMethodList(clazz);
			METHODS.put(clazz, methods);
		}

		return methods;
	}

	protected static List<Method> createMethodList(Class<?> clazz) {
		List<Method> methods = new ArrayList<>();

		while (clazz != null) {
			Collections.addAll(methods, clazz.getDeclaredMethods());
			clazz = clazz.getSuperclass();
		}

		return Collections.unmodifiableList(methods);
	}

	public static Method getMethod(Class<?> clazz, String name, Class<?>... parameterTypes) {
		List<Method> methods = getMethods(clazz);
		Signature resultSignature = new Signature(DEFAULT_RETURN_TYPE, name, parameterTypes);
		Method result = null;

		for (Method method : methods) {
			Signature signature = new Signature(DEFAULT_RETURN_TYPE, method.getName(), method.getParameterTypes());

			if (resultSignature.equals(signature)) {
				result = method;
				break;
			}
		}

		return result;
	}

	public static <T> T invoke(Object object, String methodName, Object... arguments) {
		List<Method> methods;
		Object container = object;
		Class<?> clazz = ClassUtils.getClass(object);

		if (object instanceof Class) {
			clazz = (Class<?>) object;
			container = null;
		}

		methods = getMethods(clazz);

		for (Method method : methods) {
			if (method.getName().equals(methodName) && ClassUtils.areAssignableFrom(arguments, method.getParameterTypes())) {
				return invoke(container, method, arguments);
			}
		}

		throw new RuntimeException(String.format("No such method %s", methodName));
	}

	public static <T> T invoke(Object object, Method method, Object... arguments) {
		T value = null;

		if (method != null) {
			try {
				Reflection.makeAccessible(method);
				value =  (T) method.invoke(object, arguments);
			} catch (ReflectiveOperationException e) {
				throw new RuntimeException("Unable to invoke " + method, e);
			}
		}

		return value;
	}

	public static <T> T invoke(String className, String methodName, Object... arguments) {
		Class<?> clazz = ClassUtils.getClass(className);
		return invoke(clazz, methodName, arguments);
	}

}
