package org.coco.topaz.util;

import java.io.*;

public class Streams {

	private Streams() {

	}
	
	public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
		BufferedInputStream bufferedInputStream = toBufferedInputStream(inputStream);
		BufferedOutputStream bufferedOutputStream = toBufferedOutputStream(outputStream);
		int b = bufferedInputStream.read();

		while (b != -1) {
			bufferedOutputStream.write(b);
			b = bufferedInputStream.read();
		}

		bufferedOutputStream.flush();
	}

	public static void copy(Reader reader, Writer writer) throws IOException {
		BufferedReader bufferedReader = toBufferedReader(reader);
		BufferedWriter bufferedWriter = toBufferedWriter(writer);
		int b = bufferedReader.read();

		while (b != -1) {
			bufferedWriter.write(b);
			b = bufferedReader.read();
		}

		bufferedWriter.flush();
	}

	public static byte[] readAll(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byte[] bytes;

		try {
			copy(inputStream, byteArrayOutputStream);
			bytes = byteArrayOutputStream.toByteArray();
		} finally {
			Resources.close(inputStream, byteArrayOutputStream);
		}

		return bytes;
	}

	public static String readAll(Reader reader) throws IOException {
		CharArrayWriter charArrayWriter = new CharArrayWriter();
		char[] chars;

		try {
			copy(reader, charArrayWriter);
			chars = charArrayWriter.toCharArray();
		} finally {
			Resources.close(reader, charArrayWriter);
		}

		return new String(chars);
	}

	public static FileInputStream newFileInputStream(String fileLocation) throws IOException {
		return new FileInputStream(fileLocation);
	}

	public static FileOutputStream newFileOutputStream(File file, boolean append) throws IOException {
		return new FileOutputStream(file, append);
	}

	public static ByteArrayOutputStream newByteArrayOutputStream() {
		return new ByteArrayOutputStream();
	}

	public static BufferedInputStream toBufferedInputStream(InputStream inputStream) {
		BufferedInputStream bufferedInputStream;

		if (inputStream instanceof BufferedInputStream) {
			bufferedInputStream = (BufferedInputStream) inputStream;
		} else {
			bufferedInputStream = new BufferedInputStream(inputStream);
		}

		return bufferedInputStream;
	}

	public static BufferedOutputStream toBufferedOutputStream(OutputStream outputStream) {
		BufferedOutputStream bufferedOutputStream;

		if (outputStream instanceof BufferedOutputStream) {
			bufferedOutputStream = (BufferedOutputStream) outputStream;
		} else {
			bufferedOutputStream = new BufferedOutputStream(outputStream);
		}

		return bufferedOutputStream;
	}


	public static BufferedReader toBufferedReader(Reader reader) {
		BufferedReader bufferedReader;

		if (reader instanceof BufferedReader) {
			bufferedReader = (BufferedReader) reader;
		} else {
			bufferedReader = new BufferedReader(reader);
		}

		return bufferedReader;
	}

	public static BufferedWriter toBufferedWriter(Writer writer) {
		BufferedWriter bufferedWriter;

		if (writer instanceof BufferedWriter) {
			bufferedWriter = (BufferedWriter) writer;
		} else {
			bufferedWriter = new BufferedWriter(writer);
		}

		return bufferedWriter;
	}

}
