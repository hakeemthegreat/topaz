package org.coco.topaz.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

public class Annotations {

	private Annotations() {

	}

	public static <A extends Annotation> A getAnnotation(AnnotatedElement annotatedElement, Class<A> annotationClass) {
		A result = null;

		if (annotatedElement != null) {
			result = annotatedElement.getAnnotation(annotationClass);
		}

		return result;
	}

	public static <A extends Annotation> boolean hasAnnotation(AnnotatedElement annotatedElement, Class<A> annotationClass) {
		return getAnnotation(annotatedElement, annotationClass) != null;
	}

}
