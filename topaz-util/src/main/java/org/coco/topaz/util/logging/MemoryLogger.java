package org.coco.topaz.util.logging;

import java.util.*;

public class MemoryLogger extends AbstractLogger {

	private static final List<Message> MESSAGES = new ArrayList<>();

	public MemoryLogger(Class<?> clazz) {
		super(clazz);
	}

	@Override
	protected void log(Message message) {
		MESSAGES.add(message);
	}

	public List<Message> getMessages() {
		return Collections.unmodifiableList(MESSAGES);
	}

}
