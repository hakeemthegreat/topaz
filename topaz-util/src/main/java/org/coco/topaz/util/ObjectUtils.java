package org.coco.topaz.util;

import java.util.Collection;

public class ObjectUtils {

	private ObjectUtils() {

	}

	public static boolean isEmpty(Object object) {
		boolean result = false;

		if (object == null) {
			result = true;
		} else if (object instanceof String) {
			result = object.toString().isEmpty();
		} else if (object instanceof Collection) {
			result = ((Collection<?>) object).isEmpty();
		}

		return result;
	}

	public static boolean isNotEmpty(Object object) {
		return !isEmpty(object);
	}

	public static <T extends Comparable<T>> int compare(T left, T right) {
		int result;

		if (left == right) {
			result = 0;
		} else if (left == null) {
			result = -1;
		} else if (right == null) {
			result = 1;
		} else {
			result = left.compareTo(right);
		}

		return result;
	}

}
