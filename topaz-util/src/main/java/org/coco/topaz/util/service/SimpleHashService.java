package org.coco.topaz.util.service;

public class SimpleHashService implements HashService {

	@Override
	public int hash(Object object) {
		int code = 0;

		if (object != null) {
			code = object.hashCode();
		}

		return code;
	}

	@Override
	public String hashToHex(Object object) {
		return Integer.toHexString(hash(object)).toLowerCase();
	}

}
