package org.coco.topaz.util.logging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MasterLogger implements Logger {

	private final List<Logger> loggers = new ArrayList<>();

	public MasterLogger(Logger... loggers) {
		if (loggers == null || loggers.length == 0) {
			SystemErrLogger systemErrLogger = new SystemErrLogger(MasterLogger.class);
			systemErrLogger.warn("No loggers have been configured");
		} else {
			Collections.addAll(this.loggers, loggers);
		}
	}

	@Override
	public void trace(String message) {
		loggers.forEach(logger -> logger.trace(message));
	}

	@Override
	public void trace(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.trace(message, throwable));
	}

	@Override
	public void debug(String message) {
		loggers.forEach(logger -> logger.debug(message));
	}

	@Override
	public void debug(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.debug(message, throwable));
	}

	@Override
	public void info(String message) {
		loggers.forEach(logger -> logger.info(message));
	}

	@Override
	public void info(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.info(message, throwable));
	}

	@Override
	public void warn(String message) {
		loggers.forEach(logger -> logger.warn(message));
	}

	@Override
	public void warn(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.warn(message, throwable));
	}

	@Override
	public void error(String message) {
		loggers.forEach(logger -> logger.error(message));
	}

	@Override
	public void error(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.error(message, throwable));
	}

	@Override
	public void fatal(String message) {
		loggers.forEach(logger -> logger.fatal(message));
	}

	@Override
	public void fatal(String message, Throwable throwable) {
		loggers.forEach(logger -> logger.fatal(message, throwable));
	}

}
