package org.coco.topaz.util.html;

import lombok.Getter;
import org.coco.topaz.util.Constants;
import org.coco.topaz.util.Replace;

import java.util.List;

public class TextElement extends HTMLElement {

	@Getter
	private String value = DEFAULT_VALUE;

	public static final String DEFAULT_VALUE = Constants.Strings.EMPTY;

	public TextElement() {
		super(DEFAULT_VALUE);
	}

	public TextElement(String value) {
		this();
		setValue(value);
	}

	@Override
	public void addChild(HTMLElement child) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<HTMLElement> getChildren() {
		throw new UnsupportedOperationException();
	}

	public void setValue(String value) {
		this.value = Replace.with(DEFAULT_VALUE).should(value).beNull();
	}

	@Override
	public String toString() {
		return value;
	}

}
