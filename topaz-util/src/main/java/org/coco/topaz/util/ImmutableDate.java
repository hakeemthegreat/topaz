package org.coco.topaz.util;

import java.util.Date;

@SuppressWarnings({"deprecation", "DeprecatedIsStillUsed"})
public class ImmutableDate extends Date {

	public ImmutableDate(long date) {
		super(date);
	}

	@Override
	@Deprecated
	public void setYear(int year) {
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setMonth(int month) {
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setDate(int date) {
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setHours(int hours) {
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setMinutes(int minutes) {
		throw new UnsupportedOperationException();
	}

	@Override
	@Deprecated
	public void setSeconds(int seconds) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTime(long time) {
		throw new UnsupportedOperationException();
	}

}
