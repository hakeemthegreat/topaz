package org.coco.topaz.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Resources {

	private Resources() {

	}

	public static List<Exception> close(AutoCloseable... resources) {
		List<Exception> exceptions = new ArrayList<>();

		if (resources != null) {
			exceptions = close(Arrays.asList(resources));
		}

		return exceptions;
	}

	public static List<Exception> close(Iterable<? extends AutoCloseable> resources) {
		List<Exception> exceptions = new ArrayList<>();

		if (resources != null) {
			for (AutoCloseable resource : resources) {
				if (resource != null) {
					try {
						resource.close();
					} catch (Exception e) {
						exceptions.add(e);
					}
				}
			}
		}

		return exceptions;
	}

}
