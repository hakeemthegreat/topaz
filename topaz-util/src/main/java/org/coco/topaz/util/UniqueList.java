package org.coco.topaz.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class UniqueList<E> extends ArrayList<E> {
	
	public UniqueList() {

	}

	@SafeVarargs
	public UniqueList(E... elements) {
		Collections.addAll(this, elements);
	}

	public UniqueList(Collection<E> collection) {
		this.addAll(collection);
	}

	@Override
	public boolean add(E element) {
		boolean result = false;

		if (!contains(element)) {
			result = super.add(element);
		}

		return result;
	}

	@Override
	public void add(int index, E element) {
		if (!contains(element)) {
			super.add(index, element);
		}
	}
	
}
