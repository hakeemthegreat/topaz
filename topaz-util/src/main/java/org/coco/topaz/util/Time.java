package org.coco.topaz.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Time {

	private long hours;
	private long minutes;
	private long seconds;

	private static final long SECONDS_PER_MINUTE = 60;
	private static final long SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60;
	private static final long MILLISECONDS_PER_SECONDS = 1000;

	public Time() {
		this(0);
	}

	public Time(long milliseconds) {
		this.seconds = milliseconds / MILLISECONDS_PER_SECONDS;
		this.hours = seconds / SECONDS_PER_HOUR;
		this.minutes = seconds % SECONDS_PER_HOUR / SECONDS_PER_MINUTE;
		this.seconds = seconds % SECONDS_PER_HOUR % SECONDS_PER_MINUTE;
	}

	public long toMilliseconds() {
		long result = 0;

		result += hours * SECONDS_PER_HOUR * MILLISECONDS_PER_SECONDS;
		result += minutes * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECONDS;
		result += seconds * MILLISECONDS_PER_SECONDS;

		return result;
	}

	@Override
	public String toString() {
		return String.format("%s:%s.%s", hours, minutes, seconds);
	}

}
