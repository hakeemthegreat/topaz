package org.coco.topaz.util.logging;

public class SystemOutLogger extends PrintStreamLogger {

	public SystemOutLogger(Class<?> clazz) {
		super(clazz, System.out);
	}

}