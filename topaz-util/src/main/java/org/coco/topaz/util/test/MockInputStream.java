package org.coco.topaz.util.test;

import java.io.InputStream;

import static org.coco.topaz.util.Constants.Strings.Messages.STREAM_ALREADY_CLOSED;

public class MockInputStream extends InputStream {

	private int index = 0;
	private boolean closed;

	private static final int LARGE_PRIME_NUMBER = 37591;
	private static final int[] ARRAY = new int[LARGE_PRIME_NUMBER];

	static {
		for (int i = 0 ; i < LARGE_PRIME_NUMBER ; i++) {
			ARRAY[i] = i % 256;
		}
	}

	@Override
	public int read() {
		int b = -1;

		if (closed) {
			throw new UnsupportedOperationException(STREAM_ALREADY_CLOSED);
		} else if (index < ARRAY.length) {
			b = ARRAY[index++];
		}

		return b;
	}

	@Override
	public void close() {
		this.closed = true;
	}

}
