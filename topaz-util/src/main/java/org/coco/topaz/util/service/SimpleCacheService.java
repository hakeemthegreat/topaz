package org.coco.topaz.util.service;

import java.util.HashMap;
import java.util.Map;

public class SimpleCacheService implements CacheService {

	private final Map<Object, Object> map = new HashMap<>();

	@Override
	public void put(Object key, Object value) {
		map.put(key, value);
	}

	@Override
	public <T> T get(Object key) {
		return (T) map.get(key);
	}

}
