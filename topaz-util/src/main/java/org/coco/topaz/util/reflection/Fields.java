package org.coco.topaz.util.reflection;

import org.coco.topaz.util.ClassUtils;

import java.lang.reflect.Field;
import java.util.*;

public class Fields {

	private static final Map<Class<?>, List<Field>> FIELDS = new HashMap<>();

	private Fields() {

	}

	public static Map<String, Field> getFieldMap(Class<?> clazz) {
		Map<String, Field> map = new HashMap<>();
		List<Field> fields = getFields(clazz);

		if (clazz != null) {
			fields.forEach(field -> map.put(field.getName(), field));
		}

		return map;
	}

	public static Map<Field, Object> getFieldValues(Object object) {
		Map<Field, Object> values = new HashMap<>();
		Class<?> clazz = ClassUtils.getClass(object);
		List<Field> fields = getFields(clazz);

		for (Field field : fields) {
			values.put(field, getValue(field, object));
		}

		return values;
	}

	public static List<Field> getFields(Class<?> clazz) {
		List<Field> fields = FIELDS.get(clazz);

		if (fields == null) {
			fields = createFieldList(clazz);
			FIELDS.put(clazz, fields);
		}

		return fields;
	}

	protected static List<Field> createFieldList(Class<?> clazz) {
		List<Field> fields = new ArrayList<>();

		while (clazz != null) {
			Collections.addAll(fields, clazz.getDeclaredFields());
			clazz = clazz.getSuperclass();
		}

		fields.removeIf(field -> field.getName().startsWith("$"));
		return Collections.unmodifiableList(fields);
	}

	public static void set(Object object, String fieldName, Object value) {
		Field field = getField(fieldName, object);
		set(object, field, value);
	}

	public static void set(Object object, Field field, Object value) {
		if (field != null) {
			try {
				Reflection.makeAccessible(field);
				Reflection.removeFinalModifier(field);
				field.set(object, value);
			} catch (ReflectiveOperationException e) {
				throw new RuntimeException("Cannot set value on " + field, e);
			}
		}
	}

	public static Field getField(String fieldName, Object object) {
		Class<?> clazz = ClassUtils.getClass(object);
		List<Field> fields = getFields(clazz);
		return fields.stream().filter(f -> f.getName().equals(fieldName)).findFirst().orElse(null);
	}

	public static Object getValue(Field field, Object object) {
		try {
			Object container = object;

			if (Reflection.isStatic(field)) {
				container = ClassUtils.getClass(object);
			}

			Reflection.makeAccessible(field);
			return field.get(container);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Cannot get value from " + field, e);
		}
	}

}
