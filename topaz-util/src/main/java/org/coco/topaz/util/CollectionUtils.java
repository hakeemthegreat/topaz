package org.coco.topaz.util;

import org.coco.topaz.util.reflection.Constructors;

import java.lang.reflect.Constructor;
import java.util.*;

public class CollectionUtils {

	private CollectionUtils() {

	}

	public static boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isNotEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}

	public static <C extends Collection<T>, T> C filter(Collection<?> collection, Class<T> desiredClass) {
		C results = null;

		if (collection != null) {
			Class<?> clazz = collection.getClass();
			Constructor<C> constructor = (Constructor<C>) Constructors.getDefaultConstructor(clazz);

			results = Constructors.newInstance(constructor);

			for (Object item : collection) {
				if (desiredClass.isInstance(item)) {
					results.add((T) item);
				}
			}
		}

		return results;
	}

	@SafeVarargs
	public static <T> List<T> toList(T... array) {
		List<T> list = new ArrayList<>();

		if (array != null) {
			Collections.addAll(list, array);
		}

		return list;
	}

	public static <T> List<T> toList(Collection<T> collection) {
		List<T> list = new ArrayList<>();

		if (collection instanceof List) {
			list = (List<T>) collection;
		} else if (collection != null) {
			list.addAll(collection);
		}

		return list;
	}

	@SafeVarargs
	public static <T> Set<T> toSet(T... array) {
		Set<T> list = new HashSet<>();

		if (array != null) {
			Collections.addAll(list, array);
		}

		return list;
	}

	public static <T> Set<T> toSet(Collection<T> collection) {
		Set<T> set = new HashSet<>();

		if (collection instanceof Set) {
			set = (Set<T>) collection;
		} else if (collection != null) {
			set.addAll(collection);
		}

		return set;
	}

	public static <T> T get(List<T> list, int index) {
		T item = null;

		if (list != null) {
			if (-1 < index && index < list.size()) {
				item = list.get(index);
			}
		}

		return item;
	}

}
