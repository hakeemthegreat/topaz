package org.coco.topaz.util.logging;

import mockit.Deencapsulation;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class LoggersTest {

	@Test
	public void getLogger_Test() {
		Logger result = Loggers.getLogger(Object.class);
		assertTrue(result instanceof MasterLogger);

		List<Logger> loggers = Deencapsulation.getField(result, "loggers");
		assertNotNull(loggers);
		assertEquals(1, loggers.size());

		Logger logger0 = loggers.get(0);
		assertTrue(logger0 instanceof ConsoleLogger);
	}

}
