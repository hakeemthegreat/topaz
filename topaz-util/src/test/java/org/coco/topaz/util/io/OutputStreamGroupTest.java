package org.coco.topaz.util.io;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class OutputStreamGroupTest {

	private ByteArrayOutputStream one;

	private ByteArrayOutputStream two;

	private OutputStreamGroup instance;

	private static final int DATA_LENGTH = 100000;
	private static final byte[] DATA = new byte[DATA_LENGTH];

	static {
		for (int i = 0; i < DATA_LENGTH; i++) {
			DATA[i] = (byte) i;
		}
	}

	@Before
	public void before() {
		one = new ByteArrayOutputStream();
		two = new ByteArrayOutputStream();
		instance = new OutputStreamGroup(one, two, null);
	}

	@Test
	public void write_Byte_By_Byte_Test() throws Exception {
		for (byte b : DATA) {
			instance.write(b);
		}

		byte[] result1 = one.toByteArray();
		byte[] result2 = two.toByteArray();

		assertArrayEquals(DATA, result1);
		assertArrayEquals(DATA, result2);
	}


	@Test
	public void write_Entire_Array_Test() throws Exception {
		instance.write(DATA);

		byte[] result1 = one.toByteArray();
		byte[] result2 = two.toByteArray();

		assertArrayEquals(DATA, result1);
		assertArrayEquals(DATA, result2);
	}

	@Test
	public void write_Partial_Array_Test() throws Exception {
		int from = 0;
		int to = DATA_LENGTH / 2;
		byte[] subArray = Arrays.copyOfRange(DATA, from, to);

		instance.write(DATA, from, to);

		byte[] result1 = one.toByteArray();
		byte[] result2 = two.toByteArray();

		assertArrayEquals(subArray, result1);
		assertArrayEquals(subArray, result2);
	}

	@Test
	public void close_Test(@Mocked ByteArrayOutputStream byteArrayOutputStream, @Mocked BufferedOutputStream bufferedOutputStream) throws Exception {
		OutputStreamGroup instance = new OutputStreamGroup(byteArrayOutputStream, bufferedOutputStream);

		instance.close();

		new Verifications() {{
			byteArrayOutputStream.close(); times = 1;
			bufferedOutputStream.close(); times = 1;
		}};
	}

	@Test(expected = IOException.class)
	public void close_Exception_Test(@Mocked ByteArrayOutputStream byteArrayOutputStream, @Mocked BufferedOutputStream bufferedOutputStream) throws Exception {
		OutputStreamGroup instance = new OutputStreamGroup(byteArrayOutputStream, bufferedOutputStream);
		IOException ioException = new IOException();

		new Expectations() {{
			byteArrayOutputStream.close(); result = ioException;
		}};

		instance.close();

		new Verifications() {{
			bufferedOutputStream.close(); times = 1;
		}};
	}

	@Test
	public void flush_Test(@Mocked ByteArrayOutputStream byteArrayOutputStream, @Mocked BufferedOutputStream bufferedOutputStream) throws Exception {
		OutputStreamGroup instance = new OutputStreamGroup(byteArrayOutputStream, bufferedOutputStream);

		instance.flush();

		new Verifications() {{
			byteArrayOutputStream.flush(); times = 1;
			bufferedOutputStream.flush(); times = 1;
		}};
	}

}
