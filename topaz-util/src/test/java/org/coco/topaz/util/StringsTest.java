package org.coco.topaz.util;

import static org.coco.topaz.util.Constants.Strings.*;
import static org.junit.Assert.*;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.*;

public class StringsTest {

	private static final String BLANK = "\t \r\n";
	private static final List<String> COLLECTION = Arrays.asList("dummy", "dummy2", "dummy3");
	
	@Test
	public void isEmpty_Null_Test() {
		boolean result = Strings.isEmpty(null);
		assertTrue(result);
	}
	
	@Test
	public void isEmpty_Empty_Test() {
		boolean result = Strings.isEmpty(EMPTY);
		assertTrue(result);
	}
	
	@Test
	public void isNotEmpty_True_Test() {
		boolean result = Strings.isNotEmpty(Dummy.STRING);
		assertTrue(result);
	}

	@Test
	public void isNotEmpty_False_Test() {
		boolean result = Strings.isNotEmpty(EMPTY);
		assertFalse(result);
	}

	@Test
	public void escape_Test() {
		String result = Strings.escape(NEWLINE);
		assertEquals("\\n", result);
	}

	@Test
	public void escape_Null_Test() {
		String result = Strings.escape(NULL);
		assertNull(result);
	}

	@Test
	public void toUpperCase_Test() {
		String result = Strings.toUpperCase(Dummy.STRING);
		assertEquals(Dummy.STRING.toUpperCase(), result);
	}

	@Test
	public void toUpperCase_Null_Test() {
		String result = Strings.toUpperCase(NULL);
		assertNull(result);
	}

	@Test
	public void toStringList_Test() {
		List<Integer> collection = Arrays.asList(1, 2, 3);

		List<String> results = Strings.toStringList(collection);
		assertEquals(3, results.size());

		Iterator<String> iterator = results.iterator();
		assertEquals("1", iterator.next());
		assertEquals("2", iterator.next());
		assertEquals("3", iterator.next());
	}

	@Test
	public void toStringList_Null_Test() {
		List<String> results = Strings.toStringList(null);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void trim_Test() {
		String result = Strings.trim(BLANK);
		assertEquals("", result);
	}

	@Test
	public void trim_Null_Test() {
		String result = Strings.trim(null);
		assertNull(result);
	}

	@Test
	public void isBlank_True_Test() {
		boolean result = Strings.isBlank(BLANK);
		assertTrue(result);
	}

	@Test
	public void isBlank_Null_Test() {
		boolean result = Strings.isBlank(null);
		assertTrue(result);
	}

	@Test
	public void isBlank_False_Test() {
		boolean result = Strings.isBlank(Dummy.STRING);
		assertFalse(result);
	}

	@Test
	public void isNotBlank_False_Test() {
		boolean result = Strings.isNotBlank(BLANK);
		assertFalse(result);
	}

	@Test
	public void isNotBlank_Null_Test() {
		boolean result = Strings.isNotBlank(null);
		assertFalse(result);
	}

	@Test
	public void isNotBlank_True_Test() {
		boolean result = Strings.isNotBlank(Dummy.STRING);
		assertTrue(result);
	}
	
	@Test
	public void toCommaDelimited_Test() {
		String result = Strings.toCommaDelimited(COLLECTION);
		assertEquals("dummy, dummy2, dummy3", result);
	}

	@Test
	public void toString_Test() {
		String result = Strings.toString(1);
		assertEquals("1", result);
	}

	@Test
	public void toString_Null_Test() {
		String result = Strings.toString(null);
		assertNull(result);
	}

	@Test
	public void standardizeSlashes_Test() {
		String result = Strings.standardizeSlashes("\\root\\folder\\file.txt");
		assertEquals("/root/folder/file.txt", result);
	}

	@Test
	public void standardizeSlashes_Null_Test() {
		String result = Strings.standardizeSlashes(null);
		assertNull(result);
	}

	@Test
	public void toHexString_Test() {
		String result = Strings.toHexString('\uABCD');
		assertEquals("ABCD", result);
	}

	@Test
	public void toCharacterSet_Test() {
		Set<Character> results = Strings.toCharacterSet("abcbcdcde");
		assertNotNull(results);
		assertEquals(5, results.size());
		assertTrue(results.contains('a'));
		assertTrue(results.contains('b'));
		assertTrue(results.contains('c'));
		assertTrue(results.contains('d'));
		assertTrue(results.contains('e'));
	}

	@Test
	public void toCharacterSet_Null_Value_Test() {
		Set<Character> results = Strings.toCharacterSet(null);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void getWordsFromCamelCase_Test() {
		List<String> results = Strings.getWordsFromCamelCase("oneTwoThreeFour");
		assertNotNull(results);
		assertEquals(4, results.size());
		assertEquals("one", results.get(0));
		assertEquals("Two", results.get(1));
		assertEquals("Three", results.get(2));
		assertEquals("Four", results.get(3));
	}

	@Test
	public void getWordsFromCamelCase_Null_Test() {
		List<String> results = Strings.getWordsFromCamelCase(null);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void count_Test() {
		int result = Strings.count("TestTestTestTestTestTestTest", "Test");
		assertEquals(7, result);
	}

	@Test
	public void count_Null_Test() {
		int result = Strings.count(null, "a");
		assertEquals(0, result);
	}

	@Test
	public void count_Null_Test2() {
		int result = Strings.count("a", null);
		assertEquals(0, result);
	}

	@Test
	public void replace_Test() {
		String result = Strings.replace("aaa", "a", "b");
		assertEquals("bbb", result);
	}

	@Test
	public void replace_Null_Test() {
		String result = Strings.replace(null, "a", "b");
		assertNull(result);
	}

}
