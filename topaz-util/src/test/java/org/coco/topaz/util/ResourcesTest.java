package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import static org.junit.Assert.*;

public class ResourcesTest {

	@Test
	public void close_Test(@Mocked Connection connection, @Mocked PreparedStatement preparedStatement, @Mocked ResultSet resultSet) throws Exception {
		AutoCloseable[] resources = {null, connection, preparedStatement, resultSet};

		List<Exception> result = Resources.close(resources);
		assertNotNull(result);
		assertEquals(0, result.size());

		new Verifications() {{
			connection.close(); times = 1;
			preparedStatement.close(); times = 1;
			resultSet.close(); times = 1;
		}};
	}

	@Test
	public void close_Null_Array_Test() {
		AutoCloseable[] resources = null;

		List<Exception> result = Resources.close(resources);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void close_Null_Collection_Test() {
		List<AutoCloseable> resources = null;

		List<Exception> result = Resources.close(resources);
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void close_Exception_Test(@Mocked Connection connection, @Mocked PreparedStatement preparedStatement, @Mocked ResultSet resultSet) throws Exception {
		AutoCloseable[] resources = {connection, preparedStatement, resultSet};
		Exception ioException = new IOException();

		new Expectations() {{
			connection.close(); result = ioException;
		}};

		List<Exception> result = Resources.close(resources);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(ioException, result.get(0));

		new Verifications() {{
			connection.close(); times = 1;
			preparedStatement.close(); times = 1;
			resultSet.close(); times = 1;
		}};
	}

}
