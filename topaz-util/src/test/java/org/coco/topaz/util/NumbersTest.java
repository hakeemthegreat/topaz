package org.coco.topaz.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumbersTest {

	private static final double DELTA = 0;

	@Test
	public void isPrime_Test() {
		assertFalse(Numbers.isPrime(-3));
		assertFalse(Numbers.isPrime(-2));
		assertFalse(Numbers.isPrime(-1));
		assertFalse(Numbers.isPrime(0));
		assertFalse(Numbers.isPrime(0.5));
		assertFalse(Numbers.isPrime(1));
		assertTrue(Numbers.isPrime(2));
		assertFalse(Numbers.isPrime(2.5));
		assertTrue(Numbers.isPrime(3));
		assertFalse(Numbers.isPrime(4));
		assertTrue(Numbers.isPrime(5));
		assertFalse(Numbers.isPrime(6));
		assertTrue(Numbers.isPrime(7));
		assertFalse(Numbers.isPrime(8));
		assertFalse(Numbers.isPrime(9));
		assertFalse(Numbers.isPrime(10));
		assertTrue(Numbers.isPrime(11));
		assertFalse(Numbers.isPrime(12));
		assertTrue(Numbers.isPrime(13));
		assertTrue(Numbers.isPrime(37591));
	}

	@Test(timeout = 30000)
	public void isPrime_Performance_Test() {
		Numbers.isPrime(Long.MAX_VALUE); //do not care about the value returned
	}

	@Test
	public void isWhole_Test() {
		assertTrue(Numbers.isWhole(1));
		assertFalse(Numbers.isWhole(1.5));
	}

	@Test
	public void isOdd_Test() {
		assertFalse(Numbers.isOdd(0.5));
		assertTrue(Numbers.isOdd(1));
		assertFalse(Numbers.isOdd(2));
	}

	@Test
	public void isEven_Test() {
		assertFalse(Numbers.isEven(0.5));
		assertFalse(Numbers.isEven(1));
		assertTrue(Numbers.isEven(2));
	}

	@Test
	public void isNatural_Test() {
		assertFalse(Numbers.isNatural(-1));
		assertFalse(Numbers.isNatural(0));
		assertTrue(Numbers.isNatural(1));
	}

	@Test
	public void round_Test() {
		assertEquals(100, Numbers.round(100.44748279546519735683748324163281473847264, 0), DELTA);
		assertEquals(100.4, Numbers.round(100.44748279546519735683748324163281473847264, 1), DELTA);
		assertEquals(100.45, Numbers.round(100.44748279546519735683748324163281473847264, 2), DELTA);
		assertEquals(100.447, Numbers.round(100.44748279546519735683748324163281473847264, 3), DELTA);
	}

	@Test
	public void truncate_Test() {
		assertEquals(100, Numbers.truncate(100.44748279546519735683748324163281473847264, 0), DELTA);
		assertEquals(100.4, Numbers.truncate(100.44748279546519735683748324163281473847264, 1), DELTA);
		assertEquals(100.44, Numbers.truncate(100.44748279546519735683748324163281473847264, 2), DELTA);
		assertEquals(100.447, Numbers.truncate(100.44748279546519735683748324163281473847264, 3), DELTA);
	}

}
