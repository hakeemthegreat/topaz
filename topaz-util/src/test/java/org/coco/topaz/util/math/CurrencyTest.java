package org.coco.topaz.util.math;

import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class CurrencyTest extends GetterSetterTest<Currency> {

	public CurrencyTest() {
		super(new Currency(5));
	}

	@Test
	public void add_Test() {
		Currency expected = new Currency(6.00);
		Currency result = instance.add(1);
		assertEquals(expected, result);
	}

	@Test
	public void subtract_Test() {
		Currency expected = new Currency(4.00);
		Currency result = instance.subtract(1);
		assertEquals(expected, result);
	}

	@Test
	public void multiply_Test() {
		Currency expected = new Currency(10.00);
		Currency result = instance.multiply(2);
		assertEquals(expected, result);
	}

	@Test
	public void divide_Test() {
		Currency expected = new Currency(2.50);
		Currency result = instance.divide(2);
		assertEquals(expected, result);
	}

	@Test
	public void byteValue_Test() {
		byte result = instance.byteValue();
		assertEquals(5, result);
	}

	@Test
	public void shortValue_Test() {
		short result = instance.shortValue();
		assertEquals(5, result);
	}

	@Test
	public void intValue_Test() {
		int result = instance.intValue();
		assertEquals(5, result);
	}

	@Test
	public void longValue_Test() {
		long result = instance.longValue();
		assertEquals(5, result);
	}

	@Test
	public void doubleValue_Test() {
		double result = instance.doubleValue();
		assertEquals(5.00, result, 0);
	}

	@Test
	public void floatValue_Test() {
		float result = instance.floatValue();
		assertEquals(5.00, result, 0.0000001);
	}

	@Test
	public void compareTo_Test() {
		Currency currency = new Currency(1);

		int result = instance.compareTo(currency);
		assertTrue(result > 0);
	}

	@Test
	public void compareTo_Null_Test() {
		int result = instance.compareTo(null);
		assertTrue(result > 0);
	}

	@Test
	public void equals_True_Test() {
		Currency currency = new Currency(5);

		boolean result = instance.equals(currency);
		assertTrue(result);
	}

	@Test
	public void equals_False_Test() {
		Currency currency = new Currency(6);

		boolean result = instance.equals(currency);
		assertFalse(result);
	}

	@Test
	public void equals_Null_Test() {
		boolean result = instance.equals(null);
		assertFalse(result);
	}

	@Test
	public void hashCode_Equal_Test() {
		Currency one = new Currency(1);
		Currency two = new Currency(1);

		int result1 = one.hashCode();
		int result2 = two.hashCode();
		assertEquals(result1, result2);
		assertEquals(one, two);
	}

	@Test
	public void hashCode_Not_Equal_Test() {
		Currency one = new Currency(1);
		Currency two = new Currency(2);

		int result1 = one.hashCode();
		int result2 = two.hashCode();
		assertNotEquals(result1, result2);
		assertNotEquals(one, two);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("$5.00", result);
	}

}
