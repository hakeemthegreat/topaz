package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrieTest {
	
	@Tested
	private Trie instance;

	@Test
	public void add_Test() {
		String string = "abc";

		boolean result = instance.add(string);
		assertTrue(result);

		boolean result2 = instance.contains(string);
		assertTrue(result2);
	}

	@Test
	public void add_Upper_Case_Test() {
		String string = "aBc";

		boolean result = instance.add(string);
		assertTrue(result);

		boolean result2 = instance.contains(string);
		assertTrue(result2);
	}

	@Test
	public void add_Null_Test() {

		boolean result = instance.add(null);
		assertFalse(result);
	}

	@Test
	public void add_Input_Contains_Digits_Test() {

		boolean result = instance.add("test0");
		assertFalse(result);
	}

	@Test
	public void contains_Empty_Try_Test() {
		String string = "abc";

		boolean result = instance.contains(string);
		assertFalse(result);
	}

	@Test
	public void contains_Multiple_Values_In_Try_Test() {
		String mop = "mop";
		String alt = "alt";
		String pen = "pen";
		String car = "car";
		String fox = "fox";

		instance.add(mop);
		instance.add(alt);
		instance.add(pen);
		instance.add(car);
		instance.add(fox);

		assertTrue(instance.contains(fox));
		assertTrue(instance.contains(car));
		assertTrue(instance.contains(pen));
		assertTrue(instance.contains(alt));
		assertTrue(instance.contains(mop));
	}

	@Test
	public void contains_Null_String_Test() {
		String string = null;

		boolean result = instance.contains(string);
		assertFalse(result);
	}

	@Test
	public void size_Multiple_Children_Test() {
		String mop = "mop";
		String alt = "alt";
		String pen = "pen";
		String car = "car";
		String fox = "fox";

		instance.add(mop);
		instance.add(alt);
		instance.add(pen);
		instance.add(car);
		instance.add(fox);

		int result = instance.size();
		assertEquals(5, result);
	}
	
	@Test
	public void size_One_Child_Test() {
		String mop = "mop";

		instance.add(mop);

		int result = instance.size();
		assertEquals(1, result);
	}

	@Test
	public void size_No_Children_Test() {

		int result = instance.size();
		assertEquals(0, result);
	}

	@Test
	public void clear_Test() {
		String mop = "mop";
		String alt = "alt";
		String pen = "pen";
		String car = "car";
		String fox = "fox";

		instance.add(mop);
		instance.add(alt);
		instance.add(pen);
		instance.add(car);
		instance.add(fox);

		instance.clear();
		assertEquals(0, instance.size());
	}

	@Test
	public void isEmpty_True_Test() {
		int size = 0;

		new Expectations(instance) {{
			instance.size(); result = size;
		}};

		boolean result = instance.isEmpty();
		assertTrue(result);
	}

	@Test
	public void isEmpty_False_Test() {
		int size = 2;

		new Expectations(instance) {{
			instance.size(); result = size;
		}};

		boolean result = instance.isEmpty();
		assertFalse(result);
	}

	@Test
	public void isNotEmpty_True_Test() {
		boolean isEmpty = false;

		new Expectations(instance) {{
			instance.isEmpty(); result = isEmpty;
		}};

		boolean result = instance.isNotEmpty();
		assertTrue(result);
	}

	@Test
	public void isNotEmpty_False_Test() {
		boolean isEmpty = true;

		new Expectations(instance) {{
			instance.isEmpty(); result = isEmpty;
		}};

		boolean result = instance.isNotEmpty();
		assertFalse(result);
	}

	@Test
	public void contains_Object_Test() {
		Object object = new Object();

		boolean result = instance.contains(object);
		assertFalse(result);
	}

	@Test
	public void contains_String_As_Object_Test() {
		String string = "string";
		Object object = string;
		boolean containsString = true;

		new Expectations(instance) {{
			instance.contains(anyString); result = containsString;
		}};

		boolean result = instance.contains(object);
		assertEquals(containsString, result);

		new Verifications() {{
			instance.contains(string); times = 1;
		}};
	}

	@Test
	public void toString_Test() {
		Trie trie = new Trie();

		trie.add("apple");
		trie.add("banana");
		trie.add("blueberry");
		trie.add("cherry");

		String result = trie.toString();
		assertEquals("* -> abc", result);
	}

}
