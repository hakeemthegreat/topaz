package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Mocked;
import org.coco.topaz.util.logging.Logger;
import org.coco.topaz.util.test.Dummy;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.*;

public class DatesTest {

	@Mocked
	private Logger logger;

	private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");

	public DatesTest() {
		simpleDateFormat.setTimeZone(UTC);
	}

	@Before
	public void before() {
		Dates.logger = logger;
	}

	@Test
	public void parseDate_Null_Test() throws Exception {
		Date result = Dates.parseDate(null);
		assertNull(result);
	}

	@Test
	public void parseDate_Test() throws Exception {
		Date result = Dates.parseDate("2020-01-24");
		assertNotNull(result);
		assertEquals("2020-01-24 00:00", simpleDateFormat.format(result));
	}

	@Test
	public void parseDate_Test2() throws Exception {
		Date result = Dates.parseDate("20200124");
		assertNotNull(result);
		assertEquals("2020-01-24 00:00", simpleDateFormat.format(result));
	}

	@Test
	public void parseDate_Test3() throws Exception {
		Date result = Dates.parseDate("2020-01-24 15:30");
		assertNotNull(result);
		assertEquals("2020-01-24 15:30", simpleDateFormat.format(result));
	}

	@Test(expected = ParseException.class)
	public void parseDate_Exception_Test() throws Exception {
		new Expectations(Dates.class) {{
			Dates.parseDate(anyString, anyString); result = new Exception();
		}};

		Dates.parseDate(Dummy.STRING);
	}

	@Test
	public void parseDate_With_Format_Test() throws Exception {
		Date result = Dates.parseDate("2020-01-24", "yyyy-MM-dd");
		assertNotNull(result);
		assertEquals("2020-01-24 00:00", simpleDateFormat.format(result));
	}

	@Test(expected = ParseException.class)
	public void parseDate_ParseException_Test() throws Exception {
		Dates.parseDate(Dummy.STRING, "yyyy-MM-dd");
	}

}
