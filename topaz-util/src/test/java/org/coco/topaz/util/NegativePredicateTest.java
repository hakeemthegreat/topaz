package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.coco.topaz.util.reflection.Methods;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class NegativePredicateTest {

	@Injectable
	private GetterPredicate getterPredicate;

	@Tested
	private NegativePredicate<Method> instance;

	private static final Method METHOD;

	static {
		METHOD = Methods.getMethod(Object.class, "toString");
	}

	@Test
	public void test_True() {
		new Expectations() {{
			getterPredicate.test(METHOD); result = false;
		}};

		assertTrue(instance.test(METHOD));
	}

	@Test
	public void test_False() {
		new Expectations() {{
			getterPredicate.test(METHOD); result = true;
		}};

		assertFalse(instance.test(METHOD));
	}

}
