package org.coco.topaz.util.logging;

import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

public class MasterLoggerTest {

	@Injectable
	private FileLogger fileLogger;

	@Injectable
	private ConsoleLogger consoleLogger;

	@Tested
	public MasterLogger instance;

	@Test
	public void null_Loggers_Array_Test(@Mocked SystemErrLogger systemErrLogger) {
		Logger[] loggers = null;
		new MasterLogger(loggers);

		new Verifications() {{
			systemErrLogger.warn("No loggers have been configured"); times = 1;
		}};
	}

	@Test
	public void empty_Loggers_Array_Test(@Mocked SystemErrLogger systemErrLogger) {
		Logger[] loggers = {};
		new MasterLogger(loggers);

		new Verifications() {{
			systemErrLogger.warn("No loggers have been configured"); times = 1;
		}};
	}

	@Test
	public void valid_Loggers_Array_Test(@Mocked SystemErrLogger systemErrLogger) {
		Logger[] loggers = {consoleLogger};
		new MasterLogger(loggers);

		new Verifications() {{
			systemErrLogger.warn(anyString); times = 0;
		}};
	}

	@Test
	public void trace_Test() {
		String message = "message";

		instance.trace(message);

		new Verifications() {{
			fileLogger.trace(message); times = 1;
			consoleLogger.trace(message); times = 1;
		}};
	}

	@Test
	public void trace_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.trace(message, exception);

		new Verifications() {{
			fileLogger.trace(message, exception); times = 1;
			consoleLogger.trace(message, exception); times = 1;
		}};
	}

	@Test
	public void debug_Test() {
		String message = "message";

		instance.debug(message);

		new Verifications() {{
			fileLogger.debug(message); times = 1;
			consoleLogger.debug(message); times = 1;
		}};
	}

	@Test
	public void debug_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.debug(message, exception);

		new Verifications() {{
			fileLogger.debug(message, exception); times = 1;
			consoleLogger.debug(message, exception); times = 1;
		}};
	}

	@Test
	public void info_Test() {
		String message = "message";

		instance.info(message);

		new Verifications() {{
			fileLogger.info(message); times = 1;
			consoleLogger.info(message); times = 1;
		}};
	}

	@Test
	public void info_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.info(message, exception);

		new Verifications() {{
			fileLogger.info(message, exception); times = 1;
			consoleLogger.info(message, exception); times = 1;
		}};
	}

	@Test
	public void warn_Test() {
		String message = "message";

		instance.warn(message);

		new Verifications() {{
			fileLogger.warn(message); times = 1;
			consoleLogger.warn(message); times = 1;
		}};
	}

	@Test
	public void warn_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.warn(message, exception);

		new Verifications() {{
			fileLogger.warn(message, exception); times = 1;
			consoleLogger.warn(message, exception); times = 1;
		}};
	}

	@Test
	public void error_Test() {
		String message = "message";

		instance.error(message);

		new Verifications() {{
			fileLogger.error(message); times = 1;
			consoleLogger.error(message); times = 1;
		}};
	}

	@Test
	public void error_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.error(message, exception);

		new Verifications() {{
			fileLogger.error(message, exception); times = 1;
			consoleLogger.error(message, exception); times = 1;
		}};
	}

	@Test
	public void fatal_Test() {
		String message = "message";

		instance.fatal(message);

		new Verifications() {{
			fileLogger.fatal(message); times = 1;
			consoleLogger.fatal(message); times = 1;
		}};
	}

	@Test
	public void fatal_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.fatal(message, exception);

		new Verifications() {{
			fileLogger.fatal(message, exception); times = 1;
			consoleLogger.fatal(message, exception); times = 1;
		}};
	}

}
