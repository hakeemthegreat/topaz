package org.coco.topaz.util.exception;

public class DuplicateKeyExceptionTest extends ExceptionTest<DuplicateKeyException> {

	public DuplicateKeyExceptionTest() {
		super(DuplicateKeyException.class);
	}

}
