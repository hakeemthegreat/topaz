package org.coco.topaz.util.reflection;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import mockit.Verifications;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.util.List;

import static org.junit.Assert.*;

public class ConstructorsTest extends JavaTest {

	@Getter
	@Setter
	private static class Employee {

		private Long id;
		private String name;

		public Employee() {

		}

		@SuppressWarnings("unused") //used for code coverage
		private Employee(Long id) {
			this.id = id;
		}

	}

	private static final Employee EMPLOYEE = new Employee();

	static {
		EMPLOYEE.setId(Dummy.LONG);
		EMPLOYEE.setName(Dummy.STRING);
	}

	@Test
	public void newInstance_Test() throws Exception {
		Constructor<Long> constructor = Long.class.getConstructor(String.class);
		Long result = Constructors.newInstance(constructor, Dummy.LONG.toString());
		assertEquals(Dummy.LONG, result);
	}

	@Test
	public void newInstance_Null_Test() {
		String result = Constructors.newInstance(null);
		assertNull(result);
	}

	@Test
	public void newInstance_ReflectiveOperationException_Test() throws Exception {
		Constructor<Employee> privateConstructor = Employee.class.getDeclaredConstructor(Long.class);
		ReflectiveOperationException reflectiveOperationException = new ReflectiveOperationException();

		new Expectations(privateConstructor) {{
			privateConstructor.newInstance((Object[]) any); result = reflectiveOperationException;
		}};

		expect(RuntimeException.class, reflectiveOperationException);

		Constructors.newInstance(privateConstructor);

		new Verifications() {{
			privateConstructor.newInstance((Object[]) null); times = 1;
		}};
	}

	@Test
	public void getConstructor_Test() {
		Constructor<Long> result = Constructors.getConstructor(Long.class, String.class);
		assertNotNull(result);
	}

	@Test
	public void getConstructor_ReflectiveOperationException_Test() {
		Constructor<Long> result = Constructors.getConstructor(Long.class);
		assertNull(result);
	}

	@Test
	public void getConstructors_Create_Then_Get_From_Cache_Test() {
		List<Constructor<?>> results = Constructors.getConstructors(Employee.class);
		assertNotNull(results);
		assertFalse(results.isEmpty());

		List<Constructor<?>> results2 = Constructors.getConstructors(Employee.class);
		assertSame(results, results2);
	}

	@Test
	public void getDefaultConstructor_Test() throws Exception {
		Constructor<Object> defaultConstructor = Object.class.getConstructor();

		Constructor<Object> result = Constructors.getDefaultConstructor(Object.class);
		assertEquals(defaultConstructor, result);
	}

	@Test
	public void getDefaultConstructor_No_Default_Constructor_Test() {
		Constructor<Long> result = Constructors.getDefaultConstructor(Long.class);
		assertNull(result);
	}

	@Test
	public void getDefaultConstructor_Null_Class_Test() {
		Constructor<Long> result = Constructors.getDefaultConstructor(null);
		assertNull(result);
	}

}
