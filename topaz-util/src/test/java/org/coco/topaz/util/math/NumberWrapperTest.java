package org.coco.topaz.util.math;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.*;

public class NumberWrapperTest {

	private static final double VALUE = 1.0;
	private final NumberWrapper instance = new NumberWrapper(VALUE);

	@Test
	public void value_Getter_Setter_Test() {
		instance.setValue(Dummy.LONG);
		assertEquals(Dummy.LONG, instance.getValue());
	}

	@Test
	public void isGreaterThan_False_Test() {
		boolean result = instance.isGreaterThan(2);
		assertFalse(result);
	}

	@Test
	public void isGreaterThan_True_Test() {
		boolean result = instance.isGreaterThan(0);
		assertTrue(result);
	}

	@Test
	public void isGreaterThan_Null_Test() {
		instance.setValue(null);

		boolean result = instance.isGreaterThan(2);
		assertFalse(result);
	}

	@Test
	public void isLessThan_False_Test() {
		boolean result = instance.isLessThan(0);
		assertFalse(result);
	}

	@Test
	public void isLessThan_True_Test() {
		boolean result = instance.isLessThan(2);
		assertTrue(result);
	}

	@Test
	public void isLessThan_Null_Test() {
		instance.setValue(null);

		boolean result = instance.isLessThan(2);
		assertFalse(result);
	}

	@Test
	public void isEqualTo_False_Test() {
		boolean result = instance.isEqualTo(0);
		assertFalse(result);
	}

	@Test
	public void isEqualTo_True_Test() {
		boolean result = instance.isEqualTo(1);
		assertTrue(result);
	}

	@Test
	public void isEqualTo_Null_Test() {
		instance.setValue(null);

		boolean result = instance.isEqualTo(2);
		assertFalse(result);
	}

}
