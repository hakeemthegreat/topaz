package org.coco.topaz.util.test;

public class AbstractFactoryTestTest extends AbstractFactoryTest {

	private static class Factory {

		public String newString() {
			return "";
		}

		public Long newLong() {
			return 0L;
		}

	}

	public AbstractFactoryTestTest() {
		super(new Factory());
	}

}
