package org.coco.topaz.util;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class MapsTest {

	@Getter
	@Setter
	private static class Employee implements Serializable {
		private Long id;
		private String name;
		private static final long serialVersionUID = 1L;
	}

	@Test
	public void toMap_Test() {
		Map<String, Object> map = new HashMap<>();
		Employee employee = new Employee();

		new Expectations(Maps.class) {{
			Maps.toMap(any, anyBoolean); result = map;
		}};

		Map<String, Object> result = Maps.toMap(employee);
		assertSame(map, result);

		new Verifications() {{
			Maps.toMap(employee, true); times = 1;
		}};
	}

	@Test
	public void toMap_Include_Static_Fields_Test() {
		Long id = 123L;
		String name = "name";
		Employee employee = new Employee();

		employee.setId(id);
		employee.setName(name);

		Map<String, Object> results = Maps.toMap(employee, true);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertEquals(id, results.get("id"));
		assertEquals(name, results.get("name"));
		assertEquals(Employee.serialVersionUID, results.get("serialVersionUID"));
	}

	@Test
	public void toMap_Do_Not_Include_Static_Fields_Test() {
		Long id = 123L;
		String name = "name";
		Employee employee = new Employee();

		employee.setId(id);
		employee.setName(name);

		Map<String, Object> results = Maps.toMap(employee, false);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertEquals(id, results.get("id"));
		assertEquals(name, results.get("name"));
	}

	@Test
	public void toMap_Null_Test() {
		Map<String, Object> result = Maps.toMap(null);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

}
