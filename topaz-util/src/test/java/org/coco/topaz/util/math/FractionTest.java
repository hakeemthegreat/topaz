package org.coco.topaz.util.math;

import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest extends GetterSetterTest<Fraction> {

	private static final double DELTA = 0.1;

	public FractionTest() {
		super(new Fraction(5, 2));
	}

	@Test
	public void add_Test() {
		Fraction fraction = new Fraction(3, 7);
		Fraction expected = new Fraction(41, 14);

		Fraction result = instance.add(fraction);
		assertEquals(expected, result);
	}

	@Test
	public void subtract_Test() {
		Fraction fraction = new Fraction(3, 7);
		Fraction expected = new Fraction(29, 14);

		Fraction result = instance.subtract(fraction);
		assertEquals(expected, result);
	}

	@Test
	public void multiply_Test() {
		Fraction fraction = new Fraction(3, 7);
		Fraction expected = new Fraction(15, 14);

		Fraction result = instance.multiply(fraction);
		assertEquals(expected, result);
	}

	@Test
	public void divide_Test() {
		Fraction fraction = new Fraction(3, 7);
		Fraction expected = new Fraction(35, 6);

		Fraction result = instance.divide(fraction);
		assertEquals(expected, result);
	}

	@Test
	public void invert_Test() {
		Fraction expected = new Fraction(2, 5);

		Fraction result = instance.invert();
		assertEquals(expected, result);
	}

	@Test
	public void byteValue_Test() {
		byte result = instance.byteValue();
		assertEquals(2, result);
	}

	@Test
	public void shortValue_Test() {
		short result = instance.shortValue();
		assertEquals(2, result);
	}

	@Test
	public void intValue_Test() {
		int result = instance.intValue();
		assertEquals(2, result);
	}

	@Test
	public void longValue_Test() {
		long result = instance.longValue();
		assertEquals(2, result);
	}

	@Test
	public void floatValue_Test() {
		float result = instance.floatValue();
		assertEquals(2.5, result, DELTA);
	}

	@Test
	public void doubleValue_Test() {
		double result = instance.doubleValue();
		assertEquals(2.5, result, DELTA);
	}

	@Test
	public void compareTo_Equals_Test() {
		Fraction fraction = new Fraction(10, 4);

		int result = instance.compareTo(fraction);
		assertEquals(0, result);
	}

	@Test
	public void compareTo_Less_Than_Test() {
		Fraction fraction = new Fraction(30, 10);

		int result = instance.compareTo(fraction);
		assertTrue(result < 0);
	}

	@Test
	public void compareTo_Greater_Than_Test() {
		Fraction fraction = new Fraction(1, 10);

		int result = instance.compareTo(fraction);
		assertTrue(result > 0);
	}

	@Test
	public void equals_True_Test() {
		Fraction fraction = new Fraction(5, 2);

		boolean result = instance.equals(fraction);
		assertTrue(result);
	}

	@Test
	public void equals_False_Test() {
		Fraction fraction = new Fraction(9, 37);

		boolean result = instance.equals(fraction);
		assertFalse(result);
	}

	@Test
	public void hashCode_Test() {
		int result = instance.hashCode();
		assertEquals(2, result);
	}

	@Test
	public void equals_Wrong_Object_Test() {
		boolean result = instance.equals(Dummy.OBJECT);
		assertFalse(result);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("5/2", result);
	}

}
