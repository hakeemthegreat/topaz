package org.coco.topaz.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReplaceTest {

	private static final Object REPLACEMENT = new Object();

	@Test
	public void with_Should_Be_Null_Value_Is_Null_Test() {
		Object result = Replace.with(REPLACEMENT).should(null).beNull();
		assertEquals(REPLACEMENT, result);
	}

	@Test
	public void with_Should_Be_Null_Value_Is_Not_Null_Test() {
		Object object = new Object();

		Object result = Replace.with(REPLACEMENT).should(object).beNull();
		assertEquals(object, result);
	}

	@Test
	public void with_Should_Be_Same_Object_Test() {
		Object object = new Object();

		Object result = Replace.with(REPLACEMENT).should(object).be(object);
		assertEquals(REPLACEMENT, result);
	}

	@Test
	public void with_Should_Be_Different_Objects_Test() {
		Object object = new Object();
		Object object2 = new Object();

		Object result = Replace.with(REPLACEMENT).should(object).be(object2);
		assertEquals(object, result);
	}

	@Test
	public void with_Should_Equal_Values_Are_Equal_Test() {
		int value = 123;
		int otherValue = 123;

		Object result = Replace.with(REPLACEMENT).should(value).beEqualTo(otherValue);
		assertEquals(REPLACEMENT, result);
	}

	@Test
	public void with_Should_Equal_Values_Are_Not_Equal_Test() {
		int value = 123;
		int otherValue = 456;

		Object result = Replace.with(REPLACEMENT).should(value).beEqualTo(otherValue);
		assertEquals(value, result);
	}

	@Test
	public void with_Should_Be_Less_Than_True_Test() {
		int value = 1;
		int otherValue = 2;

		Object result = Replace.with(REPLACEMENT).should(value).beLessThan(otherValue);
		assertEquals(REPLACEMENT, result);
	}

	@Test
	public void with_Should_Be_Less_Than_False_Test() {
		int value = 2;
		int otherValue = 1;

		Object result = Replace.with(REPLACEMENT).should(value).beLessThan(otherValue);
		assertEquals(value, result);
	}

	@Test
	public void with_Should_Be_Greater_Than_False_Test() {
		int value = 1;
		int otherValue = 2;

		Object result = Replace.with(REPLACEMENT).should(value).beGreaterThan(otherValue);
		assertEquals(value, result);
	}

	@Test
	public void with_Should_Be_Greater_Than_True_Test() {
		int value = 2;
		int otherValue = 1;

		Object result = Replace.with(REPLACEMENT).should(value).beGreaterThan(otherValue);
		assertEquals(REPLACEMENT, result);
	}

	@Test
	public void compare_Value_Is_A_Number_Test() {
		Object value = 123;
		Number number = 123;

		int result = Replace.compare(value, number);
		assertEquals(0, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void compare_Value_Is_Not_A_Number_Test() {
		Object value = "value";
		Number number = 123;

		Replace.compare(value, number);
	}

}
