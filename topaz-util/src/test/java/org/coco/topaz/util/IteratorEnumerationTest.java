package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class IteratorEnumerationTest {

	@Injectable
	private Iterator<Object> iterator;

	@Tested
	private IteratorEnumeration<Object> instance;

	@Test
	public void hasMoreElements_Test() {
		boolean expected = true;

		new Expectations() {{
			iterator.hasNext(); result = expected;
		}};

		boolean result = instance.hasMoreElements();
		assertEquals(expected, result);
	}

	@Test
	public void nextElement_Test() {
		Object expected = new Object();

		new Expectations() {{
			iterator.next(); result = expected;
		}};

		Object result = instance.nextElement();
		assertEquals(expected, result);
	}

}
