package org.coco.topaz.util.html;

import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TextElementTest {
	
	@Injectable
	private String text = "text";
	
	@Tested
	private TextElement instance;

	@Test(expected = UnsupportedOperationException.class)
	public void addChild_Test() {
		HTMLElement htmlElement = new HTMLElement("custom");
		instance.addChild(htmlElement);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getChildren_Test() {
		instance.getChildren();
	}
	
	@Test
	public void value_Getter_Setter_Test() {
		String moreText = "moreText";
		
		instance.setValue(moreText);
		assertEquals(moreText, instance.getValue());
	}

	@Test
	public void value_Getter_Setter_Null_Test() {
		instance.setValue(null);
		assertEquals(TextElement.DEFAULT_VALUE, instance.getValue());
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals(text, result);
	}
	
}
