package org.coco.topaz.util.logging;

import org.coco.topaz.util.logging.Logger.Classifier;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class PrintStreamLoggerTest {

	private final Class<?> clazz = Object.class;
	private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	private final PrintStream printStream = new PrintStream(byteArrayOutputStream);
	private final PrintStreamLogger instance = new PrintStreamLogger(clazz, printStream);

	private static final String MESSAGE = "message";
	private static final Throwable THROWABLE = new Throwable();

	@Before
	public void before() {
		byteArrayOutputStream.reset();
	}

	@Test
	public void log_Message_Only_Test() {
		Classifier classifier = Classifier.DEBUG;

		instance.log(classifier, MESSAGE, null);

		String result = byteArrayOutputStream.toString();
		assertNotNull(result);
		assertTrue(result.contains(classifier.toString()));
		assertTrue(result.contains(clazz.getName()));
		assertTrue(result.contains(":31"));
		assertTrue(result.contains(MESSAGE));
	}

	@Test
	public void log_Message_And_Throwable_Test() {
		Classifier classifier = Classifier.DEBUG;

		instance.log(classifier, MESSAGE, THROWABLE);

		String result = byteArrayOutputStream.toString();
		assertNotNull(result);
		assertTrue(result.contains(classifier.toString()));
		assertTrue(result.contains(clazz.getName()));
		assertTrue(result.contains(":45"));
		assertTrue(result.contains(MESSAGE));
		assertTrue(result.contains(THROWABLE.toString()));
	}

	@Test
	public void info_Message_Only_Test() {
		instance.info(MESSAGE);

		String result = byteArrayOutputStream.toString();
		assertNotNull(result);
		assertTrue(result.contains(Classifier.INFO.toString()));
		assertTrue(result.contains(clazz.getName()));
		assertTrue(result.contains(":58"));
		assertTrue(result.contains(MESSAGE));
	}

	@Test
	public void info_Message_And_Throwable_Test() {
		instance.info(MESSAGE, THROWABLE);

		String result = byteArrayOutputStream.toString();
		assertNotNull(result);
		assertTrue(result.contains(Classifier.INFO.toString()));
		assertTrue(result.contains(clazz.getName()));
		assertTrue(result.contains(":70"));
		assertTrue(result.contains(MESSAGE));
		assertTrue(result.contains(THROWABLE.toString()));
	}

}