package org.coco.topaz.util.node;

import org.coco.topaz.util.node.TrieNode;
import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TrieNodeTest extends GetterSetterTest<TrieNode> {

	public TrieNodeTest() {
		super(new TrieNode('*'));
	}

	@Test
	public void child_Getter_Setter_Test() {
		TrieNode child = new TrieNode('a');
		int index = 7;

		instance.setChild(index, child);

		TrieNode result = instance.getChild(index);
		assertEquals(child, result);
	}

	@Test
	public void toString_Test() {
		TrieNode trieNode = new TrieNode('*');

		trieNode.setChild(0, new TrieNode('a'));
		trieNode.setChild(1, new TrieNode('b'));
		trieNode.setChild(2, new TrieNode('c'));

		String result = trieNode.toString();
		assertEquals("* -> abc", result);
	}

}
