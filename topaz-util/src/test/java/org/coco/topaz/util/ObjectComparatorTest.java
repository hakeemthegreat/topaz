package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ObjectComparatorTest {

	@Tested
	private ObjectComparator instance;

	@Test
	public void compare_Test() {
		Object left = new Object();
		Object right = new Object();
		String leftString = "leftString";
		String rightString = "rightString";
		int expected = 3;

		new Expectations(Strings.class, ObjectUtils.class) {{
			Strings.toString(any); returns(leftString, rightString); times = 2;
			ObjectUtils.compare(anyString, anyString); result = expected;
		}};

		int result = instance.compare(left, right);
		assertEquals(expected, result);

		new Verifications() {{
			Strings.toString(left); times = 1;
			Strings.toString(right); times = 1;
			ObjectUtils.compare(leftString, rightString); times = 1;
		}};
	}

}
