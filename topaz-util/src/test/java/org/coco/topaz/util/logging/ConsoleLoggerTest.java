package org.coco.topaz.util.logging;

import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.Before;
import org.junit.Test;

public class ConsoleLoggerTest {

	@Mocked
	private SystemOutLogger systemOutLogger;

	@Mocked
	private SystemErrLogger systemErrLogger;

	public ConsoleLogger instance;

	private static final Class<?> CLASS = Object.class;

	@Before
	public void before() {
		instance = new ConsoleLogger(CLASS);
		Deencapsulation.setField(instance, "systemOutLogger", systemOutLogger);
		Deencapsulation.setField(instance, "systemErrLogger", systemErrLogger);
	}

	@Test
	public void trace_Test() {
		String message = "message";

		instance.trace(message);

		new Verifications() {{
			systemOutLogger.trace(message); times = 1;
		}};
	}

	@Test
	public void trace_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.trace(message, exception);

		new Verifications() {{
			systemOutLogger.trace(message, exception); times = 1;
		}};
	}

	@Test
	public void debug_Test() {
		String message = "message";

		instance.debug(message);

		new Verifications() {{
			systemOutLogger.debug(message); times = 1;
		}};
	}

	@Test
	public void debug_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.debug(message, exception);

		new Verifications() {{
			systemOutLogger.debug(message, exception); times = 1;
		}};
	}

	@Test
	public void info_Test() {
		String message = "message";

		instance.info(message);

		new Verifications() {{
			systemOutLogger.info(message); times = 1;
		}};
	}

	@Test
	public void info_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.info(message, exception);

		new Verifications() {{
			systemOutLogger.info(message, exception); times = 1;
		}};
	}

	@Test
	public void warn_Test() {
		String message = "message";

		instance.warn(message);

		new Verifications() {{
			systemErrLogger.warn(message); times = 1;
		}};
	}

	@Test
	public void warn_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.warn(message, exception);

		new Verifications() {{
			systemErrLogger.warn(message, exception); times = 1;
		}};
	}

	@Test
	public void error_Test() {
		String message = "message";

		instance.error(message);

		new Verifications() {{
			systemErrLogger.error(message); times = 1;
		}};
	}

	@Test
	public void error_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.error(message, exception);

		new Verifications() {{
			systemErrLogger.error(message, exception); times = 1;
		}};
	}

	@Test
	public void fatal_Test() {
		String message = "message";

		instance.fatal(message);

		new Verifications() {{
			systemErrLogger.fatal(message); times = 1;
		}};
	}

	@Test
	public void fatal_With_Exception_Test() {
		String message = "message";
		Exception exception = new Exception();

		instance.fatal(message, exception);

		new Verifications() {{
			systemErrLogger.fatal(message, exception); times = 1;
		}};
	}

}
