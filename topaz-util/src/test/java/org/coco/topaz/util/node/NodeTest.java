package org.coco.topaz.util.node;

import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.*;

public class NodeTest {

	@Tested
	private Node instance;

	private static final String VALUE = "value";

	@Test
	public void getter_Setter_Test() {
		instance.setValue(VALUE);

		String result = instance.getValue();
		assertEquals(VALUE, result);
	}

	@Test
	public void getter_Setter_ClassCastException_Test() {
		Long result = null;

		try {
			instance.setValue(VALUE);
			result = instance.getValue();
			fail();
		} catch (ClassCastException e) {
			assertNull(result);
		}
	}

}
