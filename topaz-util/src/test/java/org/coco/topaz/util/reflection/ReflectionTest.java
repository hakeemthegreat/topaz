package org.coco.topaz.util.reflection;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static org.junit.Assert.*;

public class ReflectionTest extends JavaTest {

	@Getter
	@Setter
	private static class Employee {

		private Long id;
		private String name;
		private final Object finalField = "finalField";

		public Employee() {

		}

	}

	private static class Task {

		private Task() {

		}

	}

	private static class SingletonClass {

		private static final SingletonClass INSTANCE = new SingletonClass();

		private SingletonClass() {

		}

		public static SingletonClass getInstance() {
			return INSTANCE;
		}

	}

	private static final Employee EMPLOYEE = new Employee();

	static {
		EMPLOYEE.setId(Dummy.LONG);
		EMPLOYEE.setName(Dummy.STRING);
	}

	@Test
	public void getInstance_Test() {
		Employee result = Reflection.newInstance(Employee.class);
		assertNotNull(result);
	}


	@Test
	public void newInstance_ReflectiveOperationException_Test() throws Exception {
		ReflectiveOperationException reflectiveOperationException = new ReflectiveOperationException();
		Constructor<?> constructor = Object.class.getConstructor();
		Object[] arguments = {"a", 1, new Object()};

		new Expectations(constructor) {{
			constructor.newInstance((Object[]) any); result = reflectiveOperationException;
		}};

		expect(RuntimeException.class, reflectiveOperationException);

		Reflection.newInstance(constructor, arguments);
	}

	@Test
	public void newInstance_With_Object_Constructor_Test() throws Exception {
		Constructor<?> constructor = Object.class.getConstructor();

		Object result = Reflection.newInstance(constructor);
		assertNotNull(result);
	}

	@Test
	public void newInstance_With_Null_Constructor_Test() {
		Constructor<Long> constructor = null;

		Long result = Reflection.newInstance(constructor);
		assertNull(result);
	}

	@Test
	public void setAccessible_Test(@Mocked AccessibleObject accessibleObject) {
		Reflection.makeAccessible(accessibleObject);

		new Verifications() {{
			accessibleObject.setAccessible(true); times = 1;
		}};
	}

	@Test
	public void setAccessible_Null_Test(@Mocked AccessibleObject accessibleObject) {
		Reflection.makeAccessible(null);

		new Verifications() {{
			accessibleObject.setAccessible(anyBoolean); times = 0;
		}};
	}

	@Test
	public void getInstance_Class_With_Private_Constructor_Test() {
		Task result = Reflection.newInstance(Task.class);
		assertNotNull(result);
	}

	@Test
	public void removeFinalModifier_Test() throws Exception {
		Employee employee = new Employee();
		Field finalField = Fields.getField("finalField", employee);
		String newValueForFinalField = "newValueForFinalField";

		Reflection.removeFinalModifier(finalField);
		finalField.setAccessible(true);
		finalField.set(employee, newValueForFinalField);
		assertEquals(newValueForFinalField, employee.getFinalField());
	}

	@Test
	public void removeModifier_Test() throws Exception {
		Employee employee = new Employee();
		Field finalField = Fields.getField("finalField", employee);
		String newValueForFinalField = "newValueForFinalField";
		int modifier = Modifier.FINAL;

		Reflection.removeModifier(finalField, modifier);
		finalField.setAccessible(true);
		finalField.set(employee, newValueForFinalField);
		assertEquals(newValueForFinalField, employee.getFinalField());
	}

	@Test
	public void removeModifier_Null_Test() {
		int modifier = Modifier.NATIVE;
		Reflection.removeModifier(null, modifier);
	}

	@Test
	public void removeModifier_IllegalAccessException_Test(@Mocked Field field, @Mocked Field modifiers) throws Exception {
		IllegalAccessException illegalAccessException = new IllegalAccessException();
		int modifier = Modifier.NATIVE;
		int modifierValues = modifier;

		new Expectations(Fields.class, Reflection.class) {{
			Fields.getField(anyString, any); result = modifiers;
			Reflection.makeAccessible((AccessibleObject) any); //do nothing
			field.getModifiers(); result = modifierValues;
			modifiers.set(any, any); result = illegalAccessException;
		}};

		expect(RuntimeException.class, illegalAccessException);

		Reflection.removeModifier(field, modifier);

		new Verifications() {{
			Fields.getField("modifiers", field); times = 1;
			Reflection.makeAccessible(modifiers); times = 1;
			modifiers.set(field, 0); times = 1;
		}};
	}

	@Test
	public void isStatic_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		assertFalse(Reflection.isStatic(id));
	}

	@Test
	public void isNotStatic_True_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		assertTrue(Reflection.isNotStatic(id));
	}

	@Test
	public void isNotStatic_False_Test() throws Exception {
		Field serialVersionUID = String.class.getDeclaredField("serialVersionUID");
		assertFalse(Reflection.isNotStatic(serialVersionUID));
	}

	@Test
	public void isFinal_Test() throws Exception {
		Field name = Employee.class.getDeclaredField("name");
		assertFalse(Reflection.isFinal(name));
	}

	@Test
	public void isNotFinal_True_Test() throws Exception {
		Field name = Employee.class.getDeclaredField("name");
		assertTrue(Reflection.isNotFinal(name));
	}

	@Test
	public void isNotFinal_False_Test() throws Exception {
		Field finalField = Employee.class.getDeclaredField("finalField");
		assertFalse(Reflection.isNotFinal(finalField));
	}

}
