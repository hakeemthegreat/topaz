package org.coco.topaz.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CollectionUtilsTest {

	private static final Collection<Object> NULL_COLLECTION = null;

	private static final Collection<Object> EMPTY_COLLECTION = Collections.emptyList();

	private static final List<Object> NON_EMPTY_LIST = new ArrayList<>(Arrays.asList(Dummy.INT, Dummy.INT2, Dummy.INT3, Dummy.LONG));
	private static final Collection<Object> NON_EMPTY_COLLECTION = NON_EMPTY_LIST;

	@Test
	public void isEmpty_Empty_Collection_Test() {
		boolean result = CollectionUtils.isEmpty(EMPTY_COLLECTION);
		assertTrue(result);
	}

	@Test
	public void isEmpty_Null_Collection_Test() {
		boolean result = CollectionUtils.isEmpty(NULL_COLLECTION);
		assertTrue(result);
	}

	@Test
	public void isEmpty_Non_Empty_Collection_Test() {
		boolean result = CollectionUtils.isEmpty(NON_EMPTY_COLLECTION);
		assertFalse(result);
	}

	@Test
	public void isNotEmpty_Empty_Collection_Test() {
		boolean result = CollectionUtils.isNotEmpty(EMPTY_COLLECTION);
		assertFalse(result);
	}

	@Test
	public void isNotEmpty_Null_Collection_Test() {
		boolean result = CollectionUtils.isNotEmpty(NULL_COLLECTION);
		assertFalse(result);
	}

	@Test
	public void isNotEmpty_Non_Empty_Collection_Test() {
		boolean result = CollectionUtils.isNotEmpty(NON_EMPTY_COLLECTION);
		assertTrue(result);
	}

	@Test
	public void filter_Test() {
		List<Integer> results = CollectionUtils.filter(NON_EMPTY_COLLECTION, Integer.class);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertEquals(Dummy.INT, results.get(0));
		assertEquals(Dummy.INT2, results.get(1));
		assertEquals(Dummy.INT3, results.get(2));
	}

	@Test
	public void filter_Null_Test() {
		Set<Integer> results = CollectionUtils.filter(NULL_COLLECTION, Integer.class);
		assertNull(results);
	}

	@Test
	public void toList_With_List_Test() {
		Collection<Object> collection = new ArrayList<>();

		collection.add(Dummy.OBJECT);
		collection.add(Dummy.OBJECT2);
		collection.add(Dummy.OBJECT3);

		List<Object> results = CollectionUtils.toList(collection);
		assertSame(collection, results);
	}

	@Test
	public void toList_With_Set_Test() {
		Collection<Object> collection = new HashSet<>();

		collection.add(Dummy.OBJECT);
		collection.add(Dummy.OBJECT2);
		collection.add(Dummy.OBJECT3);

		List<Object> results = CollectionUtils.toList(collection);
		assertTrue(results.containsAll(collection));
	}

	@Test
	public void toList_With_Object_Test() {
		List<Object> results = CollectionUtils.toList(Dummy.OBJECT);
		assertEquals(1, results.size());
		assertTrue(results.contains(Dummy.OBJECT));
	}

	@Test
	public void toList_Array_Test() {
		Object[] array = {Dummy.OBJECT, Dummy.OBJECT2, Dummy.OBJECT3};

		List<Object> results = CollectionUtils.toList(array);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertEquals(Dummy.OBJECT, results.get(0));
		assertEquals(Dummy.OBJECT2, results.get(1));
		assertEquals(Dummy.OBJECT3, results.get(2));
	}

	@Test
	public void toList_Null_Array_Test() {
		Object[] array = null;

		List<Object> results = CollectionUtils.toList(array);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void toList_Null_Collection_Test() {
		Collection<Object> collection = null;

		List<Object> results = CollectionUtils.toList(collection);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void toSet_With_List_Test() {
		Collection<Object> collection = new ArrayList<>();

		collection.add(Dummy.OBJECT);
		collection.add(Dummy.OBJECT2);
		collection.add(Dummy.OBJECT3);

		Set<Object> results = CollectionUtils.toSet(collection);
		assertTrue(results.containsAll(collection));
	}

	@Test
	public void toSet_With_Set_Test() {
		Collection<Object> collection = new HashSet<>();

		collection.add(Dummy.OBJECT);
		collection.add(Dummy.OBJECT2);
		collection.add(Dummy.OBJECT3);

		Set<Object> results = CollectionUtils.toSet(collection);
		assertSame(collection, results);
	}

	@Test
	public void toSet_With_Object_Test() {
		Set<Object> results = CollectionUtils.toSet(Dummy.OBJECT);
		assertEquals(1, results.size());
		assertTrue(results.contains(Dummy.OBJECT));
	}

	@Test
	public void toSet_Array_Test() {
		Object[] array = {Dummy.OBJECT, Dummy.OBJECT2, Dummy.OBJECT3};

		Set<Object> results = CollectionUtils.toSet(array);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertTrue(results.contains(Dummy.OBJECT));
		assertTrue(results.contains(Dummy.OBJECT2));
		assertTrue(results.contains(Dummy.OBJECT3));
	}

	@Test
	public void toSet_Null_Array_Test() {
		Object[] array = null;

		Set<Object> results = CollectionUtils.toSet(array);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void toSet_Null_Test() {
		Set<Object> results = CollectionUtils.toSet((Collection<Object>) null);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void get_Test() {
		Object result = CollectionUtils.get(NON_EMPTY_LIST, 1);
		assertEquals(Dummy.INT2, result);
	}

	@Test
	public void get_Index_Too_Small_Test() {
		Object result = CollectionUtils.get(NON_EMPTY_LIST, Integer.MIN_VALUE);
		assertNull(result);
	}

	@Test
	public void get_Index_Too_Big_Test() {
		Object result = CollectionUtils.get(NON_EMPTY_LIST, Integer.MAX_VALUE);
		assertNull(result);
	}

	@Test
	public void get_Null_List_Test() {
		Object result = CollectionUtils.get(null, 1);
		assertNull(result);
	}

}
