package org.coco.topaz.util.html;

import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StyleTest {

	@Tested
	private Style instance;

	@Test
	public void toString_Test() {
		instance.add("margin", "auto");
		instance.add("border", "outset");

		String result = instance.toString();
		assertEquals("border: outset; margin: auto; ", result);
	}

}
