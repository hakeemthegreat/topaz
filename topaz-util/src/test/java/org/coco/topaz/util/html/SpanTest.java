package org.coco.topaz.util.html;

import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpanTest {

	@Tested
	private Span instance;

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("<span></span>", result);
	}

}
