package org.coco.topaz.util;

import org.junit.Test;

import java.util.Calendar;

@SuppressWarnings("deprecation")
public class ImmutableDateTest {

	private final ImmutableDate instance = new ImmutableDate(123456789);

	@Test(expected = UnsupportedOperationException.class)
	public void setYear_Test() {
		instance.setYear(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setDate_Test() {
		instance.setDate(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setHours_Test() {
		instance.setHours(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setMinutes_Test() {
		instance.setMinutes(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setMonth_Test() {
		instance.setMonth(Calendar.JANUARY);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setSeconds_Test() {
		instance.setSeconds(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void setTimer_Test() {
		instance.setTime(0);
	}

}
