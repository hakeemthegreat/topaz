package org.coco.topaz.util;

import org.coco.topaz.util.math.Fraction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParsingTest {

	private final Parsing instance = Parsing.getInstance();

	@Test
	public void boolean_Test() {
		Boolean expected = true;
		String source = expected.toString();

		Boolean result = instance.from(source).to(Boolean.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void byte_Test() {
		Byte expected = 1;
		String source = expected.toString();

		Byte result = instance.from(source).to(Byte.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void double_Test() {
		Double expected = 12.34;
		String source = expected.toString();

		Double result = instance.from(source).to(Double.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void float_Test() {
		Float expected = 12.34F;
		String source = expected.toString();

		Float result = instance.from(source).to(Float.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void integer_Test() {
		Integer expected = 1234;
		String source = expected.toString();

		Integer result = instance.from(source).to(Integer.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void long_Test() {
		Long expected = 1234L;
		String source = expected.toString();

		Long result = instance.from(source).to(Long.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void short_Test() {
		Short expected = 1234;
		String source = expected.toString();

		Short result = instance.from(source).to(Short.class).now();
		assertEquals(expected, result);
	}

	@Test
	public void null_Test() {
		Double expected = null;
		String source = null;

		Double result = instance.from(source).to(Double.class).now();
		assertEquals(expected, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void IllegalArgumentException_Test() {
		Fraction expected = new Fraction(2, 5);
		String source = expected.toString();

		Fraction result = instance.from(source).to(Fraction.class).now();
		assertEquals(expected, result);
	}

}
