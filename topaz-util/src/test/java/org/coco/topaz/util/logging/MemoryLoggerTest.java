package org.coco.topaz.util.logging;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MemoryLoggerTest {

	private final MemoryLogger instance = new MemoryLogger(Object.class);

	@Test
	public void log_Test() {
		Throwable throwable = new Throwable();
		String message = "message";

		instance.info(message, throwable);
		instance.warn(message, throwable);
		instance.error(message, throwable);

		List<Message> result = instance.getMessages();
		assertNotNull(result);
		assertEquals(3, result.size());
	}

}
