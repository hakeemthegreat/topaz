package org.coco.topaz.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EnsureTest {

	private static final List<Object> OBJECT_COLLECTION = Arrays.asList(Dummy.OBJECT, Dummy.OBJECT);

	@Test
	public void notNull_Test() {
		Ensure.notNull(Dummy.OBJECT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notNull_IllegalArgumentException_Test() {
		Ensure.notNull(null);
	}

	@Test
	public void isTrue_True_Test() {
		Ensure.isTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isTrue_False_Test() {
		Ensure.isTrue(false);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isTrue_Null_Test() {
		Ensure.isTrue(null);
	}

	@Test
	public void empty_True_Test() {
		Ensure.empty(Collections.emptyList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void empty_False_Test() {
		Ensure.empty(Dummy.stringList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void notEmpty_False_Test() {
		Ensure.notEmpty(Collections.emptyList());
	}

	@Test
	public void notEmpty_True_Test() {
		Ensure.notEmpty(Dummy.stringList());
	}

	@Test
	public void hasAtMost_Test() {
		Ensure.hasAtMost(OBJECT_COLLECTION, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void hasAtMost_IllegalArgumentException_Test() {
		Ensure.hasAtMost(OBJECT_COLLECTION, 1);
	}

	@Test
	public void areEqual_True_Test() {
		Ensure.areEqual(Dummy.OBJECT, Dummy.OBJECT);
	}

	@Test
	public void areEqual_Double_Null_Test() {
		Ensure.areEqual(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void areEqual_First_Argument_Null_Test() {
		Ensure.areEqual(null, Dummy.OBJECT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void areEqual_Second_Argument_Null_Test() {
		Ensure.areEqual(Dummy.OBJECT, null);
	}

}
