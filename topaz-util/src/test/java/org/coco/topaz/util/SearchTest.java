package org.coco.topaz.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SearchTest {

	private static final String ALPHA = "alpha";
	private static final String BRAVO = "bravo";
	private static final String CHARLIE = "charlie";
	private static final String DELTA = "delta";
	private static final String ECHO = "echo";

	@Test
	public void nearestIndexOf_Object_Is_In_List_Test() {
		List<String> list = Arrays.asList(ALPHA, BRAVO, CHARLIE, DELTA, ECHO);

		assertEquals(0, Search.nearestIndexOf(list, ALPHA));
		assertEquals(1, Search.nearestIndexOf(list, BRAVO));
		assertEquals(2, Search.nearestIndexOf(list, CHARLIE));
		assertEquals(3, Search.nearestIndexOf(list, DELTA));
		assertEquals(4, Search.nearestIndexOf(list, ECHO));
	}

	@Test
	public void nearestIndexOf_Object_Is_Not_In_List_Test() {
		List<String> list = Arrays.asList(ALPHA, BRAVO, CHARLIE, DELTA, ECHO);

		assertEquals(0, Search.nearestIndexOf(list, ""));
		assertEquals(1, Search.nearestIndexOf(list, "buy"));
		assertEquals(4, Search.nearestIndexOf(list, "sierra"));
	}

	@Test
	public void nearestIndexOf_Empty_List_Test() {
		List<String> list = Collections.emptyList();

		assertEquals(0, Search.nearestIndexOf(list, CHARLIE));
	}

}