package org.coco.topaz.util;

import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class StreamsTest extends JavaTest {

	private static final int LARGE_PRIME_NUMBER = 37591;
	private static final byte[] BYTE_ARRAY = new byte[LARGE_PRIME_NUMBER];
	private static final char[] CHAR_ARRAY = new char[LARGE_PRIME_NUMBER];

	static {
		for (int i = 0 ; i < LARGE_PRIME_NUMBER ; i++) {
			BYTE_ARRAY[i] = (byte) i;
			CHAR_ARRAY[i] = (char) i;
		}
	}

	@Test
	public void copy_Byte_Test() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream(BYTE_ARRAY);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		try {
			Streams.copy(inputStream, outputStream);
			byte[] results = outputStream.toByteArray();
			assertArrayEquals(BYTE_ARRAY, results);
		} finally {
			Resources.close(inputStream, outputStream);
		}
	}

	@Test
	public void copy_Char_Test() throws Exception {
		CharArrayReader inputStream = new CharArrayReader(CHAR_ARRAY);
		CharArrayWriter outputStream = new CharArrayWriter();

		try {
			Streams.copy(inputStream, outputStream);
			char[] results = outputStream.toCharArray();
			assertArrayEquals(CHAR_ARRAY, results);
		} finally {
			Resources.close(inputStream, outputStream);
		}
	}

	@Test
	public void readAll_InputStream_Test() throws Exception {
		InputStream inputStream = new ByteArrayInputStream(BYTE_ARRAY);

		byte[] result = Streams.readAll(inputStream);
		assertArrayEquals(BYTE_ARRAY, result);
	}

	@Test
	public void readAll_Reader_Test() throws Exception {
		Reader reader = new CharArrayReader(CHAR_ARRAY);
		String expected = new String(CHAR_ARRAY);

		String result = Streams.readAll(reader);
		assertEquals(expected, result);
	}

	@Test
	public void newFileInputStream_Test() throws Exception {
		File file = temporaryFolder.newFile();

		FileInputStream result = Streams.newFileInputStream(file.getAbsolutePath());
		assertNotNull(result);
	}

	@Test
	public void newByteArrayOutputStream_Test() {
		ByteArrayOutputStream result = Streams.newByteArrayOutputStream();
		assertNotNull(result);
	}

	@Test
	public void newFileOutputStream_Test() throws Exception {
		File file = temporaryFolder.newFile();
		OutputStream outputStream = null;

		try {
			outputStream = Streams.newFileOutputStream(file, true);
			assertNotNull(outputStream);
		} finally {
			Resources.close(outputStream);
		}
	}

	@Test
	public void toBufferedInputStream_Not_A_BufferedInputStream_Test() {
		InputStream inputStream = new ByteArrayInputStream(BYTE_ARRAY);

		BufferedInputStream result = Streams.toBufferedInputStream(inputStream);
		assertNotNull(result);
		assertNotSame(inputStream, result);
	}

	@Test
	public void toBufferedInputStream_Already_A_BufferedInputStream_Test() {
		InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(BYTE_ARRAY));

		BufferedInputStream result = Streams.toBufferedInputStream(inputStream);
		assertNotNull(result);
		assertSame(inputStream, result);
	}

	@Test
	public void toBufferedOutputStream_Not_A_BufferedOutputStream_Test() {
		OutputStream outputStream = new ByteArrayOutputStream();

		BufferedOutputStream result = Streams.toBufferedOutputStream(outputStream);
		assertNotNull(result);
		assertNotSame(outputStream, result);
	}

	@Test
	public void toBufferedOutputStream_Already_A_BufferedOutputStream_Test() {
		OutputStream outputStream = new BufferedOutputStream(new ByteArrayOutputStream());

		BufferedOutputStream result = Streams.toBufferedOutputStream(outputStream);
		assertNotNull(result);
		assertSame(outputStream, result);
	}

	@Test
	public void toBufferedReader_Not_A_BufferedReader_Test() {
		Reader reader = new CharArrayReader(CHAR_ARRAY);

		BufferedReader result = Streams.toBufferedReader(reader);
		assertNotNull(result);
		assertNotSame(reader, result);
	}

	@Test
	public void toBufferedReader_Already_A_BufferedReader_Test() {
		Reader reader = new BufferedReader(new CharArrayReader(CHAR_ARRAY));

		BufferedReader result = Streams.toBufferedReader(reader);
		assertNotNull(result);
		assertSame(reader, result);
	}

	@Test
	public void toBufferedWriter_Not_A_BufferedWriter_Test() {
		Writer writer = new CharArrayWriter();

		BufferedWriter result = Streams.toBufferedWriter(writer);
		assertNotNull(result);
		assertNotSame(writer, result);
	}

	@Test
	public void toBufferedWriter_Already_A_BufferedWriter_Test() {
		Writer writer = new BufferedWriter(new CharArrayWriter());

		BufferedWriter result = Streams.toBufferedWriter(writer);
		assertNotNull(result);
		assertSame(writer, result);
	}

}
