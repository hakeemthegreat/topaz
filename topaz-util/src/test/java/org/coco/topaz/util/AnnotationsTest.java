package org.coco.topaz.util;

import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.*;

public class AnnotationsTest {

	@Resource
	private static class TransactionController {

	}

	@Test
	public void getAnnotation_Test() {
		Resource result = Annotations.getAnnotation(TransactionController.class, Resource.class);
		assertNotNull(result);
	}

	@Test
	public void getAnnotation_Null_Test() {
		Resource result = Annotations.getAnnotation(null, Resource.class);
		assertNull(result);
	}

	@Test
	public void hasAnnotation_Test() {
		boolean result = Annotations.hasAnnotation(TransactionController.class, Resource.class);
		assertTrue(result);
	}

	@Test
	public void hasAnnotation_Null_Test() {
		boolean result = Annotations.hasAnnotation(null, Resource.class);
		assertFalse(result);
	}

}
