package org.coco.topaz.util.reflection;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class FieldsTest extends JavaTest {

	@Getter
	@Setter
	private static class Employee {
		private Long id;
		private String name;
	}

	private static final Employee EMPLOYEE = new Employee();

	static {
		EMPLOYEE.setId(Dummy.LONG);
		EMPLOYEE.setName(Dummy.STRING);
	}

	@Test
	public void getFieldMap_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		Field name = Employee.class.getDeclaredField("name");

		Map<String, Field> results = Fields.getFieldMap(Employee.class);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertEquals(id, results.get("id"));
		assertEquals(name, results.get("name"));
	}

	@Test
	public void getFieldMap_Null_Test() {
		Map<String, Field> results = Fields.getFieldMap(null);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	@Test
	public void getFields_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		Field name = Employee.class.getDeclaredField("name");
		
		List<Field> results = Fields.getFields(Employee.class);
		assertNotNull(results);
		assertTrue(results.contains(id));
		assertTrue(results.contains(name));
	}

	@Test
	public void getFieldValues_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		Field name = Employee.class.getDeclaredField("name");

		Map<Field, Object> results = Fields.getFieldValues(EMPLOYEE);
		assertNotNull(results);
		assertEquals(Dummy.LONG, results.get(id));
		assertEquals(Dummy.STRING, results.get(name));
	}

	@Test
	public void getValue_Static_Field_Test() throws Exception {
		Field serialVersionUID = String.class.getDeclaredField("serialVersionUID");

		Object result = Fields.getValue(serialVersionUID, EMPLOYEE);
		assertNotNull(result);
	}

	@Test
	public void getValue_IllegalAccessException_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		IllegalAccessException illegalAccessException = new IllegalAccessException();

		new Expectations(Reflection.class) {{
			Reflection.isStatic(id); result = illegalAccessException;
		}};

		expect(RuntimeException.class, illegalAccessException);

		Fields.getValue(id, EMPLOYEE);
	}

	@Test
	public void getFields_Create_Then_Get_From_Cache_Test() {
		List<Field> results = Fields.getFields(Employee.class);
		assertNotNull(results);
		assertFalse(results.isEmpty());

		List<Field> results2 = Fields.getFields(Employee.class);
		assertEquals(results, results2);
	}

	@Test
	public void set_With_Field_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");

		Fields.set(EMPLOYEE, id, Dummy.LONG);
		assertEquals(Dummy.LONG, EMPLOYEE.getId());
	}

	@Test
	public void set_With_Field_Name_Test() {
		Fields.set(EMPLOYEE, "id", Dummy.LONG);
		assertEquals(Dummy.LONG, EMPLOYEE.getId());
	}

	@Test
	public void set_Null_Test() {
		new Expectations(Reflection.class) {{
			Reflection.makeAccessible((Field) any); times = 0;
		}};

		Fields.set(EMPLOYEE, (Field) null, Dummy.LONG);
		assertEquals(Dummy.LONG, EMPLOYEE.getId());
	}

	@Test
	public void set_ReflectiveOperationException_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");
		ReflectiveOperationException reflectiveOperationException = new ReflectiveOperationException();

		new Expectations(Reflection.class) {{
			Reflection.makeAccessible(id); result = reflectiveOperationException;
		}};

		expect(RuntimeException.class, reflectiveOperationException);

		Fields.set(EMPLOYEE, id, Dummy.LONG);
		assertEquals(Dummy.LONG, EMPLOYEE.getId());
	}

	@Test
	public void getField_Test() throws Exception {
		Field id = Employee.class.getDeclaredField("id");

		Field result = Fields.getField("id", EMPLOYEE);
		assertEquals(id, result);
	}

}
