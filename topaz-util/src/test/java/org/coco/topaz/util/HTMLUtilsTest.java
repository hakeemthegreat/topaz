package org.coco.topaz.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.coco.topaz.util.html.Cell;
import org.coco.topaz.util.html.Row;
import org.coco.topaz.util.html.Table;
import org.junit.Test;

import static org.junit.Assert.*;

public class HTMLUtilsTest {

	@Getter
	@Setter
	private static class Employee {
		private Long id;
		private String name;
	}
	
	private static final Employee EMPLOYEE = new Employee();
	private static final Employee EMPLOYEE2 = new Employee();
	private static final Employee EMPLOYEE3 = new Employee();
	private static final List<Employee> EMPLOYEES = Arrays.asList(EMPLOYEE, EMPLOYEE2, EMPLOYEE3);
	
	static {
		EMPLOYEE.setId(1L);
		EMPLOYEE.setName("dummy");

		EMPLOYEE2.setId(2L);
		EMPLOYEE2.setName("dummy2");

		EMPLOYEE3.setId(3L);
		EMPLOYEE3.setName("dummy3");
	}
	
	@Test
	public void toTable_Test() {
		Table table = HTMLUtils.toTable(EMPLOYEES);
		assertEquals("<table><tr><th>ID</th><th>NAME</th></tr><tr><td>1</td><td>dummy</td></tr><tr><td>2</td><td>dummy2</td></tr><tr><td>3</td><td>dummy3</td></tr></table>", table.toString());
	}

	@Test
	public void toRow_Null_Test() {
		Row result = HTMLUtils.toRow(null, Collections.emptyList());
		assertNull(result);
	}

	@Test
	public void toCell_Test() {
		String text = "text";

		Cell result = HTMLUtils.toCell(text);
		assertNotNull(result);
		assertEquals("<td>text</td>", result.toString());
	}

}
