package org.coco.topaz.util;

import org.coco.topaz.util.test.JavaTest;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;

public class FileUtilsTest extends JavaTest {

	private File rootDirectory;
	private File file1;
	private File file2;
	private File subDirectory1;
	private File subFile1;

	@Before
	public void before() throws Exception {
		rootDirectory = temporaryFolder.newFolder();
		file1 = new File(rootDirectory, "dummy");
		file2 = new File(rootDirectory, "dummy2");
		subDirectory1 = new File(rootDirectory, "dummy3");
		subFile1 = new File(subDirectory1, "dummy4");

		file1.createNewFile();
		file2.createNewFile();
		subDirectory1.mkdir();
		subFile1.createNewFile();
	}

	@Test
	public void readAll_Test() throws Exception {
		File file = createFile();

		String result = FileUtils.readAll(file);
		assertEquals("dummy", result);
	}

	@Test
	public void buildFileWrapper_Test() {
		FileWrapper result = FileUtils.buildFileWrapper(rootDirectory, null);
		assertNotNull(result);

		List<File> files = result.getFiles();
		assertNotNull(files);
		assertEquals(2, files.size());
		assertTrue(files.contains(file1));
		assertTrue(files.contains(file2));

		List<File> directories = result.getDirectories();
		assertNotNull(directories);
		assertEquals(1, directories.size());
		assertTrue(directories.contains(subDirectory1));
	}

	@Test
	public void getFiles_Directory_Test() {
		List<File> results = FileUtils.getFiles(rootDirectory);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertTrue(results.contains(file1));
		assertTrue(results.contains(file2));
		assertTrue(results.contains(subFile1));
	}

	@Test
	public void getFiles_File_Test() {
		List<File> results = FileUtils.getFiles(file1);
		assertNotNull(results);
		assertEquals(0, results.size());
	}

	private File createFile() throws IOException {
		File file;
		Writer writer = null;

		try {
			file = temporaryFolder.newFile();
			writer = new FileWriter(file);
			writer.write("dummy");
			writer.flush();
		} finally {
			Resources.close(writer);
		}

		return file;
	}

	@Test
	public void newFile_Test() {
		File result = FileUtils.newFile("dummy");
		assertNotNull(result);

		String absolutePath = result.getAbsolutePath();
		assertTrue(absolutePath.endsWith("dummy"));
	}

}
