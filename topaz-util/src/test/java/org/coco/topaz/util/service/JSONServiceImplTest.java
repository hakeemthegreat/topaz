package org.coco.topaz.util.service;

import lombok.Getter;
import lombok.Setter;
import org.coco.topaz.util.node.LinearNode;
import org.coco.topaz.util.test.Dummy;
import mockit.Tested;
import org.junit.Test;

import java.util.*;

import static org.coco.topaz.util.Constants.Strings.DOUBLE_QUOTE;
import static org.junit.Assert.assertEquals;

public class JSONServiceImplTest {

	private enum State {
		SUCCESS
	}

	@Getter
	@Setter
	private static class Employee {
		private Long id;
		private String name;
	}

	@Tested
	private JSONServiceImpl instance;

	private static final Integer[] ARRAY = {0, 1, 2, 3, 4};
	private static final List<Integer> COLLECTION = Arrays.asList(ARRAY);

	@Test
	public void toJSON_String_Test() {
		String expected = DOUBLE_QUOTE + Dummy.STRING + DOUBLE_QUOTE;
		String result = instance.toJSON(Dummy.STRING);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_String_Double_Quote_Test() {
		String expected = DOUBLE_QUOTE + "one\\\"two" + DOUBLE_QUOTE;
		String result = instance.toJSON("one\"two");
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_String_Backslash_Test() {
		String expected = DOUBLE_QUOTE + "one\\\\two" + DOUBLE_QUOTE;
		String result = instance.toJSON("one\\two");
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Character_Test() {
		String expected = DOUBLE_QUOTE + Dummy.CHAR + DOUBLE_QUOTE;
		String result = instance.toJSON(Dummy.CHAR);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Number_Test() {
		String expected = Dummy.LONG.toString();
		String result = instance.toJSON(Dummy.LONG);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Boolean_Test() {
		String expected = Boolean.TRUE.toString();
		String result = instance.toJSON(Boolean.TRUE);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Date_Test() {
		Long time = 1234567890L;
		Date date = new Date(time);

		String result = instance.toJSON(date);
		assertEquals(time.toString(), result);
	}

	@Test
	public void toJSON_Blank_Object_Test() {
		String expected = "{}";
		String result = instance.toJSON(Dummy.OBJECT);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Object_Test() {
		Employee employee = new Employee();

		employee.setId(1L);
		employee.setName("dummy");

		String result = instance.toJSON(employee);
		assertEquals("{\"id\": 1, \"name\": \"dummy\"}", result);
	}

	@Test
	public void toJSON_Null_Test() {
		String expected = "null";
		String result = instance.toJSON(null);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_List_Test() {
		String expected = "[0, 1, 2, 3, 4]";
		String result = instance.toJSON(COLLECTION);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Array_Test() {
		String expected = "[0, 1, 2, 3, 4]";
		String result = instance.toJSON(ARRAY);
		assertEquals(expected, result);
	}

	@Test
	public void toJSON_Map_With_String_Keys_Test() {
		Map<String, Object> map = new HashMap<>();

		map.put("dummy", 1);
		map.put("dummy2", 'a');
		map.put("dummy3", new Object());

		String result = instance.toJSON(map);
		assertEquals("{\"dummy\": 1, \"dummy2\": \"a\", \"dummy3\": {}}", result);
	}

	@Test
	public void toJSON_Map_With_Non_String_Keys_Test() {
		Map<Long, Object> map = new HashMap<>();

		map.put(2L, "two");
		map.put(4L, "four");
		map.put(1L, "one");
		map.put(null, "zero");
		map.put(3L, "three");

		String result = instance.toJSON(map);
		assertEquals("{null: \"zero\", \"1\": \"one\", \"2\": \"two\", \"3\": \"three\", \"4\": \"four\"}", result);
	}

	@Test
	public void toJSON_Circular_Dependency_Node_Test() {
		LinearNode node = new LinearNode();

		node.setValue(123);
		node.setNext(node);

		String result = instance.toJSON(node);
		assertEquals("{\"next\": {}, \"serialVersionUID\": 1, \"value\": 123}", result);
	}

	@Test
	public void toJSON_Circular_Dependency_Map_Test() {
		Map<Object, Object> map = new HashMap<>();
		Object next = map;

		map.put("value", 123);
		map.put("next", next);

		String result = instance.toJSON(map);
		assertEquals("{\"next\": {}, \"value\": 123}", result);
	}

	@Test
	public void toJSON_Enum_Test() {
		Enum<?> e = State.SUCCESS;

		String result = instance.toJSON(e);
		assertEquals("\"SUCCESS\"", result);
	}

	@Test
	public void toJSON_Multiple_Invocations_Test() {
		Long aLong = 123L;
		String expected = aLong.toString();

		String result = instance.toJSON(aLong);
		assertEquals(expected, result);

		String result2 = instance.toJSON(aLong);
		assertEquals(expected, result2);
	}

}
