package org.coco.topaz.util;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SortedListTest {

	private final SortedList<String> instance = new SortedList<>();

	private static final String ALPHA = "alpha";
	private static final String BRAVO = "bravo";
	private static final String CHARLIE = "charlie";
	private static final String DELTA = "delta";
	private static final String ECHO = "echo";
	private static final String FOXTROT = "foxtrot";
	private static final String GULF = "gulf";
	private static final String HOTEL = "hotel";
	private static final String INDIA = "india";
	private static final String JULIET = "juliet";
	private static final String KILO = "kilo";
	private static final Object OBJECT = new Object();
	private static final String STRING = "string";

	@Before
	public void before_Test() {
		instance.clear();
		assertTrue(instance.isEmpty());
		instance.add(INDIA);
		instance.add(FOXTROT);
		instance.add(CHARLIE);
		instance.add(GULF);
		instance.add(KILO);
		instance.add(ALPHA);
		instance.add(HOTEL);
		instance.add(DELTA);
		instance.add(BRAVO);
		instance.add(JULIET);
		instance.add(ECHO);
		assertEquals(11, instance.size());
	}

	@Test
	public void default_Constructor_Test() {
		SortedList<String> sortedList = new SortedList<>();
		assertEquals(0, sortedList.size());
	}

	@Test
	public void ellipse_Constructor_Test() {
		String[] strings = {ALPHA, BRAVO, CHARLIE};

		SortedList<String> sortedList = new SortedList<>(strings);
		assertEquals(3, sortedList.size());
		assertEquals(ALPHA, sortedList.get(0));
		assertEquals(BRAVO, sortedList.get(1));
		assertEquals(CHARLIE, sortedList.get(2));
	}

	@Test
	public void collection_Constructor_Test() {
		List<String> strings = Arrays.asList(ALPHA, BRAVO, CHARLIE);

		SortedList<String> sortedList = new SortedList<>(strings);
		assertEquals(3, sortedList.size());
		assertEquals(ALPHA, sortedList.get(0));
		assertEquals(BRAVO, sortedList.get(1));
		assertEquals(CHARLIE, sortedList.get(2));
	}

	@Test
	public void get_Test() {
		assertEquals(ALPHA, instance.get(0));
		assertEquals(BRAVO, instance.get(1));
		assertEquals(CHARLIE, instance.get(2));
		assertEquals(DELTA, instance.get(3));
		assertEquals(ECHO, instance.get(4));
		assertEquals(FOXTROT, instance.get(5));
		assertEquals(GULF, instance.get(6));
		assertEquals(HOTEL, instance.get(7));
		assertEquals(INDIA, instance.get(8));
		assertEquals(JULIET, instance.get(9));
		assertEquals(KILO, instance.get(10));
	}

	@Test
	public void remove_By_Index_Test() {
		assertEquals(KILO, instance.remove(10));
		assertEquals(JULIET, instance.remove(9));
		assertEquals(INDIA, instance.remove(8));
		assertEquals(HOTEL, instance.remove(7));
		assertEquals(GULF, instance.remove(6));
		assertEquals(FOXTROT, instance.remove(5));
		assertEquals(ECHO, instance.remove(4));
		assertEquals(DELTA, instance.remove(3));
		assertEquals(CHARLIE, instance.remove(2));
		assertEquals(BRAVO, instance.remove(1));
		assertEquals(ALPHA, instance.remove(0));
		assertEquals(0, instance.size());
	}

	@Test
	public void remove_By_Object_Test() {
		instance.remove(ALPHA);
		instance.remove(BRAVO);
		instance.remove(CHARLIE);
		instance.remove(DELTA);
		instance.remove(ECHO);
		instance.remove(FOXTROT);
		instance.remove(GULF);
		instance.remove(HOTEL);
		instance.remove(INDIA);
		instance.remove(JULIET);
		instance.remove(KILO);
		instance.remove(OBJECT);
		assertEquals(0, instance.size());
	}

	@Test
	public void contains_Test() {
		assertTrue(instance.contains(ALPHA));
		assertTrue(instance.contains(BRAVO));
		assertTrue(instance.contains(CHARLIE));
		assertTrue(instance.contains(DELTA));
		assertTrue(instance.contains(ECHO));
		assertTrue(instance.contains(FOXTROT));
		assertTrue(instance.contains(GULF));
		assertTrue(instance.contains(HOTEL));
		assertTrue(instance.contains(INDIA));
		assertTrue(instance.contains(JULIET));
		assertTrue(instance.contains(KILO));
		assertFalse(instance.contains(STRING));
		assertFalse(instance.contains(OBJECT));
	}

	@Test
	public void indexOf_Test() {
		assertEquals(0, instance.indexOf(ALPHA));
		assertEquals(1, instance.indexOf(BRAVO));
		assertEquals(2, instance.indexOf(CHARLIE));
		assertEquals(3, instance.indexOf(DELTA));
		assertEquals(4, instance.indexOf(ECHO));
		assertEquals(5, instance.indexOf(FOXTROT));
		assertEquals(6, instance.indexOf(GULF));
		assertEquals(7, instance.indexOf(HOTEL));
		assertEquals(8, instance.indexOf(INDIA));
		assertEquals(9, instance.indexOf(JULIET));
		assertEquals(10, instance.indexOf(KILO));
		assertEquals(-1, instance.indexOf(STRING));
		assertEquals(-1, instance.indexOf(OBJECT));
		assertEquals(-1, new SortedList<>().indexOf(STRING));
	}

	@Test
	public void lastIndexOf_Test() {
		assertEquals(0, instance.lastIndexOf(ALPHA));
		assertEquals(1, instance.lastIndexOf(BRAVO));
		assertEquals(2, instance.lastIndexOf(CHARLIE));
		assertEquals(3, instance.lastIndexOf(DELTA));
		assertEquals(4, instance.lastIndexOf(ECHO));
		assertEquals(5, instance.lastIndexOf(FOXTROT));
		assertEquals(6, instance.lastIndexOf(GULF));
		assertEquals(7, instance.lastIndexOf(HOTEL));
		assertEquals(8, instance.lastIndexOf(INDIA));
		assertEquals(9, instance.lastIndexOf(JULIET));
		assertEquals(10, instance.lastIndexOf(KILO));
		assertEquals(-1, instance.lastIndexOf(STRING));
		assertEquals(-1, instance.lastIndexOf(OBJECT));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void add_At_A_Specific_Index_Test() {
		instance.add(0, ALPHA);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void set_Test() {
		instance.set(0, ALPHA);
	}

	@Test
	public void clear_Test() {
		instance.clear();
		assertEquals(0, instance.size());
	}

}