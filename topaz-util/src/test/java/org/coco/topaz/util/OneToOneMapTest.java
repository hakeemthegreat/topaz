package org.coco.topaz.util;

import mockit.Tested;
import org.coco.topaz.util.exception.DuplicateKeyException;
import org.coco.topaz.util.exception.DuplicateValueException;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OneToOneMapTest {

	@Tested
	private OneToOneMap<String, Long> instance;

	@Test
	public void put_getValue_getKey_Test() {
		instance.put(Dummy.STRING, Dummy.LONG);

		Long result = instance.getValue(Dummy.STRING);
		assertEquals(Dummy.LONG, result);

		String result2 = instance.getKey(Dummy.LONG);
		assertEquals(Dummy.STRING, result2);
	}

	@Test(expected = DuplicateKeyException.class)
	public void put_Duplicate_Key_Test() {
		instance.put(Dummy.STRING, Dummy.LONG);
		instance.put(Dummy.STRING, Dummy.LONG2);
	}

	@Test(expected = DuplicateValueException.class)
	public void put_Duplicate_Value_Test() {
		instance.put(Dummy.STRING, Dummy.LONG);
		instance.put(Dummy.STRING2, Dummy.LONG);
	}

	@Test
	public void containsKey_Test() {
		instance.put(Dummy.STRING, Dummy.LONG);

		boolean result = instance.containsKey(Dummy.STRING);
		assertTrue(result);
	}

	@Test
	public void containsValue_Test() {
		instance.put(Dummy.STRING, Dummy.LONG);

		boolean result = instance.containsValue(Dummy.LONG);
		assertTrue(result);
	}

}
