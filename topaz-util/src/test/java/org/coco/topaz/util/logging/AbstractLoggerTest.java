package org.coco.topaz.util.logging;

import mockit.Expectations;
import mockit.Tested;
import mockit.Verifications;
import org.coco.topaz.util.logging.Logger.Classifier;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AbstractLoggerTest {

	@Tested
	private final Class<?> clazz = Object.class;

	@Tested
	private AbstractLogger instance;

	private static final String MESSAGE = "message";
	private static final Throwable THROWABLE = new Throwable();

	@Test
	public void trace_Message_Only_Test() {
		new Expectations(instance) {{
			instance.trace(anyString, (Throwable) any);
		}};

		instance.trace(MESSAGE);

		new Verifications() {{
			instance.trace(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void trace_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.trace(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.TRACE, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void debug_Message_Only_Test() {
		new Expectations(instance) {{
			instance.debug(anyString, (Throwable) any);
		}};

		instance.debug(MESSAGE);

		new Verifications() {{
			instance.debug(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void debug_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.debug(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.DEBUG, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void info_Message_Only_Test() {
		new Expectations(instance) {{
			instance.info(anyString, (Throwable) any);
		}};

		instance.info(MESSAGE);

		new Verifications() {{
			instance.info(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void info_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.info(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.INFO, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void warn_Message_Only_Test() {
		new Expectations(instance) {{
			instance.warn(anyString, (Throwable) any);
		}};

		instance.warn(MESSAGE);

		new Verifications() {{
			instance.warn(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void warn_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.warn(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.WARN, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void error_Message_Only_Test() {
		new Expectations(instance) {{
			instance.error(anyString, (Throwable) any);
		}};

		instance.error(MESSAGE);

		new Verifications() {{
			instance.error(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void error_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.error(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.ERROR, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void fatal_Message_Only_Test() {
		new Expectations(instance) {{
			instance.fatal(anyString, (Throwable) any);
		}};

		instance.fatal(MESSAGE);

		new Verifications() {{
			instance.fatal(MESSAGE, null); times = 1;
		}};
	}

	@Test
	public void fatal_Message_And_Throwable_Test() {
		new Expectations(instance) {{
			instance.log((Classifier) any, anyString, (Throwable) any);
		}};

		instance.fatal(MESSAGE, THROWABLE);

		new Verifications() {{
			instance.log(Classifier.FATAL, MESSAGE, THROWABLE); times = 1;
		}};
	}

	@Test
	public void getLineNumber_Test() {
		assertEquals(181, instance.getLineNumber());
		assertEquals(182, instance.getLineNumber());
		assertEquals(183, instance.getLineNumber());
		assertEquals(184, instance.getLineNumber());
		assertEquals(185, instance.getLineNumber());
	}

	@Test
	public void buildMessage_Test() {
		Throwable throwable = new Throwable();
		String message = "message";

		Message result = instance.buildMessage(Logger.Classifier.DEBUG, message, throwable);
		assertNotNull(result);
		assertEquals(Logger.Classifier.DEBUG, result.getClassifier());
		assertEquals(message, result.getContent());
		assertEquals(Object.class, result.getClazz());
		assertEquals(throwable, result.getThrowable());
		assertEquals(193, result.getLineNumber());
		assertNotNull(result.getDate());
		assertNotNull(result.getId());
	}

}