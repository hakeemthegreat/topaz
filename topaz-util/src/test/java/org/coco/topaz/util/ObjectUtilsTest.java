package org.coco.topaz.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.*;

public class ObjectUtilsTest {

	@Test
	public void isEmpty_Null_Test() {
		boolean result = ObjectUtils.isEmpty(null);
		assertTrue(result);
	}

	@Test
	public void isEmpty_String_Test() {
		boolean result = ObjectUtils.isEmpty(Dummy.STRING);
		assertFalse(result);
	}

	@Test
	public void isEmpty_Collection_Test() {
		boolean result = ObjectUtils.isEmpty(Dummy.objectList());
		assertFalse(result);
	}

	@Test
	public void isEmpty_Object_Test() {
		boolean result = ObjectUtils.isEmpty(Dummy.OBJECT);
		assertFalse(result);
	}

	@Test
	public void isNotEmpty_True_Test() {
		boolean result = ObjectUtils.isNotEmpty(Dummy.OBJECT);
		assertTrue(result);
	}

	@Test
	public void isNotEmpty_False_Test() {
		boolean result = ObjectUtils.isNotEmpty(null);
		assertFalse(result);
	}

	@Test
	public void compare_Same_Object_Test() {
		String left = "abc";
		String right = left;

		int result = ObjectUtils.compare(left, right);
		assertEquals(0, result);
	}

	@Test
	public void compare_Left_Null_Test() {
		String left = null;
		String right = "abc";

		int result = ObjectUtils.compare(left, right);
		assertTrue(result < 0);
	}

	@Test
	public void compare_Right_Null_Test() {
		String left = "abc";
		String right = null;

		int result = ObjectUtils.compare(left, right);
		assertTrue(result > 0);
	}

	@Test
	public void compare_Both_Null_Test() {
		String left = null;
		String right = null;

		int result = ObjectUtils.compare(left, right);
		assertEquals(0, result);
	}

	@Test
	public void compare_Both_Non_Null_Test() {
		int left = 1;
		int right = 2;

		int result = ObjectUtils.compare(left, right);
		assertTrue(result < 0);
	}

}
