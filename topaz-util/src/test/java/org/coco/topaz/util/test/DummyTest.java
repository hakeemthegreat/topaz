package org.coco.topaz.util.test;

import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class DummyTest {

	@Test
	public void string_List_Test() {
		String[] array = Dummy.stringArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<String> list = Dummy.stringList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<String> set = Dummy.stringSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void chars_Test() {
		Character[] array = Dummy.characterArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Character> list = Dummy.characterList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Character> set = Dummy.characterSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void longs_Test() {
		Long[] array = Dummy.longArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Long> list = Dummy.longList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Long> set = Dummy.longSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void doubles_Test() {
		Double[] array = Dummy.doubleArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Double> list = Dummy.doubleList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Double> set = Dummy.doubleSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void dates_Test() {
		Date[] array = Dummy.dateArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Date> list = Dummy.dateList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Date> set = Dummy.dateSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void objects_Test() {
		Object[] array = Dummy.objectArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Object> list = Dummy.objectList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Object> set = Dummy.objectSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void integers_Test() {
		Integer[] array = Dummy.integerArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Integer> list = Dummy.integerList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Integer> set = Dummy.integerSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

	@Test
	public void bytes_Test() {
		Byte[] array = Dummy.byteArray();
		assertNotNull(array);
		assertEquals(7, array.length);

		List<Byte> list = Dummy.byteList();
		assertNotNull(list);
		assertEquals(7, list.size());

		for (int i = 0; i < array.length; i++) {
			assertEquals(array[i], list.get(i));
		}

		Set<Byte> set = Dummy.byteSet();
		assertNotNull(set);
		assertTrue(set.containsAll(list));
	}

}
