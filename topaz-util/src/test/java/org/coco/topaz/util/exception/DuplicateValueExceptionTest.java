package org.coco.topaz.util.exception;

public class DuplicateValueExceptionTest extends ExceptionTest<DuplicateValueException> {

	public DuplicateValueExceptionTest() {
		super(DuplicateValueException.class);
	}

}
