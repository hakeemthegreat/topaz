package org.coco.topaz.util;

import mockit.Tested;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UniqueListTest {

	@Tested
	private UniqueList<String> instance;

	private static final String ALPHA = "alpha";
	private static final String BRAVO = "bravo";
	private static final String CHARLIE = "charlie";

	@Test
	public void add_Test() {
		instance.add(ALPHA);
		instance.add(ALPHA);
		instance.add(BRAVO);
		instance.add(CHARLIE);

		assertEquals(3, instance.size());
		assertEquals(ALPHA, instance.get(0));
		assertEquals(BRAVO, instance.get(1));
		assertEquals(CHARLIE, instance.get(2));
	}

	@Test
	public void add_At_Specific_Index_Test() {
		instance.add(0, ALPHA);
		instance.add(0, ALPHA);
		instance.add(1, BRAVO);
		instance.add(2, CHARLIE);

		assertEquals(3, instance.size());
		assertEquals(ALPHA, instance.get(0));
		assertEquals(BRAVO, instance.get(1));
		assertEquals(CHARLIE, instance.get(2));
	}

	@Test
	public void default_Constructor_Test() {
		UniqueList<String> uniqueList = new UniqueList<>();
		assertEquals(0, uniqueList.size());
	}

	@Test
	public void ellipse_Constructor_Test() {
		String[] strings = {ALPHA, BRAVO, CHARLIE};

		UniqueList<String> uniqueList = new UniqueList<>(strings);
		assertEquals(3, uniqueList.size());
		assertEquals(ALPHA, uniqueList.get(0));
		assertEquals(BRAVO, uniqueList.get(1));
		assertEquals(CHARLIE, uniqueList.get(2));
	}

	@Test
	public void collection_Constructor_Test() {
		List<String> strings = Arrays.asList(ALPHA, BRAVO, CHARLIE);

		UniqueList<String> uniqueList = new UniqueList<>(strings);
		assertEquals(3, uniqueList.size());
		assertEquals(ALPHA, uniqueList.get(0));
		assertEquals(BRAVO, uniqueList.get(1));
		assertEquals(CHARLIE, uniqueList.get(2));
	}

}