package org.coco.topaz.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class ArrayUtilsTest {

	private static final Character CHAR = 'a';
	private static final Character CHAR2 = 'b';
	private static final Character CHAR3 = 'c';

	private static final Byte BYTE = 0;
	private static final Byte BYTE2 = 1;
	private static final Byte BYTE3 = 2;
	private static final List<Byte> BYTES = Arrays.asList(BYTE, BYTE2, BYTE3);

	private static final String ALPHA = "alpha";
	private static final String BETA = "beta";
	private static final String CHARLIE = "charlie";
	private static final String[] NULL_STRING_ARRAY = null;
	private static final String[] STRING_ARRAY = {ALPHA, BETA, CHARLIE};

	@Test
	public void toArray_Test() {
		Byte[] results = ArrayUtils.toArray(BYTES);
		assertNotNull(results);
		assertEquals(BYTES.size(), results.length);
		assertEquals(BYTE, results[0]);
		assertEquals(BYTE2, results[1]);
		assertEquals(BYTE3, results[2]);
	}

	@Test
	public void toArray_Empty_List_Test() {
		Byte[] results = ArrayUtils.toArray(Collections.emptyList());
		assertArrayEquals(null, results);
	}

	@Test
	public void toArray_Object_Test() {
		Object[] results = ArrayUtils.toArray(Dummy.objectList(), Object.class);
		assertNotNull(results);
		assertEquals(Dummy.objectList().size(), results.length);
		assertEquals(Dummy.OBJECT, results[0]);
		assertEquals(Dummy.OBJECT2, results[1]);
		assertEquals(Dummy.OBJECT3, results[2]);
	}

	@Test
	public void toArray_Null_Test() {
		Object[] results = ArrayUtils.toArray(null, Object.class);
		assertArrayEquals(null, results);
	}

	@Test
	public void toPrimitive_Byte_Array_Test() {
		Byte[] array = {BYTE, BYTE2, BYTE3};
		byte[] results = ArrayUtils.toPrimitive(array);
		assertEquals(array.length, results.length);
		assertEquals(BYTE.byteValue(), results[0]);
		assertEquals(BYTE2.byteValue(), results[1]);
		assertEquals(BYTE3.byteValue(), results[2]);
	}

	@Test
	public void toPrimitive_Null_Byte_Array_Test() {
		byte[] results = ArrayUtils.toPrimitive((Byte[]) null);
		assertNotNull(results);
		assertEquals(0, results.length);
	}

	@Test
	public void toPrimitive_Character_Array_Test() {
		Character[] array = {CHAR, CHAR2, CHAR3};
		char[] results = ArrayUtils.toPrimitive(array);
		assertEquals(array.length, results.length);
		assertEquals(CHAR.charValue(), results[0]);
		assertEquals(CHAR2.charValue(), results[1]);
		assertEquals(CHAR3.charValue(), results[2]);
	}

	@Test
	public void toPrimitive_Null_Character_Array_Test() {
		char[] results = ArrayUtils.toPrimitive((Character[]) null);
		assertNotNull(results);
		assertEquals(0, results.length);
	}

	@Test
	public void isArray_Null_Test() {
		Object object = null;

		boolean result = ArrayUtils.isArray(object);
		assertFalse(result);
	}

	@Test
	public void isArray_True_Test() {
		Object object = new byte[] {};

		boolean result = ArrayUtils.isArray(object);
		assertTrue(result);
	}

	@Test
	public void isArray_False_Test() {
		Object object = new Object();

		boolean result = ArrayUtils.isArray(object);
		assertFalse(result);
	}

	@Test
	public void asSet_Test() {
		Set<Character> result = ArrayUtils.asSet(CHAR, CHAR2, CHAR3);
		assertNotNull(result);
		assertEquals(3, result.size());
		assertTrue(result.contains(CHAR));
		assertTrue(result.contains(CHAR2));
		assertTrue(result.contains(CHAR3));
	}

	@Test
	public void asSet_Null_Test() {
		Character[] array = null;
		Set<Character> result = ArrayUtils.asSet(array);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void get_With_Null_Array_Test() {
		String result = ArrayUtils.get(NULL_STRING_ARRAY, 1);
		assertNull(result);
	}

	@Test
	public void get_With_Non_Null_Array_And_Valid_Index_Test() {
		String result0 = ArrayUtils.get(STRING_ARRAY, 0);
		String result1 = ArrayUtils.get(STRING_ARRAY, 1);
		String result2 = ArrayUtils.get(STRING_ARRAY, 2);

		assertEquals(ALPHA, result0);
		assertEquals(BETA, result1);
		assertEquals(CHARLIE, result2);
	}

	@Test
	public void get_With_Non_Null_Array_And_Invalid_Index_Test() {
		String result = ArrayUtils.get(STRING_ARRAY, Integer.MAX_VALUE);
		assertNull(result);
	}

}
