package org.coco.topaz.util.html;

import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ImageTest {

	@Tested
	private Image instance;

	@Test
	public void source_Getter_Setter_Test() {
		String source = "source";

		instance.setSource(source);

		String result = instance.getSource();
		assertEquals(source, result);
	}

	@Test
	public void toString_Test() {
		assertNotNull(instance);
	}

}
