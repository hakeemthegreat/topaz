package org.coco.topaz.util;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.coco.topaz.util.Constants.Strings.Messages.UNABLE_TO_PERFORM_OPERATION;
import static org.junit.Assert.*;

public class ExceptionsTest {

	@Test
	public void toException_Cause_Already_RuntimeException_Test() {
		Exception cause = new RuntimeException();

		RuntimeException result = Exceptions.toException(cause, RuntimeException.class);
		assertSame(cause, result);
	}

	@Test
	public void toException_Cause_Not_RuntimeException_Test() {
		Exception cause = new Exception();

		RuntimeException result = Exceptions.toException(cause, RuntimeException.class);
		assertNotNull(result);
		assertEquals(UNABLE_TO_PERFORM_OPERATION, result.getMessage());
		assertSame(cause, result.getCause());
	}

	@Test
	public void getStackTrace_Test() {
		Exception exception = new Exception(Dummy.STRING);

		String result = Exceptions.getStackTrace(exception);
		assertNotNull(result);
	}

}
