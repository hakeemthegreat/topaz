package org.coco.topaz.util.service;

import mockit.Expectations;
import org.junit.Test;

import java.io.IOException;

public class SerializingCloneServiceTest extends CloneServiceTest<SerializingCloneService> {

	public SerializingCloneServiceTest() {
		super(new SerializingCloneService());
	}

	@Test
	public void clone_IOException_Test() throws Exception {
		IOException ioException = new IOException();
		String cloneMe = "cloneMe";

		new Expectations(instance) {{
			instance.deserialize((byte[]) any); result = ioException;
		}};

		expect(RuntimeException.class, ioException);

		instance.clone(cloneMe);
	}

	@Test
	public void clone_ClassNotFoundException_Test() throws Exception {
		ClassNotFoundException classNotFoundException = new ClassNotFoundException();
		String cloneMe = "cloneMe";

		new Expectations(instance) {{
			instance.deserialize((byte[]) any); result = classNotFoundException;
		}};

		expect(RuntimeException.class, classNotFoundException);

		instance.clone(cloneMe);
	}

}
