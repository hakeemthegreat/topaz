package org.coco.topaz.util.test;

import lombok.Getter;
import lombok.Setter;
import org.junit.Test;

public class GetterSetterTestTest extends GetterSetterTest<GetterSetterTestTest.Employee> {

	@SuppressWarnings("unused")
	public static class Employee {

		@Getter
		@Setter
		private Long id;

		@Getter
		@Setter
		private String name;

		@Getter
		private String fieldWithOnlyGetter;

		@Setter
		private String fieldWithOnlySetter;

	}

	public GetterSetterTestTest() {
		super(new Employee());
	}

	@Test(expected = IllegalArgumentException.class)
	public void run_IllegalArgumentException_Test() {
		GetterSetterTest<Object> getterSetterTest = new GetterSetterTest<Object>(Dummy.OBJECT) {};
		getterSetterTest.run();
	}

}
