package org.coco.topaz.util.service;

import mockit.Tested;
import org.coco.topaz.util.service.SimpleHashService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleHashServiceTest {

	private final Object object = new Object() {
		@Override
		public int hashCode() {
			return HASH_CODE;
		}
	};

	@Tested
	private SimpleHashService instance;

	private static final int HASH_CODE = Integer.MAX_VALUE;

	@Test
	public void hash_Test() {
		int result = instance.hash(object);
		assertEquals(HASH_CODE, result);
	}

	@Test
	public void hash_Null_Test() {
		int result = instance.hash(null);
		assertEquals(0, result);
	}

	@Test
	public void hashToHex_Test() {
		String result = instance.hashToHex(object);
		assertEquals("7fffffff", result);
	}

}
