package org.coco.topaz.util.node;

import org.junit.Test;

import static org.junit.Assert.*;

public class FinalNodeTest {

	private static final String VALUE = "value";

	@Test
	public void default_Constructor_Test() {
		FinalNode node = new FinalNode();

		Object result = node.getValue();
		assertNull(result);
	}

	@Test
	public void constructor_With_Argument_Test() {
		FinalNode node = new FinalNode(VALUE);

		Object result = node.getValue();
		assertEquals(VALUE, result);
	}

}
