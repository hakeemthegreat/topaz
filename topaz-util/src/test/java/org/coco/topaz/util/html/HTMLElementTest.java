package org.coco.topaz.util.html;

import mockit.Injectable;
import mockit.Deencapsulation;
import mockit.Tested;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class HTMLElementTest {

	@Injectable
	private String name = "table";

	@Tested
	private HTMLElement instance;

	private static final HTMLElement ROW = new Row();
	private static final HTMLElement ROW2 = new Row();
	private static final HTMLElement ROW3 = new Row();
	private static final HTMLElement CELL = new Cell();

	static {
		ROW.addText("ROW");
		ROW2.addText("ROW2");
		ROW3.addText("ROW3");
		CELL.addText("CELL");
	}

	@Test
	public void getName_Test() {
		String result = instance.getName();
		assertEquals(name, result);
	}

	@Test
	public void attribute_Getter_Setter_Test() {
		String name = "name";
		String value = "value";

		instance.addAttribute(name, value);

		String result = instance.getAttribute(name);
		assertEquals(value, result);
	}

	@Test
	public void addChild_Test() {
		instance.addChild(ROW);

		List<HTMLElement> children = instance.getChildren();
		assertEquals(1, children.size());
		assertEquals(ROW, instance.getFirstChild());
	}

	@Test
	public void getChild_Empty_List_Test() {
		HTMLElement child = instance.getChild(0);
		assertNull(child);
	}

	@Test
	public void getChildren_All_Test() {
		instance.addChild(ROW);
		instance.addChild(CELL);

		List<HTMLElement> children = instance.getChildren();
		assertEquals(2, children.size());
		assertEquals(ROW, children.get(0));
		assertEquals(CELL, children.get(1));
	}

	@Test
	public void getChildren_Specific_Type_Test() {
		instance.addChild(ROW);
		instance.addChild(CELL);

		List<Cell> grandChildren = instance.getChildren(Cell.class);
		assertEquals(1, grandChildren.size());
		assertEquals(CELL, grandChildren.get(0));
	}

	@Test
	public void addAttribute_Test() {
		instance.addAttribute(Dummy.STRING, Dummy.STRING2);

		Attributes results = Deencapsulation.getField(instance, "attributes");
		assertEquals(Dummy.STRING2, results.get(Dummy.STRING));
	}

	@Test
	public void addStyle_Test() {
		instance.addStyle(Dummy.STRING, Dummy.STRING2);
		instance.addStyle(Dummy.STRING3, Dummy.STRING4);

		Attributes attributes = Deencapsulation.getField(instance, "attributes");
		Style style = (Style) attributes.get("style");
		assertEquals(Dummy.STRING2, style.get(Dummy.STRING));
		assertEquals(Dummy.STRING4, style.get(Dummy.STRING3));
	}

	@Test
	public void toString_Test() {
		instance.addChild(ROW);
		instance.addChild(ROW2);
		instance.addChild(ROW3);

		String result = instance.toString();
		assertEquals("<table><tr>ROW</tr><tr>ROW2</tr><tr>ROW3</tr></table>", result);
	}

}
