package org.coco.topaz.util;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import org.coco.topaz.util.node.Node;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.io.*;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class GenesisTest extends JavaTest {

	private enum State {
		PASS,
		FAIL
	}

	private enum EnumWithNoValues {

	}

	private static class SelfReferencingClass {

		private final SelfReferencingClass selfReferencingClass;

		public SelfReferencingClass(SelfReferencingClass selfReferencingClass) {
			this.selfReferencingClass = selfReferencingClass;
		}

	}

	private static final int REPEAT = 1024;

	@Test
	public void create_Null_Test() {
		Object result = Genesis.create(null);
		assertNull(result);
	}

	@Test
	public void create_Object_Test() {
		Object result = Genesis.create(Object.class);
		assertNotNull(result);
	}

	@Test
	public void create_Node_Test() {
		Node result = Genesis.create(Node.class);
		assertNotNull(result);
	}

	@Test
	public void create_Integer_Test() {
		Integer result = Genesis.create(Integer.class);
		assertNotNull(result);
	}

	@Test
	public void create_int_Test() {
		Integer result = Genesis.create(int.class);
		assertNotNull(result);
	}

	@Test
	public void create_Long_Test() {
		Long result = Genesis.create(Long.class);
		assertNotNull(result);
	}

	@Test
	public void create_Primitive_Long_Test() {
		Long result = Genesis.create(long.class);
		assertNotNull(result);
	}

	@Test
	public void create_Double_Test() {
		Double result = Genesis.create(Double.class);
		assertNotNull(result);
	}

	@Test
	public void create_Primitive_Double_Test() {
		Double result = Genesis.create(double.class);
		assertNotNull(result);
	}

	@Test
	public void create_Date_Test() {
		Date result = Genesis.create(Date.class);
		assertNotNull(result);
	}

	@Test
	public void create_String_Test() {
		String result = Genesis.create(String.class);
		assertNotNull(result);
	}

	@Test
	public void createByteArray_Test() {
		byte[] result = Genesis.create(byte[].class);
		assertNotNull(result);
	}

	@Test
	public void create_Blob_Test() {
		Blob result = Genesis.create(Blob.class);
		assertNotNull(result);
	}

	@Test
	public void create_Clob_Test() {
		Clob result = Genesis.create(Clob.class);
		assertNotNull(result);
	}

	@Test
	public void create_UUID_Test() {
		UUID result = Genesis.create(UUID.class);
		assertNotNull(result);
	}

	@Test
	public void create_InputStream_Test() {
		InputStream result = Genesis.create(InputStream.class);

		try {
			assertTrue(result instanceof ByteArrayInputStream);
		} finally {
			Resources.close(result);
		}
	}

	@Test
	public void create_OutputStream_Test() {
		OutputStream result = Genesis.create(OutputStream.class);

		try {
			assertTrue(result instanceof ByteArrayOutputStream);
		} finally {
			Resources.close(result);
		}
	}

	@Test
	public void create_Class_Test() {
		Class<?> result = Genesis.create(Class.class);
		assertNotNull(result);
	}

	@Test
	public void create_Class_Array_Test() {
		Class<?>[] result = Genesis.create(Class[].class);
		assertNotNull(result);
	}

	@Test
	public void create_Byte_Array_Test() {
		byte[] result = Genesis.create(byte[].class);
		assertNotNull(result);
	}

	@Test
	public void create_Collection_Test() {
		Collection<Long> result = Genesis.create(Collection.class);
		assertTrue(result instanceof LinkedList);
		assertEquals(0, result.size());
	}

	@Test
	public void create_List_Test() {
		List<Long> result = Genesis.create(List.class);
		assertTrue(result instanceof ArrayList);
		assertEquals(0, result.size());
	}

	@Test
	public void create_Set_Test() {
		Set<Long> result = Genesis.create(Set.class);
		assertTrue(result instanceof HashSet);
		assertEquals(0, result.size());
	}

	@Test
	public void create_Map_Test() {
		Map<String, Long> result = Genesis.create(Map.class);
		assertTrue(result instanceof HashMap);
		assertEquals(0, result.size());
	}

	@Test
	public void create_Enum_Test() {
		State result = Genesis.create(State.class);
		assertNotNull(result);

		List<State> states = Arrays.asList(State.values());
		assertNotNull(states);
		assertTrue(states.contains(result));
	}

	@Test(expected = RuntimeException.class)
	public void createBlob_SQLException_Test() {
		new Expectations(Genesis.class) {{
			Genesis.create((Class<?>) any); result = new SQLException();
		}};

		Genesis.createBlob();
	}

	@Test(expected = RuntimeException.class)
	public void createClob_SQLException_Test() {
		new Expectations(Genesis.class) {{
			Genesis.create((Class<?>) any); result = new SQLException();
		}};

		Genesis.createClob();
	}

	@Test
	public void create_IllegalArgumentException_Test(@Mocked Supplier<Object> supplier) {
		Class<Object> clazz = Object.class;
		Exception exception = new Exception();

		new Expectations(Genesis.class) {{
			Genesis.getSupplier((Class<?>) any); result = supplier;
			supplier.get(); result = exception;
		}};

		expect(IllegalArgumentException.class, exception);

		Genesis.create(clazz);

		new Verifications() {{
			Genesis.getSupplier(clazz); times = 1;
		}};
	}

	@Test
	public void create_Enum_No_Values_Test() {
		Object result = Genesis.create(EnumWithNoValues.class);
		assertNull(result);
	}

	@Test
	public void create_Reader_Test() {
		Reader result = Genesis.create(Reader.class);

		try {
			assertTrue(result instanceof CharArrayReader);
		} finally {
			Resources.close(result);
		}
	}

	@Test
	public void create_Writer_Test() {
		Writer result = Genesis.create(Writer.class);

		try {
			assertTrue(result instanceof CharArrayWriter);
		} finally {
			Resources.close(result);
		}
	}

	@Test
	public void create_Constructor_Accepts_Same_Class_Test() {
		SelfReferencingClass result = Genesis.create(SelfReferencingClass.class);
		assertNotNull(result);
		assertNull(result.selfReferencingClass);
	}

	@Test
	public void create_boolean_Test() {
		Set<Boolean> values = new HashSet<>(Arrays.asList(true, false));
		Set<Boolean> result = new HashSet<>();

		for (int i = 0; i < REPEAT; i++) {
			boolean b = Genesis.create(boolean.class);
			result.add(b);
		}

		assertEquals(values, result);
	}

	@Test(timeout = 4000)
	public void create_byte_Test() {
		byte result;
		byte result2;

		do {
			result = Genesis.create(byte.class);
			result2 = Genesis.create(byte.class);
		} while (result == result2);
	}

	@Test
	public void create_char_Test() {
		Set<Character> alphabet = Strings.toCharacterSet("abcdefghijklmnopqrstuvwxyz");
		Set<Character> result = new HashSet<>();

		for (int i = 0; i < REPEAT; i++) {
			char c = Genesis.create(char.class);
			result.add(c);
		}

		assertEquals(alphabet, result);
	}

	@Test
	public void create_double_Test() {
		double result = Genesis.create(double.class);
		double result2 = Genesis.create(double.class);

		assertNotEquals(result, result2);
	}

	@Test
	public void create_float_Test() {
		float result = Genesis.create(float.class);
		float result2 = Genesis.create(float.class);

		assertNotEquals(result, result2);
	}

	@Test
	public void create_long_Test() {
		long result = Genesis.create(long.class);
		long result2 = Genesis.create(long.class);

		assertNotEquals(result, result2);
	}

	@Test
	public void create_short_Test() {
		short result = Genesis.create(short.class);
		short result2 = Genesis.create(short.class);

		assertNotEquals(result, result2);
	}

	@Test
	public void create_Number_Test() {
		Number result = Genesis.create(Number.class);
		Number result2 = Genesis.create(Number.class);

		assertNotEquals(result, result2);
	}

}
