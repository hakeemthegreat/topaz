package org.coco.topaz.util.reflection;

import lombok.Getter;
import lombok.Setter;
import mockit.Expectations;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Date;

import static org.coco.topaz.util.Constants.Strings.Messages.ERROR_WHILE_EXECUTING_STATIC_BLOCK;
import static org.junit.Assert.*;

public class MethodsTest {

	@Getter
	@Setter
	private static class Employee {
		private Long id;
		private String name;
	}

	private static final Employee EMPLOYEE = new Employee();
	private static final Method GET_ID;
	private static final Method SET_ID;
	private static final Method GET_NAME;
	private static final Method SET_NAME;
	private static final Method TO_STRING;

	static {
		try {
			GET_ID = Employee.class.getDeclaredMethod("getId");
			SET_ID = Employee.class.getDeclaredMethod("setId", Long.class);
			GET_NAME = Employee.class.getDeclaredMethod("getName");
			SET_NAME = Employee.class.getDeclaredMethod("setName", String.class);
			TO_STRING = Object.class.getMethod("toString");
			EMPLOYEE.setId(Dummy.LONG);
			EMPLOYEE.setName(Dummy.STRING);
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException(ERROR_WHILE_EXECUTING_STATIC_BLOCK, e);
		}
	}

	@Test
	public void getMethods_Test() {
		List<Method> results = Methods.getMethods(Employee.class);
		assertTrue(results.contains(GET_ID));
		assertTrue(results.contains(SET_ID));
		assertTrue(results.contains(GET_NAME));
		assertTrue(results.contains(SET_NAME));
	}

	@Test
	public void invoke_Test() {
		Object result = Methods.invoke(EMPLOYEE, GET_NAME);
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void invoke_Null_Method_Test() {
		new Expectations(Reflection.class) {{
			Reflection.makeAccessible((Method) any); times = 0;
		}};

		Object result = Methods.invoke(EMPLOYEE, (Method) null);
		assertNull(result);
	}

	@Test(expected = RuntimeException.class)
	public void invoke_ReflectiveOperationException_Test() {
		new Expectations(Reflection.class) {{
			Reflection.makeAccessible(GET_NAME); result = new ReflectiveOperationException();
		}};

		Object result = Methods.invoke(EMPLOYEE, GET_NAME);
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void invoke_New_Signature_Test() {
		Method method = TO_STRING;
		Object object = Dummy.OBJECT;
		Object[] arguments = Dummy.objectArray();

		new Expectations(Methods.class) {{
			Methods.invoke(object, method, arguments); result = Dummy.DATE;
		}};

		Object result = Methods.invoke(object, method, arguments);
		assertEquals(Dummy.DATE, result);
	}

	@Test
	public void getMethods_Create_Then_Get_From_Cache_Test() {
		List<Method> results = Methods.getMethods(Employee.class);
		assertNotNull(results);
		assertFalse(results.isEmpty());

		List<Method> results2 = Methods.getMethods(Employee.class);
		assertEquals(results, results2);
	}

	@Test
	public void getMethod_Test() {
		Method result = Methods.getMethod(String.class, "isEmpty");
		assertNotNull(result);
		assertEquals(boolean.class, result.getReturnType());
		assertEquals(0, result.getParameterCount());
	}

	@Test
	public void getMethod_Null_Result_Test() {
		Method result = Methods.getMethod(Date.class, "isEmpty");
		assertNull(result);
	}

	@Test
	public void invoke_With_Method_Name_Test() {
		Object result = Methods.invoke(Dummy.OBJECT, "toString");
		assertNotNull(result);
	}

	@Test
	public void invoke_With_Static_Method_Name_Test() {
		Object result = Methods.invoke(String.class, "valueOf", 1);
		assertEquals("1", result);
	}

	@Test(expected = RuntimeException.class)
	public void invoke_With_Method_Name_That_Does_Not_Exist_Test() {
		Methods.invoke(Dummy.OBJECT, "madeUpMethod", Dummy.STRING, Dummy.LONG);
	}

	@Test
	public void invoke_Static_Method() {
		Long value = 123L;
		String valueAsString = value.toString();
		String className = "java.lang.Long";
		String methodName = "parseLong";

		Object result = Methods.invoke(className, methodName, valueAsString);
		assertEquals(value, result);

	}

}
