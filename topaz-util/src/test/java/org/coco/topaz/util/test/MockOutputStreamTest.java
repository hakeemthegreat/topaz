package org.coco.topaz.util.test;

import mockit.Tested;
import org.junit.Test;

public class MockOutputStreamTest {

	@Tested
	private MockOutputStream instance;

	@Test
	public void write_Test() {
		instance.write(1);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void close_Test() {
		instance.close();
		instance.write(1);
	}

}
