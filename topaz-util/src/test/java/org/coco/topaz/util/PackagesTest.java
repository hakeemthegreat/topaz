package org.coco.topaz.util;

import mockit.Verifications;

import mockit.Expectations;
import org.coco.topaz.util.node.BinaryNode;
import org.coco.topaz.util.node.LinearNode;
import org.coco.topaz.util.node.Node;
import org.coco.topaz.util.node.TrieNode;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class PackagesTest {

	private static final String PACKAGE_NAME = "org.coco.topaz.util.node";

	@Test
	public void getClasses_By_Package_Name_Test() {
		List<Class<?>> results = Packages.getClasses(PACKAGE_NAME);
		assertNotNull(results);
		assertTrue(results.contains(BinaryNode.class));
		assertTrue(results.contains(LinearNode.class));
		assertTrue(results.contains(Node.class));
		assertTrue(results.contains(TrieNode.class));
	}

	@Test(expected = RuntimeException.class)
	public void getClasses_By_Package_Name_IOException_Test() throws Exception {
		new Expectations(Packages.class) {{
			Packages.getClasses0(anyString);
			result = new IOException();
		}};

		Packages.getClasses(PACKAGE_NAME);
	}

	@Test(expected = RuntimeException.class)
	public void getClasses_By_Package_Name_ClassNotFoundException_Test() throws Exception {
		new Expectations(Packages.class) {{
			Packages.getClasses0(anyString);
			result = new ClassNotFoundException();
		}};

		Packages.getClasses(PACKAGE_NAME);
	}

	@Test
	public void getClasses_By_Package_Test() {
		Package p = Node.class.getPackage();
		List<Class<?>> classes = Collections.singletonList(Object.class);

		new Expectations(Packages.class) {{
			Packages.getClasses(anyString);
			result = classes;
		}};

		List<Class<?>> result = Packages.getClasses(p);
		assertEquals(classes, result);

		new Verifications() {{
			Packages.getClasses("org.coco.topaz.util.node"); times = 1;
		}};
	}

	@Test
	public void getClasses_By_Package_Null_Test() {
		new Expectations(Packages.class) {{
			Packages.getClasses(anyString); times = 0;
		}};

		List<Class<?>> result = Packages.getClasses((Package) null);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void getParentPackage_Test() {
		Package p = Node.class.getPackage();
		Package parentPackage = Package.getPackage("org.coco.topaz.util");

		Package result = Packages.getParentPackage(p);
		assertEquals(parentPackage, result);
	}

	@Test
	public void getParentPackageName_By_Package_Test() {
		Package p = Node.class.getPackage();

		String result = Packages.getParentPackageName(p);
		assertEquals("org.coco.topaz.util", result);
	}

	@Test
	public void getParentPackageName_By_Package_Null_Test() {
		String result = Packages.getParentPackageName((Package) null);
		assertNull(result);
	}

	@Test
	public void getParentPackageName_By_Package_Name_Test() {
		String result = Packages.getParentPackageName("one.two.three");
		assertEquals("one.two", result);
	}

	@Test
	public void getParentPackageName_By_Package_Name_No_Period_Test() {
		String result = Packages.getParentPackageName("one");
		assertNull(result);
	}

}