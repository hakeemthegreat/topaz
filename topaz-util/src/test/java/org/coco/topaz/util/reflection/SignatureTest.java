package org.coco.topaz.util.reflection;

import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.*;

public class SignatureTest {

	private final Class<?> returnType = Object.class;
	private final String name = Dummy.STRING;
	private final Class<?>[] parameterTypes = {Object.class};

	private final Signature instance = new Signature(returnType, name, parameterTypes);

	@Test
	public void returnType_GetterSetter_Test() {
		instance.setReturnType(InputStream.class);
		assertEquals(InputStream.class, instance.getReturnType());
	}

	@Test
	public void name_GetterSetter_Test() {
		instance.setName(Dummy.STRING3);
		assertEquals(Dummy.STRING3, instance.getName());
	}

	@Test
	public void parameterTypes_GetterSetter_Test() {
		Class<?>[] parameterTypes = {InputStream.class};

		instance.setParameterTypes(parameterTypes);
		assertArrayEquals(parameterTypes, instance.getParameterTypes());
	}

	@Test
	public void equals_Test() {
		Signature signature = new Signature(returnType, name, parameterTypes);
		assertEquals(instance, signature);
	}

	@Test
	public void equals_Test2() {
		Signature signature = new Signature(returnType, name);
		assertNotEquals(instance, signature);
	}

	@Test
	public void equals_Test3() {
		Signature signature = new Signature(returnType, Dummy.STRING2);
		assertNotEquals(instance, signature);
	}

	@Test
	public void equals_Test4() {
		Signature signature = new Signature(Date.class, name);
		assertNotEquals(instance, signature);
	}

	@Test
	public void equals_Different_Object_Test() {
		Object object = new Object();

		boolean result = instance.equals(object);
		assertFalse(result);
	}

	@Test
	public void hashCode_Test() {
		Integer hashCode = instance.hashCode();
		assertNotNull(hashCode);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertNotNull(result);
	}

}
