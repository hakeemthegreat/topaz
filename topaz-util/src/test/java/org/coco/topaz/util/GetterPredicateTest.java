package org.coco.topaz.util;

import mockit.Tested;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class GetterPredicateTest {

	@Tested
	private GetterPredicate instance;

	@Test
	public void test_Return_Type() throws Exception {
		Method method = newMethod(void.class, 0);

		assertFalse(instance.test(method));
	}

	@Test
	public void test_Parameter_Count() throws Exception {
		Method method = newMethod(Object.class, 3);

		assertFalse(instance.test(method));
	}

	@Test
	public void test_Valid_Getter() throws Exception {
		Method method = newMethod(Object.class, 0);

		assertTrue(instance.test(method));
	}

	private Method newMethod(Class<?> returnType, int parameterCount) throws ReflectiveOperationException {
		Class<?> declaringClass = Object.class;
		String name = Dummy.STRING;
		Class<?>[] parameterTypes = new Class<?>[parameterCount];
		Class<?>[] checkedExceptions = {};
		int modifiers = Dummy.INT;
		int slot = Dummy.INT2;
		String signature = Dummy.STRING;
		byte[] annotations = ArrayUtils.toPrimitive(Dummy.byteArray());
		byte[] parameterAnnotations = ArrayUtils.toPrimitive(Dummy.byteArray());
		byte[] annotationDefault = ArrayUtils.toPrimitive(Dummy.byteArray());

		for (int i = 0; i < parameterCount; i++) {
			parameterTypes[i] = Object.class;
		}

		return newMethod(declaringClass, name, parameterTypes, returnType, checkedExceptions, modifiers, slot, signature, annotations, parameterAnnotations, annotationDefault);
	}

	private Method newMethod(Class<?> declaringClass, String name, Class<?>[] parameterTypes, Class<?> returnType, Class<?>[] checkedExceptions, int modifiers, int slot, String signature, byte[] annotations, byte[] parameterAnnotations, byte[] annotationDefault) throws ReflectiveOperationException {
		Constructor<?> constructor = Method.class.getDeclaredConstructors()[0];
		constructor.setAccessible(true);
		return (Method) constructor.newInstance(declaringClass, name, parameterTypes, returnType, checkedExceptions, modifiers, slot, signature, annotations, parameterAnnotations, annotationDefault);
	}

}
