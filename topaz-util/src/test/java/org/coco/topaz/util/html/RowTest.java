package org.coco.topaz.util.html;

import mockit.Tested;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RowTest {

	@Tested(availableDuringSetup = true)
	private Row instance;

	private static final HTMLElement CUSTOM = new HTMLElement("custom");

	private static final Cell CELL = new Cell();
	private static final Cell CELL2 = new Cell();
	private static final Cell CELL3 = new Cell();
	private static final List<Cell> CELLS = Arrays.asList(CELL, CELL2, CELL3);

	@Before
	public void before() {
		instance.addChild(CUSTOM);
		instance.addChild(CELL);
		instance.addChild(CUSTOM);
		instance.addChild(CELL2);
		instance.addChild(CUSTOM);
		instance.addChild(CELL3);
		instance.addChild(CUSTOM);
	}

	@Test
	public void getCell_Test() {
		Cell result = instance.getCell(1);
		assertEquals(CELL2, result);
	}

	@Test
	public void getCells_Test() {
		List<Cell> results = instance.getCells();
		assertEquals(CELLS, results);
	}

	@Test
	public void getFirstCell_Test() {
		Cell result = instance.getFirstCell();
		assertEquals(CELL, result);
	}

}
