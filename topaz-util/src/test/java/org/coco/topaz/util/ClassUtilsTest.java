package org.coco.topaz.util;

import org.coco.topaz.util.node.Node;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class ClassUtilsTest {

	@Test
	public void getDataType_Test() {
		Class<?> result = ClassUtils.getDataType(Dummy.stringList());
		assertEquals(String.class, result);
	}

	@Test
	public void getDataType_Empty_Collection_Test() {
		Class<?> result = ClassUtils.getDataType(Collections.emptyList());
		assertNull(result);
	}

	@Test
	public void getDataType_Null_Collection_Test() {
		Class<?> result = ClassUtils.getDataType(null);
		assertNull(result);
	}

	@Test
	public void getDataType_Array_Test() {
		String[] array = {"string"};

		Class<?> result = ClassUtils.getDataType(array);
		assertEquals(String.class, result);
	}

	@Test
	public void getDataType_Null_Array_Test() {
		Object[] array = null;

		Class<?> result = ClassUtils.getDataType(array);
		assertNull(result);
	}

	@Test
	public void areAssignableFrom_True_Test() {
		Object[] arguments = {Dummy.STRING, Dummy.LONG};
		Class<?>[] classes = {String.class, Object.class};

		boolean result = ClassUtils.areAssignableFrom(arguments, classes);
		assertTrue(result);
	}

	@Test
	public void areAssignableFrom_False_Test() {
		Object[] arguments = {Dummy.STRING, Dummy.LONG};
		Class<?>[] classes = {String.class, String.class};

		boolean result = ClassUtils.areAssignableFrom(arguments, classes);
		assertFalse(result);
	}

	@Test
	public void areAssignableFrom_False_Test2() {
		Object[] arguments = {Dummy.STRING};
		Class<?>[] classes = {String.class, String.class};

		boolean result = ClassUtils.areAssignableFrom(arguments, classes);
		assertFalse(result);
	}

	@Test
	public void instanceOf_True_Test() {
		boolean result = ClassUtils.instanceOf(Integer.class, Number.class);
		assertTrue(result);
	}

	@Test
	public void instanceOf_False_Test() {
		boolean result = ClassUtils.instanceOf(Integer.class, String.class);
		assertFalse(result);
	}

	@Test
	public void instanceOf_Double_Null_Test() {
		boolean result = ClassUtils.instanceOf(null, null);
		assertFalse(result);
	}

	@Test
	public void instanceOf_First_Argument_Null_Test() {
		boolean result = ClassUtils.instanceOf(null, String.class);
		assertFalse(result);
	}

	@Test
	public void instanceOf_Second_Argument_Null_Test() {
		boolean result = ClassUtils.instanceOf(String.class, null);
		assertFalse(result);
	}

	@Test
	public void isNativeJavaClass_True_Test() {
		boolean result = ClassUtils.isNativeJavaClass(long.class);
		assertTrue(result);
	}

	@Test
	public void isNativeJavaClass_False_Test() {
		boolean result = ClassUtils.isNativeJavaClass(Node.class);
		assertFalse(result);
	}

	@Test
	public void getClass_Test() {
		Class<?> result = ClassUtils.getClass(Dummy.OBJECT);
		assertEquals(Object.class, result);
	}

	@Test
	public void getClass_Null_Test() {
		Class<?> result = ClassUtils.getClass((Object) null);
		assertNull(result);
	}

	@Test
	public void getClassName_Test() {
		assertEquals("java.lang.Object", ClassUtils.getClassName(Dummy.OBJECT));
		assertNull(ClassUtils.getClassName(null));
	}

	@Test
	public void getClass_By_Class_Name_Test() {
		Class<?> result = ClassUtils.getClass("java.lang.Object");
		assertEquals(Object.class, result);
	}

	@Test(expected = RuntimeException.class)
	public void getClass_By_Class_Name_ClassNotFoundException_Test() {
		ClassUtils.getClass("java.lang.ClassThatDoesNotExist");
	}

}
