package org.coco.topaz.util.html;

import mockit.Tested;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TableTest {

	@Tested(availableDuringSetup = true)
	private Table instance;

	private static final HTMLElement CUSTOM = new HTMLElement("custom");

	private static final Row ROW = new Row();
	private static final Row ROW2 = new Row();
	private static final Row ROW3 = new Row();
	private static final List<Row> ROWS = Arrays.asList(ROW, ROW2, ROW3);

	@Before
	public void before() {
		instance.addChild(CUSTOM);
		instance.addChild(ROW);
		instance.addChild(CUSTOM);
		instance.addChild(ROW2);
		instance.addChild(CUSTOM);
		instance.addChild(ROW3);
		instance.addChild(CUSTOM);
	}

	@Test
	public void getRow_Test() {
		Row result = instance.getRow(1);
		assertEquals(ROW2, result);
	}

	@Test
	public void getRows_Test() {
		List<Row> results = instance.getRows();
		assertEquals(ROWS, results);
	}

	@Test
	public void getFirstRow_Test() {
		Row result = instance.getFirstRow();
		assertEquals(ROW, result);
	}

}
