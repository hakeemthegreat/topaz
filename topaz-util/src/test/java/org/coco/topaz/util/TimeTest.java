package org.coco.topaz.util;

import mockit.Injectable;
import mockit.Tested;
import org.coco.topaz.util.test.GetterSetterTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeTest extends GetterSetterTest<Time> {

	@Injectable
	private long milliseconds = 45296000;

	@Tested
	private Time instance;

	public TimeTest() {
		super(new Time());
	}
	
	@Test
	public void default_Constructor_Test() {
		Time time = new Time();
		assertEquals(0, time.getHours());
		assertEquals(0, time.getMinutes());
		assertEquals(0, time.getSeconds());
	}

	@Test
	public void toMilliseconds_Test() {
		long result = instance.toMilliseconds();
		assertEquals(milliseconds, result);
	}

	@Test
	public void toString_Test() {
		String result = instance.toString();
		assertEquals("12:34.56", result);
	}

}
