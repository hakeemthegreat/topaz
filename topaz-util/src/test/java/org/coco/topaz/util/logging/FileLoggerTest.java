package org.coco.topaz.util.logging;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.Assert.*;

public class FileLoggerTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void write_Test() throws Exception {
		File file = temporaryFolder.newFile();
		Path path = file.toPath();
		String message = "Unable to ping server";

		FileLogger instance = new FileLogger(Object.class, file);
		instance.warn(message);

		List<String> lines = Files.readAllLines(path);
		assertNotNull(lines);
		assertEquals(1, lines.size());

		String line = lines.get(0);
		assertNotNull(line);
		assertTrue(line.contains(message));
	}

}
