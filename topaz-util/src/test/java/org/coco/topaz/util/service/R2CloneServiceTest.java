package org.coco.topaz.util.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class R2CloneServiceTest extends CloneServiceTest<R2CloneService> {

	public R2CloneServiceTest() {
		super(new R2CloneService());
	}

	@Test
	public void cloneComplexObject_Test() throws Exception {
		Object object = new Object();

		Object result = instance.cloneComplexObject(object);
		assertNotNull(result);
		assertNotSame(object, result);
	}

	@Test
	public void cloneComplexObject_Number_Test() throws Exception {
		Long object = 123L;

		Long result = instance.cloneComplexObject(object);
		assertNotSame(object, result);
		assertEquals(object, result);
	}

}
