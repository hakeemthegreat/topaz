package org.coco.topaz.util.service;

import org.coco.topaz.util.exception.CloneException;
import org.coco.topaz.util.node.BinaryNode;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.*;

public abstract class CloneServiceTest<T extends CloneService> extends JavaTest {

	protected T instance;

	private static final String STRING = "aString";
	private static final Long LONG = 12345L;
	private static final Date DATE = new Date();

	public CloneServiceTest(T instance) {
		this.instance = Objects.requireNonNull(instance);
	}

	@Test
	public void clone_String_Test() throws CloneException {
		String result = instance.clone(STRING);
		assertEquals(STRING, result);
		assertNotSame(STRING, result);
	}

	@Test
	public void clone_Date_Test() throws CloneException {
		Date result = instance.clone(DATE);
		assertEquals(DATE, result);
		assertNotSame(DATE, result);
	}

	@Test
	public void clone_Long_Test() throws CloneException {
		Long result = instance.clone(LONG);
		assertEquals(LONG, result);
		assertNotSame(LONG, result);
	}

	@Test
	public void clone_Node_Test() throws CloneException {
		BinaryNode node = new BinaryNode(STRING);
		BinaryNode left = new BinaryNode(LONG);
		BinaryNode right = new BinaryNode(DATE);

		node.setLeft(left);
		node.setRight(right);

		BinaryNode clone = instance.clone(node);
		assertNotNull(clone);
		assertNotSame(node, clone);
		assertNotSame(STRING, clone.getValue());
		assertEquals(STRING, clone.getValue());

		BinaryNode leftClone = clone.getLeft();
		assertNotNull(leftClone);
		assertNotSame(left, leftClone);
		assertNotSame(LONG, leftClone.getValue());
		assertEquals(LONG, leftClone.getValue());

		BinaryNode rightClone = clone.getRight();
		assertNotNull(rightClone);
		assertNotSame(right, rightClone);
		assertNotSame(DATE, rightClone.getValue());
		assertEquals(DATE, rightClone.getValue());
	}

	@Test
	public void clone_Null_Test() throws CloneException {
		Object result = instance.clone(null);
		assertNull(result);
	}

	@Test
	public void clone_Map_Test() throws CloneException {
		Map<String, String> map = new HashMap<>();

		map.put("key1", "value1");
		map.put("key2", "value2");
		map.put("key3", "value3");

		Map<String, String> result = instance.clone(map);
		assertEquals(map, result);
		assertNotSame(map, result);
	}

	@Test
	public void clone_Array_Test() throws CloneException {
		String[] array = {"one", "two", "three"};

		String[] result = instance.clone(array);
		assertArrayEquals(array, result);
		assertNotSame(array, result);
	}

}
