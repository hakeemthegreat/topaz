package org.coco.topaz.util.test;

import mockit.Deencapsulation;
import mockit.Tested;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MockInputStreamTest {

	@Tested
	private MockInputStream instance;

	private static final int LARGE_PRIME_NUMBER = Deencapsulation.getField(MockInputStream.class, "LARGE_PRIME_NUMBER");

	@Test
	public void read_Test() {
		int b = instance.read();
		int read = 0;

		while (b != -1) {
			assertTrue(0 <= b);
			assertTrue(b <= 255);
			b = instance.read();
			read++;
		}

		b = instance.read();
		assertEquals(-1, b);
		assertEquals(LARGE_PRIME_NUMBER, read);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void close_Test() {
		instance.close();
		instance.read();
	}

}
