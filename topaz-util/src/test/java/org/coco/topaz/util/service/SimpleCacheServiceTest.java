package org.coco.topaz.util.service;

import mockit.Tested;
import org.coco.topaz.util.service.SimpleCacheService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleCacheServiceTest {

	@Tested
	private SimpleCacheService instance;

	private static final String KEY = "key";
	private static final String VALUE = "value";

	@Test
	public void cache_Test() {
		instance.put(KEY, VALUE);

		String result = instance.get(KEY);
		assertEquals(VALUE, result);
	}

}
