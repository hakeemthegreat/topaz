package org.coco.topaz.ia;

import java.lang.reflect.Constructor;
import java.util.Objects;
import java.util.Set;
import java.util.function.UnaryOperator;

import org.coco.topaz.util.Ensure;
import org.coco.topaz.util.Strings;
import org.coco.topaz.util.reflection.Constructors;

/**
 * <p><strong>Sanitization.</strong>  A combination of escaping, filtering, and validation that ensures that an input to a system function does not trigger an unexpected and unauthorized behavior.</p>
 */
public abstract class Sanitizer {

	protected final UnaryOperator<String> function;
	protected Sanitizer previous;

	private static final String VALIDATION_MESSAGE = "Your input contains illegal character '%s' (\\u%s). Please remove it.";

	public Sanitizer(UnaryOperator<String> function) {
		this.function = Objects.requireNonNull(function);
	}

	public final <T> T apply(T object) {
		T result = null;

		if (object != null) {
			if (object instanceof String) {
				result = (T) applyTo((String) object);
			} else {
				result = applyTo(object);
			}
		}

		return result;
	}

	protected <T> T applyTo(T object) {
		throw new UnsupportedOperationException();
	}

	protected String applyTo(String string) {
		if (previous != null) {
			string = previous.apply(string);
		}

		return function.apply(string);
	}

	public Sanitizer and(Sanitizer sanitizer) {
		Ensure.notNull(sanitizer);
		Constructor<? extends Sanitizer> constructor = Constructors.getConstructor(sanitizer.getClass(), UnaryOperator.class);

		sanitizer = Constructors.newInstance(constructor, sanitizer.function);
		sanitizer.previous = this;

		return sanitizer;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;

		if (object instanceof Sanitizer) {
			Sanitizer sanitizer = (Sanitizer) object;
			result = function.equals(sanitizer.function);
		}

		return result;
	}

	@Override
	public int hashCode() {
		return function.hashCode();
	}

	/**
	 * <p><strong>Validation.</strong>  Comparison of an input against a white list or regular expression to detect control characters or other character sequences that would trigger an unauthorized behavior.  For example, an account number entered by a user might be validated against a list of account numbers known to be tied to the user.</p>
	 */
	public static String validate(final String string) {
		if (string != null) {
			for (int i = 0; i < string.length(); i++) {
				Character c = string.charAt(i);

				if (!getWhitelist().contains(c)) {
					String message = String.format(VALIDATION_MESSAGE, c, Strings.toHexString(c));
					throw new IllegalArgumentException(message);
				}
			}
		}

		return string;
	}

	/**
	 * <p><strong>Filtering</strong>.  Like escaping, but instead of replacing the control character, it is simply removed.</p>
	 */
	public static String filter(String string) {
		String filtered = null;

		if (string != null) {
			StringBuilder sb = new StringBuilder(string.length());

			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);

				if (!getBlacklist().contains(c)) {
					sb.append(c);
				}
			}

			filtered = sb.toString();
		}

		return filtered;
	}

	/**
	 * <p><strong>Escaping</strong>.  Converting a control character to its escape sequence.  For example, a <code>&#60;</code> symbol may be converted to <code>&#38;&#35;60;</code> so that the characters following the <code>&#60;</code> are not interpreted as an XML tag instead of XML content.</p>
	 */
	public static String escape(String string) {
		String escaped = null;

		if (string != null) {
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);

				sb.append("&#");
				sb.append((int) c);
				sb.append(";");
			}

			escaped = sb.toString();
		}

		return escaped;
	}

	protected static IAConfiguration getIAConfiguration() {
		return IAConfiguration.getInstance();
	}

	protected static Set<Character> getBlacklist() {
		return getIAConfiguration().getBlacklist();
	}

	protected static Set<Character> getWhitelist() {
		return getIAConfiguration().getWhitelist();
	}

}
