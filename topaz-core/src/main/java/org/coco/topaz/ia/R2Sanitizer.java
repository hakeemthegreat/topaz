package org.coco.topaz.ia;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.coco.topaz.util.Replace;
import org.coco.topaz.util.reflection.Constructors;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.UnaryOperator;

public class R2Sanitizer extends Sanitizer {

	private final ObjectMapper objectMapper = new ObjectMapper();
	private final Map<Object, Set<Object>> map = new HashMap<>();

	public R2Sanitizer(UnaryOperator<String> function) {
		super(function);
	}

	@Override
	public <T> T applyTo(T object) {
		T result = null;
		boolean flag;

		if (objectAlreadySanitized(object)) {
			result = object;
			flag = false;
		} else {
			markObjectAsSanitized(object);
			flag = true;
		}

		if (flag) {
			if (object.getClass().isArray()) {
				result = sanitizeArray(object);
			} else if (isWhitelisted(object)) {
				result = object;
			} else if (object instanceof Map) {
				result = sanitizeMap(object);
			} else {
				result = sanitizeObject(object);
			}
		}

		return result;
	}

	protected <T> T sanitizeObject(T object) {
		Class<T> clazz = (Class<T>) object.getClass();
		List<Field> fields = Fields.getFields(clazz);
		T result = getNewInstance(object);

		for (Field field : fields) {
			if (Reflection.isNotStatic(field)) {
				Object value;

				field.setAccessible(true);
				Reflection.removeFinalModifier(field);
				value = Fields.getValue(field, object);
				Fields.set(result, field, this.apply(value));
			}
		}

		return result;
	}

	protected <K, V, M extends Map<K, V>> M sanitizeMap(Object object) {
		M map = (M) object;
		Class<M> clazz = (Class<M>) map.getClass();
		Constructor<M> constructor = Constructors.getDefaultConstructor(clazz);
		M result = Constructors.newInstance(constructor);

		for (Entry<K, V> entry : map.entrySet()) {
			K key = entry.getKey();
			V value = entry.getValue();

			key = this.apply(key);
			value = this.apply(value);
			result.put(key, value);
		}

		return result;
	}

	protected <T> T sanitizeArray(T array) {
		int length = Array.getLength(array);
		T result = (T) Array.newInstance(array.getClass().getComponentType(), length);

		for (int i = 0; i < length; i++) {
			Object value = Array.get(array, i);
			Array.set(result, i, this.apply(value));
		}

		return result;
	}

	protected <T> T getNewInstance(T object) {
		Class<T> clazz = (Class<T>) object.getClass();
		Constructor<T> constructor = Constructors.getDefaultConstructor(clazz);
		T newInstance;

		try {
			if (constructor == null) {
				String json = objectMapper.writeValueAsString(object);
				newInstance = objectMapper.convertValue(json, clazz);
			} else {
				newInstance = Constructors.newInstance(constructor);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to get new instance " + clazz.getName(), e);
		}

		return newInstance;
	}

	protected boolean isWhitelisted(Object object) {
		boolean result = false;

		if (object instanceof Boolean) {
			result = true;
		} else if (object instanceof Number) {
			result = true;
		} else if (object instanceof Date) {
			result = true;
		}

		return result;
	}

	protected boolean objectAlreadySanitized(Object object) {
		Set<Object> sanitizedObjects = getMap().get(object);

		sanitizedObjects = Replace.with(new HashSet<>()).should(sanitizedObjects).beNull();
		map.put(object, sanitizedObjects);

		return sanitizedObjects.stream().anyMatch(item -> item == object);
	}

	protected void markObjectAsSanitized(Object object) {
		getMap().get(object).add(object);
	}

	protected Map<Object, Set<Object>> getMap() {
		return map;
	}

}
