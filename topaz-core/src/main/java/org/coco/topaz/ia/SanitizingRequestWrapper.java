package org.coco.topaz.ia;

import org.coco.topaz.util.IteratorEnumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;

public class SanitizingRequestWrapper extends HttpServletRequestWrapper {
	
	private final Sanitizer sanitizer;

	public SanitizingRequestWrapper(HttpServletRequest request, Sanitizer sanitizer) {
		super(request);
		this.sanitizer = Objects.requireNonNull(sanitizer);
	}
	
	@Override
	public String getParameter(String name) {
		return sanitizer.apply(super.getParameter(name));
	}
	
	@Override
	public String[] getParameterValues(String name) {
		return sanitizer.apply(super.getParameterValues(name));
	}
	
	@Override
	public String getContextPath() {
		return sanitizer.apply(super.getContextPath());
	}
	
	@Override
	public String getServletPath() {
		return sanitizer.apply(super.getServletPath());
	}

	@Override
	public String getAuthType() {
		return sanitizer.apply(super.getAuthType());
	}

	@Override
	public Cookie[] getCookies() {
		return sanitizer.apply(super.getCookies());
	}

	@Override
	public String getHeader(String name) {
		return sanitizer.apply(super.getHeader(name));
	}

	@Override
	public Enumeration<String> getHeaders(String name) {
		return sanitizeEnumeration(super.getHeaders(name));
	}

	@Override
	public Enumeration<String> getHeaderNames() {
		return sanitizeEnumeration(super.getHeaderNames());
	}

	@Override
	public String getMethod() {
		return sanitizer.apply(super.getMethod());
	}

	@Override
	public String getPathInfo() {
		return sanitizer.apply(super.getPathInfo());
	}

	@Override
	public String getPathTranslated() {
		return sanitizer.apply(super.getPathTranslated());
	}

	@Override
	public String getQueryString() {
		return sanitizer.apply(super.getQueryString());
	}

	@Override
	public String getRemoteUser() {
		return sanitizer.apply(super.getRemoteUser());
	}

	@Override
	public Principal getUserPrincipal() {
		return sanitizer.apply(super.getUserPrincipal());
	}

	@Override
	public String getRequestedSessionId() {
		return sanitizer.apply(super.getRequestedSessionId());
	}

	@Override
	public String getRequestURI() {
		return sanitizer.apply(super.getRequestURI());
	}

	@Override
	public StringBuffer getRequestURL() {
		String unsanitized = super.getRequestURL().toString();
		String sanitized = sanitizer.apply(unsanitized);

		return new StringBuffer(sanitized);
	}

	@Override
	public HttpSession getSession(boolean create) {
		return sanitizer.apply(super.getSession(create));
	}

	@Override
	public HttpSession getSession() {
		return sanitizer.apply(super.getSession());
	}

	@Override
	public ServletRequest getRequest() {
		return sanitizer.apply(super.getRequest());
	}

	@Override
	public Object getAttribute(String name) {
		return sanitizer.apply(super.getAttribute(name));
	}

	@Override
	public Enumeration<String> getAttributeNames() {
		return sanitizeEnumeration(super.getAttributeNames());
	}

	@Override
	public String getCharacterEncoding() {
		return sanitizer.apply(super.getCharacterEncoding());
	}

	@Override
	public String getContentType() {
		return sanitizer.apply(super.getContentType());
	}

	@Override
	public Map<String, String> getParameterMap() {
		return sanitizer.apply(super.getParameterMap());
	}

	@Override
	public Enumeration<String> getParameterNames() {
		return sanitizeEnumeration(super.getParameterNames());
	}

	@Override
	public String getProtocol() {
		return sanitizer.apply(super.getProtocol());
	}

	@Override
	public String getScheme() {
		return sanitizer.apply(super.getScheme());
	}

	@Override
	public String getServerName() {
		return sanitizer.apply(super.getServerName());
	}

	@Override
	public String getRemoteAddr() {
		return sanitizer.apply(super.getRemoteAddr());
	}

	@Override
	public String getRemoteHost() {
		return sanitizer.apply(super.getRemoteHost());
	}

	@Override
	public Locale getLocale() {
		return sanitizer.apply(super.getLocale());
	}

	@Override
	public Enumeration<Locale> getLocales() {
		return sanitizeEnumeration(super.getLocales());
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		return sanitizer.apply(super.getRequestDispatcher(path));
	}

	@Override
	public String getRealPath(String path) {
		return sanitizer.apply(super.getRealPath(path));
	}

	@Override
	public String getLocalName() {
		return sanitizer.apply(super.getLocalName());
	}

	@Override
	public String getLocalAddr() {
		return sanitizer.apply(super.getLocalAddr());
	}

	protected <T> Enumeration<T> sanitizeEnumeration(Enumeration<T> enumeration) {
		List<T> list = new ArrayList<>();

		while (enumeration.hasMoreElements()) {
			T element = enumeration.nextElement();
			list.add(sanitizer.apply(element));
		}

		return new IteratorEnumeration<>(list.iterator());
	}

}
