package org.coco.topaz.ia;

import java.io.*;
import java.util.function.UnaryOperator;

public class SerializingSanitizer extends Sanitizer {

	private static final String ERROR_MESSAGE = "An error occurred while sanitizing the data";

	public SerializingSanitizer(UnaryOperator<String> function) {
		super(function);
	}

	@Override
	public <T> T applyTo(T object) {
		try {
			byte[] bytes = serialize(object);
			return (T) deserialize(bytes);
		} catch (IOException | ClassNotFoundException e) {
			throw new RuntimeException(ERROR_MESSAGE, e);
		}
	}

	protected byte[] serialize(Object object) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

		try {
			objectOutputStream.writeObject(object);
			return byteArrayOutputStream.toByteArray();
		} finally {
			objectOutputStream.close();
		}
	}

	protected Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		ObjectInputStream objectInputStream = new SanitizingObjectInputStream(byteArrayInputStream, this);

		try {
			return objectInputStream.readObject();
		} finally {
			objectInputStream.close();
		}
	}

}
