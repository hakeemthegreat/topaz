package org.coco.topaz.ia;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public abstract class SanitizingFilter implements Filter  {

	private final Sanitizer sanitizer;

	public SanitizingFilter(Sanitizer sanitizer) {
		this.sanitizer = Objects.requireNonNull(sanitizer);
	}

	@Override
	public void init(FilterConfig filterConfig) {
		doNothing();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		SanitizingRequestWrapper sanitizingRequestWrapper;

		if (request instanceof HttpServletRequest) {
			sanitizingRequestWrapper = new SanitizingRequestWrapper((HttpServletRequest) request, sanitizer);
		} else {
			throw new IllegalArgumentException(String.valueOf(request));
		}
		
		chain.doFilter(sanitizingRequestWrapper, response);
	}

	@Override
	public void destroy() {
		doNothing();
	}
	
	public void doNothing() {
		//do nothing :)
	}

}
