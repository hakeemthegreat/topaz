package org.coco.topaz.ia;

public class Sanitizers {

	private Sanitizers() {

	}

	public static final Sanitizer FILTER = new R2Sanitizer(Sanitizer::filter);
	public static final Sanitizer VALIDATE = new R2Sanitizer(Sanitizer::validate);
	public static final Sanitizer ESCAPE = new R2Sanitizer(Sanitizer::escape);

}
