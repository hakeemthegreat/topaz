package org.coco.topaz.ia;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class IAConfiguration {

	protected final Set<Character> blacklist = new HashSet<>();
	protected final Set<Character> whitelist = new HashSet<>();

	private static final IAConfiguration INSTANCE = new IAConfiguration();

	private IAConfiguration() {

	}

	public static IAConfiguration getInstance() {
		return INSTANCE;
	}

	public void setBlacklist(Collection<Character> blacklist) {
		this.blacklist.clear();
		this.blacklist.addAll(blacklist);
	}

	public void setWhitelist(Collection<Character> whitelist) {
		this.whitelist.clear();
		this.whitelist.addAll(whitelist);
	}

	public Set<Character> getBlacklist() {
		return new HashSet<>(blacklist);
	}

	public Set<Character> getWhitelist() {
		return new HashSet<>(whitelist);
	}

	protected String unescapeWhitespaces(String string) {
		if (string != null) {
			string = string.replaceAll("\\\\f", "\f");
			string = string.replaceAll("\\\\n", "\n");
			string = string.replaceAll("\\\\r", "\r");
			string = string.replaceAll("\\\\t", "\t");
		}

		return string;
	}

}
