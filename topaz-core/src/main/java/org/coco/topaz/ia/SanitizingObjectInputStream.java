package org.coco.topaz.ia;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Objects;

public class SanitizingObjectInputStream extends ObjectInputStream {

	private final Sanitizer sanitizer;

	public SanitizingObjectInputStream(InputStream inputStream, Sanitizer sanitizer) throws IOException {
		super(Objects.requireNonNull(inputStream));
		this.sanitizer = Objects.requireNonNull(sanitizer);
		enableResolveObject(true);
	}

	@Override
	protected Object resolveObject(Object object) {
		if (object instanceof String) {
			object = sanitizer.apply((String) object);
		}

		return object;
	}

}
