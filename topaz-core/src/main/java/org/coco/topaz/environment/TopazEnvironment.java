package org.coco.topaz.environment;

public class TopazEnvironment extends Environment {

	protected TopazEnvironment() {
		super();
	}

	protected static ClassLoader getClassLoader() {
		return TopazEnvironment.class.getClassLoader();
	}

	public static String getDatabaseDriver() {
		return getProperty("topaz.database.driver");
	}

	public static String getDatabaseUrl() {
		return getProperty("topaz.database.url");
	}

	public static String getGeneratedSourcesLocation() {
		return getProjectRoot() + "/target/generated-sources";
	}

	public static String getDatabaseSQL() {
		return getProperty("topaz.database.sql");
	}

	public static String getBlacklist() {
		return getProperty("topaz.ia.blacklist", false);
	}

	public static String getWhitelist() {
		return getProperty("topaz.ia.whitelist", false);
	}

	public static String getDatabaseDelimiter() {
		return getProperty("topaz.database.delimiter", false);
	}

}
