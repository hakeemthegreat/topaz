package org.coco.topaz.environment;

import org.coco.topaz.exception.PropertyNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class Environment {

	private static final Properties PROPERTIES = new Properties();
	protected static final String MISSING_PROPERTIES = "Please verify that '%s' is in your resources folder";

	protected Environment() {

	}

	protected static void loadProperties(String propertiesFile, ClassLoader classLoader) {
		try (InputStream inputStream = getResourceAsStream(propertiesFile, classLoader)) {
			loadProperties(inputStream);
		} catch (Exception e) {
			String message = String.format(MISSING_PROPERTIES, propertiesFile);
			throw new RuntimeException(message, e);
		}
	}

	protected static void loadProperties(InputStream inputStream) throws IOException {
		PROPERTIES.load(inputStream);
	}

	public static URL getResource(String resource, ClassLoader classLoader) {
		return classLoader.getResource(resource);
	}

	public static InputStream getResourceAsStream(String resource, ClassLoader classLoader) {
		return classLoader.getResourceAsStream(resource);
	}
	
	public static String getProperty(String name, boolean required) {
		String value = PROPERTIES.getProperty(name);

		if (required && !PROPERTIES.containsKey(name)) {
			throw new PropertyNotFoundException(String.format("Could not find '%s' in properties", name));
		}

		return value;
	}

	public static String getProperty(String name) {
		return getProperty(name, true);
	}

	public static Boolean getPropertyAsBoolean(String name) {
		Boolean value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Boolean.parseBoolean(stringValue);
		}

		return value;
	}

	public static Short getPropertyAsShort(String name) {
		Short value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Short.parseShort(stringValue);
		}

		return value;
	}

	public static Integer getPropertyAsInteger(String name) {
		Integer value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Integer.parseInt(stringValue);
		}

		return value;
	}

	public static Long getPropertyAsLong(String name) {
		Long value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Long.parseLong(stringValue);
		}

		return value;
	}

	public static Float getPropertyAsFloat(String name) {
		Float value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Float.parseFloat(stringValue);
		}

		return value;
	}

	public static Double getPropertyAsDouble(String name) {
		Double value = null;
		String stringValue = getProperty(name);

		if (stringValue != null) {
			value = Double.parseDouble(stringValue);
		}

		return value;
	}

	public static String getSystemProperty(String name) {
		return System.getProperty(name);
	}

	public static String getTemporaryDirectory() {
		return getSystemProperty("java.io.tmpdir");
	}

	public static String getProjectRoot() {
		return getSystemProperty("user.dir");
	}

	public static String getEnvironmentVariable(String name) {
		return System.getenv(name);
	}

}
