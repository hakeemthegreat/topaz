package org.coco.topaz.injection;

import java.util.HashMap;
import java.util.Map;

public class Beans {

	private final Map<Class<?>, Bean> map = new HashMap<>();

	private static final Beans INSTANCE = new Beans();

	private Beans() {

	}

	public static Beans getInstance() {
		return INSTANCE;
	}

	public <D, I extends D> void newBean(Class<D> dependency, Class<I> implementation) {
		map.put(dependency, new Bean(dependency, implementation));
	}

	public <D> Bean get(Class<D> dependency) {
		return map.get(dependency);
	}

	public int getCount() {
		return map.size();
	}

}
