package org.coco.topaz.injection;

import org.coco.topaz.util.service.*;

public class TopazBeans {

	private static final Beans BEANS = Beans.getInstance();

	private TopazBeans() {

	}

	public static void init() {
		BEANS.newBean(CacheService.class, SimpleCacheService.class);
		BEANS.newBean(CloneService.class, SerializingCloneService.class);
		BEANS.newBean(HashService.class, SimpleHashService.class);
		BEANS.newBean(JSONService.class, JSONServiceImpl.class);
	}

}
