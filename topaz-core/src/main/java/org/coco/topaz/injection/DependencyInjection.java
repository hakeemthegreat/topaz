package org.coco.topaz.injection;

import org.coco.topaz.exception.AmbiguousConstructorException;
import org.coco.topaz.exception.UnresolvedDependencyException;
import org.coco.topaz.util.Annotations;
import org.coco.topaz.util.ClassUtils;
import org.coco.topaz.util.reflection.Fields;
import org.coco.topaz.util.reflection.Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

public class DependencyInjection {
	
	private DependencyInjection() {
		
	}

	public static <T> T newInstance(Class<T> clazz) {
		Class<?> implementation = DependencyInjection.getImplementation(clazz);
		Constructor<?> constructor = DependencyInjection.getConstructor(implementation);
		Object[] resolvedDependencies = DependencyInjection.resolveDependencies(constructor);
		T instance = (T) Reflection.newInstance(constructor, resolvedDependencies);

		DependencyInjection.injectDependencies(instance);
		return instance;
	}
	
	public static void injectDependencies(Object object) {
		List<Field> fields = Fields.getFields(ClassUtils.getClass(object));
		
		for (Field field : fields) {
			if (field.isAnnotationPresent(Dependency.class)) {
				Class<?> implementation = getImplementation(field.getType());
				Object resolvedDependency = DependencyInjection.newInstance(implementation);

				Fields.set(object, field, resolvedDependency);
			}
		}
	}

	public static Object[] resolveDependencies(Constructor<?> constructor) {
		Class<?>[] dependencies = constructor.getParameterTypes();
		Object[] resolved = new Object[dependencies.length];

		for (int i = 0; i < dependencies.length; i++) {
			Class<?> dependency = dependencies[i];
			Class<?> implementation = getImplementation(dependency);

			resolved[i] = DependencyInjection.newInstance(implementation);
		}

		return resolved;
	}

	public static Class<?> getImplementation(Class<?> dependency) {
		Beans beans = Beans.getInstance();
		Bean bean = beans.get(dependency);
		Class<?> implementation = null;

		if (bean != null) {
			implementation = bean.getImplementation();
		}

		if (implementation == null) {
			if (isConcrete(dependency)) {
				implementation = dependency;
			} else {
				throw new UnresolvedDependencyException(dependency.toString());
			}
		}

		return implementation;
	}

	protected static boolean isConcrete(Class<?> clazz) {
		boolean result = true;

		if (clazz.isInterface()) {
			result = false;
		} else if (isAbstract(clazz)) {
			result = false;
		}

		return result;
	}

	protected static boolean isAbstract(Class<?> clazz) {
		return Modifier.isAbstract(clazz.getModifiers());
	}

	public static <T> Constructor<T> getConstructor(Class<T> clazz) {
		Constructor<T>[] allConstructors = (Constructor<T>[]) clazz.getDeclaredConstructors();
		Constructor<T> constructor;

		if (allConstructors.length == 1) {
			constructor = allConstructors[0];
		} else {
			List<Constructor<T>> validConstructors = getValidConstructors(allConstructors);

			if (validConstructors.size() != 1) {
				throw new AmbiguousConstructorException(clazz);
			}

			constructor = validConstructors.get(0);
		}

		return constructor;
	}

	protected static <T> List<Constructor<T>> getValidConstructors(Constructor<T>[] constructors) {
		List<Constructor<T>> validConstructors = new ArrayList<>();

		for (Constructor<T> constructor : constructors) {
			if (Annotations.hasAnnotation(constructor, Dependency.class)) {
				validConstructors.add(constructor);
			}
		}

		return validConstructors;
	}

}
