package org.coco.topaz.injection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bean {

	private Class<?> dependency;
	private Class<?> implementation;

	public <D, I extends D> Bean(Class<D> dependency, Class<I> implementation) {
		setDependency(dependency);
		setImplementation(implementation);
	}

}
