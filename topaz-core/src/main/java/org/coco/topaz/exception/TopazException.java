package org.coco.topaz.exception;

import java.util.Objects;

public abstract class TopazException extends RuntimeException {

	private static final String NULL_MESSAGE = "Cannot have a null message";
	private static final String NULL_CAUSE = "Cannot have a null cause";

	public TopazException() {
		super();
	}

	public TopazException(String message) {
		super(Objects.requireNonNull(message, NULL_MESSAGE));
	}

	public TopazException(Object message) {
		super(Objects.requireNonNull(message, NULL_MESSAGE).toString());
	}

	public TopazException(String message, Throwable cause) {
		super(Objects.requireNonNull(message, NULL_MESSAGE), Objects.requireNonNull(cause, NULL_CAUSE));
	}

	public TopazException(Throwable cause) {
		super(Objects.requireNonNull(cause, NULL_CAUSE));
	}
	
}
