package org.coco.topaz.exception;

import java.util.Objects;

public class AmbiguousConstructorException extends TopazException {

	public AmbiguousConstructorException(Class<?> clazz) {
		super(Objects.requireNonNull(clazz).getName());
	}

}
