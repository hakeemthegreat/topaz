package org.coco.topaz.exception;

public class LoginException extends TopazException {

	public LoginException(String message, Throwable cause) {
		super(message, cause);
	}

}
