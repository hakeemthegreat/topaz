package org.coco.topaz.exception;

public class UnresolvedDependencyException extends TopazException {

	public UnresolvedDependencyException(String message) {
		super(message);
	}

}
