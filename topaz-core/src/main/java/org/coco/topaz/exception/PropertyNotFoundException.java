package org.coco.topaz.exception;

public class PropertyNotFoundException extends TopazException {

	public PropertyNotFoundException(String message) {
		super(message);
	}

}
