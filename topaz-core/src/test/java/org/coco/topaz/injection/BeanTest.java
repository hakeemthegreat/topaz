package org.coco.topaz.injection;

import org.coco.topaz.util.test.GetterSetterTest;

public class BeanTest extends GetterSetterTest<Bean> {

	public BeanTest() {
		super(new Bean(Object.class, String.class));
	}

}
