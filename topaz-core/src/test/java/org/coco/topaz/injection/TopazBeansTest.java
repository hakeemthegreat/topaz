package org.coco.topaz.injection;

import org.coco.topaz.util.service.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TopazBeansTest {

	private final Beans instance = Beans.getInstance();

	static {
		TopazBeans.init();
	}

	@Test
	public void getCount_Test() {
		assertEquals(4, instance.getCount());
	}

	@Test
	public void get_Test() {
		assertEquals(CacheService.class, instance.get(CacheService.class).getDependency());
		assertEquals(SimpleCacheService.class, instance.get(CacheService.class).getImplementation());

		assertEquals(CloneService.class, instance.get(CloneService.class).getDependency());
		assertEquals(SerializingCloneService.class, instance.get(CloneService.class).getImplementation());

		assertEquals(HashService.class, instance.get(HashService.class).getDependency());
		assertEquals(SimpleHashService.class, instance.get(HashService.class).getImplementation());

		assertEquals(JSONService.class, instance.get(JSONService.class).getDependency());
		assertEquals(JSONServiceImpl.class, instance.get(JSONService.class).getImplementation());
	}

}
