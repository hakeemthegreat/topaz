package org.coco.topaz.injection;

import mockit.Expectations;
import mockit.Verifications;
import org.coco.topaz.exception.AmbiguousConstructorException;
import org.coco.topaz.exception.UnresolvedDependencyException;
import org.coco.topaz.ia.IAConfiguration;
import org.coco.topaz.util.Annotations;
import org.coco.topaz.util.service.HashService;
import org.coco.topaz.util.service.JSONService;
import org.coco.topaz.util.service.JSONServiceImpl;
import org.coco.topaz.util.service.SimpleHashService;
import org.junit.Test;

import java.io.Closeable;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.sql.Blob;
import java.util.*;

import static org.junit.Assert.*;

public class DependencyInjectionTest {

	private static class TestDataAccessObject {

	}

	private static class TestDataService {

		@Dependency
		private TestDataAccessObject testDataAccessObject;

		public TestDataService() {

		}

	}

	private static class TestService {

		@Dependency
		private TestDataService testDataService;

		public TestService() {

		}
		
	}

	private static class TestOrchestrator {

		private final TestService testService;

		public TestOrchestrator(TestService testService) {
			this.testService = Objects.requireNonNull(testService);
		}

	}

	static {
		TopazBeans.init();
	}

	@Test
	public void injectDependencies_Test() {
		TestService testService = new TestService();

		DependencyInjection.injectDependencies(testService);
		assertNotNull(testService.testDataService);
		assertNotNull(testService.testDataService.testDataAccessObject);
	}

	@Test
	public void resolveDependencies_Test() throws Exception {
		Constructor<TestOrchestrator> constructor = TestOrchestrator.class.getConstructor(TestService.class);

		Object[] results = DependencyInjection.resolveDependencies(constructor);
		assertNotNull(results);
		assertEquals(1, results.length);
		assertTrue(results[0] instanceof TestService);
	}

	@Test(expected = AmbiguousConstructorException.class)
	public void resolveDependencies_UnresolvedDependencyException_Test() throws Exception {
		Constructor<Date> constructor = Date.class.getConstructor(long.class);

		new Expectations(DependencyInjection.class) {{
			DependencyInjection.getImplementation(long.class); result = long.class;
		}};

		DependencyInjection.resolveDependencies(constructor);
	}

	@Test
	public void getImplementation_Test() {
		Class<?> result = DependencyInjection.getImplementation(HashService.class);
		assertEquals(SimpleHashService.class, result);
	}

	@Test
	public void getImplementation_Test2() {
		Class<?> result = DependencyInjection.getImplementation(Object.class);
		assertEquals(Object.class, result);
	}

	@Test(expected = UnresolvedDependencyException.class)
	public void getImplementation_UnresolvedDependencyException_Test() {
		DependencyInjection.getImplementation(Blob.class);
	}

	@Test(expected = UnresolvedDependencyException.class)
	public void getImplementation_UnresolvedDependencyException_Test2() {
		DependencyInjection.getImplementation(InputStream.class);
	}

	@Test
	public void isConcrete_True_Test() {
		boolean result = DependencyInjection.isConcrete(Object.class);
		assertTrue(result);
	}

	@Test
	public void isConcrete_False_With_Abstract_Class_Test() {
		boolean result = DependencyInjection.isConcrete(InputStream.class);
		assertFalse(result);
	}

	@Test
	public void isConcrete_False_With_Interface_Test() {
		boolean result = DependencyInjection.isConcrete(Closeable.class);
		assertFalse(result);
	}

	@Test
	public void isAbstract_True_Test() {
		boolean result = DependencyInjection.isAbstract(InputStream.class);
		assertTrue(result);
	}

	@Test
	public void isAbstract_False_With_Concrete_Class_Test() {
		boolean result = DependencyInjection.isAbstract(Object.class);
		assertFalse(result);
	}

	@Test
	public void getConstructor_Default_Constructor_Returned_Test() throws Exception {
		Constructor<?> constructor = Object.class.getConstructor();

		Constructor<?> result = DependencyInjection.getConstructor(Object.class);
		assertEquals(constructor, result);
	}

	@Test
	public void getConstructor_Annotated_Constructor_Returned_Test() throws Exception {
		Constructor<?> constructor = TestService.class.getConstructor();

		Constructor<?> result = DependencyInjection.getConstructor(TestService.class);
		assertEquals(constructor, result);
	}

	@Test(expected = AmbiguousConstructorException.class)
	public void getConstructor_AmbiguousConstructorException_Integration_Test() {
		DependencyInjection.getConstructor(Long.class);
	}

	@Test
	public void getConstructor_Multiple_Constructors_And_Only_One_Is_Valid_Test() throws Exception {
		Constructor<String> constructor = String.class.getConstructor();
		List<Constructor<String>> validConstructors = Collections.singletonList(constructor);
		Constructor<String>[] declaredConstructors = (Constructor<String>[]) String.class.getDeclaredConstructors();

		new Expectations(DependencyInjection.class) {{
			DependencyInjection.getValidConstructors((Constructor<String>[]) any); result = validConstructors;
		}};

		Constructor<String> result = DependencyInjection.getConstructor(String.class);
		assertEquals(constructor, result);

		new Verifications() {{
			DependencyInjection.getValidConstructors(declaredConstructors); times = 1;
		}};
	}

	@Test(expected = AmbiguousConstructorException.class)
	public void getConstructor_Multiple_Constructors_And_None_Are_Valid_Test() {
		List<Constructor<String>> validConstructors = Collections.emptyList();

		new Expectations(DependencyInjection.class) {{
			DependencyInjection.getValidConstructors((Constructor<String>[]) any); result = validConstructors;
		}};

		DependencyInjection.getConstructor(String.class);
	}

	@Test(expected = AmbiguousConstructorException.class)
	public void getConstructor_Multiple_Constructors_And_Multiple_Are_Valid_Test() throws Exception {
		Constructor<String> constructor1 = String.class.getConstructor();
		Constructor<String> constructor2 = String.class.getConstructor(String.class);
		List<Constructor<String>> validConstructors = Arrays.asList(constructor1, constructor2);

		new Expectations(DependencyInjection.class) {{
			DependencyInjection.getValidConstructors((Constructor<String>[]) any); result = validConstructors;
		}};

		DependencyInjection.getConstructor(String.class);
	}

	@Test
	public void getValidConstructors_Test() throws Exception {
		Constructor<String> blankConstructor = String.class.getConstructor();
		Constructor<String>[] declaredConstructors = (Constructor<String>[]) String.class.getDeclaredConstructors();

		new Expectations(Annotations.class) {{
			Annotations.hasAnnotation(blankConstructor, Dependency.class); result = true;
		}};

		List<Constructor<String>> result = DependencyInjection.getValidConstructors(declaredConstructors);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(blankConstructor, result.get(0));
	}

	@Test
	public void newInstance_With_Constructor_Based_Dependency_Injection_Test() {
		TestOrchestrator result = DependencyInjection.newInstance(TestOrchestrator.class);
		assertNotNull(result);
		assertNotNull(result.testService);
		assertNotNull(result.testService.testDataService);
		assertNotNull(result.testService.testDataService.testDataAccessObject);
	}

	@Test
	public void newInstance_With_Concrete_Class_Test() {
		IAConfiguration result = DependencyInjection.newInstance(IAConfiguration.class);
		assertNotNull(result);
	}

	@Test
	public void newInstance_With_Interface_Test() {
		JSONService result = DependencyInjection.newInstance(JSONService.class);
		assertTrue(result instanceof JSONServiceImpl);
	}

	@Test(expected = UnresolvedDependencyException.class)
	public void newInstance_Exception_Thrown_Test() {
		DependencyInjection.newInstance(InputStream.class);
	}


}
