package org.coco.topaz.ia;

import mockit.Expectations;
import mockit.Verifications;
import org.coco.topaz.util.reflection.Constructors;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.sql.Date;
import java.util.*;

import static org.junit.Assert.*;

public class R2SanitizerTest extends AbstractSanitizerTest<R2Sanitizer> {

	public R2SanitizerTest() {
		super(new R2Sanitizer(FUNCTION));
	}

	@Test
	public void isWhitelisted_Test() {
		assertTrue(instance.isWhitelisted(true));
		assertTrue(instance.isWhitelisted(123));
		assertTrue(instance.isWhitelisted(new Date(System.currentTimeMillis())));
		assertFalse(instance.isWhitelisted(new Object()));
	}

	@Test
	public void getInstance_Class_With_No_Default_Constructor_Test() {
		Date date = new Date(System.currentTimeMillis());

		Date result = instance.getNewInstance(date);
		assertNotNull(result);
		assertNotSame(date, result);
	}

	@Test
	public void getNewInstance_Class_Has_Default_Constructor_Test() {
		String string = "string";

		String result = instance.getNewInstance(string);
		assertNotNull(result);
		assertNotSame(string, result);
	}

	@Test
	public void getNewInstance_IllegalArgumentException_Test() throws Exception {
		Object object = new Object();
		Exception exception = new Exception();
		Constructor<Object> constructor = Object.class.getConstructor();

		new Expectations(Constructors.class) {{
			Constructors.newInstance((Constructor<?>) any); result = exception;
		}};

		expect(IllegalArgumentException.class, exception);

		instance.getNewInstance(object);

		new Verifications() {{
			Constructors.newInstance(constructor); times = 1;
		}};
	}

	@Test
	public void apply_Object_Already_Sanitized_Test() {
		Map<Object, Set<Object>> map = new HashMap<>();
		Set<Object> sanitized = new HashSet<>();
		Object object = new Object();

		sanitized.add(object);
		map.put(object, sanitized);

		new Expectations(instance) {{
			instance.getMap(); result = map;
		}};

		boolean result = instance.objectAlreadySanitized(object);
		assertTrue(result);
	}

	@Test
	public void apply_Object_Not_Sanitized_Test() {
		Map<Object, Set<Object>> map = new HashMap<>();
		Set<Object> sanitized = new HashSet<>();
		Object object = new Object();

		sanitized.add(new Object());
		map.put(object, sanitized);

		new Expectations(instance) {{
			instance.getMap(); result = map;
		}};

		boolean result = instance.objectAlreadySanitized(object);
		assertFalse(result);
	}

}
