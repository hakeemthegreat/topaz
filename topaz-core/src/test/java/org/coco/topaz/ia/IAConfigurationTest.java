package org.coco.topaz.ia;

import mockit.Tested;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class IAConfigurationTest {

	@Tested
	private IAConfiguration instance;

	private static final Set<Character> CHARACTERS = new HashSet<>(Arrays.asList('a', 'b', 'c'));

	@Test
	public void blacklist_Getter_Setter_Test() {
		instance.setBlacklist(CHARACTERS);

		Set<Character> results = instance.getBlacklist();
		assertNotNull(results);
		assertNotSame(CHARACTERS, results);
		assertEquals(CHARACTERS.size(), results.size());
		assertTrue(results.containsAll(CHARACTERS));
	}

	@Test
	public void whitelist_Getter_Setter_Test() {
		instance.setWhitelist(CHARACTERS);

		Set<Character> results = instance.getWhitelist();
		assertNotNull(results);
		assertNotSame(CHARACTERS, results);
		assertEquals(CHARACTERS.size(), results.size());
		assertTrue(results.containsAll(CHARACTERS));
	}

	@Test
	public void unescapeWhitespaces_Test() {
		String result = instance.unescapeWhitespaces("\\f\\n\\r\\t");
		assertEquals("\f\n\r\t", result);
	}

	@Test
	public void unescapeWhitespaces_Null_Test() {
		String result = instance.unescapeWhitespaces(null);
		assertNull(result);
	}

}
