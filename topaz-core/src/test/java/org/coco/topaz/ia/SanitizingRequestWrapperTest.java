package org.coco.topaz.ia;

import mockit.*;
import org.coco.topaz.util.IteratorEnumeration;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.security.Principal;
import java.util.*;

import static org.junit.Assert.*;

public class SanitizingRequestWrapperTest {

	@Injectable
	private Sanitizer sanitizer;

	@Injectable
	private HttpServletRequest request;

	@Tested
	private SanitizingRequestWrapper instance;

	private static final String NAME = "name";
	private static final String PATH = "path";

	private static final String UNSANITIZED = "unsanitized";
	private static final String SANITIZED = "sanitized";

	private static final String UNSANITIZED2 = "unsanitized2";
	private static final String SANITIZED2 = "sanitized2";

	private static final String UNSANITIZED3 = "unsanitized3";
	private static final String SANITIZED3 = "sanitized3";

	private static final String[] UNSANITIZED_ARRAY = {UNSANITIZED, UNSANITIZED2, UNSANITIZED3};
	private static final String[] SANITIZED_ARRAY = {SANITIZED, SANITIZED2, SANITIZED3};

	@Test
	public void getParameter_Test() {
		new Expectations() {{
			request.getParameter(anyString); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getParameter(NAME);
		assertEquals(SANITIZED, result);

		new Verifications() {{
			request.getParameter(NAME); times = 1;
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getParameterValues_Test() {
		new Expectations() {{
			request.getParameterValues(anyString); result = UNSANITIZED_ARRAY;
			sanitizer.apply((String[]) any); result = SANITIZED_ARRAY;
		}};

		String[] results = instance.getParameterValues(NAME);
		assertArrayEquals(SANITIZED_ARRAY, results);

		new Verifications() {{
			request.getParameterValues(NAME); times = 1;
			sanitizer.apply(UNSANITIZED_ARRAY); times = 1;
		}};
	}

	@Test
	public void getContextPath_Test() {
		new Expectations() {{
			request.getContextPath(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getContextPath();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getServletPath_Test() {
		new Expectations() {{
			request.getServletPath(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getServletPath();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getAuthType_Test() {
		new Expectations() {{
			request.getAuthType(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getAuthType();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getCookies_Test() {
		Cookie unsanitizedCookie = new Cookie("unsanitizedName", "unsanitizedValue");
		Cookie sanitizedCookie = new Cookie("sanitizedName", "sanitizedValue");
		Cookie[] unsanitizedCookies = {unsanitizedCookie};
		Cookie[] sanitizedCookies = {sanitizedCookie};

		new Expectations() {{
			request.getCookies(); result = unsanitizedCookies;
			sanitizer.apply(any); result = sanitizedCookies;
		}};

		Cookie[] result = instance.getCookies();
		assertArrayEquals(sanitizedCookies, result);

		new Verifications() {{
			sanitizer.apply(unsanitizedCookies); times = 1;
		}};
	}

	@Test
	public void getHeader_Test() {
		new Expectations() {{
			request.getHeader(anyString); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getHeader(NAME);
		assertEquals(SANITIZED, result);

		new Verifications() {{
			request.getHeader(NAME); times = 1;
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void sanitizeEnumeration_Test() {
		Iterator<String> iterator = Collections.singleton(UNSANITIZED).iterator();
		Enumeration<String> enumeration = new IteratorEnumeration<>(iterator);

		new Expectations() {{
			sanitizer.apply(any); result = SANITIZED;
		}};

		Enumeration<String> result = instance.sanitizeEnumeration(enumeration);
		assertNotNull(result);
		assertTrue(result.hasMoreElements());
		assertEquals(SANITIZED, result.nextElement());
		assertFalse(result.hasMoreElements());

		new Verifications() {{
			Object object;
			sanitizer.apply(object = withCapture()); times = 1;
			assertEquals(UNSANITIZED, object);
		}};
	}

	@Test
	public void getHeaders_Test(@Mocked Enumeration<String> unsanitized, @Mocked Enumeration<String> sanitized) {
		new Expectations(instance) {{
			request.getHeaders(anyString); result = unsanitized;
			instance.sanitizeEnumeration((Enumeration<?>) any); result = sanitized;
		}};

		Enumeration<String> result = instance.getHeaders(NAME);
		assertSame(sanitized, result);

		new Verifications() {{
			request.getHeaders(NAME); times = 1;
			instance.sanitizeEnumeration(unsanitized); times = 1;
		}};
	}

	@Test
	public void getHeaderNames_Test(@Mocked Enumeration<String> unsanitized, @Mocked Enumeration<String> sanitized) {
		new Expectations(instance) {{
			request.getHeaderNames(); result = unsanitized;
			instance.sanitizeEnumeration((Enumeration<?>) any); result = sanitized;
		}};

		Enumeration<String> result = instance.getHeaderNames();
		assertSame(sanitized, result);

		new Verifications() {{
			instance.sanitizeEnumeration(unsanitized); times = 1;
		}};
	}

	@Test
	public void getMethod_Test() {
		new Expectations() {{
			request.getMethod(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getMethod();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getPathInfo_Test() {
		new Expectations() {{
			request.getPathInfo(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getPathInfo();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getPathTranslated_Test() {
		new Expectations() {{
			request.getPathTranslated(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getPathTranslated();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getQueryString_Test() {
		new Expectations() {{
			request.getQueryString(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getQueryString();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getRemoteUser_Test() {
		new Expectations() {{
			request.getRemoteUser(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRemoteUser();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getPrincipal_Test(@Mocked Principal unsanitized, @Mocked Principal sanitized) {
		new Expectations() {{
			request.getUserPrincipal(); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		Principal result = instance.getUserPrincipal();
		assertSame(sanitized, result);

		new Verifications() {{
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getRequestedSessionId_Test() {
		new Expectations() {{
			request.getRequestedSessionId(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRequestedSessionId();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getRequestURI_Test() {
		new Expectations() {{
			request.getRequestURI(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRequestURI();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getRequestURL_Test() {
		new Expectations() {{
			request.getRequestURL(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		StringBuffer result = instance.getRequestURL();
		assertNotNull(result);
		assertEquals(SANITIZED, result.toString());

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getSession_Test1(@Mocked HttpSession unsanitized, @Mocked HttpSession sanitized) {
		boolean create = true;

		new Expectations() {{
			request.getSession(anyBoolean); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		HttpSession result = instance.getSession(create);
		assertSame(sanitized, result);

		new Verifications() {{
			request.getSession(create); times = 1;
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getSession_Test2(@Mocked HttpSession unsanitized, @Mocked HttpSession sanitized) {
		new Expectations() {{
			request.getSession(); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		HttpSession result = instance.getSession();
		assertSame(sanitized, result);

		new Verifications() {{
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getRequest_Test(@Mocked ServletRequest sanitized) {
		new Expectations() {{
			sanitizer.apply(any); result = sanitized;
		}};

		ServletRequest result = instance.getRequest();
		assertSame(sanitized, result);

		new Verifications() {{
			sanitizer.apply(request); times = 1;
		}};
	}

	@Test
	public void getAttribute_Test() {
		Object unsanitized = new Object();
		Object sanitized = new Object();

		new Expectations() {{
			request.getAttribute(anyString); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		Object result = instance.getAttribute(NAME);
		assertEquals(sanitized, result);

		new Verifications() {{
			request.getAttribute(NAME); times = 1;
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getAttributeNames_Test(@Mocked Enumeration<String> unsanitized, @Mocked Enumeration<String> sanitized) {
		new Expectations(instance) {{
			request.getAttributeNames(); result = unsanitized;
			instance.sanitizeEnumeration((Enumeration<?>) any); result = sanitized;
		}};

		Enumeration<String> result = instance.getAttributeNames();
		assertSame(sanitized, result);

		new Verifications() {{
			instance.sanitizeEnumeration(unsanitized); times = 1;
		}};
	}

	@Test
	public void getCharacterEncoding_Test() {
		new Expectations() {{
			request.getCharacterEncoding(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getCharacterEncoding();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getContentType_Test() {
		new Expectations() {{
			request.getContentType(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getContentType();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getParameterMap_Test() {
		Map<String, String> unsanitized = new HashMap<>();
		Map<String, String> sanitized = new HashMap<>();

		unsanitized.put(NAME, UNSANITIZED);
		sanitized.put(NAME, SANITIZED);

		new Expectations() {{
			request.getParameterMap(); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		Map<String, String> result = instance.getParameterMap();
		assertEquals(sanitized, result);

		new Verifications() {{
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getParameterNames_Test(@Mocked Enumeration<String> unsanitized, @Mocked Enumeration<String> sanitized) {
		new Expectations(instance) {{
			request.getParameterNames(); result = unsanitized;
			instance.sanitizeEnumeration((Enumeration<?>) any); result = sanitized;
		}};

		Enumeration<String> result = instance.getParameterNames();
		assertSame(sanitized, result);

		new Verifications() {{
			instance.sanitizeEnumeration(unsanitized); times = 1;
		}};
	}

	@Test
	public void getProtocol_Test() {
		new Expectations() {{
			request.getProtocol(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getProtocol();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getScheme_Test() {
		new Expectations() {{
			request.getScheme(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getScheme();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getServerName_Test() {
		new Expectations() {{
			request.getServerName(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getServerName();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getRemoteAddr_Test() {
		new Expectations() {{
			request.getRemoteAddr(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRemoteAddr();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getRemoteHost_Test() {
		new Expectations() {{
			request.getRemoteHost(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRemoteHost();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getLocale_Test() {
		Locale unsanitized = Locale.CHINA;
		Locale sanitized = Locale.US;

		new Expectations() {{
			request.getLocale(); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		Locale result = instance.getLocale();
		assertEquals(sanitized, result);

		new Verifications() {{
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void getLocales_Test(@Mocked Enumeration<Locale> unsanitized, @Mocked Enumeration<Locale> sanitized) {
		new Expectations(instance) {{
			request.getLocales(); result = unsanitized;
			instance.sanitizeEnumeration((Enumeration<?>) any); result = sanitized;
		}};

		Enumeration<Locale> result = instance.getLocales();
		assertSame(sanitized, result);

		new Verifications() {{
			instance.sanitizeEnumeration(unsanitized); times = 1;
		}};
	}

	@Test
	public void getRequestDispatcher_Test(@Mocked RequestDispatcher unsanitized, @Mocked RequestDispatcher sanitized) {
		new Expectations() {{
			request.getRequestDispatcher(anyString); result = unsanitized;
			sanitizer.apply(any); result = sanitized;
		}};

		RequestDispatcher result = instance.getRequestDispatcher(PATH);
		assertSame(sanitized, result);

		new Verifications() {{
			request.getRequestDispatcher(PATH); times = 1;
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	@SuppressWarnings("deprecation")
	public void getRealPath_Test() {
		new Expectations() {{
			request.getRealPath(anyString); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getRealPath(PATH);
		assertEquals(SANITIZED, result);

		new Verifications() {{
			request.getRealPath(PATH); times = 1;
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getLocalName_Test() {
		new Expectations() {{
			request.getLocalName(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getLocalName();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}

	@Test
	public void getLocalAddr_Test() {
		new Expectations() {{
			request.getLocalAddr(); result = UNSANITIZED;
			sanitizer.apply(anyString); result = SANITIZED;
		}};

		String result = instance.getLocalAddr();
		assertEquals(SANITIZED, result);

		new Verifications() {{
			sanitizer.apply(UNSANITIZED); times = 1;
		}};
	}
	
}
