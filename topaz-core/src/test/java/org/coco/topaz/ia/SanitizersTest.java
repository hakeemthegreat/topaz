package org.coco.topaz.ia;

import org.coco.topaz.util.Strings;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class SanitizersTest {

	private static final Set<Character> WHITESPACE = Strings.toCharacterSet("\r\n\t\f ");
	private static final Set<Character> LOWERCASE = Strings.toCharacterSet("abcdefghijklmnopqrstuvwxyz");

	static {
		IAConfiguration.getInstance().setBlacklist(WHITESPACE);
		IAConfiguration.getInstance().setWhitelist(LOWERCASE);
	}

	@Test
	public void verify_Correct_Sanitizers_Test() throws Exception {
		List<Sanitizer> result = getSanitizers();
		assertNotNull(result);
		assertEquals(3, result.size());
		assertTrue(result.contains(Sanitizers.ESCAPE));
		assertTrue(result.contains(Sanitizers.FILTER));
		assertTrue(result.contains(Sanitizers.VALIDATE));
	}

	@Test
	public void filter_And_Escape_Test() {
		Sanitizer sanitizer = Sanitizers.FILTER.and(Sanitizers.ESCAPE);

		String result = sanitizer.apply("a\rb\nc");
		assertEquals("&#97;&#98;&#99;", result);
	}

	private List<Sanitizer> getSanitizers() throws IllegalAccessException {
		Field[] fields = Sanitizers.class.getDeclaredFields();
		List<Sanitizer> sanitizers = new ArrayList<>();

		for (Field field : fields) {
			Object value;

			field.setAccessible(true);
			value = field.get(Sanitizers.class);

			if (value instanceof Sanitizer) {
				sanitizers.add((Sanitizer) value);
			}
		}

		return sanitizers;
	}

}
