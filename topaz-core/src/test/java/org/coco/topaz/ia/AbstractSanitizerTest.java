package org.coco.topaz.ia;

import org.coco.topaz.util.node.*;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.Strings;
import org.coco.topaz.util.test.JavaTest;
import org.junit.Test;

import java.util.*;
import java.util.function.UnaryOperator;

import static org.junit.Assert.*;

public abstract class AbstractSanitizerTest<S extends Sanitizer> extends JavaTest {

	private static final String UNSANITIZED = "unsanitized";
	private static final String SANITIZED = "sanitized";

	private static final String UNSANITIZED2 = "unsanitized2";
	private static final String SANITIZED2 = "sanitized2";

	private static final String UNSANITIZED3 = "unsanitized3";
	private static final String SANITIZED3 = "sanitized3";

	private static final String[] UNSANITIZED_ARRAY = {UNSANITIZED, UNSANITIZED2, UNSANITIZED3};
	private static final String[] SANITIZED_ARRAY = {SANITIZED, SANITIZED2, SANITIZED3};

	private static final List<String> UNSANITIZED_LIST = new ArrayList<>(Arrays.asList(UNSANITIZED_ARRAY));
	private static final List<String> SANITIZED_LIST = new ArrayList<>(Arrays.asList(SANITIZED_ARRAY));

	private static final Set<String> UNSANITIZED_SET = new HashSet<>(UNSANITIZED_LIST);
	private static final Set<String> SANITIZED_SET = new HashSet<>(SANITIZED_LIST);

	private static final Set<Character> WHITESPACE = Strings.toCharacterSet("\r\n\t\f ");
	private static final Set<Character> LOWERCASE = Strings.toCharacterSet("abcdefghijklmnopqrstuvwxyz");

	protected static final UnaryOperator<String> FUNCTION = string -> Strings.replace(string, "un", "");

	protected final S instance;

	static {
		IAConfiguration.getInstance().setBlacklist(WHITESPACE);
		IAConfiguration.getInstance().setWhitelist(LOWERCASE);
	}

	public AbstractSanitizerTest(S instance) {
		this.instance = Objects.requireNonNull(instance);
	}

	@Test
	public void apply_Array_Test() {
		String[] result = instance.apply(UNSANITIZED_ARRAY);
		assertArrayEquals(SANITIZED_ARRAY, result);
	}

	@Test
	public void apply_List_Test() {
		List<String> result = instance.apply(UNSANITIZED_LIST);
		assertEquals(SANITIZED_LIST, result);
	}

	@Test
	public void apply_Set_Test() {
		Set<String> result = instance.apply(UNSANITIZED_SET);
		assertEquals(SANITIZED_SET, result);
	}

	@Test
	public void apply_Node_Test() {
		Node root  = new Node(UNSANITIZED);

		Node result = instance.apply(root);
		assertNotNull(result);
		assertEquals(SANITIZED, result.getValue());
	}

	@Test
	public void apply_LinearNode_Test() {
		LinearNode root  = new LinearNode();
		Node next = new Node();

		root.setValue(UNSANITIZED);
		root.setNext(next);
		next.setValue(UNSANITIZED2);

		LinearNode result = instance.apply(root);
		assertNotNull(result);
		assertEquals(SANITIZED, result.getValue());

		Node result2 = result.getNext();
		assertNotNull(result2);
		assertEquals(SANITIZED2, result2.getValue());
	}

	@Test
	public void apply_Circular_Reference_Test() {
		LinearNode start  = new LinearNode();
		LinearNode end = new LinearNode();

		start.setValue(UNSANITIZED);
		start.setNext(end);
		end.setValue(UNSANITIZED2);
		end.setNext(start);

		LinearNode result = instance.apply(start);
		assertNotNull(result);
		assertEquals(SANITIZED, result.getValue());

		Node result2 = result.getNext();
		assertNotNull(result2);
		assertEquals(SANITIZED2, result2.getValue());
	}

	@Test
	public void apply_Circular_Reference_Test2() {
		Date result = instance.apply(Dummy.DATE);
		assertEquals(Dummy.DATE, result);
	}

	@Test
	public void apply_BinaryNode_Test() {
		BinaryNode root  = new BinaryNode();
		Node left = new Node();
		Node right = new Node();

		root.setValue(UNSANITIZED);
		root.setLeft(left);
		root.setRight(right);
		left.setValue(UNSANITIZED2);
		right.setValue(UNSANITIZED3);

		BinaryNode result = instance.apply(root);
		assertNotNull(result);
		assertEquals(SANITIZED, result.getValue());

		Node result2 = result.getLeft();
		assertNotNull(result2);
		assertEquals(SANITIZED2, result2.getValue());

		Node result3 = result.getRight();
		assertNotNull(result3);
		assertEquals(SANITIZED3, result3.getValue());
	}

	@Test
	public void apply_Map_Test() {
		Map<Object, Object> map = new HashMap<>();
		String DUMMY = "dummy";

		map.put(Dummy.LONG, UNSANITIZED);
		map.put(UNSANITIZED2, DUMMY);
		map.put(DUMMY, UNSANITIZED3);

		Map<Object, Object> results = instance.apply(map);
		assertNotNull(results);
		assertEquals(SANITIZED, results.get(Dummy.LONG));
		assertEquals(DUMMY, results.get(SANITIZED2));
		assertEquals(SANITIZED3, results.get(DUMMY));
	}

	@Test
	public void apply_Null_Test() {
		Object result = instance.apply(null);
		assertNull(result);
	}

	@Test
	public void apply_Class_With_Final_Field_Test() {
		FinalNode node = new FinalNode(UNSANITIZED);

		FinalNode result = instance.apply(node);
		assertNotNull(result);
		assertEquals(SANITIZED, result.getValue());
	}

}
