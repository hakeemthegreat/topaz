package org.coco.topaz.ia;

import mockit.Mocked;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.Strings;
import org.junit.Test;

import java.util.*;
import java.util.function.UnaryOperator;

import static org.junit.Assert.*;

public class SanitizerTest {

	private static final String UNSANITIZED = "unsanitized";
	private static final String SANITIZED = "sanitized";

	private static final Set<Character> WHITESPACE = Strings.toCharacterSet("\r\n\t\f ");
	private static final Set<Character> LOWERCASE = Strings.toCharacterSet("abcdefghijklmnopqrstuvwxyz");

	private final UnaryOperator<String> function = string -> Strings.replace(string, "un", "");

	private final Sanitizer instance = new Sanitizer(function) {};

	static {
		IAConfiguration.getInstance().setBlacklist(WHITESPACE);
		IAConfiguration.getInstance().setWhitelist(LOWERCASE);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void apply_Object_Test() {
		Object object = new Object();
		instance.apply(object);
	}

	@Test
	public void apply_Object_But_Value_Underneath_Is_A_String_Test() {
		Object object = UNSANITIZED;

		Object result = instance.apply(object);
		assertEquals(SANITIZED, result);
	}

	@Test
	public void apply_String_Unsanitized_Test() {
		String result = instance.apply(UNSANITIZED);
		assertEquals(SANITIZED, result);
	}

	@Test
	public void apply_String_Sanitized_Test() {
		String result = instance.apply(SANITIZED);
		assertEquals(SANITIZED, result);
	}

	@Test
	public void and_Test() {
		Sanitizer result = instance.and(Sanitizers.VALIDATE);
		assertEquals(Sanitizers.VALIDATE, result);
		assertNotSame(Sanitizers.VALIDATE, result);

		Sanitizer result2 = result.previous;
		assertEquals(instance, result2);

		Sanitizer result3 = instance.previous;
		assertNull(result3);
	}

	@Test
	public void equals_Same_Function_Test() {
		Sanitizer sanitizer = new Sanitizer(function) {};

		boolean result = instance.equals(sanitizer);
		assertTrue(result);
	}

	@Test
	public void equals_Different_Functions_Test(@Mocked UnaryOperator<String> function) {
		Sanitizer sanitizer = new Sanitizer(function) {};

		boolean result = instance.equals(sanitizer);
		assertFalse(result);
	}

	@Test
	public void equals_Different_Object_Test() {
		boolean result = instance.equals(Dummy.OBJECT);
		assertFalse(result);
	}

	@Test
	public void hashCode_Test() {
		int result = instance.hashCode();
		assertEquals(function.hashCode(), result);
	}


	@Test
	public void validate_Test() {
		String result = Sanitizer.validate(Dummy.STRING);
		assertEquals(Dummy.STRING, result);
	}

	@Test
	public void validate_Null_Test() {
		String result = Sanitizer.validate(null);
		assertNull(result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void validate_IllegalArgumentException_Test() {
		Sanitizer.validate("a<b");
	}

	@Test
	public void filter_Test() {
		String result = Sanitizer.filter("a\rb\nc\t");
		assertEquals("abc", result);
	}

	@Test
	public void filter_Null_Test() {
		String result = Sanitizer.filter(null);
		assertNull(result);
	}

	@Test
	public void escape_Test() {
		String result = Sanitizer.escape("abc");
		assertEquals("&#97;&#98;&#99;", result);
	}

	@Test
	public void escape_Null_Test() {
		String result = Sanitizer.escape(null);
		assertNull(result);
	}

}
