package org.coco.topaz.ia;

import mockit.*;
import org.junit.Test;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

public class SanitizingFilterTest {

	@Injectable
	private Sanitizer sanitizer;

	@Tested
	private SanitizingFilter instance;

	@Test
	public void init_Test(@Mocked FilterConfig filterConfig) {
		new Expectations(instance) {{
			instance.doNothing(); times = 1;
		}};

		instance.init(filterConfig);
	}

	@Test
	public void doFilter_Test(@Mocked HttpServletRequest request, @Mocked ServletResponse response, @Mocked FilterChain chain) throws Exception {
		instance.doFilter(request, response, chain);

		new Verifications() {{
			chain.doFilter((SanitizingRequestWrapper) any, response); times = 1;
		}};
	}

	@Test(expected = IllegalArgumentException.class)
	public void doFilter_IllegalArgumentException_Test(@Mocked ServletRequest request, @Mocked ServletResponse response, @Mocked FilterChain chain) throws Exception {
		instance.doFilter(request, response, chain);
	}

	@Test
	public void destroy_Test() {
		new Expectations(instance) {{
			instance.doNothing(); //do nothing
		}};

		instance.destroy();
	}

	@Test
	public void doNothing_Test() {
		instance.doNothing();
		//nothing to assert, expect, or verify
	}

}
