package org.coco.topaz.ia;

import mockit.Expectations;
import org.junit.Test;

import java.io.IOException;

public class SerializingSanitizerTest extends AbstractSanitizerTest<SerializingSanitizer> {

	public SerializingSanitizerTest() {
		super(new SerializingSanitizer(FUNCTION));
	}

	@Test
	public void apply_IOException_Test() throws Exception {
		IOException ioException = new IOException();
		Object object = new Object();

		new Expectations(instance) {{
			instance.serialize(object); result = ioException;
		}};

		expect(RuntimeException.class, ioException);

		instance.apply(object);
	}

	@Test
	public void apply_ClassNotFoundException_Test() throws Exception {
		ClassNotFoundException classNotFoundException = new ClassNotFoundException();
		Object object = new Object();

		new Expectations(instance) {{
			instance.serialize(object); result = classNotFoundException;
		}};

		expect(RuntimeException.class, classNotFoundException);

		instance.apply(object);
	}

}
