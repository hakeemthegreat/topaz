package org.coco.topaz.ia;

import mockit.*;
import org.coco.topaz.util.Resources;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static org.junit.Assert.assertEquals;

public class SanitizingObjectInputStreamTest {

	@Mocked private Sanitizer sanitizer;

	private SanitizingObjectInputStream instance;

	@Before
	public void before() throws Exception {
		byte[] bytes = getBytes();
		ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

		instance = new SanitizingObjectInputStream(inputStream, sanitizer);
	}

	@Test
	public void resolveObject_String_Test() {
		String unsanitized = "unsanitized";
		String sanitized = "sanitized";

		new Expectations() {{
			sanitizer.apply(anyString); result = sanitized;
		}};

		Object result = instance.resolveObject(unsanitized);
		assertEquals(sanitized, result);

		new Verifications() {{
			sanitizer.apply(unsanitized); times = 1;
		}};
	}

	@Test
	public void resolveObject_Other_Test() {
		Object unsanitized = new Object();

		Object result = instance.resolveObject(unsanitized);
		assertEquals(unsanitized, result);

		new Verifications() {{
			sanitizer.apply(any); times = 0;
		}};
	}

	private byte[] getBytes() throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = null;
		String string = "string";
		byte[] bytes;

		try {
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(string);
			objectOutputStream.flush();
			bytes = byteArrayOutputStream.toByteArray();
		} finally {
			Resources.close(byteArrayOutputStream, objectOutputStream);
		}

		return bytes;
	}

}
