package org.coco.topaz.environment;

import mockit.Expectations;
import mockit.Verifications;
import org.coco.topaz.exception.PropertyNotFoundException;
import org.coco.topaz.util.test.Dummy;
import org.junit.Test;

import static org.junit.Assert.*;

public class TopazEnvironmentTest {

	private static final String PROPERTY_VALUE = "propertyValue";

	@Test
	public void constructor_Test() {
		TopazEnvironment topazEnvironment = new TopazEnvironment();
		assertNotNull(topazEnvironment);
	}

	@Test
	public void getClassLoader_Test() {
		ClassLoader result = TopazEnvironment.getClassLoader();
		assertNotNull(result);
	}

	@Test(expected = PropertyNotFoundException.class)
	public void getProperty_Required_Test() {
		TopazEnvironment.getProperty(Dummy.STRING, true);
	}

	@Test
	public void getProperty_Test() {
		String result = TopazEnvironment.getProperty(Dummy.STRING, false);
		assertNull(result);
	}

	@Test
	public void getDatabaseUrl_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getDatabaseUrl();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.database.url"); times = 1;
		}};
	}

	@Test
	public void getGeneratedSourcesLocation_Test() {
		String result = TopazEnvironment.getGeneratedSourcesLocation();
		assertNotNull(result);
	}

	@Test
	public void getDatabaseSQL_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getDatabaseSQL();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.database.sql"); times = 1;
		}};
	}

	@Test
	public void getDatabaseDriver_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getDatabaseDriver();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.database.driver"); times = 1;
		}};
	}

	@Test
	public void getBlacklist_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString, anyBoolean); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getBlacklist();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.ia.blacklist", false); times = 1;
		}};
	}

	@Test
	public void getWhitelist_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString, anyBoolean); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getWhitelist();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.ia.whitelist", false); times = 1;
		}};
	}

	@Test
	public void getDatabaseDelimiter_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString, anyBoolean); result = PROPERTY_VALUE;
		}};

		String result = TopazEnvironment.getDatabaseDelimiter();
		assertEquals(PROPERTY_VALUE, result);

		new Verifications() {{
			Environment.getProperty("topaz.database.delimiter", false); times = 1;
		}};
	}

}
