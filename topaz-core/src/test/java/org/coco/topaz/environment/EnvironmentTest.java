package org.coco.topaz.environment;

import mockit.Expectations;
import mockit.Verifications;
import org.coco.topaz.exception.PropertyNotFoundException;
import org.coco.topaz.util.test.Dummy;
import org.coco.topaz.util.test.JavaTest;
import org.coco.topaz.util.test.MockInputStream;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.*;

public class EnvironmentTest extends JavaTest {

	private final ClassLoader classLoader = EnvironmentTest.class.getClassLoader();

	@Test
	public void getResource_Test() {
		URL result = Environment.getResource(Dummy.STRING, classLoader);
		assertNull(result);
	}

	@Test
	public void getResourceAsStream_Test() {
		InputStream result = Environment.getResourceAsStream(Dummy.STRING, classLoader);
		assertNull(result);
	}

	@Test(expected = PropertyNotFoundException.class)
	public void getProperty_Test() {
		String result = Environment.getProperty(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void loadProperties_Test() {
		String propertiesFile = "test.properties";

		Environment.loadProperties(propertiesFile, classLoader);

		String user = Environment.getProperty("user");
		assertEquals("john.doe", user);

		String password = Environment.getProperty("password");
		assertEquals("somePassword", password);
	}

	@Test
	public void loadProperties_Exception_Test() {
		String propertiesFile = "propertiesFile";
		IOException ioException = new IOException();

		new Expectations(Environment.class) {{
			Environment.getResourceAsStream(anyString, (ClassLoader) any); result = ioException;
		}};

		expect(Exception.class, ioException);

		Environment.loadProperties(propertiesFile, classLoader);

		new Verifications() {{
			Environment.getResourceAsStream(propertiesFile, classLoader); times = 1;
		}};
	}

	@Test
	public void loadProperties_With_InputStream_Test() throws Exception {
		InputStream inputStream = new MockInputStream();

		Environment.loadProperties(inputStream);
	}

	@Test
	public void getProjectRoot_Test() {
		String result = Environment.getProjectRoot();
		assertNotNull(result);
	}

	@Test
	public void getTemporaryDirectory_Test() {
		String result = Environment.getTemporaryDirectory();
		assertNotNull(result);
	}

	@Test
	public void getSystemProperty_Test() {
		String result = Environment.getSystemProperty(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getEnvironmentVariable_Test() {
		String result = Environment.getEnvironmentVariable(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsBoolean_Test() {
		Boolean value = true;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Boolean result = Environment.getPropertyAsBoolean(Dummy.STRING);
		assertEquals(value, result);
	}

	@Test
	public void getPropertyAsShort_Test() {
		Short value = 1;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Short result = Environment.getPropertyAsShort(Dummy.STRING);
		assertEquals(value, result);
	}

	@Test
	public void getPropertyAsInteger_Test() {
		Integer value = 2;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Integer result = Environment.getPropertyAsInteger(Dummy.STRING);
		assertEquals(value, result);
	}

	@Test
	public void getPropertyAsLong_Test() {
		Long value = 3L;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Long result = Environment.getPropertyAsLong(Dummy.STRING);
		assertEquals(value, result);
	}

	@Test
	public void getPropertyAsFloat_Test() {
		Float value = 4F;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Float result = Environment.getPropertyAsFloat(Dummy.STRING);
		assertEquals(value, result);
	}

	@Test
	public void getPropertyAsDouble_Test() {
		Double value = 5D;
		String stringValue = value.toString();

		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = stringValue;
		}};

		Double result = Environment.getPropertyAsDouble(Dummy.STRING);
		assertEquals(value, result);
	}


	@Test
	public void getPropertyAsBoolean_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Boolean result = Environment.getPropertyAsBoolean(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsShort_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Short result = Environment.getPropertyAsShort(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsInteger_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Integer result = Environment.getPropertyAsInteger(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsLong_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Long result = Environment.getPropertyAsLong(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsFloat_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Float result = Environment.getPropertyAsFloat(Dummy.STRING);
		assertNull(result);
	}

	@Test
	public void getPropertyAsDouble_Null_Test() {
		new Expectations(Environment.class) {{
			Environment.getProperty(anyString); result = null;
		}};

		Double result = Environment.getPropertyAsDouble(Dummy.STRING);
		assertNull(result);
	}

}
