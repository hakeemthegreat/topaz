package org.coco.topaz.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class PropertyNotFoundExceptionTest extends ExceptionTest<PropertyNotFoundException> {

	public PropertyNotFoundExceptionTest() {
		super(PropertyNotFoundException.class);
	}

}
