package org.coco.topaz.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class UnresolvedDependencyExceptionTest extends ExceptionTest<UnresolvedDependencyException> {

	public UnresolvedDependencyExceptionTest() {
		super(UnresolvedDependencyException.class);
	}

}
