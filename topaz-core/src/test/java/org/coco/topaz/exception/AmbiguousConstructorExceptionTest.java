package org.coco.topaz.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class AmbiguousConstructorExceptionTest extends ExceptionTest<AmbiguousConstructorException> {

	public AmbiguousConstructorExceptionTest() {
		super(AmbiguousConstructorException.class);
	}

}
