package org.coco.topaz.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class ServiceExceptionTest extends ExceptionTest<ServiceException> {

	public ServiceExceptionTest() {
		super(ServiceException.class);
	}

}
