package org.coco.topaz.exception;

import org.coco.topaz.util.exception.ExceptionTest;

public class LoginExceptionTest extends ExceptionTest<LoginException> {

	public LoginExceptionTest() {
		super(LoginException.class);
	}

}
