package org.coco.topaz.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TopazExceptionTest {

	protected static final Exception CAUSE = new Exception();
	protected static final String STRING_MESSAGE = "stringMessage";
	protected static final Object OBJECT_MESSAGE = new Object();

	@Test
	public void message_Cause_Constructor_Test() {
		TopazException result = new TopazException(STRING_MESSAGE, CAUSE) {};
		assertEquals(STRING_MESSAGE, result.getMessage());
		assertEquals(CAUSE, result.getCause());
	}

	@Test
	public void cause_Constructor_Test() {
		TopazException result = new TopazException(CAUSE) {};
		assertEquals(CAUSE, result.getCause());
	}

	@Test
	public void message_As_String_Constructor_Test() {
		TopazException result = new TopazException(STRING_MESSAGE) {};
		assertEquals(STRING_MESSAGE, result.getMessage());
		assertNull(result.getCause());
	}

	@Test
	public void blank_Constructor_Test() {
		TopazException result = new TopazException() {};
		assertNull(result.getMessage());
		assertNull(result.getCause());
	}

	@Test
	public void message_As_Object_Constructor_Test() {
		String objectMessageString = OBJECT_MESSAGE.toString();

		TopazException result = new TopazException(OBJECT_MESSAGE) {};
		assertEquals(objectMessageString, result.getMessage());
		assertNull(result.getCause());
	}

}
